﻿using Vina.DAO;
using Vina.DTO;
using VinaVN.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility;
using System.Text.RegularExpressions;
using ViNaPinCode.FIBOSMSService;
using System.Threading;

namespace VinaVN.Controllers
{
    public class SMSProcessController : Controller
    {
        //
        // GET: /SMSProcess/
        private readonly SqlMtConfigDao mtConfigDao = new SqlMtConfigDao();
        private readonly SqlMoDao moDao = new SqlMoDao();
        private readonly SqlConfigDao configDao = new SqlConfigDao();
        private readonly SqlSubKeyDao subKeyDao = new SqlSubKeyDao();
        private readonly SqlPinCodeDao pinCodeDao = new SqlPinCodeDao();
        private readonly SqlMtDao mtDao = new SqlMtDao();
        private readonly SqlKeywordDao kwDao = new SqlKeywordDao();
        private readonly SqlProductTypeDao productTypeDao = new SqlProductTypeDao();
		private readonly SqlCustomerDao customerDao = new SqlCustomerDao();

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult TestSMS()
        {
            return View(new TestMessageModel());
        }
        [HttpPost]
        public ActionResult TestSMS(TestMessageModel viewModel)
        {
            return MtProcess_bk(viewModel.message, viewModel.phone, viewModel.service, viewModel.port, viewModel.guids,
                viewModel.main, viewModel.sub);
        }

        public ActionResult ReturnData(string xml)
        {
            Response.Flush();
            Response.Write(xml);
            Response.End();
            return View();
        }

		public ActionResult ReceiveStatusSMS(string SMSGUID,string SMSStatus,string SecureCode)
		{
			string code = "2";
			
			try
			{
				if (SecureCode != ConfigurationManager.AppSettings["SecureCodeReceiveStatus"])
				{
					code = "1";
				}
				else
				{
					switch (SMSStatus)
					{
						case "2":
							if (!mtDao.UpdateStatus(SMSGUID, (short)MTStatus.Success))
							{
								code = "1";
							}
							break;
						default:
							if (!mtDao.UpdateStatus(SMSGUID, (short)MTStatus.Fail))
							{
								code = "1";
							}
							break;
					}
				}
			}
			catch
			{
				code = "1";
			}

			return ReturnData(BuildMt2ways(code));
		}
		/// <summary>
		/// message=TTDEN+558694+CAO+OC+2B+NGUYEN+THI+MINH+KHAI%2c%3b+%3a+-+20%2f02%2f2016+12%3a02%3b+TDV%3aVan+Thanh&phone=841686996679&service=&port=&main=&sub=&guid=766667&receive=20160220125800
		/// http://localhost:45281/smsprocess/mtprocess?message=vifa fa123 vi123&phone=841686996679&service=&port=&main=&sub=&guid=766667&receive=20160220125800
		/// </summary>
		/// <param name="message"></param>
		/// <param name="phone"></param>
		/// <param name="service"></param>
		/// <param name="port"></param>
		/// <param name="main"></param>
		/// <param name="sub"></param>
		/// <param name="guid"></param>
		/// <param name="receive"></param>
		/// <returns></returns>
		public ActionResult MtProcess(string message, string phone, string service, string port, string main, string sub, string guid, string receive)
		{
			/*trạng thái của tin nhắn MO*/
			MtType mtType = MtType.WrongSystax;

			/*Luôn luu lại tin nhắn khi nhận được*/
			Mo mo = new Mo();
			mo.PhoneNumber = phone.Trim();
			mo.MoMessage = message;
			mo.GUID = guid;
			mo.MainKey = "VIFA";
			mo.PinCodeID = 0;
			mo.SubKeyID = 0;
			mo.ReferralCustomerId = 0;
			mo.CustomerId = 0;


			/*nội dung tra ve cho push MO từ fibo*/
			string responeMessage = "2";

			/*Lấy nội dung tra về khi MO là sai cú pháp*/
			string defaultWrongSyntaxMessage = "He thong loi, thu lai sau.";
			MtConfig conf = mtConfigDao.GetDefaultWrongSyntaxMessage();
			if (conf != null)
			{
				defaultWrongSyntaxMessage = conf.MtMessage;
			}

			List<MtConfig> mtConfigList=null;

			string subkey;
			string pincode;
			string customerCode;
			string phoneReferral = "";
			string customerCodeNew = "";

			try
			{
				/*Kiểm tra đúng cú pháp không*/
				message=message.Trim().ToUpper();
				string pattern = @"^(VIFA){1}[ ]+([\w]+)[ ]*([A-Z]{2}[0-9]{3})*$";
				var math=Regex.Match(message, pattern);

				/*Chuyển sdt về luôn là 84...*/
				phone = ConvertPhone(phone.Trim());
				if (string.IsNullOrEmpty(phone))
				{
					throw new System.ArgumentException("Phone not valid");
				}
				

				if (!math.Success)
				{
					if (message.StartsWith("VIFA"))
					{
						List<Keyword> keywordList = kwDao.GetAllKeyword();
						Keyword kw = keywordList.Where(m => m.KeywordValue.ToUpper() == "VIFA").FirstOrDefault();
						SubKey sk = subKeyDao.GetWithFilter(1000, 1, -1, kw.ID, "VIFA", 0).FirstOrDefault();
						mtConfigList = mtConfigDao.GetConfigByAccountID(sk.AccountManagerID);

						throw new System.ArgumentException("WrongSyntax");
					}
					else
					{
						throw new System.ArgumentException("WrongMainSyntax");
					}
				}
				else
				{
					subkey = math.Groups[1].Value;
					pincode = math.Groups[2].Value;
					customerCode = math.Groups[3].Value;

					//	/*Tìm keyword*/
					List<Keyword> keywordList = kwDao.GetAllKeyword();
					Keyword kw = keywordList.Where(m => m.KeywordValue.ToUpper() == "VIFA").FirstOrDefault();
					SubKey sk = subKeyDao.GetWithFilter(1000, 1, -1, kw.ID, subkey, 0).FirstOrDefault();

					mtConfigList = mtConfigDao.GetConfigByAccountID(sk.AccountManagerID);

					if(mtConfigList==null)
					{
						mo.SubKeyID = sk.ID;
						throw new System.ArgumentException("Other");
					}

					PinCodeObj pinCode = pinCodeDao.GetPinCode(pincode, sk.AccountManagerID);

					if(pinCode==null)
					{
						mo.SubKeyID = sk.ID;
						throw new System.ArgumentException("WrongPincode");
					}
					else
					{
						if(pinCode.PinCodeStatus==PinCodeStatus.Used)
						{
							mo.SubKeyID = sk.ID;
							throw new System.ArgumentException("PincodeUsed");
						}

						if (pinCode.PinCodeStatus == PinCodeStatus.Deleted)
						{
							mo.SubKeyID = sk.ID;
							throw new System.ArgumentException("WrongPincode");
						}

						if (pinCode.PinCodeStatus == PinCodeStatus.JustCreated)
						{					
							/*kiem tra khách hàng*/
							var customer=customerDao.GetCustomerBy(phone, "");

							if(customer!=null)
							{
								/*có rồi thì kiểm tra xem cú pháp có nhập code không*/
								if(string.IsNullOrEmpty(customerCode))
								{
									/*sai cu pháp 2, phải dùng ng gioi thiệu*/
									mo.SubKeyID = sk.ID;
									throw new System.ArgumentException("WrongSyntax2");
								}
								else
								{
								
									var customerre = customer.CustomerCode==customerCode.ToUpper() ? customer : customerDao.GetCustomerBy("", customerCode);
									if(customerre==null)
									{
										/*mã khach hang khong ton tai*/
										mo.SubKeyID = sk.ID;
										throw new System.ArgumentException("WrongCustomer");
									}
									else
									{
										/*Đúng khách hàng gioi thieu*/
										mo.PinCodeID = pinCode.ID;
										mo.SubKeyID = sk.ID;
										mo.ReferralCustomerId = customerre.ID;
										mo.CustomerId = customer.ID;

										phoneReferral = customerre.CustomerPhone;
										customerCodeNew = customer.CustomerCode;

										throw new System.ArgumentException("Success2");
									}
								}
							}
							else
							{

								/*chua có khách hàng thì tạo*/
								Customer cus = new Customer();
								cus.ID = -1;
								cus.CustomerCode = GenarateCustomerCode(5, 3, 2);
								cus.CustomerPhone = phone;
								cus.CreatedDate = DateTime.Now;
								cus.UpdatedDate = DateTime.Now;

								Customer customerre = null;
								if (!string.IsNullOrEmpty(customerCode))
								{
									customerre = customerDao.GetCustomerBy("", customerCode);
									if(customerre!=null)
									{
										cus.ReferralCustomerId = customerre.ID;
									}
								}

								var cusId = customerDao.InsertAndGetId(cus);

								if (cusId > 0)
								{
									if (!string.IsNullOrEmpty(customerCode))
									{
										//var customerre = customerDao.GetCustomerBy("", customerCode);
										if (customerre == null)
										{
											/*mã khach hang khong ton tai*/
											mo.SubKeyID = sk.ID;
											throw new System.ArgumentException("WrongCustomer");
										}
										else
										{
											/*Đúng khách hàng gioi thieu có tồn tại*/

											mo.PinCodeID = pinCode.ID;
											mo.SubKeyID = sk.ID;
											mo.ReferralCustomerId = customerre.ID;
											mo.CustomerId = cus.ID;

											phoneReferral = customerre.CustomerPhone;
											customerCodeNew = cus.CustomerCode;

											throw new System.ArgumentException("Success2");
										}
									}
									else
									{
										mo.PinCodeID = pinCode.ID;
										mo.SubKeyID = sk.ID;
										mo.CustomerId = cusId;
										mo.ReferralCustomerId = 0;

										customerCodeNew = cus.CustomerCode;

										throw new System.ArgumentException("Success1");
									}
								}
								else
								{
									mo.SubKeyID = sk.ID;
									throw new System.ArgumentException("Other");
								}

								
							}
						}
						else
						{
							mo.SubKeyID = sk.ID;
							throw new System.ArgumentException("WrongPincode");
						}
					}
				}


			}
			catch(Exception ex)
			{
				switch(ex.Message)
				{
					case "Phone not valid":
						responeMessage = "2";
						mo.MoStatus = MoStatus.WrongPhoneNumber;
						break;
					case "WrongSyntax":
						//	/*Tìm keyword*/
						List<Keyword> keywordList = kwDao.GetAllKeyword();
						Keyword kw = keywordList.Where(m => m.KeywordValue.ToUpper() == "VIFA").FirstOrDefault();
						SubKey sk = subKeyDao.GetWithFilter(1000, 1, -1, kw.ID, "VIFA", 0).FirstOrDefault();

						responeMessage = "2";
						mo.SubKeyID = sk.ID;
						mo.MoStatus = MoStatus.WrongSyntax;
						break;
					case "WrongMainSyntax":
						responeMessage = "2";
						mo.MoStatus = MoStatus.WrongSyntaxMain;
						break;
					case "WrongPincode":
						responeMessage = "2";
						mo.MoStatus = MoStatus.WrongPinCode;
						break;
					case "PincodeUsed":
						responeMessage = "2";
						mo.MoStatus = MoStatus.PinCodeIsUsed;
						break;
					case "WrongSyntax2":
						responeMessage = "2";
						mo.MoStatus = MoStatus.WrongSyntaxCustomer;
						break;
					case "WrongCustomer":
						responeMessage = "2";
						mo.MoStatus = MoStatus.WrongCustomer;
						break;
					case "Success2":
						responeMessage = "2";
						mo.MoStatus = MoStatus.SuccessWithCustomer;
						break;
					case "Success1":
						responeMessage = "2";
						mo.MoStatus = MoStatus.Success;
						break;
					case"Other":
					default:
						responeMessage = "1";
						mo.MoStatus = MoStatus.Exception;
						break;
						
				}
			}
			finally
			{
				/*luu lai tin MO*/
				#region Save Data
				//mo.MoStatus = MoStatus.Success;
				if (moDao.Insert(mo))
				{
					List<Mt> listMt = new List<Mt>();

					switch(mo.MoStatus)
					{
						case MoStatus.Success:

							pinCodeDao.UpdatePinCodeStatus((int)PinCodeStatus.Used, mo.PinCodeID.AsLong());

							listMt.Add(new Mt() 
							{ 
								MoID = mo.ID,
								PhoneNumber = ConvertPhone84To0(phone),
								MtMessage = mtConfigList.Where(m => m.MtType == Utility.MtType.Success).FirstOrDefault().MtMessage.Replace("{0}", customerCodeNew)
							});

							break;
						case MoStatus.SuccessWithCustomer:
							pinCodeDao.UpdatePinCodeStatus((int)PinCodeStatus.Used, mo.PinCodeID.AsLong());

							//var cus = customerDao.GetSingle(mo.ReferralCustomerId);

							listMt.Add(new Mt()
							{
								MoID = mo.ID,
								PhoneNumber =  ConvertPhone84To0(phoneReferral),
								MtMessage = mtConfigList.Where(m => m.MtType == Utility.MtType.Success2).FirstOrDefault().MtMessage
							});

							Thread.Sleep(10);

							if (!string.IsNullOrEmpty(customerCodeNew))
							{
								listMt.Add(new Mt()
								{
									MoID = mo.ID,
									PhoneNumber = ConvertPhone84To0(phone),
									MtMessage = mtConfigList.Where(m => m.MtType == Utility.MtType.Success).FirstOrDefault().MtMessage.Replace("{0}", customerCodeNew)
								});
							}

							Thread.Sleep(10);

							listMt.Add(new Mt()
							{
								MoID = mo.ID,
								PhoneNumber = ConvertPhone84To0(phone),
								MtMessage = mtConfigList.Where(m => m.MtType == Utility.MtType.Success3).FirstOrDefault().MtMessage
							});
							break;
						case MoStatus.PinCodeIsUsed:
							listMt.Add(new Mt()
							{
								MoID = mo.ID,
								PhoneNumber = ConvertPhone84To0(phone),
								MtMessage = mtConfigList.Where(m => m.MtType == Utility.MtType.PinCodeIsUsed).FirstOrDefault().MtMessage
							});

							break;
						case MoStatus.WrongCustomer:
							listMt.Add(new Mt()
							{
								MoID = mo.ID,
								PhoneNumber = ConvertPhone84To0(phone),
								MtMessage = mtConfigList.Where(m => m.MtType == Utility.MtType.WrongCustomer).FirstOrDefault().MtMessage
							});

							break;
						case MoStatus.WrongPinCode:
							listMt.Add(new Mt()
							{
								MoID = mo.ID,
								PhoneNumber = ConvertPhone84To0(phone),
								MtMessage = mtConfigList.Where(m => m.MtType == Utility.MtType.WrongPinCode).FirstOrDefault().MtMessage
							});

							break;
						case MoStatus.WrongSyntax:
							listMt.Add(new Mt()
							{
								MoID = mo.ID,
								PhoneNumber = ConvertPhone84To0(phone),
								MtMessage = mtConfigList.Where(m => m.MtType == Utility.MtType.WrongSystax).FirstOrDefault().MtMessage
							});
							break;
						case MoStatus.WrongSyntaxCustomer:
							listMt.Add(new Mt()
							{
								MoID = mo.ID,
								PhoneNumber = ConvertPhone84To0(phone),
								MtMessage = mtConfigList.Where(m => m.MtType == Utility.MtType.WrongSyntax2).FirstOrDefault().MtMessage
							});
							break;
						case MoStatus.WrongSyntaxMain:
							listMt.Add(new Mt()
							{
								MoID = mo.ID,
								PhoneNumber = ConvertPhone84To0(phone),
								MtMessage = defaultWrongSyntaxMessage,
							});
							break;
					}

					foreach (var item in listMt)
					{
						SendSMSToFibo(item);
					}
				}
				#endregion
			}

			return ReturnData(BuildMt2ways(responeMessage));
		}

		private void SendSMSToFibo(Mt sms)
		{
			try
			{
				ServiceSoapClient service = new ServiceSoapClient();
				string result = service.SendSMS(ConfigurationManager.AppSettings["ClientNo"], ConfigurationManager.AppSettings["PasswordClientNo"], sms.PhoneNumber, sms.MtMessage, sms.GUID, int.Parse(ConfigurationManager.AppSettings["ServiceTypeId"]));
				sms.Remark = result;
				if (sms.Remark.Contains("200"))
				{
					sms.MtStatus = MTStatus.Sending;
				}
				else
				{
					sms.MtStatus = MTStatus.Error;
				}
			}
			catch(Exception ex)
			{
				sms.Remark = ex.Message;
				sms.MtStatus = MTStatus.Error;
			}

			mtDao.Insert(sms);
		}

		private string GenarateCustomerCode(int length, int countNumber, int countAphaB)
		{
			string code = GFunction.CreateCode(length, countNumber, countAphaB);

			while (customerDao.ExistsCustomerCode(code))
			{
				code = GFunction.CreateCode(length, countNumber, countAphaB);
			}

			return code;
		}

		private string ConvertPhone84To0(string phone)
		{
			try
			{
				phone = phone.Trim();
				phone = phone.Replace("+", "");

				if (phone.StartsWith("84"))
				{
					phone = "0" + phone.Substring(2) ;
				}

				if (!phone.StartsWith("0"))
				{
					phone = "0" + phone;
				}
				return phone;
			}
			catch
			{
				return phone;
			}
		}

		private string ConvertPhone(string phone)
		{
			try
			{
				if (string.IsNullOrEmpty(phone))
				{
					return "";
				}

				if (phone.Length < 9)
				{
					return "";
				}

				phone = phone.Replace("+", "");

				if (phone.StartsWith("0"))
				{
					phone = "84" + phone.Substring(1);
				}

				if (!phone.StartsWith("84"))
				{
					phone = "84" + phone;
				}

				return phone;
			}
			catch
			{
				return "";
			}
		}
		

        public ActionResult MtProcess_bk(string message, string phone, string service, string port, string guid,
            string main, string sub)
        {

            try
            {
                string defaultWrongSyntaxMessage = "";//ConfigurationManager.AppSettings["DefaultWrongSyntaxMessage"].ToString();
                MtConfig conf = mtConfigDao.GetDefaultWrongSyntaxMessage();
                if(conf != null)
                {
                    defaultWrongSyntaxMessage = conf.MtMessage;
                }
                
                //string mainKey = ConfigurationManager.AppSettings["SMSMainKeyword"].ToString();
                string responeMessage = "";
                if (string.IsNullOrEmpty(phone))
                    return ReturnData("Du lieu ko hop le");
                bool status = false;
                message = message.ToUpper();
                List<string> messageArray = message.Trim().Split(' ').ToList();
                messageArray.RemoveAll(t => t == "");

                List<Keyword> keywordList = kwDao.GetAllKeyword();
                Keyword kw = keywordList.Where(m => m.KeywordValue == messageArray[0]).FirstOrDefault();

                Mo mo = new Mo();
                mo.PhoneNumber = phone;
                mo.MoMessage = message;

                mo.MainKey = messageArray[0];
                mo.GUID = guid;
                if (messageArray.Count() == 3)
                {
                    if (kw != null)
                    {
                        List<SubKey> listSubKey = subKeyDao.GetWithFilter(1000, 1, -1, kw.ID, "", 0);
                        SubKey sk = (listSubKey != null) ? (SubKey)listSubKey.Where(m => m.SubKeyValue == messageArray[1].ToUpper()).FirstOrDefault() : null;
                        //List<ProductType> productTypeList = productTypeDao.GetProductTypeByAccountID(kw.AccountManagerID);
                        //ProductType pt = (productTypeList != null) ? (ProductType)productTypeList.Where(m => m.ProductTypeKey == messageArray[1]).FirstOrDefault() : null;

                        if (kw != null && sk != null)
                        {
                            mo.MainKey = kw.KeywordValue;
                            mo.SubKeyID = sk.ID;
                            List<MtConfig> mtConfigList = mtConfigDao.GetConfigByAccountID(sk.AccountManagerID);
                            MtConfig mtSuccess = mtConfigList.Where(m => m.MtType == Utility.MtType.Success).FirstOrDefault();
                            MtConfig mtWrongSyntax = mtConfigList.Where(m => m.MtType == Utility.MtType.WrongSystax).FirstOrDefault();
                            MtConfig mtWrongPinCode = mtConfigList.Where(m => m.MtType == Utility.MtType.WrongPinCode).FirstOrDefault();
                            MtConfig mtPinCodeIsUsed = mtConfigList.Where(m => m.MtType == Utility.MtType.PinCodeIsUsed).FirstOrDefault();
                            MtConfig mtSubKeyIsDeactive = mtConfigList.Where(m => m.MtType == Utility.MtType.SubKeyIsDeactive).FirstOrDefault();
                            PinCodeObj pinCode = pinCodeDao.GetPinCode(messageArray[2], sk.AccountManagerID);
                            if (kw.KeywordStatus == KeywordStatus.Active  && sk.SubKeyStatus == SubKeyStatus.Active)
                            {
                                if (pinCode != null)
                                {
                                    if (pinCode.ID > 0)
                                    {
                                        if (pinCode.PinCodeStatus == Utility.PinCodeStatus.JustCreated)
                                        {
                                            // Success
                                            status = true;
                                            mo.MoStatus = MoStatus.JustCreated;
                                            responeMessage = (mtSuccess != null) ? mtSuccess.MtMessage : "";
                                        }
                                        else
                                        {
                                            mo.MoStatus = MoStatus.PinCodeIsUsed;
                                            responeMessage = (mtPinCodeIsUsed != null) ? mtPinCodeIsUsed.MtMessage : "";
                                            //Pincode Is Used
                                        }
                                        mo.PinCodeID = pinCode.ID;
                                    }
                                    else
                                    {
                                        responeMessage = (mtWrongPinCode != null) ? mtWrongPinCode.MtMessage : "";
                                        mo.PinCodeID = -1;
                                        mo.MoStatus = MoStatus.WrongPinCode;
                                        // Wrong Pincode
                                    }
                                }
                                else
                                {
                                    responeMessage = (mtWrongPinCode != null) ? mtWrongPinCode.MtMessage : "";
                                    mo.PinCodeID = -1;
                                    mo.MoStatus = MoStatus.WrongPinCode;
                                }
                            }
                            else
                            {
                                responeMessage = (mtSubKeyIsDeactive != null) ? mtSubKeyIsDeactive.MtMessage : "";
                                mo.PinCodeID = (pinCode != null) ? pinCode.ID : -1;
                                mo.MoStatus = MoStatus.AccountIsDeactive;
                            }
                        }
                        else
                        {
                            responeMessage = defaultWrongSyntaxMessage;
                            // Wrong Syntax default
                            mo.PinCodeID = -1;
                            mo.MoStatus = MoStatus.WrongSyntax;
                        }
                    }
                    else
                    {
                        responeMessage = defaultWrongSyntaxMessage;
                        // Wrong Syntax default
                        mo.PinCodeID = -1;
                        mo.MoStatus = MoStatus.WrongSyntax;
                    }
                }
                else
                {
                    responeMessage = defaultWrongSyntaxMessage;
                    // Wrong Syntax default
                    mo.PinCodeID = -1;
                    mo.MoStatus = MoStatus.WrongSyntax;
                }

                #region Save Data
                mo.MoStatus = MoStatus.Success;
                if (moDao.Insert(mo))
                {
                    if (status)
                    {
                        pinCodeDao.UpdatePinCodeStatus((int)PinCodeStatus.Used, mo.PinCodeID.AsLong());
                    }
                    // Save MT
                    Mt mt = new Mt();
                    mt.MoID = mo.ID;
                    mt.PhoneNumber = phone;
                    mt.MtMessage = responeMessage;
                    mtDao.Insert(mt);
                }
                #endregion

                if (responeMessage == "")
                {
                    responeMessage = "He thong chua duoc cau hinh noi dung thong bao den khach hang. Ban vui long lien he nha cung cap san pham de duoc ho tro";
                }
                //responeMessage = message;
                return ReturnData(BuildMt(responeMessage, phone, service));
            }
            catch (Exception ex)
            {
                return ReturnData(BuildMt("Xay ra su co voi he thong. Ban vui long thu lai vao it giay sau", phone, service));
            }
            #region BK
            //Mo mo = new Mo
            //{
            //    MoMessage = message,
            //    PhoneNumber = phone,
            //    CreatedDate = DateTime.Now,
            //    PinCodeID = null,
            //    UpdatedDate = DateTime.Now,
            //    GUID = guid
            //};
            //moDao.Insert(mo);

            //try
            //{
            //    Config conf = con
            //    if (
            //        DateTime.Now.Subtract(Convert.ToDateTime(ConfigurationManager.AppSettings["ExpiredDate"])).TotalDays >=
            //        0)
            //    {
            //        mo.MoType = MoType.Expired;
            //        _moDao.Update(mo);
            //        mtMessage = config.MtExpired;
            //    }
            //    else
            //    {
            //        //xử lý trường hợp khách viết LIENA[4 số][Mã thẻ]
            //        if (messageArray.Count == 1 && messageArray[0].Length == 16)
            //        {
            //            List<string> data = new List<string>()
            //            {
            //                messageArray[0].Substring(0, 4),
            //                messageArray[0].Substring(4, 4),
            //                messageArray[0].Substring(8, 8)
            //            };
            //            messageArray = data;
            //        }// xử lý trường hợp khách nt LIENA[cách][4 số][Mã thẻ]
            //        else if (messageArray.Count == 2 && messageArray[1].Length == 12)
            //        {
            //            List<string> data = new List<string>()
            //            {
            //                messageArray[0],
            //                messageArray[1].Substring(0, 4),
            //                messageArray[1].Substring(4, 8)
            //            };
            //            messageArray = data;
            //        }//xử lý trường hợp khách nt LIENA[4 số][cách][mã thẻ]
            //        else if (messageArray.Count == 2 && messageArray[0].Length == 8)
            //        {
            //            List<string> data = new List<string>()
            //            {
            //                messageArray[0].Substring(0, 4),
            //                messageArray[0].Substring(4, 4),
            //                messageArray[1]
            //            };
            //            messageArray = data;
            //        }


            //        if (!main.ToLower().Equals(_keyword) || !sub.ToLower().Equals(_subkey) || !port.Equals(_port) ||
            //            (messageArray.Count < 3))
            //        {
            //            mo.MoType = MoType.WrongSyntax;
            //            _moDao.Update(mo);
            //            mtMessage = config.MtWrongSyntax;
            //        }
            //        else
            //        {
            //            string cardCode = messageArray[2];

            //            Card card = _cardDao.SearchCard(cardCode).SingleOrDefault();

            //            if (card != null && card.CardStatus == CardStatus.JustCreated)
            //            {
            //                mo.CardId = card.ID;
            //                mo.MoType = MoType.ValidCardNo;
            //                _moDao.Update(mo);

            //                card.CardStatus = CardStatus.Used;
            //                _cardDao.Update(card);

            //                int serviceTypeId = -1;
            //                if (card.Value == "10000")
            //                    serviceTypeId = 3817;
            //                else if (card.Value == "20000")
            //                    serviceTypeId = 3923;
            //                else if (card.Value == "30000")
            //                    serviceTypeId = 3924;
            //                else if (card.Value == "50000")
            //                    serviceTypeId = 3925;
            //                else if (card.Value == "100000")
            //                    serviceTypeId = 3926;
            //                else if (card.Value == "200000")
            //                    serviceTypeId = 3935;
            //                else
            //                    serviceTypeId = -1;

            //                try
            //                {
            //                    TopUp topup = new TopUp();
            //                    topup.PhoneNumber = mo.PhoneNumber;
            //                    topup.MoId = mo.ID;
            //                    topup.TopUpStatus = TopUpStatus.JustCreated;
            //                    topup.TopUpGuid = Guid.NewGuid().ToString();
            //                    topup.ServiceTypeId = serviceTypeId;
            //                    topup.SentTimes = 1;
            //                    topup.Message = "TopUp cho chuong trinh Lien A";
            //                    _topUpDao.Insert(topup);

            //                    string clientNo = ConfigurationManager.AppSettings["ClientNo"];
            //                    string password = ConfigurationManager.AppSettings["ClientPass"];

            //                    string sign = GetMD5(string.Format("{0}-{1}", clientNo, GetMD5(password)));
            //                    string result = _topupService.TopupMobile(clientNo, topup.PhoneNumber, topup.TopUpGuid,
            //                        serviceTypeId, sign, topup.Message);
            //                    //string result = "00";
            //                    topup.Remark = string.Format("FiboStatus:{0}", result);
            //                    if (result == "00")
            //                        topup.TopUpStatus = TopUpStatus.Pending;
            //                    else
            //                    {
            //                        topup.TopUpStatus = TopUpStatus.Fail;
            //                        topup.IsSentSms = IsSentSms.SentFail;
            //                    }

            //                    _topUpDao.Update(topup);
            //                }
            //                catch (Exception ex)
            //                {
            //                }

            //                mtMessage = config.MtSuccessContent.Replace("[card_value]", card.CardCode)
            //                    .Replace("[topup_value]", card.Value);
            //            }
            //            else if (card != null && card.CardStatus == CardStatus.Used)
            //            {
            //                mo.MoType = MoType.CardIsUsed;
            //                _moDao.Update(mo);

            //                mtMessage = config.MtCardUsed.Replace("[card_value]", card.CardCode);
            //            }
            //            else
            //            {
            //                mo.MoType = MoType.InvalidCarNo;
            //                _moDao.Update(mo);

            //                mtMessage = config.MtCardNotExits.Replace("[card_value]", cardCode);
            //                ;
            //            }
            //        }
            //    }

            //    var mt = new Mt
            //    {
            //        MoId = mo.ID,
            //        ID = 0,
            //        CreatedDate = DateTime.Now,
            //        UpdatedDate = DateTime.Now,
            //        PhoneNumber = phone,
            //        Message = mtMessage
            //    };
            //    _mtDao.Insert(mt);

            //    return ReturnData(BuildMt(mt.Message, phone, service));
            //}
            //catch (Exception ex)
            //{
            //    //lưu Mo trạng thái sai cú pháp
            //    mo.MoType = MoType.WrongSyntax;
            //    _moDao.Insert(mo);

            //    // trả về tin sai cú pháp
            //    var mt = new Mt
            //    {
            //        MoId = mo.ID,
            //        ID = 0,
            //        CreatedDate = DateTime.Now,
            //        UpdatedDate = DateTime.Now,
            //        PhoneNumber = phone,
            //        Message = config.MtWrongSyntax,

            //    };
            //    _mtDao.Insert(mt);
            //    return ReturnData(BuildMt(mt.Message, phone, service));
            //}
            #endregion
        }


        private string BuildMt(string mt, string phone, string service)
        {
            if (mt.Length > 160)
            {
                string mess = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ClientResponse>";
                string childmess = string.Empty;
                string[] array = mt.Split(' ');
                for (int i = 0; i < array.Length; i++)
                {
                    if ((childmess.Length + array[i].Length + 1) > 160)
                    {
                        mess += "<Message><PhoneNumber>" + phone + "</PhoneNumber>" +
                                "<Message>" + childmess.Trim() + "</Message>" +
                                "<SMSID>" + Guid.NewGuid() + "</SMSID>" +
                                "<ServiceNo>" + service + "</ServiceNo>" +
                                "<ContentType>0</ContentType>" +
                                "</Message>";
                        childmess = string.Empty;
                    }
                    else
                    {
                        childmess += array[i] + " ";
                    }
                }
                mess += "</ClientResponse>";
                return mess;
            }
            return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                   "<ClientResponse><Message>" +
                   "<PhoneNumber>" + phone + "</PhoneNumber>" +
                   "<Message>" + mt + "</Message>" +
                   "<SMSID>" + Guid.NewGuid().ToString() + "</SMSID>" +
                   "<ServiceNo>" + service + "</ServiceNo>" +
                   "<ContentType>0</ContentType>" +
                   "</Message></ClientResponse>";
        }

		private string BuildMt2ways(string code)
		{
			string response = string.Format("<?xml version=\"1.0\" encoding=\"UTF-8\"?><Status>{0}</Status>", code);
			return response;
		}
    }
}
