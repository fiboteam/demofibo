﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;
using VinaVN.Models;
using Vina.DAO;
using Utility;
using Vina.DTO;

namespace VinaVN.Controllers
{
   
    public class AccountController : Controller
    {
        public SqlAccountDao accountDao = new SqlAccountDao();
        //
        // GET: /Account/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            AccountModel model = new AccountModel();
            return View(model);
        }

        public ActionResult Forgot(string Email)
        {
            //var all = accountDao.GetAll();
            //if (all != null && all.Count > 0 && all[0].Email == Email)
            //{
            //    var current = all.FirstOrDefault();
            //    string email = current.Email;
            //    string pass = Guid.NewGuid().ToString().Substring(0, 4).ToUpper();
            //    var oMail = new SendMail
            //    {
            //        EmailFromName = "Vina Website",
            //        EmailTo = email,
            //        IsBodyHtml = true,
            //        EmailSubject = "Thông tin mật khẩu mới!!",
            //        EmailBody = "<p>Chào " + current.AccountNo + "!!!</p> Mật khẩu đăng nhập mới của bạn là :<strong>" + pass + "</strong>"
            //    };
            //    oMail.send();
            //    current.Password = GFunction.GetMD5(pass);
            //    if (accountDao.Update(current))
            //    {
            //        ViewBag.Message = "Đã gửi mật khẩu về email của bạn";
            //    }
            //}
            return View();
        }

        [HttpPost]
        public ActionResult Login(AccountModel model)
        {
            if (ModelState.IsValid)
            {
                Account accountLogin = accountDao.CheckLogin(model.LoginName, model.PassWord);
                Session["AccountLogin"] = accountLogin;
                if(accountLogin != null)
                {
                    if (accountLogin.ParentId == 0)
                    {
                        return Redirect("/Home/ManagerPanel");
                    }
                    return Redirect("/Home");
                }
            }
            ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không chính xác.");
            return View(model);
        }

        public ActionResult ShowLoginInfo()
        {
            ViewBag.AccountLogin = (Session["AccountLogin"] != null) ? Session["AccountLogin"] : new Account();
            return View("~/Views/Account/ShowLoginInfo.cshtml");
        }

        public ActionResult Logout()
        {
            Session["AccountLogin"] = null;
            return Redirect("/Account/Login");

        }

        public ActionResult ChangePassword()
        {
            if (Session["AccountLogin"] == null)
            {
                return Redirect("/Account/Logout");
            }
            ViewBag.AccountLogin = (Account)Session["AccountLogin"];
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            Account accountLogin = null;
            if (Session["AccountLogin"] != null)
            {
                accountLogin = (Account)Session["AccountLogin"];
                if (GFunction.GetMD5(model.CurrentPassword) == accountLogin.PassWord)
                {
                    if (model.NewPassword == model.ConfirmNewPassword)
                    {
                        if (accountDao.ChangePassword(accountLogin.LoginName, GFunction.GetMD5(model.NewPassword)) > 0)
                        {
                            ViewBag.SuccessMessage = "Mật khẩu đã được cập nhật";
                        }
                        else
                        {
                            ViewBag.ErrorMessage = "Đổi mật khẩu thất bại";
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessage = "Mật khẩu mới không trùng khớp";
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = "Mật khẩu hiện tại không trùng khớp";
                }
            }
            ViewBag.AccountLogin = accountLogin;
            return View();
        }

        
    }
}
