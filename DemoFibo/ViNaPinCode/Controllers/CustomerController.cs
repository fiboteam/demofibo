﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility;
using Vina.DAO;
using Vina.DTO;
using VinaVN.Models;

namespace VinaVN.Controllers
{
	public class CustomerController : BaseController
    {
		private readonly SqlCustomerDao _customerDao = new SqlCustomerDao();
        //
        // GET: /Customer/
		

		public ActionResult Index(CustomerViewModel model)
        {
			try
			{
				if (Request.Form["submitButton"] == "Export")
				{
					//ProcessExport(model);
					return Redirect("/Mo");
				}
				else
				{
					if (model.CurrentPage != 0)
						model.Page.CurrentPage = model.CurrentPage;
					else
						model.Page.CurrentPage = 1;
					//if (model.IsPopup)
					//	model.Page.PageSize = 0;

					model.DataList = _customerDao.GetWithFilter(model.Page.PageSize, model.Page.CurrentPage,
						model.FromDate,
						model.ToDate,
						model.CustomerName ?? "",
						model.CustomerPhone ?? "",
						model.CustomerAddress ?? "",
						model.CustomerEmail ?? "",
						model.CustomerCode??"",
						model.ReferralCustomerCode??""
						);

					var firstOrDefault = model.DataList.FirstOrDefault();
					if (firstOrDefault != null)
						model.Page.TotalRecords = (model.DataList.Any())
							? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
							: 0;
					else
						model.Page.TotalRecords = 0;

					ViewBag.SuccessMessage = (Session["SuccessMessage"] != null) ? Session["SuccessMessage"] : "";
					ViewBag.ErrorMessage = (Session["ErrorMessage"] != null) ? Session["ErrorMessage"] : "";
					Session["SuccessMessage"] = "";
					Session["ErrorMessage"] = "";

					//ViewBag.SuccessMessage = GenarateCustomerCode(5, 3, 2);

					return View(model);
				}
			}
			catch (Exception)
			{
				return View(model);
			}
        }
		[HttpGet]
		public ActionResult Edit(long cusid)
		{
			var customer = _customerDao.GetSingle(cusid);
			CustomerEditModel model;
			if (customer != null)
			{
				model = new CustomerEditModel();
				model.ID = customer.ID;
				model.CustomerName = customer.CustomerName;
				model.CustomerEmail = customer.CustomerEmail;
				model.CustomerAddress = customer.CustomerAddress;
				model.CustomerNote = customer.CustomerNote;
				return View(model);
			}
			else
			{
				return Redirect("~/Customer");
			}
		}
		[HttpPost]
		public ActionResult Edit(CustomerEditModel model)
		{
			if(ModelState.IsValid)
			{
				var customer = new Customer();
				customer.ID = model.ID;
				customer.CustomerName = model.CustomerName;
				customer.CustomerEmail = model.CustomerEmail;
				customer.CustomerAddress = model.CustomerAddress;
				customer.CustomerNote = model.CustomerNote;

				var resultUpdate = _customerDao.Update(customer);
				if (resultUpdate)
				{
					Session["SuccessMessage"] = "Chỉnh sửa khách hàng thành công";
				}
				else
				{
					Session["ErrorMessage"] = "Chỉnh sửa khách hàng thất bại";
				}
			}
			else
			{
				Session["ErrorMessage"] = "Chỉnh sửa khách hàng thất bại";
			}
			return Redirect("/Customer");
		}
    }
}
