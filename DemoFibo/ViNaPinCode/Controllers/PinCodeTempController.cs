﻿using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility;
using Vina.DAO;
using Vina.DTO;

namespace VinaVN.Controllers
{
    public class PinCodeTempController : BaseController
    {
        private readonly SqlPinCodeTempDao _pinCodeTempDao = new SqlPinCodeTempDao();
        //
        // GET: /PinCodeTemp/
        public ActionResult ExportListAfterGenerate(long createdByID)
        {
            string fileName = "list.xls";
            List<PinCodeTemp> successList = _pinCodeTempDao.GetAllToExport(createdByID);
            fileName = "generate_list_" + Guid.NewGuid() + "_" + DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xls";
            ExportToExcel(successList, fileName);
            return Redirect("/Home/ManagerPanel");
        }
        
        MemoryStream GetExcelStream(HSSFWorkbook hssfworkbook)
        {
            //Write the stream data of workbook to the root directory
            MemoryStream file = new MemoryStream();
            hssfworkbook.Write(file);
            return file;
        }

        void GenerateData(HSSFWorkbook hssfworkbook, List<PinCodeTemp> pinCodeList = null)
        {
            ISheet sheet1 = hssfworkbook.CreateSheet("Sheet1");
            sheet1.CreateRow(0).CreateCell(0).SetCellValue("Danh sách mã số");
            IRow rowheader = sheet1.CreateRow(1);
            rowheader.CreateCell(0).SetCellValue("Pincode");
            rowheader.CreateCell(1).SetCellValue("Số Serial 1");
            //rowheader.CreateCell(2).SetCellValue("Số Serial 2");
            rowheader.CreateCell(2).SetCellValue("Trạng thái");
            rowheader.CreateCell(3).SetCellValue("Ngày nhập");
            rowheader.CreateCell(4).SetCellValue("Ngày cập nhật");
            int i = 2;
            if (pinCodeList == null)
                return;
            foreach (PinCodeTemp pinCode in pinCodeList)
            {
                int j = 0;
                IRow row = sheet1.CreateRow(i);
                row.CreateCell(j).SetCellValue((pinCode.PinCode != null) ? pinCode.PinCode.ToString() : "N/A");
                j++;
                row.CreateCell(j).SetCellValue((pinCode.Serial != null) ? pinCode.Serial.ToString() : "N/A");
                j++;
                row.CreateCell(j).SetCellValue(PinCodeStatus.JustCreated.ToString());
                j++;
                row.CreateCell(j).SetCellValue(pinCode.CreatedDate.AsDateTime().ToString("dd/MM/yyyy"));
                j++;
                row.CreateCell(j).SetCellValue(pinCode.UpdatedDate.AsDateTime().ToString("dd/MM/yyyy"));
                j++;
                i++;
            }
        }

        void InitializeWorkbook(HSSFWorkbook hssfworkbook)
        {
            hssfworkbook = new HSSFWorkbook();

            ////create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "NPOI Team";
            hssfworkbook.DocumentSummaryInformation = dsi;

            ////create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "NPOI SDK Example";
            hssfworkbook.SummaryInformation = si;
        }

        public void ExportToExcel(List<PinCodeTemp> pincodeList = null, string fileName = "")
        {
            HSSFWorkbook hssfworkbook = new HSSFWorkbook();

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
            Response.Clear();
            InitializeWorkbook(hssfworkbook);
            GenerateData(hssfworkbook, pincodeList);
            GetExcelStream(hssfworkbook).WriteTo(Response.OutputStream);
            Response.End();
        }

        private ICell GetCell(IRow row, int column)
        {
            ICell cell = row.GetCell(column);
            if (cell == null)
                return row.CreateCell(column);
            return cell;
        }

    }
}
