﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vina.DAO;
using Vina.DTO;
using VinaVN.Controllers;

namespace ViNaPinCode.Controllers
{
    public class PinCodeBlockController : BaseController
    {
        //
        // GET: /PinCodeBlock/
        private SqlPinCodeBlockDao _pinCodeBlockDao = new SqlPinCodeBlockDao();
        public ActionResult Index()
        {
            List<PinCodeBlock> listblock = new List<PinCodeBlock>();
            listblock = _pinCodeBlockDao.GetList(SAccount.ID);
            ViewBag.ListBlock = listblock;
            ViewBag.SuccessMessage = (Session["SuccessMessage"] != null) ? Session["SuccessMessage"] : "";
            ViewBag.ErrorMessage = (Session["ErrorMessage"] != null) ? Session["ErrorMessage"] : "";
            Session["SuccessMessage"] = "";
            Session["ErrorMessage"] = "";
            return View();
        }

        public ActionResult Delete(long pbid)
        {
            int result = _pinCodeBlockDao.DeletePCB(pbid);
            if (result > 0)
            {
                SuccessMessage = "Đã xóa thành công block và " + (result - 1) + " mả pin";
            }
            else
            {
                ErrorMessage = "Xóa thất bại";
            }
            return Redirect("/PinCodeBlock");
        }

    }
}
