USE [VifaSportDB]
GO
--/****** Object:  Database [VifaSportDB]    Script Date: 05/04/2016 11:59:45 ******/
--CREATE DATABASE [VifaSportDB] ON  PRIMARY 
--( NAME = N'InVina', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\InVina.mdf' , SIZE = 217088KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
-- LOG ON 
--( NAME = N'InVina_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\InVina_1.ldf' , SIZE = 833024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
--GO
ALTER DATABASE [VifaSportDB] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [VifaSportDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [VifaSportDB] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [VifaSportDB] SET ANSI_NULLS OFF
GO
ALTER DATABASE [VifaSportDB] SET ANSI_PADDING OFF
GO
ALTER DATABASE [VifaSportDB] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [VifaSportDB] SET ARITHABORT OFF
GO
ALTER DATABASE [VifaSportDB] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [VifaSportDB] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [VifaSportDB] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [VifaSportDB] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [VifaSportDB] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [VifaSportDB] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [VifaSportDB] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [VifaSportDB] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [VifaSportDB] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [VifaSportDB] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [VifaSportDB] SET  DISABLE_BROKER
GO
ALTER DATABASE [VifaSportDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [VifaSportDB] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [VifaSportDB] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [VifaSportDB] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [VifaSportDB] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [VifaSportDB] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [VifaSportDB] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [VifaSportDB] SET  READ_WRITE
GO
ALTER DATABASE [VifaSportDB] SET RECOVERY FULL
GO
ALTER DATABASE [VifaSportDB] SET  MULTI_USER
GO
ALTER DATABASE [VifaSportDB] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [VifaSportDB] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'Invina_bk', N'ON'
GO
USE [VifaSportDB]
GO
/****** Object:  User [userinvina]    Script Date: 05/04/2016 11:59:45 ******/
CREATE USER [userinvina] FOR LOGIN [userinvina] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [InVina]    Script Date: 05/04/2016 11:59:45 ******/
CREATE USER [InVina] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[tblSuccessMessage]    Script Date: 05/04/2016 11:59:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSuccessMessage](
	[SuccessMessageID] [bigint] IDENTITY(1,1) NOT NULL,
	[SuccessMessage] [nvarchar](max) NOT NULL,
	[SubKeyID] [bigint] NOT NULL,
	[AccountManagerID] [bigint] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[MaxCharacter] [int] NOT NULL,
 CONSTRAINT [PK_tblSuccessMessage] PRIMARY KEY CLUSTERED 
(
	[SuccessMessageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSubKey]    Script Date: 05/04/2016 11:59:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSubKey](
	[SubKeyID] [bigint] IDENTITY(1,1) NOT NULL,
	[AccountManagerID] [bigint] NOT NULL,
	[KeywordID] [bigint] NULL,
	[SubKeyValue] [nvarchar](50) NOT NULL,
	[SubKeyStatus] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_tblSubKey] PRIMARY KEY CLUSTERED 
(
	[SubKeyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblProductType]    Script Date: 05/04/2016 11:59:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblProductType](
	[ProductTypeID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductTypeName] [nvarchar](max) NOT NULL,
	[ProductTypeKey] [nvarchar](50) NOT NULL,
	[ProductTypeStatus] [nchar](10) NOT NULL,
	[ManagerID] [bigint] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_tblProductType] PRIMARY KEY CLUSTERED 
(
	[ProductTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPinCodeTemp]    Script Date: 05/04/2016 11:59:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPinCodeTemp](
	[PinCodeTempID] [bigint] IDENTITY(1,1) NOT NULL,
	[PinCode] [nvarchar](1000) NOT NULL,
	[Serial] [nvarchar](1000) NOT NULL,
	[BlockID] [bigint] NOT NULL,
	[CreatedByID] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_tblPinCodeTemp] PRIMARY KEY CLUSTERED 
(
	[PinCodeTempID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPinCodeBlock]    Script Date: 05/04/2016 11:59:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPinCodeBlock](
	[PinCodeBlockID] [bigint] IDENTITY(1,1) NOT NULL,
	[PinCodeBlockName] [nvarchar](1000) NULL,
	[PinCodeBlockStatus] [tinyint] NULL,
	[AccountManagerID] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_tblPinCodeBlock] PRIMARY KEY CLUSTERED 
(
	[PinCodeBlockID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPinCode]    Script Date: 05/04/2016 11:59:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPinCode](
	[PinCodeID] [bigint] IDENTITY(1,1) NOT NULL,
	[PinCode] [nvarchar](50) NOT NULL,
	[Serial] [nvarchar](50) NOT NULL,
	[Serial2] [nvarchar](50) NULL,
	[AccountManagerID] [bigint] NOT NULL,
	[ProductTypeID] [bigint] NULL,
	[PinCodeStatus] [tinyint] NULL,
	[PinCodeBlockID] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_tblCard] PRIMARY KEY CLUSTERED 
(
	[PinCodeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMtConfig]    Script Date: 05/04/2016 11:59:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMtConfig](
	[MtConfigID] [bigint] IDENTITY(1,1) NOT NULL,
	[MtMessage] [nvarchar](1000) NOT NULL,
	[MtType] [tinyint] NOT NULL,
	[AccountManagerID] [bigint] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_tblMtConfig] PRIMARY KEY CLUSTERED 
(
	[MtConfigID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMt]    Script Date: 05/04/2016 11:59:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMt](
	[MtID] [bigint] IDENTITY(1,1) NOT NULL,
	[MtMessage] [nvarchar](1000) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[ShortCreatedDate] [int] NULL,
	[MoID] [bigint] NULL,
	[MtStatus] [tinyint] NULL,
 CONSTRAINT [PK_tblMt] PRIMARY KEY CLUSTERED 
(
	[MtID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMo]    Script Date: 05/04/2016 11:59:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMo](
	[MoID] [bigint] IDENTITY(1,1) NOT NULL,
	[PinCodeID] [bigint] NOT NULL,
	[MoMessage] [nvarchar](1000) NOT NULL,
	[PhoneNumber] [nvarchar](50) NOT NULL,
	[ServiceType] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[ShortCreatedDate] [int] NULL,
	[MainKey] [nvarchar](50) NOT NULL,
	[SubKeyID] [bigint] NOT NULL,
	[MoStatus] [tinyint] NOT NULL,
	[Guid] [nvarchar](255) NULL,
 CONSTRAINT [PK_tblMo] PRIMARY KEY CLUSTERED 
(
	[MoID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblKeyword]    Script Date: 05/04/2016 11:59:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblKeyword](
	[KeywordID] [bigint] IDENTITY(1,1) NOT NULL,
	[KeywordValue] [nvarchar](50) NOT NULL,
	[AccountManagerID] [bigint] NOT NULL,
	[KeywordStatus] [tinyint] NOT NULL,
	[TotalProductType] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_tblKeyword] PRIMARY KEY CLUSTERED 
(
	[KeywordID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblErrorMessage]    Script Date: 05/04/2016 11:59:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblErrorMessage](
	[ErrorMessageID] [bigint] IDENTITY(1,1) NOT NULL,
	[ErrorMessage] [nvarchar](max) NOT NULL,
	[SubKeyID] [bigint] NOT NULL,
	[AccountManagerID] [bigint] NOT NULL,
	[MaxCharacter] [int] NOT NULL,
	[ErrorType] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_tblErrorMessage] PRIMARY KEY CLUSTERED 
(
	[ErrorMessageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblConfig]    Script Date: 05/04/2016 11:59:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblConfig](
	[ConfigID] [bigint] IDENTITY(1,1) NOT NULL,
	[AccountManagerID] [bigint] NULL,
	[SubKeyID] [bigint] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
 CONSTRAINT [PK_tblConfig] PRIMARY KEY CLUSTERED 
(
	[ConfigID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblAccount]    Script Date: 05/04/2016 11:59:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblAccount](
	[AccountID] [bigint] IDENTITY(1,1) NOT NULL,
	[LoginName] [nvarchar](255) NOT NULL,
	[PassWord] [nvarchar](1000) NOT NULL,
	[FullName] [nvarchar](1000) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[EmailAddress] [nvarchar](1000) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[ParentId] [bigint] NULL,
 CONSTRAINT [PK_tblAccount] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[spSubKey_GetFilter]    Script Date: 05/04/2016 11:59:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spSubKey_GetFilter 100,1,-1,9,'',0
CREATE PROCEDURE [dbo].[spSubKey_GetFilter]
	@pagesize int, 
	@pagenum int,
	@accountManagerId bigint,
	@keywordId bigint,
	@subKeyValue nvarchar(50),
	@subKeyStatus tinyint
AS 
BEGIN 
	declare @min as bigint
	declare @max as bigint
	declare @totalRec as bigint
	if(@pageSize=0)-- Get all list, now paging
	begin
		set @min=0
		set @max = 100000
	end
	else
	begin
		set @min=(@pagenum - 1) * @pageSize + 1 
		set @max =@pagenum * @pageSize
	end
	
	select @totalRec= COUNT(*)
	from tblSubKey 
	inner join tblKeyword on tblKeyword.KeywordID = tblSubKey.KeywordID
	inner join tblAccount on tblAccount.AccountID = tblSubKey.AccountManagerID
	where 	--( @accountManagerId = -1 or AccountManagerID= @accountManagerId )  and 
			( @subKeyValue = ''	or SubKeyValue= @subKeyValue ) and 
			tblSubKey.KeywordID = @keywordId and 
			SubKeyStatus <> 3 /* Deleted */
			--( @subKeyStatus = 0 or SubKeyStatus = @subKeyStatus)
		
	select @totalRec AS TotalRec,tblSubKeyTemp.*
	from ( 
	select ROW_NUMBER() OVER(ORDER BY tblSubKey.SubKeyID desc) AS RowNum, tblSubKey.*,tblKeyword.KeywordValue, tblAccount.LoginName
	from tblSubKey 
	inner join tblKeyword on tblKeyword.KeywordID = tblSubKey.KeywordID 
	inner join tblAccount on tblAccount.AccountID = tblSubKey.AccountManagerID
	where
		--( @accountManagerId = -1 or AccountManagerID= @accountManagerId )  and 
		( @subKeyValue = ''	or SubKeyValue= @subKeyValue ) and 
		tblSubKey.KeywordID = @keywordId and 
		SubKeyStatus <> 3 /* Deleted */
		--( @subKeyStatus = 0 or SubKeyStatus = @subKeyStatus)  
	) tblSubKeyTemp 
	where	 RowNum BETWEEN @min and @max
	ORDER BY SubKeyID DESC
 END
GO
/****** Object:  StoredProcedure [dbo].[spSubKey_GetAll]    Script Date: 05/04/2016 11:59:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCode_CountPinCodeIsUsedByAccountID 1
CREATE PROCEDURE [dbo].[spSubKey_GetAll]
AS 
BEGIN 
	select * from tblSubKey where SubKeyStatus <> 3 /* Active */
END

--select * from tblMo
GO
/****** Object:  StoredProcedure [dbo].[spSubKey_CheckExists]    Script Date: 05/04/2016 11:59:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spSubKey_CheckExists] 
@subKey varchar(255),
@loginName nvarchar(255)
AS 
BEGIN 
	Declare @result int = 0
    if((SELECT COUNT(*) FROM tblSubKey WHERE SubKeyValue=@subKey) > 0 or (select count(*) from tblAccount where LoginName = @loginName) > 0)
	begin
		set @result = 1 /* Subkey or loginname is exists */
	end
	select @result as Result
END
GO
/****** Object:  StoredProcedure [dbo].[spSubKey_ChangeStatus]    Script Date: 05/04/2016 11:59:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spSubKey_ChangeStatus] 
@subKeyId bigint,
@subKeyStatus tinyint
AS 
BEGIN 
    update tblSubKey set SubKeyStatus = @subKeyStatus where SubKeyID = @subKeyId
    select @@ROWCOUNT
END
GO
/****** Object:  StoredProcedure [dbo].[spSubKey_AddUpdate]    Script Date: 05/04/2016 11:59:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spConfig_AddUpdate -1,1,-1,'2015-03-22','2015-03-25',null,null
CREATE PROCEDURE [dbo].[spSubKey_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@accountManagerId bigint,
	@subkeyvalue nvarchar(1000),
	@keywordId bigint,
	@subkeystatus tinyint,
	@createdDate datetime,
	@updatedDate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id < 0) 
    begin 
    insert into tblSubKey( 
            AccountManagerID, 
			KeywordID,
            SubKeyValue, 
            SubKeyStatus,
			CreatedDate,
			UpdatedDate
		  ) 
    values(
            @accountManagerId, 
			@keywordId,
            @subkeyvalue, 
            @subkeystatus, 
            getdate(),
			getdate()
          ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblSubKey set 
        AccountManagerID = @accountManagerId,  
        SubKeyValue = @subkeyvalue,
		SubKeyStatus = @subkeystatus,
		UpdatedDate = @updatedDate
        where SubKeyID = @id
		set @id = @@ROWCOUNT 
    end 
    -- Insert statements for procedure here 
    SELECT @id as SubKeyID 
END
GO
/****** Object:  StoredProcedure [dbo].[spProductType_GetProductTypeByAccountID]    Script Date: 05/04/2016 11:59:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spProductType_GetProductTypeByAccountID] 
@managerId bigint
AS 
BEGIN 
    select * from tblProductType
    where ManagerID = @managerId
END
GO
/****** Object:  StoredProcedure [dbo].[spProductType_GetFilter]    Script Date: 05/04/2016 11:59:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spProductType_GetFilter]
	@pagesize int, 
	@pagenum int,
	@managerId bigint,
	@productTypeName nvarchar(1000),
	@productTypeKey nvarchar(1000),
	@productTypeStatus tinyint
AS 
BEGIN 
	declare @min as bigint
	declare @max as bigint
	declare @totalRec as bigint
	if(@pageSize=0)-- Get all list, now paging
	begin
		set @min=0
		set @max = 100000
	end
	else
	begin
		set @min=(@pagenum - 1) * @pageSize + 1 
		set @max =@pagenum * @pageSize
	end
	
	select @totalRec= COUNT(*)
	from tblProductType inner join tblAccount on tblAccount.AccountID = tblProductType.ManagerID
	where 	--( @accountManagerId = -1 or AccountManagerID= @accountManagerId )  and 
			( @productTypeName = ''	or ProductTypeName like N'%' + @productTypeName + '%' ) and 
			(@productTypeKey = '' or ProductTypeKey = @productTypeKey) and 
			(@productTypeStatus = 0 or ProductTypeStatus = @productTypeStatus) and 
			(tblAccount.AccountID = @managerId) and 
			ProductTypeStatus <> 100 /* Deleted */
			--( @subKeyStatus = 0 or SubKeyStatus = @subKeyStatus)
		
	select @totalRec AS TotalRec,tblProductTypeTemp.*
	from ( 
	select ROW_NUMBER() OVER(ORDER BY tblProductType.ProductTypeID desc) AS RowNum, tblProductType.*,tblAccount.LoginName
	from tblProductType inner join tblAccount on tblAccount.AccountID = tblProductType.ManagerID
	where
		--( @accountManagerId = -1 or AccountManagerID= @accountManagerId )  and 
		( @productTypeName = ''	or ProductTypeName like N'%' + @productTypeName + '%' ) and 
		(@productTypeKey = '' or ProductTypeKey = @productTypeKey) and 
		(@productTypeStatus = 0 or ProductTypeStatus = @productTypeStatus) and 
		(tblAccount.AccountID = @managerId) and 
		ProductTypeStatus <> 100 /* Deleted */
	) tblProductTypeTemp
	where	 RowNum BETWEEN @min and @max
	ORDER BY ProductTypeID DESC
 END
GO
/****** Object:  StoredProcedure [dbo].[spProductType_AddUpdate]    Script Date: 05/04/2016 11:59:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spProductType_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@productTypeName nvarchar(max),
	@productTypeKey nvarchar(100),
	@managerID bigint,
	@productTypeStatus tinyint,
	@createdDate datetime,
	@updatedDate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if ((select count(*) from tblProductType where ManagerID = @managerID and ProductTypeKey = @productTypeKey) <= 0) 
    begin 
    insert into tblProductType( 
            ProductTypeName, 
            ProductTypeKey, 
            ManagerID,
			ProductTypeStatus,
            CreatedDate, 
            UpdatedDate) 
    values(
            @productTypeName, 
            @productTypeKey, 
            @managerID,
			@productTypeStatus,
            getdate(), 
            getdate()
                ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblProductType set 
        ProductTypeName = @productTypeName,
		ProductTypeKey = @productTypeKey,
		ProductTypeStatus = @productTypeStatus,
        UpdatedDate = getdate()
        where ManagerID = @managerID and ProductTypeKey = @productTypeKey
    end 
    -- Insert statements for procedure here 
    SELECT @id as ProductTypeID 
END
GO
/****** Object:  StoredProcedure [dbo].[spPinCodeTemp_DeletePinCodeByAccountID]    Script Date: 05/04/2016 11:59:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCodeTemp_CountPinCodeByAccountID 14
CREATE PROCEDURE [dbo].[spPinCodeTemp_DeletePinCodeByAccountID]
	@accountId bigint
AS 
BEGIN 
	delete from tblPinCodeTemp where CreatedByID = @accountId
END

--select * from tblMo
GO
/****** Object:  StoredProcedure [dbo].[spPinCodeTemp_CountPinCodeByAccountID]    Script Date: 05/04/2016 11:59:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPinCodeTemp_CountPinCodeByAccountID]
	@accountId bigint
AS 
BEGIN 
	select count(pc.PinCodeTempID) from tblPinCodeTemp pc
	where pc.CreatedByID = @accountId
END

--select * from tblMo
GO
/****** Object:  StoredProcedure [dbo].[spPinCodeTemp_AddUpdate]    Script Date: 05/04/2016 11:59:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPinCodeTemp_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@pincode nvarchar(50),
	@serial nvarchar(50),
	@createdbyid bigint ,
	@blockid bigint,
	@createddate datetime,
	@updateddate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id <= 0) 
    begin 
    insert into tblPinCodeTemp( 
            PinCode, 
            Serial,
            CreatedByID,
            BlockID,
            CreatedDate
           ) 
    values(
            @pincode, 
            @serial,
            @createdbyid,
            @blockid,
            @createddate
           ) 
        set @id = @@identity 
		--update fortune.tblPinCodeBlock set TotalPinCode = TotalPinCode + 1 where PinCodeBlockID = @pincodeblockid
    end 
    else 
    begin 
        update tblPinCodeTemp set 
        PinCode = @pincode, 
        Serial = @serial
        where PinCodeTempID = @id 
    end 
    -- Insert statements for procedure here 
    SELECT @id as PinCodeTempId 
END
GO
/****** Object:  StoredProcedure [dbo].[spPinCodeBlock_GetList]    Script Date: 05/04/2016 11:59:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCodeBlock_GetList
CREATE PROCEDURE [dbo].[spPinCodeBlock_GetList]
@accountManagerId bigint
AS 
BEGIN 
	select  * from tblPinCodeBlock where PinCodeBlockStatus = 1  /* Active */ and AccountManagerID = @accountManagerId
 END
GO
/****** Object:  StoredProcedure [dbo].[spPincodeBlock_Delete]    Script Date: 05/04/2016 11:59:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCodeBlock_Delete 45
CREATE PROC [dbo].[spPincodeBlock_Delete] 
@pincodeBlockId bigint
AS 
BEGIN 
	Declare @rowEff int = 0
    update tblPinCodeBlock set PinCodeBlockStatus = 100 where PinCodeBlockID = @pincodeBlockId
	set @rowEff = @rowEff + @@ROWCOUNT
	update tblPinCode set PinCodeStatus = 100 where PinCodeBlockID = @pincodeBlockId
	set @rowEff = @rowEff + @@ROWCOUNT
	select @rowEff
END

--select * from tblPinCode where PinCodeBlockID = 45

--update tblPinCode set PinCodeStatus = 1 where PinCodeBlockID = 45
GO
/****** Object:  StoredProcedure [dbo].[spPinCodeBlock_AddUpdate]    Script Date: 05/04/2016 11:59:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCodeBlock_AddUpdate -1,'cc',1,null,null
CREATE PROCEDURE [dbo].[spPinCodeBlock_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@pinCodeBlockName nvarchar(1000),
	@pinCodeBlockStatus tinyint,
	@accountManagerId bigint,
	@createdDate datetime,
	@updatedDate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id < 0) 
    begin 
    insert into tblPinCodeBlock( 
            PinCodeBlockName,
			PinCodeBlockStatus,
			AccountManagerID,
			CreatedDate,
			UpdatedDate
			) 
    values(
            @pinCodeBlockName, 
            @pinCodeBlockStatus, 
			@accountManagerId,
			getdate(),
			getdate()
            ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblPinCodeBlock set 
        PinCodeBlockName = @pinCodeBlockName,  
        PinCodeBlockStatus = @pinCodeBlockStatus
        where PinCodeBlockID = @id
		set @id = @@ROWCOUNT 
    end 
    -- Insert statements for procedure here 
    SELECT @id as PinCodeBlockId 
END
GO
/****** Object:  StoredProcedure [dbo].[spPinCode_UpdatePinCodeStatus]    Script Date: 05/04/2016 11:59:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPinCode_UpdatePinCodeStatus]
	@pinCodeStatus tinyint,
	@pinCodeId bigint
AS 
BEGIN 
	update tblPinCode set PinCodeStatus = @pinCodeStatus,UpdatedDate = getdate() where PinCodeID = @pinCodeId
	select @@ROWCOUNT
END

--select * from tblMt
GO
/****** Object:  StoredProcedure [dbo].[spPinCode_GetPinCode]    Script Date: 05/04/2016 11:59:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCode_CountPinCodeIsUsedByAccountID 1
CREATE PROCEDURE [dbo].[spPinCode_GetPinCode]
@pinCode nvarchar(50),
@accountManagerId bigint
AS 
BEGIN 
	select top 1 * from tblPinCode where PinCode = @pinCode and AccountManagerID = @accountManagerId and PinCodeStatus <> 3 /* Is Deleted */
END

--select * from tblMo
GO
/****** Object:  StoredProcedure [dbo].[spPinCode_GetFilter]    Script Date: 05/04/2016 11:59:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCode_GetFilter 1000,1,'','',-1,-1,1,'2015-01-01','2015-08-01'
CREATE PROCEDURE [dbo].[spPinCode_GetFilter]
	@pagesize int, 
	@currentPage int,
	@pinCode nvarchar(255),
	@serial nvarchar(255),
	@accountManagerID bigint,
	@pincodeblockid bigint,
	@pinCodeStatus tinyint,
	@fromDate datetime,
	@toDate datetime
AS 
BEGIN 
	declare @min as bigint
	declare @max as bigint
	declare @totalRec as bigint
	if(@pageSize=0)-- Get all list, now paging
	begin
		set @min=0
		set @max = 100000
	end
	else
	begin
		set @min=(@currentPage - 1) * @pageSize + 1 
		set @max = @currentPage * @pageSize
	end
	select @totalRec= COUNT(*)
	from tblPinCode pc 
	left join tblPinCodeBlock pcb on pc.PinCodeBlockID = pcb.PinCodeBlockID
	where 	( @accountManagerId = -1 or pc.AccountManagerID= @accountManagerId )  and 
			( @pinCode = ''	or PinCode= @pinCode ) and 
			( @serial = '' or Serial = @serial) and 
			( @pinCodeStatus = 0 or PinCodeStatus = @pinCodeStatus) and 
			( @pincodeblockid = 0 or pc.PinCodeBlockID = @pincodeblockid) --and 
			--( pc.CreatedDate > @fromDate and pc.CreatedDate < @toDate)
		
	select @totalRec AS TotalRec,tblPinCodeTemp.*
	from ( 
	select ROW_NUMBER() OVER(ORDER BY pc.PinCodeID desc) AS RowNum, pc.*,pcb.PinCodeBlockName
	from tblPinCode pc 
	left join tblPinCodeBlock pcb on pc.PinCodeBlockID = pcb.PinCodeBlockID
	where
		( @accountManagerId = -1 or pc.AccountManagerID= @accountManagerId )  and 
		( @pinCode = ''	or PinCode= @pinCode ) and 
		( @serial = '' or Serial = @serial) and 
		( @pinCodeStatus = 0 or PinCodeStatus = @pinCodeStatus) and 
		( @pincodeblockid = 0 or pc.PinCodeBlockID = @pincodeblockid) --and 
		--( pc.CreatedDate > @fromDate and pc.CreatedDate < @toDate)
	) tblPinCodeTemp 
	where	RowNum BETWEEN @min and @max
	ORDER BY PinCodeID DESC
 END
GO
/****** Object:  StoredProcedure [dbo].[spPinCode_CountPinCodeIsUsedByAccountID]    Script Date: 05/04/2016 11:59:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCode_CountPinCodeIsUsedByAccountID 1
CREATE PROCEDURE [dbo].[spPinCode_CountPinCodeIsUsedByAccountID]
	@accountId bigint
AS 
BEGIN 
	select count(pc.PinCodeID) from tblPinCode pc
	where pc.AccountManagerID = @accountId and pc.PinCodeStatus = 2 /* IsUsed */
END

--select * from tblMo
GO
/****** Object:  StoredProcedure [dbo].[spPinCode_CountPinCodeByAccountID]    Script Date: 05/04/2016 11:59:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPinCode_CountPinCodeByAccountID]
	@accountId bigint
AS 
BEGIN 
	select count(pc.PinCodeID) from tblPinCode pc
	where pc.AccountManagerID = @accountId
END

--select * from tblMo
GO
/****** Object:  StoredProcedure [dbo].[spPinCode_CheckPinCodeExists]    Script Date: 05/04/2016 11:59:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spPinCode_CheckPinCodeExists] 
@pinCode varchar(255),
@accountManagerId bigint
AS 
BEGIN 
    SELECT COUNT(*) FROM tblPinCode WHERE PinCode=@pinCode and AccountManagerID = @accountManagerId
END
GO
/****** Object:  StoredProcedure [dbo].[spPinCode_Checking]    Script Date: 05/04/2016 11:59:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spPinCode_Checking] 
@pinCode varchar(255),
@serial varchar(255)
AS 
BEGIN 
    SELECT * from tblPinCode pc inner join tblAccount acc on acc.AccountID = pc.AccountManagerID 
	where pc.PinCode = @pinCode and 
	pc.PinCodeStatus <> 3 /* Deleted */
END
GO
/****** Object:  StoredProcedure [dbo].[spPinCode_AddUpdate]    Script Date: 05/04/2016 11:59:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPinCode_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@pincode nvarchar(50),
	@serial nvarchar(50),
	@serial2 nvarchar(50),
	@accountmanagerid bigint ,
	@productTypeId bigint,
	@pincodeblockid bigint,
	@pincodestatus tinyint,
	@createddate datetime,
	@updateddate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id <= 0) 
    begin 
    insert into tblPinCode( 
            PinCode, 
            Serial,
			Serial2, 
            AccountManagerID,
			ProductTypeID,
            PinCodeStatus,
			PinCodeBlockID,
            CreatedDate, 
            UpdatedDate) 
    values(
            @pincode, 
            @serial,
			@serial2, 
            @accountmanagerid,
			@productTypeId,
            @pincodestatus,
			@pincodeblockid,
            @createddate, 
            @updateddate
                ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblPinCode set 
        PinCode = @pincode, 
        Serial = @serial,
		Serial2 = @serial2, 
        AccountManagerID = @accountmanagerid,
        PinCodeStatus=@pincodestatus,
        UpdatedDate = @updateddate
        where PinCodeID = @id 
    end 
    -- Insert statements for procedure here 
    SELECT @id as PinCodeId 
END
GO
/****** Object:  StoredProcedure [dbo].[spMtConfig_UpdateDefaultWrongSyntaxMessage]    Script Date: 05/04/2016 11:59:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spMtConfig_UpdateDefaultWrongSyntaxMessage] 
@message nvarchar(1000)
AS 
BEGIN 
    update tblMtConfig set MtMessage = @message 
    where AccountManagerID = -1 and MtType = 100
	select @@ROWCOUNT
END
GO
/****** Object:  StoredProcedure [dbo].[spMtConfig_GetDefaultWrongSyntaxMessage]    Script Date: 05/04/2016 11:59:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spMtConfig_GetDefaultWrongSyntaxMessage] 
AS 
BEGIN 
    select * from tblMtConfig
    where AccountManagerID = -1 and MtType = 100
END
GO
/****** Object:  StoredProcedure [dbo].[spMtConfig_GetConfigByAccountID]    Script Date: 05/04/2016 11:59:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spMtConfig_GetConfigByAccountID] 
@accountId bigint
AS 
BEGIN 
    select * from tblMtConfig
    where AccountManagerID = @accountId
END
GO
/****** Object:  StoredProcedure [dbo].[spMtConfig_AddUpdate]    Script Date: 05/04/2016 11:59:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spMtConfig_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@mtMessage nvarchar(max),
	@mtType tinyint,
	@accountmanagerid bigint,
	@createdDate datetime,
	@updatedDate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if ((select count(*) from tblMtConfig where AccountManagerID = @accountmanagerid and MtType = @mtType) <= 0) 
    begin 
    insert into tblMtConfig( 
            MtMessage, 
            MtType, 
            AccountManagerID,
            CreatedDate, 
            UpdatedDate) 
    values(
            @mtMessage, 
            @mtType, 
            @accountmanagerid,
            getdate(), 
            getdate()
                ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblMtConfig set 
        MtMessage = @mtMessage,  
        UpdatedDate = getdate()
        where AccountManagerID = @accountmanagerid and MtType = @mtType
    end 
    -- Insert statements for procedure here 
    SELECT @id as MtConfigId 
END
GO
/****** Object:  StoredProcedure [dbo].[spMt_GetTotalPage]    Script Date: 05/04/2016 11:59:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spMt_GetTotalPage]
	@pagesize int,
	@moid bigint,
	@phonenumber nvarchar(50),
	@message nvarchar(500)
AS 
BEGIN 
	declare    @pageTemporary int
	set        @pageTemporary = 0 
	select     @pageTemporary =	count( *) 
	from	tblMt
	where 
		( @moid = 0	or MoId = @moid )  and 
		( @phonenumber = ''	or PhoneNumber = @phonenumber )  and 
		( @message = ''	or MtMessage = @message )   
	if(@pageTemporary%@pagesize=0) 
		begin
			set @pageTemporary = @pageTemporary/@pagesize 
		end
	else
		begin
			set @pageTemporary = @pageTemporary/@pagesize +1 
		end
	select @pageTemporary as TotalPage 
END

--select * from tblMt
GO
/****** Object:  StoredProcedure [dbo].[spMt_GetList]    Script Date: 05/04/2016 11:59:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spMt_GetList]
	@moid bigint,
	@phonenumber nvarchar(50),
	@message nvarchar(500)
AS 
BEGIN 
	select  *
	from tblMt
	where		( @moid = 0	or MoId = @moid )  and 
		( @phonenumber = ''	or PhoneNumber = @phonenumber )  and 
		( @message = ''	or MtMessage = @message )   
	order by MtId desc
 END
GO
/****** Object:  StoredProcedure [dbo].[spMt_GetFilter]    Script Date: 05/04/2016 11:59:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spMt_GetFilter]
	@pagesize int, 
	@pagenum int,
	@moid bigint,
	@phonenumber nvarchar(50),
	@message nvarchar(500),
	@accountManagerId bigint,
	@fromDate datetime,
	@toDate datetime
AS 
BEGIN 
	declare @min as bigint
	declare @max as bigint
	declare @totalRec as bigint
	if(@pageSize=0)-- Get all list, now paging
	begin
		set @min=0
		set @max = 100000
	end
	else
	begin
		set @min=(@pagenum - 1) * @pageSize + 1 
		set @max =@pagenum * @pageSize
	end
	
	select @totalRec= COUNT(*)
	from tblMt mt 
	inner join tblMo mo on mt.MoID = mo.MoID 
	inner join tblSubKey s on s.SubKeyID = mo.SubKeyID
	where 	( @moid = 0	or mt.MoId= @moid )  and 
			( @phonenumber = ''	or mt.PhoneNumber= @phonenumber )  and 
			( @message = ''	or MtMessage= @message ) and 
			( mt.CreatedDate BETWEEN  @fromdate and @todate) and
			s.AccountManagerID = @accountManagerId
		
	select @totalRec AS TotalRec,tblMtTemp.*
	from ( 
	select ROW_NUMBER() OVER(ORDER BY mt.MtId desc) AS RowNum, mt.*
	from tblMt mt 
	inner join tblMo mo on mt.MoID = mo.MoID 
	inner join tblSubKey s on s.SubKeyID = mo.SubKeyID
	where
		( @moid = 0	or mt.MoId= @moid )  and 
		( @phonenumber = ''	or mt.PhoneNumber= @phonenumber )  and 
		( @message = ''	or MtMessage= @message ) and 
		( mt.CreatedDate BETWEEN  @fromdate and @todate) and
		s.AccountManagerID = @accountManagerId
	) tblMtTemp 
	where	 RowNum BETWEEN @min and @max
	ORDER BY MtId DESC
 END
GO
/****** Object:  StoredProcedure [dbo].[spMt_AddUpdate]    Script Date: 05/04/2016 11:59:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spConfig_AddUpdate -1,1,-1,'2015-03-22','2015-03-25',null,null
--select * from tblMt
CREATE PROCEDURE [dbo].[spMt_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@mtMessage nvarchar(1000),
	@phoneNumber nvarchar(1000),
	@shortCreatedDate int,
	@moId bigint,
	@createdDate datetime,
	@updatedDate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id < 0) 
    begin 
    insert into tblMt( 
           MtMessage,
		   PhoneNumber,
		   ShortCreatedDate,
		   MoID,
		   MtStatus,
		   CreatedDate,
		   UpdatedDate
			) 
    values(
            @mtMessage, 
            @phoneNumber, 
			@shortCreatedDate,
			@moId,
			1,
			getdate(),
			getdate()
           ) 
        set @id = @@identity 
    end 
    -- Insert statements for procedure here 
    SELECT @id as MtId 
END
GO
/****** Object:  StoredProcedure [dbo].[spMo_ViewAllMoErrorSyntax]    Script Date: 05/04/2016 11:59:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spMo_ViewAllMoErrorSyntax]
	@pagesize int, 
	@pagenum int,
	@pincode nvarchar(250),
	@phonenumber nvarchar(50),
	@message nvarchar(500),
	@mostatus tinyint,
	@fromdate datetime,
	@todate datetime,
	@moId bigint,
	@accountManagerId bigint
AS 
BEGIN 
	declare @min as bigint
	declare @max as bigint
	declare @totalRec as bigint
	if(@pageSize=0)-- Get all list, now paging
	begin
		set @min=0
		set @max = 100000
	end
	else
	begin
		set @min=(@pagenum - 1) * @pageSize + 1 
		set @max =@pagenum * @pageSize
	end
	
	select @totalRec= COUNT(*)
	from tblMo mo 
	left join tblPinCode on mo.PinCodeID = tblPinCode.PinCodeID 
	--inner join tblSubKey sub on sub.SubKeyID = mo.SubKeyID
	where 	( @pincode = ''	or tblPinCode.PinCode = @pincode )  and 
			( @phonenumber = ''	or mo.PhoneNumber = @phonenumber )  and 
			( @message = ''		or mo.MoMessage like + '%' + @message + '%' )  and 
			(( @fromdate = 0 and @todate = 0)or(mo.CreatedDate BETWEEN  @fromdate and @todate )) and
			(@moId = -1 or mo.MoID = @moId) and mo.SubKeyID = 0
			--sub.AccountManagerID = @accountManagerId

	select @totalRec AS TotalRec,tblMoTemp.*
	from ( 
	select ROW_NUMBER() OVER(ORDER BY mo.MoId desc) AS RowNum, mo.*, isnull(PinCode, '')	as PinCode
	from tblMo mo
	left join tblPinCode on mo.PinCodeID = tblPinCode.PinCodeID 
	--inner join tblSubKey sub on sub.SubKeyID = mo.SubKeyID
	where
		( @pincode = ''	or tblPinCode.PinCode = @pincode )  and 
		( @phonenumber = ''	or mo.PhoneNumber = @phonenumber )  and 
		( @message = ''		or mo.MoMessage like + '%' + @message + '%' )  and 
		(( @fromdate = 0 and @todate = 0)or(mo.CreatedDate BETWEEN  @fromdate and @todate ) ) and 
		(@moId = -1 or mo.MoID = @moId) and mo.SubKeyID = 0
		--sub.AccountManagerID = @accountManagerId
	) tblMoTemp 
	where	 RowNum BETWEEN @min and @max
	ORDER BY MoId DESC
 END
GO
/****** Object:  StoredProcedure [dbo].[spMo_GetFilter]    Script Date: 05/04/2016 11:59:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spMo_GetFilter 100,1,'','','',0,0,0,-1,-1
CREATE PROCEDURE [dbo].[spMo_GetFilter]
	@pagesize int, 
	@pagenum int,
	@pincode nvarchar(250),
	@phonenumber nvarchar(50),
	@message nvarchar(500),
	@mostatus tinyint,
	@fromdate datetime,
	@todate datetime,
	@moId bigint,
	@accountManagerId bigint
AS 
BEGIN 
	declare @min as bigint
	declare @max as bigint
	declare @totalRec as bigint
	if(@pageSize=0)-- Get all list, now paging
	begin
		set @min=0
		set @max = 100000
	end
	else
	begin
		set @min=(@pagenum - 1) * @pageSize + 1 
		set @max =@pagenum * @pageSize
	end
	
	select @totalRec= COUNT(*)
	from tblMo mo 
	left join tblPinCode on mo.PinCodeID = tblPinCode.PinCodeID 
	inner join tblSubKey s on mo.SubKeyID = s.SubKeyID
	--inner join tblProductType pt on pt.ProductTypeID = mo.SubKeyID
	where 	( @pincode = ''	or tblPinCode.PinCode = @pincode )  and 
			( @phonenumber = ''	or mo.PhoneNumber = @phonenumber )  and 
			(@mostatus = 0 or mo.MoStatus = @mostatus) and 
			( @message = ''		or mo.MoMessage like + '%' + @message + '%' )  and 
			(( @fromdate = 0 and @todate = 0)or(mo.CreatedDate BETWEEN  @fromdate and @todate )) and
			(@moId = -1 or mo.MoID = @moId) and 
			s.AccountManagerID = @accountManagerId

	select @totalRec AS TotalRec,tblMoTemp.*
	from ( 
	select ROW_NUMBER() OVER(ORDER BY mo.MoId desc) AS RowNum, mo.*,s.SubKeyValue as SubKeyValue, isnull(PinCode, '')	as PinCode
	from tblMo mo
	left join tblPinCode on mo.PinCodeID = tblPinCode.PinCodeID 
	inner join tblSubKey s on mo.SubKeyID = s.SubKeyID
	where
		( @pincode = ''	or tblPinCode.PinCode = @pincode )  and 
		( @phonenumber = ''	or mo.PhoneNumber = @phonenumber )  and 
		( @message = ''		or mo.MoMessage like + '%' + @message + '%' )  and 
		(@mostatus = 0 or mo.MoStatus = @mostatus) and 
		(( @fromdate = 0 and @todate = 0)or(mo.CreatedDate BETWEEN  @fromdate and @todate ) ) and 
		(@moId = -1 or mo.MoID = @moId) and 
		s.AccountManagerID = @accountManagerId
	) tblMoTemp 
	where	 RowNum BETWEEN @min and @max
	ORDER BY MoId DESC
 END
GO
/****** Object:  StoredProcedure [dbo].[spMo_CountMoByAccountID]    Script Date: 05/04/2016 11:59:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spMo_CountMoByAccountID]
	@accountId bigint
AS 
BEGIN 
	select count(mo.MoID) from tblMo mo 
	inner join tblSubKey sub on sub.SubKeyID = mo.SubKeyID
	where sub.AccountManagerID = @accountId
END

--select * from tblMo
GO
/****** Object:  StoredProcedure [dbo].[spMo_AddUpdate]    Script Date: 05/04/2016 12:00:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spConfig_AddUpdate -1,1,-1,'2015-03-22','2015-03-25',null,null
--select * from tblMo
CREATE PROCEDURE [dbo].[spMo_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@pincodeid bigint,
	@moMessage nvarchar(1000),
	@phoneNumber nvarchar(1000),
	@shortCreatedDate int,
	@mainKey nvarchar(1000),
	@subKeyID bigint,
	@moStatus tinyint,
	@guid nvarchar(1000),
	@createdDate datetime,
	@updatedDate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id < 0) 
    begin 
    insert into tblMo( 
            PinCodeID, 
            MoMessage, 
            PhoneNumber,
            ServiceType,
			ShortCreatedDate,
			CreatedDate,
			UpdatedDate,
			MainKey,
			SubKeyID,
			MoStatus,
			[Guid]
			) 
    values(
            @pincodeid, 
            @moMessage, 
            @phoneNumber, 
            -1,
			@shortCreatedDate,
			getdate(),
			getdate(),
			@mainKey,
			@subKeyID,
			@moStatus,
			@guid
           ) 
        set @id = @@identity 
    end 
    -- Insert statements for procedure here 
    SELECT @id as MoId 
END
GO
/****** Object:  StoredProcedure [dbo].[spKeyword_GetFilter]    Script Date: 05/04/2016 12:00:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spKeyword_GetFilter]
	@pagesize int, 
	@pagenum int,
	@accountManagerId bigint,
	@keyword nvarchar(50),
	@keywordStatus tinyint
AS 
BEGIN 
	declare @min as bigint
	declare @max as bigint
	declare @totalRec as bigint
	if(@pageSize=0)-- Get all list, now paging
	begin
		set @min=0
		set @max = 100000
	end
	else
	begin
		set @min=(@pagenum - 1) * @pageSize + 1 
		set @max =@pagenum * @pageSize
	end
	
	select @totalRec= COUNT(*)
	from tblKeyword --inner join tblAccount on tblAccount.AccountID = tblKeyword.AccountManagerID
	where 	--( @accountManagerId = -1 or AccountManagerID= @accountManagerId )  and 
			( @keyword = ''	or KeywordValue= @keyword ) and 
			KeywordStatus <> 3 /* Deleted */
			--( @subKeyStatus = 0 or SubKeyStatus = @subKeyStatus)
		
	select @totalRec AS TotalRec,tblKeywordTemp.*
	from ( 
	select ROW_NUMBER() OVER(ORDER BY tblKeyword.KeywordID desc) AS RowNum, tblKeyword.*--,tblAccount.LoginName
	from tblKeyword --inner join tblAccount on tblAccount.AccountID = tblKeyword.AccountManagerID
	where
		--( @accountManagerId = -1 or AccountManagerID= @accountManagerId )  and 
		( @keyword = ''	or KeywordValue= @keyword ) and 
		KeywordStatus <> 3 /* Deleted */
		--( @subKeyStatus = 0 or SubKeyStatus = @subKeyStatus)  
	) tblKeywordTemp 
	where	 RowNum BETWEEN @min and @max
	ORDER BY KeywordID DESC
 END
GO
/****** Object:  StoredProcedure [dbo].[spKeyword_GetAll]    Script Date: 05/04/2016 12:00:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCode_CountPinCodeIsUsedByAccountID 1
CREATE PROCEDURE [dbo].[spKeyword_GetAll]
AS 
BEGIN 
	select * from tblKeyword where KeywordStatus <> 3 /* Active */
END

--select * from tblMo
GO
/****** Object:  StoredProcedure [dbo].[spKeyword_CheckExists]    Script Date: 05/04/2016 12:00:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spKeyword_CheckExists] 
@keyword varchar(255),
@loginName nvarchar(255)
AS 
BEGIN 
	Declare @result int = 0
    if((SELECT COUNT(*) FROM tblKeyword WHERE KeywordValue=@keyword) > 0 or (select count(*) from tblAccount where LoginName = @loginName) > 0)
	begin
		set @result = 1 /* Subkey or loginname is exists */
	end
	select @result as Result
END
GO
/****** Object:  StoredProcedure [dbo].[spKeyword_ChangeStatus]    Script Date: 05/04/2016 12:00:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spKeyword_ChangeStatus] 
@keywordId bigint,
@keywordStatus tinyint
AS 
BEGIN 
    update tblKeyword set KeywordStatus = @keywordStatus where KeywordID = @keywordId
    select @@ROWCOUNT
END
GO
/****** Object:  StoredProcedure [dbo].[spKeyword_AddUpdate]    Script Date: 05/04/2016 12:00:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from tblKeyword
--spConfig_AddUpdate -1,1,-1,'2015-03-22','2015-03-25',null,null
CREATE PROCEDURE [dbo].[spKeyword_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@accountManagerId bigint,
	@keywordValue nvarchar(1000),
	@keywordstatus tinyint,
	@totalProductType int,
	@createdDate datetime,
	@updatedDate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id < 0) 
    begin 
    insert into tblKeyword( 
            AccountManagerID, 
            KeywordValue, 
            KeywordStatus,
			TotalProductType,
			CreatedDate,
			UpdatedDate
		  ) 
    values(
            @accountManagerId, 
            @keywordValue, 
            @keywordstatus,
			@totalProductType, 
            getdate(),
			getdate()
          ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblKeyword set 
        AccountManagerID = @accountManagerId,  
        KeywordValue = @keywordValue,
		KeywordStatus = @keywordstatus,
		UpdatedDate = @updatedDate
        where KeywordID = @id
		set @id = @@ROWCOUNT 
    end 
    -- Insert statements for procedure here 
    SELECT @id as KeywordID 
END
GO
/****** Object:  StoredProcedure [dbo].[spConfig_GetConfigByAccountID]    Script Date: 05/04/2016 12:00:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spConfig_GetConfigByAccountID] 
@accountId bigint
AS 
BEGIN 
    select top 1 * from tblConfig
    where AccountManagerID = @accountId
END
GO
/****** Object:  StoredProcedure [dbo].[spConfig_AddUpdate]    Script Date: 05/04/2016 12:00:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spConfig_AddUpdate -1,1,-1,'2015-03-22','2015-03-25',null,null
CREATE PROCEDURE [dbo].[spConfig_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@accountManagerId bigint,
	@subkeyId bigint,
	@startDate datetime,
	@enddate datetime,
	@createdDate datetime,
	@updatedDate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if ((select count(*) from tblConfig where AccountManagerID = @accountmanagerid) <= 0) 
    begin 
    insert into tblConfig( 
            AccountManagerID, 
            SubKeyID, 
            StartDate,
            EndDate
			) 
    values(
            @accountManagerId, 
            @subkeyId, 
            @startDate, 
            @enddate
                ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblConfig set 
        StartDate = @startDate,  
        EndDate = @enddate
        where AccountManagerID = @accountmanagerid
		set @id = @@ROWCOUNT 
    end 
    -- Insert statements for procedure here 
    SELECT @id as ConfigId 
END
GO
/****** Object:  StoredProcedure [dbo].[spAccount_Login]    Script Date: 05/04/2016 12:00:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spAccount_Login] 
@userName nvarchar(255),
@passWord nvarchar(255) 
AS 
BEGIN 
    select * from tblAccount
    where LoginName = @userName and [PassWord] = @passWord
END
GO
/****** Object:  StoredProcedure [dbo].[spAccount_CheckAutoLogin]    Script Date: 05/04/2016 12:00:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spAccount_CheckAutoLogin] 
@accountId bigint,
@parentId bigint 
AS 
BEGIN 
    select * from tblAccount
    where AccountID = @accountId and ParentId = @parentId
END
GO
/****** Object:  StoredProcedure [dbo].[spAccount_ChangePassword]    Script Date: 05/04/2016 12:00:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spAccount_ChangePassword] 
@loginName nvarchar(255),
@newPassword nvarchar(255)
AS 
BEGIN 
    update tblAccount set [Password] = @newPassword where LoginName = @loginName
    select @@ROWCOUNT
END
GO
/****** Object:  StoredProcedure [dbo].[spAccount_AddUpdate]    Script Date: 05/04/2016 12:00:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spConfig_AddUpdate -1,1,-1,'2015-03-22','2015-03-25',null,null
CREATE PROCEDURE [dbo].[spAccount_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@loginName nvarchar(1000),
	@password nvarchar(1000),
	@fullname nvarchar(1000),
	@phoneNumber nvarchar(1000),
	@emailAddress nvarchar(1000),
	@parentId bigint,
	@createdDate datetime,
	@updatedDate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id < 0) 
    begin 
    insert into tblAccount( 
            LoginName, 
            [PassWord], 
            FullName,
            PhoneNumber,
			EmailAddress,
			ParentId,
			CreatedDate,
			UpdatedDate
			) 
    values(
            @loginName, 
            --'e10adc3949ba59abbe56e057f20f883e',
			@password, 
            @fullname, 
            @phoneNumber,
			@emailAddress,
			@parentId,
			getdate(),
			getdate()
            ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblAccount set 
        PhoneNumber = @phoneNumber,  
        EmailAddress = @emailAddress,
		FullName = @fullname,
		ParentId = @parentId
        where AccountID = @id
		set @id = @@ROWCOUNT 
    end 
    -- Insert statements for procedure here 
    SELECT @id as AccountId 
END
GO
