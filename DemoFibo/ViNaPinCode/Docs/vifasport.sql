USE [HOB1882_VifaSportDB]
GO
/****** Object:  Table [dbo].[tblSuccessMessage]    Script Date: 05/11/2016 10:56:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSuccessMessage](
	[SuccessMessageID] [bigint] IDENTITY(1,1) NOT NULL,
	[SuccessMessage] [nvarchar](max) NOT NULL,
	[SubKeyID] [bigint] NOT NULL,
	[AccountManagerID] [bigint] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[MaxCharacter] [int] NOT NULL,
 CONSTRAINT [PK_tblSuccessMessage] PRIMARY KEY CLUSTERED 
(
	[SuccessMessageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSubKey]    Script Date: 05/11/2016 10:56:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSubKey](
	[SubKeyID] [bigint] IDENTITY(1,1) NOT NULL,
	[AccountManagerID] [bigint] NOT NULL,
	[KeywordID] [bigint] NULL,
	[SubKeyValue] [nvarchar](50) NOT NULL,
	[SubKeyStatus] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_tblSubKey] PRIMARY KEY CLUSTERED 
(
	[SubKeyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblSubKey] ON
INSERT [dbo].[tblSubKey] ([SubKeyID], [AccountManagerID], [KeywordID], [SubKeyValue], [SubKeyStatus], [CreatedDate], [UpdatedDate]) VALUES (1, 2, 1, N'VIFA', 1, CAST(0x0000A5FC01108DE8 AS DateTime), CAST(0x0000A5FC01108DE8 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblSubKey] OFF
/****** Object:  Table [dbo].[tblProductType]    Script Date: 05/11/2016 10:56:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblProductType](
	[ProductTypeID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductTypeName] [nvarchar](max) NOT NULL,
	[ProductTypeKey] [nvarchar](50) NOT NULL,
	[ProductTypeStatus] [nchar](10) NOT NULL,
	[ManagerID] [bigint] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_tblProductType] PRIMARY KEY CLUSTERED 
(
	[ProductTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPinCodeTemp]    Script Date: 05/11/2016 10:56:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPinCodeTemp](
	[PinCodeTempID] [bigint] IDENTITY(1,1) NOT NULL,
	[PinCode] [nvarchar](1000) NOT NULL,
	[Serial] [nvarchar](1000) NOT NULL,
	[BlockID] [bigint] NOT NULL,
	[CreatedByID] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_tblPinCodeTemp] PRIMARY KEY CLUSTERED 
(
	[PinCodeTempID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPinCodeBlock]    Script Date: 05/11/2016 10:56:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPinCodeBlock](
	[PinCodeBlockID] [bigint] IDENTITY(1,1) NOT NULL,
	[PinCodeBlockName] [nvarchar](1000) NULL,
	[PinCodeBlockStatus] [tinyint] NULL,
	[AccountManagerID] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_tblPinCodeBlock] PRIMARY KEY CLUSTERED 
(
	[PinCodeBlockID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblPinCodeBlock] ON
INSERT [dbo].[tblPinCodeBlock] ([PinCodeBlockID], [PinCodeBlockName], [PinCodeBlockStatus], [AccountManagerID], [CreatedDate], [UpdatedDate]) VALUES (1, N'Block ngày 04 tháng 05 năm 2016', 100, 2, CAST(0x0000A5FC0115DCD0 AS DateTime), CAST(0x0000A5FC0115DCD0 AS DateTime))
INSERT [dbo].[tblPinCodeBlock] ([PinCodeBlockID], [PinCodeBlockName], [PinCodeBlockStatus], [AccountManagerID], [CreatedDate], [UpdatedDate]) VALUES (2, N'Block 2300 đầu tiên', 1, 2, CAST(0x0000A5FC01280BDF AS DateTime), CAST(0x0000A5FC01280BDF AS DateTime))
INSERT [dbo].[tblPinCodeBlock] ([PinCodeBlockID], [PinCodeBlockName], [PinCodeBlockStatus], [AccountManagerID], [CreatedDate], [UpdatedDate]) VALUES (3, N'Block tiếp theo', 1, 2, CAST(0x0000A5FC012A312D AS DateTime), CAST(0x0000A5FC012A312D AS DateTime))
SET IDENTITY_INSERT [dbo].[tblPinCodeBlock] OFF
/****** Object:  Table [dbo].[tblPinCode]    Script Date: 05/11/2016 10:56:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPinCode](
	[PinCodeID] [bigint] IDENTITY(1,1) NOT NULL,
	[PinCode] [nvarchar](50) NOT NULL,
	[Serial] [nvarchar](50) NOT NULL,
	[Serial2] [nvarchar](50) NULL,
	[AccountManagerID] [bigint] NOT NULL,
	[ProductTypeID] [bigint] NULL,
	[PinCodeStatus] [tinyint] NULL,
	[PinCodeBlockID] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_tblCard] PRIMARY KEY CLUSTERED 
(
	[PinCodeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblPinCode] ON
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1, N'104981', N'', NULL, 2, 0, 2, 2, CAST(0x0000A5FC0129B961 AS DateTime), CAST(0x0000A5FE012D685D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2, N'767870', N'', NULL, 2, 0, 2, 2, CAST(0x0000A5FC0129B965 AS DateTime), CAST(0x0000A5FE0130C359 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (3, N'576904', N'', NULL, 2, 0, 2, 2, CAST(0x0000A5FC0129B968 AS DateTime), CAST(0x0000A5FE013291D2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (4, N'562442', N'', NULL, 2, 0, 2, 2, CAST(0x0000A5FC0129B96B AS DateTime), CAST(0x0000A5FE01335792 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (5, N'795730', N'', NULL, 2, 0, 2, 2, CAST(0x0000A5FC0129B96E AS DateTime), CAST(0x0000A5FE013BD17C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (6, N'323486', N'', NULL, 2, 0, 2, 2, CAST(0x0000A5FC0129B970 AS DateTime), CAST(0x0000A5FE013CD844 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (7, N'970137', N'', NULL, 2, 0, 2, 2, CAST(0x0000A5FC0129B974 AS DateTime), CAST(0x0000A5FE013DDC7C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (8, N'483637', N'', NULL, 2, 0, 2, 2, CAST(0x0000A5FC0129B976 AS DateTime), CAST(0x0000A60100BB8FB7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (9, N'124972', N'', NULL, 2, 0, 2, 2, CAST(0x0000A5FC0129B979 AS DateTime), CAST(0x0000A601010EB7CE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (10, N'354547', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B97C AS DateTime), CAST(0x0000A5FC0129B97C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (11, N'763547', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B97F AS DateTime), CAST(0x0000A5FC0129B97F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (12, N'582154', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B981 AS DateTime), CAST(0x0000A5FC0129B981 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (13, N'418935', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B984 AS DateTime), CAST(0x0000A5FC0129B984 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (14, N'916097', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B987 AS DateTime), CAST(0x0000A5FC0129B987 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (15, N'846086', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B989 AS DateTime), CAST(0x0000A5FC0129B989 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (16, N'100495', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B98C AS DateTime), CAST(0x0000A5FC0129B98C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (17, N'749270', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B98F AS DateTime), CAST(0x0000A5FC0129B98F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (18, N'447812', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B993 AS DateTime), CAST(0x0000A5FC0129B993 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (19, N'605428', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B996 AS DateTime), CAST(0x0000A5FC0129B996 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (20, N'658259', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B999 AS DateTime), CAST(0x0000A5FC0129B999 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (21, N'709462', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B99C AS DateTime), CAST(0x0000A5FC0129B99C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (22, N'216588', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B99E AS DateTime), CAST(0x0000A5FC0129B99E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (23, N'330841', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9A1 AS DateTime), CAST(0x0000A5FC0129B9A1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (24, N'505059', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9A4 AS DateTime), CAST(0x0000A5FC0129B9A4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (25, N'356253', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9A6 AS DateTime), CAST(0x0000A5FC0129B9A6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (26, N'302071', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9A9 AS DateTime), CAST(0x0000A5FC0129B9A9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (27, N'765426', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9AC AS DateTime), CAST(0x0000A5FC0129B9AC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (28, N'848875', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9AF AS DateTime), CAST(0x0000A5FC0129B9AF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (29, N'645508', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9B2 AS DateTime), CAST(0x0000A5FC0129B9B2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (30, N'710330', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9B5 AS DateTime), CAST(0x0000A5FC0129B9B5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (31, N'461235', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9B8 AS DateTime), CAST(0x0000A5FC0129B9B8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (32, N'123957', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9BA AS DateTime), CAST(0x0000A5FC0129B9BA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (33, N'697504', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9BD AS DateTime), CAST(0x0000A5FC0129B9BD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (34, N'512509', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9C0 AS DateTime), CAST(0x0000A5FC0129B9C0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (35, N'593074', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9C3 AS DateTime), CAST(0x0000A5FC0129B9C3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (36, N'766431', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9C5 AS DateTime), CAST(0x0000A5FC0129B9C5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (37, N'520311', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9C8 AS DateTime), CAST(0x0000A5FC0129B9C8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (38, N'840202', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9CB AS DateTime), CAST(0x0000A5FC0129B9CB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (39, N'687537', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9CF AS DateTime), CAST(0x0000A5FC0129B9CF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (40, N'479624', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9D2 AS DateTime), CAST(0x0000A5FC0129B9D2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (41, N'270415', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9D4 AS DateTime), CAST(0x0000A5FC0129B9D4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (42, N'664027', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9D7 AS DateTime), CAST(0x0000A5FC0129B9D7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (43, N'324078', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9D9 AS DateTime), CAST(0x0000A5FC0129B9D9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (44, N'149803', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9DC AS DateTime), CAST(0x0000A5FC0129B9DC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (45, N'724911', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9DF AS DateTime), CAST(0x0000A5FC0129B9DF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (46, N'523047', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9E1 AS DateTime), CAST(0x0000A5FC0129B9E1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (47, N'644186', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9E4 AS DateTime), CAST(0x0000A5FC0129B9E4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (48, N'761250', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9E8 AS DateTime), CAST(0x0000A5FC0129B9E8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (49, N'183746', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9EB AS DateTime), CAST(0x0000A5FC0129B9EB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (50, N'902838', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9EE AS DateTime), CAST(0x0000A5FC0129B9EE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (51, N'566970', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9F1 AS DateTime), CAST(0x0000A5FC0129B9F1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (52, N'860658', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9F4 AS DateTime), CAST(0x0000A5FC0129B9F4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (53, N'307390', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9F7 AS DateTime), CAST(0x0000A5FC0129B9F7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (54, N'638129', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9F9 AS DateTime), CAST(0x0000A5FC0129B9F9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (55, N'991652', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9FC AS DateTime), CAST(0x0000A5FC0129B9FC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (56, N'130455', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129B9FE AS DateTime), CAST(0x0000A5FC0129B9FE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (57, N'383776', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA01 AS DateTime), CAST(0x0000A5FC0129BA01 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (58, N'648586', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA04 AS DateTime), CAST(0x0000A5FC0129BA04 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (59, N'699775', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA07 AS DateTime), CAST(0x0000A5FC0129BA07 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (60, N'668381', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA0A AS DateTime), CAST(0x0000A5FC0129BA0A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (61, N'692627', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA0D AS DateTime), CAST(0x0000A5FC0129BA0D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (62, N'413701', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA0F AS DateTime), CAST(0x0000A5FC0129BA0F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (63, N'670240', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA12 AS DateTime), CAST(0x0000A5FC0129BA12 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (64, N'142092', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA18 AS DateTime), CAST(0x0000A5FC0129BA18 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (65, N'464286', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA75 AS DateTime), CAST(0x0000A5FC0129BA75 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (66, N'333649', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA78 AS DateTime), CAST(0x0000A5FC0129BA78 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (67, N'966522', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA7A AS DateTime), CAST(0x0000A5FC0129BA7A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (68, N'439394', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA7D AS DateTime), CAST(0x0000A5FC0129BA7D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (69, N'443740', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA80 AS DateTime), CAST(0x0000A5FC0129BA80 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (70, N'873746', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA82 AS DateTime), CAST(0x0000A5FC0129BA82 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (71, N'673639', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA86 AS DateTime), CAST(0x0000A5FC0129BA86 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (72, N'720655', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA89 AS DateTime), CAST(0x0000A5FC0129BA89 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (73, N'580280', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA8C AS DateTime), CAST(0x0000A5FC0129BA8C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (74, N'394944', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA8F AS DateTime), CAST(0x0000A5FC0129BA8F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (75, N'465494', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA92 AS DateTime), CAST(0x0000A5FC0129BA92 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (76, N'593600', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA95 AS DateTime), CAST(0x0000A5FC0129BA95 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (77, N'322748', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA98 AS DateTime), CAST(0x0000A5FC0129BA98 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (78, N'175718', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA9A AS DateTime), CAST(0x0000A5FC0129BA9A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (79, N'516501', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BA9D AS DateTime), CAST(0x0000A5FC0129BA9D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (80, N'201481', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BAA0 AS DateTime), CAST(0x0000A5FC0129BAA0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (81, N'783180', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BAA3 AS DateTime), CAST(0x0000A5FC0129BAA3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (82, N'200094', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BAA6 AS DateTime), CAST(0x0000A5FC0129BAA6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (83, N'776220', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BAA9 AS DateTime), CAST(0x0000A5FC0129BAA9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (84, N'122463', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BAAC AS DateTime), CAST(0x0000A5FC0129BAAC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (85, N'624898', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BAAF AS DateTime), CAST(0x0000A5FC0129BAAF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (86, N'759673', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BAB2 AS DateTime), CAST(0x0000A5FC0129BAB2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (87, N'483111', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB0E AS DateTime), CAST(0x0000A5FC0129BB0E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (88, N'282373', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB11 AS DateTime), CAST(0x0000A5FC0129BB11 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (89, N'430087', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB14 AS DateTime), CAST(0x0000A5FC0129BB14 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (90, N'578936', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB17 AS DateTime), CAST(0x0000A5FC0129BB17 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (91, N'763865', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB1A AS DateTime), CAST(0x0000A5FC0129BB1A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (92, N'244854', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB1D AS DateTime), CAST(0x0000A5FC0129BB1D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (93, N'660522', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB20 AS DateTime), CAST(0x0000A5FC0129BB20 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (94, N'564806', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB23 AS DateTime), CAST(0x0000A5FC0129BB23 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (95, N'679452', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB26 AS DateTime), CAST(0x0000A5FC0129BB26 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (96, N'435119', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB28 AS DateTime), CAST(0x0000A5FC0129BB28 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (97, N'847677', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB2B AS DateTime), CAST(0x0000A5FC0129BB2B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (98, N'205247', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB2E AS DateTime), CAST(0x0000A5FC0129BB2E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (99, N'808221', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB30 AS DateTime), CAST(0x0000A5FC0129BB30 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (100, N'155134', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB33 AS DateTime), CAST(0x0000A5FC0129BB33 AS DateTime))
GO
print 'Processed 100 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (101, N'248011', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB36 AS DateTime), CAST(0x0000A5FC0129BB36 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (102, N'134743', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB93 AS DateTime), CAST(0x0000A5FC0129BB93 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (103, N'217370', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB95 AS DateTime), CAST(0x0000A5FC0129BB95 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (104, N'963034', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB98 AS DateTime), CAST(0x0000A5FC0129BB98 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (105, N'845163', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB9B AS DateTime), CAST(0x0000A5FC0129BB9B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (106, N'295790', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BB9E AS DateTime), CAST(0x0000A5FC0129BB9E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (107, N'552625', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBA1 AS DateTime), CAST(0x0000A5FC0129BBA1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (108, N'619362', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBA4 AS DateTime), CAST(0x0000A5FC0129BBA4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (109, N'538392', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBA7 AS DateTime), CAST(0x0000A5FC0129BBA7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (110, N'189568', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBA9 AS DateTime), CAST(0x0000A5FC0129BBA9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (111, N'242074', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBAC AS DateTime), CAST(0x0000A5FC0129BBAC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (112, N'490295', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBAF AS DateTime), CAST(0x0000A5FC0129BBAF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (113, N'571231', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBB1 AS DateTime), CAST(0x0000A5FC0129BBB1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (114, N'916604', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBB5 AS DateTime), CAST(0x0000A5FC0129BBB5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (115, N'320875', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBB8 AS DateTime), CAST(0x0000A5FC0129BBB8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (116, N'426545', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBBB AS DateTime), CAST(0x0000A5FC0129BBBB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (117, N'603244', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBBE AS DateTime), CAST(0x0000A5FC0129BBBE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (118, N'816241', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBC1 AS DateTime), CAST(0x0000A5FC0129BBC1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (119, N'667850', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBC4 AS DateTime), CAST(0x0000A5FC0129BBC4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (120, N'507397', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBC7 AS DateTime), CAST(0x0000A5FC0129BBC7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (121, N'629762', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBC9 AS DateTime), CAST(0x0000A5FC0129BBC9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (122, N'891861', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBCC AS DateTime), CAST(0x0000A5FC0129BBCC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (123, N'708870', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBCF AS DateTime), CAST(0x0000A5FC0129BBCF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (124, N'772425', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBD2 AS DateTime), CAST(0x0000A5FC0129BBD2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (125, N'761107', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BBD5 AS DateTime), CAST(0x0000A5FC0129BBD5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (126, N'513087', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BC36 AS DateTime), CAST(0x0000A5FC0129BC36 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (127, N'590092', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BC39 AS DateTime), CAST(0x0000A5FC0129BC39 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (128, N'622064', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BC3B AS DateTime), CAST(0x0000A5FC0129BC3B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (129, N'338821', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BC3E AS DateTime), CAST(0x0000A5FC0129BC3E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (130, N'222372', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BC41 AS DateTime), CAST(0x0000A5FC0129BC41 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (131, N'204707', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BC44 AS DateTime), CAST(0x0000A5FC0129BC44 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (132, N'147664', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BC47 AS DateTime), CAST(0x0000A5FC0129BC47 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (133, N'850419', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BC49 AS DateTime), CAST(0x0000A5FC0129BC49 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (134, N'775639', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BC4C AS DateTime), CAST(0x0000A5FC0129BC4C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (135, N'352589', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BCAA AS DateTime), CAST(0x0000A5FC0129BCAA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (136, N'632093', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BCAD AS DateTime), CAST(0x0000A5FC0129BCAD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (137, N'679836', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BCAF AS DateTime), CAST(0x0000A5FC0129BCAF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (138, N'803979', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BCB2 AS DateTime), CAST(0x0000A5FC0129BCB2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (139, N'378839', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BCB4 AS DateTime), CAST(0x0000A5FC0129BCB4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (140, N'341788', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BCB7 AS DateTime), CAST(0x0000A5FC0129BCB7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (141, N'798924', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BCBA AS DateTime), CAST(0x0000A5FC0129BCBA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (142, N'134627', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BCBC AS DateTime), CAST(0x0000A5FC0129BCBC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (143, N'302215', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BCBF AS DateTime), CAST(0x0000A5FC0129BCBF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (144, N'847062', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BCC2 AS DateTime), CAST(0x0000A5FC0129BCC2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (145, N'535150', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD1F AS DateTime), CAST(0x0000A5FC0129BD1F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (146, N'720246', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD24 AS DateTime), CAST(0x0000A5FC0129BD24 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (147, N'198457', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD27 AS DateTime), CAST(0x0000A5FC0129BD27 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (148, N'282607', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD2A AS DateTime), CAST(0x0000A5FC0129BD2A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (149, N'548903', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD2D AS DateTime), CAST(0x0000A5FC0129BD2D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (150, N'430299', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD31 AS DateTime), CAST(0x0000A5FC0129BD31 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (151, N'398151', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD34 AS DateTime), CAST(0x0000A5FC0129BD34 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (152, N'577348', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD37 AS DateTime), CAST(0x0000A5FC0129BD37 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (153, N'227111', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD3A AS DateTime), CAST(0x0000A5FC0129BD3A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (154, N'905857', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD3D AS DateTime), CAST(0x0000A5FC0129BD3D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (155, N'739198', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD40 AS DateTime), CAST(0x0000A5FC0129BD40 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (156, N'270756', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD43 AS DateTime), CAST(0x0000A5FC0129BD43 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (157, N'972533', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD46 AS DateTime), CAST(0x0000A5FC0129BD46 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (158, N'581569', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD4A AS DateTime), CAST(0x0000A5FC0129BD4A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (159, N'765227', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD4D AS DateTime), CAST(0x0000A5FC0129BD4D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (160, N'408674', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD4F AS DateTime), CAST(0x0000A5FC0129BD4F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (161, N'495831', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD52 AS DateTime), CAST(0x0000A5FC0129BD52 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (162, N'964124', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD55 AS DateTime), CAST(0x0000A5FC0129BD55 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (163, N'721641', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD57 AS DateTime), CAST(0x0000A5FC0129BD57 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (164, N'565657', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD5F AS DateTime), CAST(0x0000A5FC0129BD5F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (165, N'986185', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD62 AS DateTime), CAST(0x0000A5FC0129BD62 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (166, N'826643', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD65 AS DateTime), CAST(0x0000A5FC0129BD65 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (167, N'155367', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD68 AS DateTime), CAST(0x0000A5FC0129BD68 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (168, N'996136', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD6A AS DateTime), CAST(0x0000A5FC0129BD6A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (169, N'713612', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD6D AS DateTime), CAST(0x0000A5FC0129BD6D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (170, N'993277', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD70 AS DateTime), CAST(0x0000A5FC0129BD70 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (171, N'369775', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD72 AS DateTime), CAST(0x0000A5FC0129BD72 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (172, N'912042', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD75 AS DateTime), CAST(0x0000A5FC0129BD75 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (173, N'670688', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD78 AS DateTime), CAST(0x0000A5FC0129BD78 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (174, N'885732', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD82 AS DateTime), CAST(0x0000A5FC0129BD82 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (175, N'487306', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD85 AS DateTime), CAST(0x0000A5FC0129BD85 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (176, N'275205', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD88 AS DateTime), CAST(0x0000A5FC0129BD88 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (177, N'395343', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD8B AS DateTime), CAST(0x0000A5FC0129BD8B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (178, N'683435', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD8D AS DateTime), CAST(0x0000A5FC0129BD8D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (179, N'839608', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD8F AS DateTime), CAST(0x0000A5FC0129BD8F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (180, N'442641', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD92 AS DateTime), CAST(0x0000A5FC0129BD92 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (181, N'416428', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD95 AS DateTime), CAST(0x0000A5FC0129BD95 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (182, N'841340', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD97 AS DateTime), CAST(0x0000A5FC0129BD97 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (183, N'291493', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD9A AS DateTime), CAST(0x0000A5FC0129BD9A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (184, N'246177', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD9C AS DateTime), CAST(0x0000A5FC0129BD9C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (185, N'949364', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BD9F AS DateTime), CAST(0x0000A5FC0129BD9F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (186, N'283841', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDA2 AS DateTime), CAST(0x0000A5FC0129BDA2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (187, N'172841', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDA5 AS DateTime), CAST(0x0000A5FC0129BDA5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (188, N'810475', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDA7 AS DateTime), CAST(0x0000A5FC0129BDA7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (189, N'797623', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDAA AS DateTime), CAST(0x0000A5FC0129BDAA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (190, N'992693', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDAC AS DateTime), CAST(0x0000A5FC0129BDAC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (191, N'314951', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDAF AS DateTime), CAST(0x0000A5FC0129BDAF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (192, N'806757', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDB1 AS DateTime), CAST(0x0000A5FC0129BDB1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (193, N'758151', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDB4 AS DateTime), CAST(0x0000A5FC0129BDB4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (194, N'455172', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDB6 AS DateTime), CAST(0x0000A5FC0129BDB6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (195, N'375652', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDB9 AS DateTime), CAST(0x0000A5FC0129BDB9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (196, N'663171', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDBB AS DateTime), CAST(0x0000A5FC0129BDBB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (197, N'204537', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDBE AS DateTime), CAST(0x0000A5FC0129BDBE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (198, N'533472', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDC1 AS DateTime), CAST(0x0000A5FC0129BDC1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (199, N'110550', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDC6 AS DateTime), CAST(0x0000A5FC0129BDC6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (200, N'925784', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDC8 AS DateTime), CAST(0x0000A5FC0129BDC8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (201, N'864927', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDCB AS DateTime), CAST(0x0000A5FC0129BDCB AS DateTime))
GO
print 'Processed 200 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (202, N'780159', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDCD AS DateTime), CAST(0x0000A5FC0129BDCD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (203, N'180331', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDD0 AS DateTime), CAST(0x0000A5FC0129BDD0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (204, N'110611', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDD2 AS DateTime), CAST(0x0000A5FC0129BDD2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (205, N'669232', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDD5 AS DateTime), CAST(0x0000A5FC0129BDD5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (206, N'731038', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDD7 AS DateTime), CAST(0x0000A5FC0129BDD7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (207, N'116502', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDDA AS DateTime), CAST(0x0000A5FC0129BDDA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (208, N'814601', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDDE AS DateTime), CAST(0x0000A5FC0129BDDE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (209, N'700520', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDE0 AS DateTime), CAST(0x0000A5FC0129BDE0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (210, N'639621', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDE4 AS DateTime), CAST(0x0000A5FC0129BDE4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (211, N'383250', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDE6 AS DateTime), CAST(0x0000A5FC0129BDE6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (212, N'100595', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDE9 AS DateTime), CAST(0x0000A5FC0129BDE9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (213, N'397866', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDEB AS DateTime), CAST(0x0000A5FC0129BDEB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (214, N'665085', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDEE AS DateTime), CAST(0x0000A5FC0129BDEE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (215, N'989413', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDF1 AS DateTime), CAST(0x0000A5FC0129BDF1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (216, N'682521', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDF3 AS DateTime), CAST(0x0000A5FC0129BDF3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (217, N'409327', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDF6 AS DateTime), CAST(0x0000A5FC0129BDF6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (218, N'437958', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDF9 AS DateTime), CAST(0x0000A5FC0129BDF9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (219, N'894637', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDFB AS DateTime), CAST(0x0000A5FC0129BDFB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (220, N'529815', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BDFE AS DateTime), CAST(0x0000A5FC0129BDFE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (221, N'862794', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE01 AS DateTime), CAST(0x0000A5FC0129BE01 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (222, N'419365', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE04 AS DateTime), CAST(0x0000A5FC0129BE04 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (223, N'285581', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE07 AS DateTime), CAST(0x0000A5FC0129BE07 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (224, N'388961', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE0A AS DateTime), CAST(0x0000A5FC0129BE0A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (225, N'649450', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE0D AS DateTime), CAST(0x0000A5FC0129BE0D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (226, N'121655', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE11 AS DateTime), CAST(0x0000A5FC0129BE11 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (227, N'222014', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE13 AS DateTime), CAST(0x0000A5FC0129BE13 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (228, N'713505', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE16 AS DateTime), CAST(0x0000A5FC0129BE16 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (229, N'485959', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE19 AS DateTime), CAST(0x0000A5FC0129BE19 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (230, N'512195', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE75 AS DateTime), CAST(0x0000A5FC0129BE75 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (231, N'845956', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE78 AS DateTime), CAST(0x0000A5FC0129BE78 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (232, N'691329', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE7A AS DateTime), CAST(0x0000A5FC0129BE7A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (233, N'886937', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE7E AS DateTime), CAST(0x0000A5FC0129BE7E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (234, N'120680', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE81 AS DateTime), CAST(0x0000A5FC0129BE81 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (235, N'869266', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE84 AS DateTime), CAST(0x0000A5FC0129BE84 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (236, N'334602', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE86 AS DateTime), CAST(0x0000A5FC0129BE86 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (237, N'676952', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE89 AS DateTime), CAST(0x0000A5FC0129BE89 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (238, N'243281', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE8C AS DateTime), CAST(0x0000A5FC0129BE8C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (239, N'612704', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE8F AS DateTime), CAST(0x0000A5FC0129BE8F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (240, N'430372', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE92 AS DateTime), CAST(0x0000A5FC0129BE92 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (241, N'824726', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE95 AS DateTime), CAST(0x0000A5FC0129BE95 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (242, N'452931', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE98 AS DateTime), CAST(0x0000A5FC0129BE98 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (243, N'827987', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BE9B AS DateTime), CAST(0x0000A5FC0129BE9B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (244, N'611347', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BEF8 AS DateTime), CAST(0x0000A5FC0129BEF8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (245, N'778506', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BEFB AS DateTime), CAST(0x0000A5FC0129BEFB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (246, N'858995', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BEFE AS DateTime), CAST(0x0000A5FC0129BEFE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (247, N'445478', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF00 AS DateTime), CAST(0x0000A5FC0129BF00 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (248, N'765987', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF03 AS DateTime), CAST(0x0000A5FC0129BF03 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (249, N'156040', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF06 AS DateTime), CAST(0x0000A5FC0129BF06 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (250, N'155189', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF08 AS DateTime), CAST(0x0000A5FC0129BF08 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (251, N'691376', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF0B AS DateTime), CAST(0x0000A5FC0129BF0B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (252, N'562092', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF0E AS DateTime), CAST(0x0000A5FC0129BF0E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (253, N'887026', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF11 AS DateTime), CAST(0x0000A5FC0129BF11 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (254, N'336896', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF14 AS DateTime), CAST(0x0000A5FC0129BF14 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (255, N'717915', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF17 AS DateTime), CAST(0x0000A5FC0129BF17 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (256, N'854827', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF1A AS DateTime), CAST(0x0000A5FC0129BF1A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (257, N'409192', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF1D AS DateTime), CAST(0x0000A5FC0129BF1D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (258, N'112482', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF20 AS DateTime), CAST(0x0000A5FC0129BF20 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (259, N'890442', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF22 AS DateTime), CAST(0x0000A5FC0129BF22 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (260, N'885537', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF25 AS DateTime), CAST(0x0000A5FC0129BF25 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (261, N'960788', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF28 AS DateTime), CAST(0x0000A5FC0129BF28 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (262, N'653151', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF2A AS DateTime), CAST(0x0000A5FC0129BF2A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (263, N'916257', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF2D AS DateTime), CAST(0x0000A5FC0129BF2D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (264, N'979261', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF32 AS DateTime), CAST(0x0000A5FC0129BF32 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (265, N'106546', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF90 AS DateTime), CAST(0x0000A5FC0129BF90 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (266, N'339080', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF93 AS DateTime), CAST(0x0000A5FC0129BF93 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (267, N'378613', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF97 AS DateTime), CAST(0x0000A5FC0129BF97 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (268, N'755158', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF99 AS DateTime), CAST(0x0000A5FC0129BF99 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (269, N'711407', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF9C AS DateTime), CAST(0x0000A5FC0129BF9C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (270, N'182832', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BF9E AS DateTime), CAST(0x0000A5FC0129BF9E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (271, N'365583', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFA1 AS DateTime), CAST(0x0000A5FC0129BFA1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (272, N'551956', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFA3 AS DateTime), CAST(0x0000A5FC0129BFA3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (273, N'211757', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFA6 AS DateTime), CAST(0x0000A5FC0129BFA6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (274, N'797649', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFA8 AS DateTime), CAST(0x0000A5FC0129BFA8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (275, N'399421', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFAB AS DateTime), CAST(0x0000A5FC0129BFAB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (276, N'304557', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFAE AS DateTime), CAST(0x0000A5FC0129BFAE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (277, N'468728', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFB1 AS DateTime), CAST(0x0000A5FC0129BFB1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (278, N'211723', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFB4 AS DateTime), CAST(0x0000A5FC0129BFB4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (279, N'130806', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFB7 AS DateTime), CAST(0x0000A5FC0129BFB7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (280, N'719919', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFBA AS DateTime), CAST(0x0000A5FC0129BFBA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (281, N'599113', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFBC AS DateTime), CAST(0x0000A5FC0129BFBC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (282, N'239340', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFBF AS DateTime), CAST(0x0000A5FC0129BFBF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (283, N'595527', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFC2 AS DateTime), CAST(0x0000A5FC0129BFC2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (284, N'159774', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFC4 AS DateTime), CAST(0x0000A5FC0129BFC4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (285, N'514126', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFC7 AS DateTime), CAST(0x0000A5FC0129BFC7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (286, N'202329', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFCA AS DateTime), CAST(0x0000A5FC0129BFCA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (287, N'693268', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFCC AS DateTime), CAST(0x0000A5FC0129BFCC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (288, N'396342', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFCF AS DateTime), CAST(0x0000A5FC0129BFCF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (289, N'737833', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFD2 AS DateTime), CAST(0x0000A5FC0129BFD2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (290, N'250369', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFD5 AS DateTime), CAST(0x0000A5FC0129BFD5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (291, N'618336', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFD8 AS DateTime), CAST(0x0000A5FC0129BFD8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (292, N'834407', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFDA AS DateTime), CAST(0x0000A5FC0129BFDA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (293, N'594341', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFDD AS DateTime), CAST(0x0000A5FC0129BFDD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (294, N'363333', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFDF AS DateTime), CAST(0x0000A5FC0129BFDF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (295, N'817321', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFE2 AS DateTime), CAST(0x0000A5FC0129BFE2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (296, N'275533', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFE5 AS DateTime), CAST(0x0000A5FC0129BFE5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (297, N'175967', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFE7 AS DateTime), CAST(0x0000A5FC0129BFE7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (298, N'805594', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFEA AS DateTime), CAST(0x0000A5FC0129BFEA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (299, N'192163', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFED AS DateTime), CAST(0x0000A5FC0129BFED AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (300, N'812155', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129BFF0 AS DateTime), CAST(0x0000A5FC0129BFF0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (301, N'965258', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C04C AS DateTime), CAST(0x0000A5FC0129C04C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (302, N'128670', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C04F AS DateTime), CAST(0x0000A5FC0129C04F AS DateTime))
GO
print 'Processed 300 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (303, N'547209', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C052 AS DateTime), CAST(0x0000A5FC0129C052 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (304, N'729961', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C054 AS DateTime), CAST(0x0000A5FC0129C054 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (305, N'446811', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C057 AS DateTime), CAST(0x0000A5FC0129C057 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (306, N'344589', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C05A AS DateTime), CAST(0x0000A5FC0129C05A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (307, N'745714', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C05C AS DateTime), CAST(0x0000A5FC0129C05C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (308, N'116228', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C05F AS DateTime), CAST(0x0000A5FC0129C05F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (309, N'141723', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C063 AS DateTime), CAST(0x0000A5FC0129C063 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (310, N'448379', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C065 AS DateTime), CAST(0x0000A5FC0129C065 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (311, N'761797', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C06A AS DateTime), CAST(0x0000A5FC0129C06A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (312, N'515839', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C06C AS DateTime), CAST(0x0000A5FC0129C06C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (313, N'879044', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C06F AS DateTime), CAST(0x0000A5FC0129C06F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (314, N'872006', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C071 AS DateTime), CAST(0x0000A5FC0129C071 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (315, N'283373', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C074 AS DateTime), CAST(0x0000A5FC0129C074 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (316, N'558878', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C077 AS DateTime), CAST(0x0000A5FC0129C077 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (317, N'789056', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C07A AS DateTime), CAST(0x0000A5FC0129C07A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (318, N'548704', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C07C AS DateTime), CAST(0x0000A5FC0129C07C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (319, N'712478', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C089 AS DateTime), CAST(0x0000A5FC0129C089 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (320, N'123982', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C08B AS DateTime), CAST(0x0000A5FC0129C08B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (321, N'195596', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C08F AS DateTime), CAST(0x0000A5FC0129C08F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (322, N'647216', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C092 AS DateTime), CAST(0x0000A5FC0129C092 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (323, N'842423', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C094 AS DateTime), CAST(0x0000A5FC0129C094 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (324, N'899153', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C097 AS DateTime), CAST(0x0000A5FC0129C097 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (325, N'593333', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C09A AS DateTime), CAST(0x0000A5FC0129C09A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (326, N'734637', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C09C AS DateTime), CAST(0x0000A5FC0129C09C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (327, N'952225', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C09F AS DateTime), CAST(0x0000A5FC0129C09F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (328, N'464550', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0A2 AS DateTime), CAST(0x0000A5FC0129C0A2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (329, N'204364', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0A5 AS DateTime), CAST(0x0000A5FC0129C0A5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (330, N'654605', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0A8 AS DateTime), CAST(0x0000A5FC0129C0A8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (331, N'737844', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0AB AS DateTime), CAST(0x0000A5FC0129C0AB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (332, N'660241', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0AE AS DateTime), CAST(0x0000A5FC0129C0AE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (333, N'223776', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0B3 AS DateTime), CAST(0x0000A5FC0129C0B3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (334, N'934076', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0BA AS DateTime), CAST(0x0000A5FC0129C0BA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (335, N'671437', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0BE AS DateTime), CAST(0x0000A5FC0129C0BE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (336, N'479701', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0C5 AS DateTime), CAST(0x0000A5FC0129C0C5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (337, N'729040', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0C8 AS DateTime), CAST(0x0000A5FC0129C0C8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (338, N'244532', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0CB AS DateTime), CAST(0x0000A5FC0129C0CB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (339, N'406027', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0CD AS DateTime), CAST(0x0000A5FC0129C0CD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (340, N'680137', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0D0 AS DateTime), CAST(0x0000A5FC0129C0D0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (341, N'825864', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0D3 AS DateTime), CAST(0x0000A5FC0129C0D3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (342, N'499283', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0D7 AS DateTime), CAST(0x0000A5FC0129C0D7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (343, N'217099', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0DA AS DateTime), CAST(0x0000A5FC0129C0DA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (344, N'934283', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0DF AS DateTime), CAST(0x0000A5FC0129C0DF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (345, N'611430', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0EE AS DateTime), CAST(0x0000A5FC0129C0EE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (346, N'332003', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0F1 AS DateTime), CAST(0x0000A5FC0129C0F1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (347, N'235048', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0F8 AS DateTime), CAST(0x0000A5FC0129C0F8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (348, N'137612', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0FB AS DateTime), CAST(0x0000A5FC0129C0FB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (349, N'725738', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C0FE AS DateTime), CAST(0x0000A5FC0129C0FE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (350, N'620797', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C101 AS DateTime), CAST(0x0000A5FC0129C101 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (351, N'211109', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C104 AS DateTime), CAST(0x0000A5FC0129C104 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (352, N'472634', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C106 AS DateTime), CAST(0x0000A5FC0129C106 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (353, N'556467', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C109 AS DateTime), CAST(0x0000A5FC0129C109 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (354, N'549064', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C10B AS DateTime), CAST(0x0000A5FC0129C10B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (355, N'984236', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C10E AS DateTime), CAST(0x0000A5FC0129C10E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (356, N'316461', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C111 AS DateTime), CAST(0x0000A5FC0129C111 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (357, N'502757', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C113 AS DateTime), CAST(0x0000A5FC0129C113 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (358, N'334902', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C116 AS DateTime), CAST(0x0000A5FC0129C116 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (359, N'971348', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C119 AS DateTime), CAST(0x0000A5FC0129C119 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (360, N'303071', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C11C AS DateTime), CAST(0x0000A5FC0129C11C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (361, N'519738', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C11E AS DateTime), CAST(0x0000A5FC0129C11E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (362, N'282719', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C121 AS DateTime), CAST(0x0000A5FC0129C121 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (363, N'728315', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C124 AS DateTime), CAST(0x0000A5FC0129C124 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (364, N'475975', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C127 AS DateTime), CAST(0x0000A5FC0129C127 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (365, N'729420', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C129 AS DateTime), CAST(0x0000A5FC0129C129 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (366, N'664745', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C12C AS DateTime), CAST(0x0000A5FC0129C12C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (367, N'793570', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C12E AS DateTime), CAST(0x0000A5FC0129C12E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (368, N'453328', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C131 AS DateTime), CAST(0x0000A5FC0129C131 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (369, N'817568', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C133 AS DateTime), CAST(0x0000A5FC0129C133 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (370, N'277516', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C136 AS DateTime), CAST(0x0000A5FC0129C136 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (371, N'832111', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C139 AS DateTime), CAST(0x0000A5FC0129C139 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (372, N'239627', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C13B AS DateTime), CAST(0x0000A5FC0129C13B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (373, N'177863', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C13E AS DateTime), CAST(0x0000A5FC0129C13E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (374, N'780040', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C140 AS DateTime), CAST(0x0000A5FC0129C140 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (375, N'123977', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C143 AS DateTime), CAST(0x0000A5FC0129C143 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (376, N'132617', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C146 AS DateTime), CAST(0x0000A5FC0129C146 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (377, N'277798', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C148 AS DateTime), CAST(0x0000A5FC0129C148 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (378, N'947564', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C14B AS DateTime), CAST(0x0000A5FC0129C14B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (379, N'394617', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C14D AS DateTime), CAST(0x0000A5FC0129C14D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (380, N'744972', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C150 AS DateTime), CAST(0x0000A5FC0129C150 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (381, N'123119', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C153 AS DateTime), CAST(0x0000A5FC0129C153 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (382, N'519868', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C155 AS DateTime), CAST(0x0000A5FC0129C155 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (383, N'297805', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C158 AS DateTime), CAST(0x0000A5FC0129C158 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (384, N'934015', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C15A AS DateTime), CAST(0x0000A5FC0129C15A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (385, N'327926', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C15D AS DateTime), CAST(0x0000A5FC0129C15D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (386, N'114985', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C160 AS DateTime), CAST(0x0000A5FC0129C160 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (387, N'451356', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C163 AS DateTime), CAST(0x0000A5FC0129C163 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (388, N'103635', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C165 AS DateTime), CAST(0x0000A5FC0129C165 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (389, N'242827', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C168 AS DateTime), CAST(0x0000A5FC0129C168 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (390, N'807695', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C16A AS DateTime), CAST(0x0000A5FC0129C16A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (391, N'268454', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C16D AS DateTime), CAST(0x0000A5FC0129C16D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (392, N'765451', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C171 AS DateTime), CAST(0x0000A5FC0129C171 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (393, N'390364', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C174 AS DateTime), CAST(0x0000A5FC0129C174 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (394, N'267868', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C178 AS DateTime), CAST(0x0000A5FC0129C178 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (395, N'711769', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C17C AS DateTime), CAST(0x0000A5FC0129C17C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (396, N'803441', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C17F AS DateTime), CAST(0x0000A5FC0129C17F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (397, N'888030', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C181 AS DateTime), CAST(0x0000A5FC0129C181 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (398, N'671997', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C184 AS DateTime), CAST(0x0000A5FC0129C184 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (399, N'737636', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C187 AS DateTime), CAST(0x0000A5FC0129C187 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (400, N'543565', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C189 AS DateTime), CAST(0x0000A5FC0129C189 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (401, N'573432', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C18C AS DateTime), CAST(0x0000A5FC0129C18C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (402, N'836902', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C18F AS DateTime), CAST(0x0000A5FC0129C18F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (403, N'407815', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C192 AS DateTime), CAST(0x0000A5FC0129C192 AS DateTime))
GO
print 'Processed 400 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (404, N'771058', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C197 AS DateTime), CAST(0x0000A5FC0129C197 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (405, N'509877', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C199 AS DateTime), CAST(0x0000A5FC0129C199 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (406, N'248396', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C19D AS DateTime), CAST(0x0000A5FC0129C19D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (407, N'815531', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1A0 AS DateTime), CAST(0x0000A5FC0129C1A0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (408, N'633767', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1A3 AS DateTime), CAST(0x0000A5FC0129C1A3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (409, N'675323', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1A6 AS DateTime), CAST(0x0000A5FC0129C1A6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (410, N'463918', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1A9 AS DateTime), CAST(0x0000A5FC0129C1A9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (411, N'893841', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1AB AS DateTime), CAST(0x0000A5FC0129C1AB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (412, N'219398', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1AE AS DateTime), CAST(0x0000A5FC0129C1AE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (413, N'822299', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1B1 AS DateTime), CAST(0x0000A5FC0129C1B1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (414, N'118895', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1B4 AS DateTime), CAST(0x0000A5FC0129C1B4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (415, N'587740', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1B6 AS DateTime), CAST(0x0000A5FC0129C1B6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (416, N'726331', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1B9 AS DateTime), CAST(0x0000A5FC0129C1B9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (417, N'299928', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1BB AS DateTime), CAST(0x0000A5FC0129C1BB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (418, N'579614', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1BE AS DateTime), CAST(0x0000A5FC0129C1BE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (419, N'250530', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1C1 AS DateTime), CAST(0x0000A5FC0129C1C1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (420, N'288885', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1C3 AS DateTime), CAST(0x0000A5FC0129C1C3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (421, N'830516', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1C6 AS DateTime), CAST(0x0000A5FC0129C1C6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (422, N'443059', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1C9 AS DateTime), CAST(0x0000A5FC0129C1C9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (423, N'991179', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1CB AS DateTime), CAST(0x0000A5FC0129C1CB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (424, N'587058', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1CE AS DateTime), CAST(0x0000A5FC0129C1CE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (425, N'618240', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1D1 AS DateTime), CAST(0x0000A5FC0129C1D1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (426, N'276118', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1D4 AS DateTime), CAST(0x0000A5FC0129C1D4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (427, N'830812', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1D7 AS DateTime), CAST(0x0000A5FC0129C1D7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (428, N'247123', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1DA AS DateTime), CAST(0x0000A5FC0129C1DA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (429, N'738421', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1DC AS DateTime), CAST(0x0000A5FC0129C1DC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (430, N'919165', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1DF AS DateTime), CAST(0x0000A5FC0129C1DF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (431, N'866137', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1E2 AS DateTime), CAST(0x0000A5FC0129C1E2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (432, N'433295', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1E4 AS DateTime), CAST(0x0000A5FC0129C1E4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (433, N'182265', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1E7 AS DateTime), CAST(0x0000A5FC0129C1E7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (434, N'619545', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1EA AS DateTime), CAST(0x0000A5FC0129C1EA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (435, N'959648', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1EC AS DateTime), CAST(0x0000A5FC0129C1EC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (436, N'736379', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1EF AS DateTime), CAST(0x0000A5FC0129C1EF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (437, N'777803', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1F2 AS DateTime), CAST(0x0000A5FC0129C1F2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (438, N'504900', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1F5 AS DateTime), CAST(0x0000A5FC0129C1F5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (439, N'203449', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1F8 AS DateTime), CAST(0x0000A5FC0129C1F8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (440, N'133428', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1FA AS DateTime), CAST(0x0000A5FC0129C1FA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (441, N'659426', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C1FD AS DateTime), CAST(0x0000A5FC0129C1FD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (442, N'322320', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C200 AS DateTime), CAST(0x0000A5FC0129C200 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (443, N'791590', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C202 AS DateTime), CAST(0x0000A5FC0129C202 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (444, N'856950', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C205 AS DateTime), CAST(0x0000A5FC0129C205 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (445, N'930021', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C207 AS DateTime), CAST(0x0000A5FC0129C207 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (446, N'619753', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C20A AS DateTime), CAST(0x0000A5FC0129C20A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (447, N'694847', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C20C AS DateTime), CAST(0x0000A5FC0129C20C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (448, N'855836', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C210 AS DateTime), CAST(0x0000A5FC0129C210 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (449, N'427308', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C213 AS DateTime), CAST(0x0000A5FC0129C213 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (450, N'692100', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C217 AS DateTime), CAST(0x0000A5FC0129C217 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (451, N'557945', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C21A AS DateTime), CAST(0x0000A5FC0129C21A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (452, N'364319', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C21D AS DateTime), CAST(0x0000A5FC0129C21D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (453, N'295810', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C220 AS DateTime), CAST(0x0000A5FC0129C220 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (454, N'554381', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C222 AS DateTime), CAST(0x0000A5FC0129C222 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (455, N'217767', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C225 AS DateTime), CAST(0x0000A5FC0129C225 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (456, N'445752', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C228 AS DateTime), CAST(0x0000A5FC0129C228 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (457, N'176713', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C22B AS DateTime), CAST(0x0000A5FC0129C22B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (458, N'383340', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C232 AS DateTime), CAST(0x0000A5FC0129C232 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (459, N'454708', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C237 AS DateTime), CAST(0x0000A5FC0129C237 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (460, N'785948', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C23A AS DateTime), CAST(0x0000A5FC0129C23A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (461, N'562796', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C23E AS DateTime), CAST(0x0000A5FC0129C23E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (462, N'487974', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C242 AS DateTime), CAST(0x0000A5FC0129C242 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (463, N'488712', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C246 AS DateTime), CAST(0x0000A5FC0129C246 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (464, N'824551', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C249 AS DateTime), CAST(0x0000A5FC0129C249 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (465, N'840604', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C24C AS DateTime), CAST(0x0000A5FC0129C24C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (466, N'772293', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C24F AS DateTime), CAST(0x0000A5FC0129C24F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (467, N'390341', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C252 AS DateTime), CAST(0x0000A5FC0129C252 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (468, N'761923', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C256 AS DateTime), CAST(0x0000A5FC0129C256 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (469, N'209367', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C259 AS DateTime), CAST(0x0000A5FC0129C259 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (470, N'170217', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C25B AS DateTime), CAST(0x0000A5FC0129C25B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (471, N'803037', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C25F AS DateTime), CAST(0x0000A5FC0129C25F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (472, N'136981', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C261 AS DateTime), CAST(0x0000A5FC0129C261 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (473, N'296902', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C264 AS DateTime), CAST(0x0000A5FC0129C264 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (474, N'272023', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C267 AS DateTime), CAST(0x0000A5FC0129C267 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (475, N'762947', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C26B AS DateTime), CAST(0x0000A5FC0129C26B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (476, N'647484', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C26D AS DateTime), CAST(0x0000A5FC0129C26D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (477, N'366086', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C270 AS DateTime), CAST(0x0000A5FC0129C270 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (478, N'431397', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C274 AS DateTime), CAST(0x0000A5FC0129C274 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (479, N'395135', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C277 AS DateTime), CAST(0x0000A5FC0129C277 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (480, N'786882', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C27A AS DateTime), CAST(0x0000A5FC0129C27A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (481, N'364722', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C27C AS DateTime), CAST(0x0000A5FC0129C27C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (482, N'453117', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C27F AS DateTime), CAST(0x0000A5FC0129C27F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (483, N'708915', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C282 AS DateTime), CAST(0x0000A5FC0129C282 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (484, N'133476', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C285 AS DateTime), CAST(0x0000A5FC0129C285 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (485, N'759091', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C287 AS DateTime), CAST(0x0000A5FC0129C287 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (486, N'764617', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C28E AS DateTime), CAST(0x0000A5FC0129C28E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (487, N'652715', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C291 AS DateTime), CAST(0x0000A5FC0129C291 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (488, N'556209', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C294 AS DateTime), CAST(0x0000A5FC0129C294 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (489, N'279956', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C296 AS DateTime), CAST(0x0000A5FC0129C296 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (490, N'320948', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C299 AS DateTime), CAST(0x0000A5FC0129C299 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (491, N'755268', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C29C AS DateTime), CAST(0x0000A5FC0129C29C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (492, N'813657', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C29F AS DateTime), CAST(0x0000A5FC0129C29F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (493, N'389613', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C2A1 AS DateTime), CAST(0x0000A5FC0129C2A1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (494, N'790025', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C2A4 AS DateTime), CAST(0x0000A5FC0129C2A4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (495, N'335246', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C2A7 AS DateTime), CAST(0x0000A5FC0129C2A7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (496, N'714107', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C2A9 AS DateTime), CAST(0x0000A5FC0129C2A9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (497, N'226318', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C2AC AS DateTime), CAST(0x0000A5FC0129C2AC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (498, N'618155', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C2AF AS DateTime), CAST(0x0000A5FC0129C2AF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (499, N'442495', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C2B2 AS DateTime), CAST(0x0000A5FC0129C2B2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (500, N'939667', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C2B5 AS DateTime), CAST(0x0000A5FC0129C2B5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (501, N'270963', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C2BA AS DateTime), CAST(0x0000A5FC0129C2BA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (502, N'655619', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C2BD AS DateTime), CAST(0x0000A5FC0129C2BD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (503, N'587130', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C2C0 AS DateTime), CAST(0x0000A5FC0129C2C0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (504, N'742529', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C2C3 AS DateTime), CAST(0x0000A5FC0129C2C3 AS DateTime))
GO
print 'Processed 500 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (505, N'524121', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C2C6 AS DateTime), CAST(0x0000A5FC0129C2C6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (506, N'252513', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C2C8 AS DateTime), CAST(0x0000A5FC0129C2C8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (507, N'449193', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C326 AS DateTime), CAST(0x0000A5FC0129C326 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (508, N'569529', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C329 AS DateTime), CAST(0x0000A5FC0129C329 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (509, N'720424', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C32E AS DateTime), CAST(0x0000A5FC0129C32E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (510, N'176346', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C331 AS DateTime), CAST(0x0000A5FC0129C331 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (511, N'995267', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C334 AS DateTime), CAST(0x0000A5FC0129C334 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (512, N'946881', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C337 AS DateTime), CAST(0x0000A5FC0129C337 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (513, N'674216', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C33A AS DateTime), CAST(0x0000A5FC0129C33A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (514, N'450407', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C33D AS DateTime), CAST(0x0000A5FC0129C33D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (515, N'576173', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C33F AS DateTime), CAST(0x0000A5FC0129C33F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (516, N'998735', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C344 AS DateTime), CAST(0x0000A5FC0129C344 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (517, N'739256', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C347 AS DateTime), CAST(0x0000A5FC0129C347 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (518, N'634143', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C34F AS DateTime), CAST(0x0000A5FC0129C34F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (519, N'573107', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C352 AS DateTime), CAST(0x0000A5FC0129C352 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (520, N'736466', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C354 AS DateTime), CAST(0x0000A5FC0129C354 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (521, N'927151', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C357 AS DateTime), CAST(0x0000A5FC0129C357 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (522, N'675909', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C35A AS DateTime), CAST(0x0000A5FC0129C35A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (523, N'641868', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3B7 AS DateTime), CAST(0x0000A5FC0129C3B7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (524, N'535225', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3B9 AS DateTime), CAST(0x0000A5FC0129C3B9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (525, N'757795', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3BD AS DateTime), CAST(0x0000A5FC0129C3BD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (526, N'765118', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3BF AS DateTime), CAST(0x0000A5FC0129C3BF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (527, N'455900', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3C2 AS DateTime), CAST(0x0000A5FC0129C3C2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (528, N'401839', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3C6 AS DateTime), CAST(0x0000A5FC0129C3C6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (529, N'377786', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3C8 AS DateTime), CAST(0x0000A5FC0129C3C8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (530, N'172113', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3CB AS DateTime), CAST(0x0000A5FC0129C3CB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (531, N'308686', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3CE AS DateTime), CAST(0x0000A5FC0129C3CE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (532, N'601998', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3D1 AS DateTime), CAST(0x0000A5FC0129C3D1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (533, N'924566', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3D4 AS DateTime), CAST(0x0000A5FC0129C3D4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (534, N'945008', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3D6 AS DateTime), CAST(0x0000A5FC0129C3D6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (535, N'454302', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3D9 AS DateTime), CAST(0x0000A5FC0129C3D9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (536, N'402904', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3DC AS DateTime), CAST(0x0000A5FC0129C3DC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (537, N'859238', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3DE AS DateTime), CAST(0x0000A5FC0129C3DE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (538, N'744925', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3E1 AS DateTime), CAST(0x0000A5FC0129C3E1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (539, N'948221', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3E4 AS DateTime), CAST(0x0000A5FC0129C3E4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (540, N'716306', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3E7 AS DateTime), CAST(0x0000A5FC0129C3E7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (541, N'909675', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3EA AS DateTime), CAST(0x0000A5FC0129C3EA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (542, N'250200', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3ED AS DateTime), CAST(0x0000A5FC0129C3ED AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (543, N'764982', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C3F0 AS DateTime), CAST(0x0000A5FC0129C3F0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (544, N'858709', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C402 AS DateTime), CAST(0x0000A5FC0129C402 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (545, N'437530', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C405 AS DateTime), CAST(0x0000A5FC0129C405 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (546, N'976020', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C407 AS DateTime), CAST(0x0000A5FC0129C407 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (547, N'326293', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C40A AS DateTime), CAST(0x0000A5FC0129C40A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (548, N'279296', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C40D AS DateTime), CAST(0x0000A5FC0129C40D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (549, N'134872', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C410 AS DateTime), CAST(0x0000A5FC0129C410 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (550, N'625307', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C412 AS DateTime), CAST(0x0000A5FC0129C412 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (551, N'414612', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C415 AS DateTime), CAST(0x0000A5FC0129C415 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (552, N'461549', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C418 AS DateTime), CAST(0x0000A5FC0129C418 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (553, N'788870', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C41C AS DateTime), CAST(0x0000A5FC0129C41C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (554, N'343641', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C421 AS DateTime), CAST(0x0000A5FC0129C421 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (555, N'390781', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C424 AS DateTime), CAST(0x0000A5FC0129C424 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (556, N'683186', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C426 AS DateTime), CAST(0x0000A5FC0129C426 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (557, N'636529', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C429 AS DateTime), CAST(0x0000A5FC0129C429 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (558, N'874562', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C42C AS DateTime), CAST(0x0000A5FC0129C42C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (559, N'684083', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C42E AS DateTime), CAST(0x0000A5FC0129C42E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (560, N'789165', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C431 AS DateTime), CAST(0x0000A5FC0129C431 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (561, N'549670', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C434 AS DateTime), CAST(0x0000A5FC0129C434 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (562, N'270332', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C436 AS DateTime), CAST(0x0000A5FC0129C436 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (563, N'434852', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C43A AS DateTime), CAST(0x0000A5FC0129C43A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (564, N'427916', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C43D AS DateTime), CAST(0x0000A5FC0129C43D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (565, N'868659', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C441 AS DateTime), CAST(0x0000A5FC0129C441 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (566, N'568180', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C443 AS DateTime), CAST(0x0000A5FC0129C443 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (567, N'697468', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C446 AS DateTime), CAST(0x0000A5FC0129C446 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (568, N'878294', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C448 AS DateTime), CAST(0x0000A5FC0129C448 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (569, N'946074', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C44B AS DateTime), CAST(0x0000A5FC0129C44B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (570, N'456745', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C44D AS DateTime), CAST(0x0000A5FC0129C44D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (571, N'816431', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C450 AS DateTime), CAST(0x0000A5FC0129C450 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (572, N'431171', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C453 AS DateTime), CAST(0x0000A5FC0129C453 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (573, N'462134', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C455 AS DateTime), CAST(0x0000A5FC0129C455 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (574, N'377337', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C459 AS DateTime), CAST(0x0000A5FC0129C459 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (575, N'892641', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C45C AS DateTime), CAST(0x0000A5FC0129C45C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (576, N'562819', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C45F AS DateTime), CAST(0x0000A5FC0129C45F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (577, N'143205', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C462 AS DateTime), CAST(0x0000A5FC0129C462 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (578, N'363116', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C465 AS DateTime), CAST(0x0000A5FC0129C465 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (579, N'899795', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C467 AS DateTime), CAST(0x0000A5FC0129C467 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (580, N'519376', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C46A AS DateTime), CAST(0x0000A5FC0129C46A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (581, N'984201', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C46C AS DateTime), CAST(0x0000A5FC0129C46C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (582, N'649043', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C46F AS DateTime), CAST(0x0000A5FC0129C46F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (583, N'277112', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C472 AS DateTime), CAST(0x0000A5FC0129C472 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (584, N'590175', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C475 AS DateTime), CAST(0x0000A5FC0129C475 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (585, N'966811', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C478 AS DateTime), CAST(0x0000A5FC0129C478 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (586, N'509929', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C47B AS DateTime), CAST(0x0000A5FC0129C47B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (587, N'439613', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C47E AS DateTime), CAST(0x0000A5FC0129C47E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (588, N'646308', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C481 AS DateTime), CAST(0x0000A5FC0129C481 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (589, N'426484', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C483 AS DateTime), CAST(0x0000A5FC0129C483 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (590, N'295815', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C486 AS DateTime), CAST(0x0000A5FC0129C486 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (591, N'577218', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C488 AS DateTime), CAST(0x0000A5FC0129C488 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (592, N'372390', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C48B AS DateTime), CAST(0x0000A5FC0129C48B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (593, N'589032', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C48F AS DateTime), CAST(0x0000A5FC0129C48F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (594, N'130671', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C494 AS DateTime), CAST(0x0000A5FC0129C494 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (595, N'231059', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C496 AS DateTime), CAST(0x0000A5FC0129C496 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (596, N'273694', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C499 AS DateTime), CAST(0x0000A5FC0129C499 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (597, N'334146', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C49C AS DateTime), CAST(0x0000A5FC0129C49C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (598, N'624832', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C49F AS DateTime), CAST(0x0000A5FC0129C49F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (599, N'152775', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C4A2 AS DateTime), CAST(0x0000A5FC0129C4A2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (600, N'351157', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C4A4 AS DateTime), CAST(0x0000A5FC0129C4A4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (601, N'397096', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C4A7 AS DateTime), CAST(0x0000A5FC0129C4A7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (602, N'611204', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C4AA AS DateTime), CAST(0x0000A5FC0129C4AA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (603, N'912437', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C4AC AS DateTime), CAST(0x0000A5FC0129C4AC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (604, N'662195', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C4AF AS DateTime), CAST(0x0000A5FC0129C4AF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (605, N'232454', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C4B1 AS DateTime), CAST(0x0000A5FC0129C4B1 AS DateTime))
GO
print 'Processed 600 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (606, N'550295', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C4B6 AS DateTime), CAST(0x0000A5FC0129C4B6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (607, N'754886', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C4B8 AS DateTime), CAST(0x0000A5FC0129C4B8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (608, N'739358', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C4BB AS DateTime), CAST(0x0000A5FC0129C4BB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (609, N'753786', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C4BE AS DateTime), CAST(0x0000A5FC0129C4BE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (610, N'264769', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C4C1 AS DateTime), CAST(0x0000A5FC0129C4C1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (611, N'610574', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C4C3 AS DateTime), CAST(0x0000A5FC0129C4C3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (612, N'186788', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C520 AS DateTime), CAST(0x0000A5FC0129C520 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (613, N'750462', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C523 AS DateTime), CAST(0x0000A5FC0129C523 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (614, N'372042', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C526 AS DateTime), CAST(0x0000A5FC0129C526 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (615, N'944482', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C528 AS DateTime), CAST(0x0000A5FC0129C528 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (616, N'744059', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C52B AS DateTime), CAST(0x0000A5FC0129C52B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (617, N'373828', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C52E AS DateTime), CAST(0x0000A5FC0129C52E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (618, N'991694', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C531 AS DateTime), CAST(0x0000A5FC0129C531 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (619, N'541558', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C534 AS DateTime), CAST(0x0000A5FC0129C534 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (620, N'583859', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C538 AS DateTime), CAST(0x0000A5FC0129C538 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (621, N'800434', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C53A AS DateTime), CAST(0x0000A5FC0129C53A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (622, N'522012', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C53D AS DateTime), CAST(0x0000A5FC0129C53D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (623, N'608191', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C53F AS DateTime), CAST(0x0000A5FC0129C53F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (624, N'625617', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C542 AS DateTime), CAST(0x0000A5FC0129C542 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (625, N'629942', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C545 AS DateTime), CAST(0x0000A5FC0129C545 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (626, N'183390', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C547 AS DateTime), CAST(0x0000A5FC0129C547 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (627, N'844299', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C54A AS DateTime), CAST(0x0000A5FC0129C54A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (628, N'114833', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C54E AS DateTime), CAST(0x0000A5FC0129C54E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (629, N'791982', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C551 AS DateTime), CAST(0x0000A5FC0129C551 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (630, N'424417', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C555 AS DateTime), CAST(0x0000A5FC0129C555 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (631, N'618876', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C557 AS DateTime), CAST(0x0000A5FC0129C557 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (632, N'792134', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C55A AS DateTime), CAST(0x0000A5FC0129C55A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (633, N'404330', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C55D AS DateTime), CAST(0x0000A5FC0129C55D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (634, N'255346', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C55F AS DateTime), CAST(0x0000A5FC0129C55F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (635, N'548948', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C562 AS DateTime), CAST(0x0000A5FC0129C562 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (636, N'619160', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C564 AS DateTime), CAST(0x0000A5FC0129C564 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (637, N'894018', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C567 AS DateTime), CAST(0x0000A5FC0129C567 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (638, N'690756', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C56A AS DateTime), CAST(0x0000A5FC0129C56A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (639, N'364733', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C56F AS DateTime), CAST(0x0000A5FC0129C56F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (640, N'546040', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C572 AS DateTime), CAST(0x0000A5FC0129C572 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (641, N'293416', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C576 AS DateTime), CAST(0x0000A5FC0129C576 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (642, N'594326', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C578 AS DateTime), CAST(0x0000A5FC0129C578 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (643, N'211615', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C57D AS DateTime), CAST(0x0000A5FC0129C57D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (644, N'640821', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C580 AS DateTime), CAST(0x0000A5FC0129C580 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (645, N'933121', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C582 AS DateTime), CAST(0x0000A5FC0129C582 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (646, N'707006', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C585 AS DateTime), CAST(0x0000A5FC0129C585 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (647, N'860537', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C588 AS DateTime), CAST(0x0000A5FC0129C588 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (648, N'718895', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C594 AS DateTime), CAST(0x0000A5FC0129C594 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (649, N'737070', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C597 AS DateTime), CAST(0x0000A5FC0129C597 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (650, N'549642', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C59A AS DateTime), CAST(0x0000A5FC0129C59A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (651, N'216678', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C59C AS DateTime), CAST(0x0000A5FC0129C59C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (652, N'893062', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C59F AS DateTime), CAST(0x0000A5FC0129C59F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (653, N'773597', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5A2 AS DateTime), CAST(0x0000A5FC0129C5A2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (654, N'562323', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5A5 AS DateTime), CAST(0x0000A5FC0129C5A5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (655, N'515169', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5A8 AS DateTime), CAST(0x0000A5FC0129C5A8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (656, N'819424', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5AB AS DateTime), CAST(0x0000A5FC0129C5AB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (657, N'132924', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5AE AS DateTime), CAST(0x0000A5FC0129C5AE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (658, N'258342', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5B4 AS DateTime), CAST(0x0000A5FC0129C5B4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (659, N'914485', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5B7 AS DateTime), CAST(0x0000A5FC0129C5B7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (660, N'887151', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5B9 AS DateTime), CAST(0x0000A5FC0129C5B9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (661, N'149867', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5BC AS DateTime), CAST(0x0000A5FC0129C5BC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (662, N'878573', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5BF AS DateTime), CAST(0x0000A5FC0129C5BF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (663, N'174017', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5C1 AS DateTime), CAST(0x0000A5FC0129C5C1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (664, N'195321', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5C4 AS DateTime), CAST(0x0000A5FC0129C5C4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (665, N'448432', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5C8 AS DateTime), CAST(0x0000A5FC0129C5C8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (666, N'198274', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5CB AS DateTime), CAST(0x0000A5FC0129C5CB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (667, N'502076', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5CE AS DateTime), CAST(0x0000A5FC0129C5CE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (668, N'147693', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5D1 AS DateTime), CAST(0x0000A5FC0129C5D1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (669, N'443131', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5D3 AS DateTime), CAST(0x0000A5FC0129C5D3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (670, N'792839', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5D6 AS DateTime), CAST(0x0000A5FC0129C5D6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (671, N'585947', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5D8 AS DateTime), CAST(0x0000A5FC0129C5D8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (672, N'199852', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5DB AS DateTime), CAST(0x0000A5FC0129C5DB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (673, N'669759', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5DE AS DateTime), CAST(0x0000A5FC0129C5DE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (674, N'587861', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5E0 AS DateTime), CAST(0x0000A5FC0129C5E0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (675, N'886326', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5EF AS DateTime), CAST(0x0000A5FC0129C5EF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (676, N'463654', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5F1 AS DateTime), CAST(0x0000A5FC0129C5F1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (677, N'596165', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5F4 AS DateTime), CAST(0x0000A5FC0129C5F4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (678, N'861245', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5F7 AS DateTime), CAST(0x0000A5FC0129C5F7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (679, N'725028', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5F9 AS DateTime), CAST(0x0000A5FC0129C5F9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (680, N'864708', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5FC AS DateTime), CAST(0x0000A5FC0129C5FC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (681, N'374850', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C5FF AS DateTime), CAST(0x0000A5FC0129C5FF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (682, N'483155', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C602 AS DateTime), CAST(0x0000A5FC0129C602 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (683, N'139627', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C604 AS DateTime), CAST(0x0000A5FC0129C604 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (684, N'953486', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C607 AS DateTime), CAST(0x0000A5FC0129C607 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (685, N'842838', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C609 AS DateTime), CAST(0x0000A5FC0129C609 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (686, N'721515', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C60D AS DateTime), CAST(0x0000A5FC0129C60D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (687, N'441459', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C610 AS DateTime), CAST(0x0000A5FC0129C610 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (688, N'480552', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C612 AS DateTime), CAST(0x0000A5FC0129C612 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (689, N'472694', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C615 AS DateTime), CAST(0x0000A5FC0129C615 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (690, N'649470', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C618 AS DateTime), CAST(0x0000A5FC0129C618 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (691, N'599878', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C61A AS DateTime), CAST(0x0000A5FC0129C61A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (692, N'778781', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C61D AS DateTime), CAST(0x0000A5FC0129C61D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (693, N'876178', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C61F AS DateTime), CAST(0x0000A5FC0129C61F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (694, N'500749', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C623 AS DateTime), CAST(0x0000A5FC0129C623 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (695, N'236704', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C626 AS DateTime), CAST(0x0000A5FC0129C626 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (696, N'578687', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C629 AS DateTime), CAST(0x0000A5FC0129C629 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (697, N'388045', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C630 AS DateTime), CAST(0x0000A5FC0129C630 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (698, N'694771', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C633 AS DateTime), CAST(0x0000A5FC0129C633 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (699, N'503236', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C635 AS DateTime), CAST(0x0000A5FC0129C635 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (700, N'993360', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C638 AS DateTime), CAST(0x0000A5FC0129C638 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (701, N'759603', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C63C AS DateTime), CAST(0x0000A5FC0129C63C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (702, N'123862', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C640 AS DateTime), CAST(0x0000A5FC0129C640 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (703, N'330817', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C644 AS DateTime), CAST(0x0000A5FC0129C644 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (704, N'215163', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C649 AS DateTime), CAST(0x0000A5FC0129C649 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (705, N'578295', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C64D AS DateTime), CAST(0x0000A5FC0129C64D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (706, N'411572', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C650 AS DateTime), CAST(0x0000A5FC0129C650 AS DateTime))
GO
print 'Processed 700 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (707, N'735063', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C653 AS DateTime), CAST(0x0000A5FC0129C653 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (708, N'799602', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C657 AS DateTime), CAST(0x0000A5FC0129C657 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (709, N'474407', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C65A AS DateTime), CAST(0x0000A5FC0129C65A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (710, N'559139', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C65C AS DateTime), CAST(0x0000A5FC0129C65C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (711, N'404419', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C65F AS DateTime), CAST(0x0000A5FC0129C65F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (712, N'888328', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C662 AS DateTime), CAST(0x0000A5FC0129C662 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (713, N'109782', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C665 AS DateTime), CAST(0x0000A5FC0129C665 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (714, N'157971', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C66B AS DateTime), CAST(0x0000A5FC0129C66B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (715, N'697501', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C66D AS DateTime), CAST(0x0000A5FC0129C66D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (716, N'979666', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C670 AS DateTime), CAST(0x0000A5FC0129C670 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (717, N'237073', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C673 AS DateTime), CAST(0x0000A5FC0129C673 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (718, N'265575', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C676 AS DateTime), CAST(0x0000A5FC0129C676 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (719, N'172187', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C678 AS DateTime), CAST(0x0000A5FC0129C678 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (720, N'180606', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C67B AS DateTime), CAST(0x0000A5FC0129C67B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (721, N'336350', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C67D AS DateTime), CAST(0x0000A5FC0129C67D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (722, N'642314', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C681 AS DateTime), CAST(0x0000A5FC0129C681 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (723, N'978700', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C685 AS DateTime), CAST(0x0000A5FC0129C685 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (724, N'439793', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C688 AS DateTime), CAST(0x0000A5FC0129C688 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (725, N'877462', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C68A AS DateTime), CAST(0x0000A5FC0129C68A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (726, N'983550', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C68D AS DateTime), CAST(0x0000A5FC0129C68D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (727, N'390850', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C690 AS DateTime), CAST(0x0000A5FC0129C690 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (728, N'350652', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C693 AS DateTime), CAST(0x0000A5FC0129C693 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (729, N'145983', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C696 AS DateTime), CAST(0x0000A5FC0129C696 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (730, N'884750', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C698 AS DateTime), CAST(0x0000A5FC0129C698 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (731, N'930836', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C69B AS DateTime), CAST(0x0000A5FC0129C69B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (732, N'134727', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C6F8 AS DateTime), CAST(0x0000A5FC0129C6F8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (733, N'485811', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C6FC AS DateTime), CAST(0x0000A5FC0129C6FC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (734, N'769382', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C6FF AS DateTime), CAST(0x0000A5FC0129C6FF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (735, N'478860', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C701 AS DateTime), CAST(0x0000A5FC0129C701 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (736, N'611633', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C704 AS DateTime), CAST(0x0000A5FC0129C704 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (737, N'516074', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C706 AS DateTime), CAST(0x0000A5FC0129C706 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (738, N'787786', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C709 AS DateTime), CAST(0x0000A5FC0129C709 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (739, N'297952', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C70C AS DateTime), CAST(0x0000A5FC0129C70C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (740, N'227495', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C70F AS DateTime), CAST(0x0000A5FC0129C70F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (741, N'957686', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C711 AS DateTime), CAST(0x0000A5FC0129C711 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (742, N'741154', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C714 AS DateTime), CAST(0x0000A5FC0129C714 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (743, N'915369', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C717 AS DateTime), CAST(0x0000A5FC0129C717 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (744, N'979923', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C71B AS DateTime), CAST(0x0000A5FC0129C71B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (745, N'564884', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C71D AS DateTime), CAST(0x0000A5FC0129C71D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (746, N'167894', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C720 AS DateTime), CAST(0x0000A5FC0129C720 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (747, N'930995', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C724 AS DateTime), CAST(0x0000A5FC0129C724 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (748, N'466570', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C781 AS DateTime), CAST(0x0000A5FC0129C781 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (749, N'750795', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C784 AS DateTime), CAST(0x0000A5FC0129C784 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (750, N'698959', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C786 AS DateTime), CAST(0x0000A5FC0129C786 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (751, N'259698', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C789 AS DateTime), CAST(0x0000A5FC0129C789 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (752, N'894586', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C78C AS DateTime), CAST(0x0000A5FC0129C78C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (753, N'204151', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C78E AS DateTime), CAST(0x0000A5FC0129C78F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (754, N'131756', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C792 AS DateTime), CAST(0x0000A5FC0129C792 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (755, N'326345', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C796 AS DateTime), CAST(0x0000A5FC0129C796 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (756, N'603217', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C799 AS DateTime), CAST(0x0000A5FC0129C799 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (757, N'835644', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C79E AS DateTime), CAST(0x0000A5FC0129C79E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (758, N'781673', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C7FB AS DateTime), CAST(0x0000A5FC0129C7FB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (759, N'649427', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C7FE AS DateTime), CAST(0x0000A5FC0129C7FE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (760, N'844489', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C801 AS DateTime), CAST(0x0000A5FC0129C801 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (761, N'106871', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C804 AS DateTime), CAST(0x0000A5FC0129C804 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (762, N'819543', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C807 AS DateTime), CAST(0x0000A5FC0129C807 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (763, N'840907', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C809 AS DateTime), CAST(0x0000A5FC0129C809 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (764, N'412562', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C80C AS DateTime), CAST(0x0000A5FC0129C80C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (765, N'438403', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C810 AS DateTime), CAST(0x0000A5FC0129C810 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (766, N'212989', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C813 AS DateTime), CAST(0x0000A5FC0129C813 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (767, N'524680', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C816 AS DateTime), CAST(0x0000A5FC0129C816 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (768, N'497516', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C81A AS DateTime), CAST(0x0000A5FC0129C81A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (769, N'561744', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C81D AS DateTime), CAST(0x0000A5FC0129C81D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (770, N'311889', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C820 AS DateTime), CAST(0x0000A5FC0129C820 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (771, N'617583', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C823 AS DateTime), CAST(0x0000A5FC0129C823 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (772, N'566893', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C826 AS DateTime), CAST(0x0000A5FC0129C826 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (773, N'199418', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C828 AS DateTime), CAST(0x0000A5FC0129C828 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (774, N'584255', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C82B AS DateTime), CAST(0x0000A5FC0129C82B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (775, N'380016', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C830 AS DateTime), CAST(0x0000A5FC0129C830 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (776, N'827792', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C832 AS DateTime), CAST(0x0000A5FC0129C832 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (777, N'500518', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C836 AS DateTime), CAST(0x0000A5FC0129C836 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (778, N'349440', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C838 AS DateTime), CAST(0x0000A5FC0129C838 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (779, N'537838', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C83B AS DateTime), CAST(0x0000A5FC0129C83B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (780, N'655489', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C83E AS DateTime), CAST(0x0000A5FC0129C83E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (781, N'773522', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C840 AS DateTime), CAST(0x0000A5FC0129C840 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (782, N'776541', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C843 AS DateTime), CAST(0x0000A5FC0129C843 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (783, N'606589', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C846 AS DateTime), CAST(0x0000A5FC0129C846 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (784, N'163349', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C848 AS DateTime), CAST(0x0000A5FC0129C848 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (785, N'560951', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C84B AS DateTime), CAST(0x0000A5FC0129C84B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (786, N'500263', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C850 AS DateTime), CAST(0x0000A5FC0129C850 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (787, N'713151', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C853 AS DateTime), CAST(0x0000A5FC0129C853 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (788, N'680774', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C856 AS DateTime), CAST(0x0000A5FC0129C856 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (789, N'436106', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C859 AS DateTime), CAST(0x0000A5FC0129C859 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (790, N'979598', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C85C AS DateTime), CAST(0x0000A5FC0129C85C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (791, N'549391', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C85E AS DateTime), CAST(0x0000A5FC0129C85E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (792, N'727746', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C861 AS DateTime), CAST(0x0000A5FC0129C861 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (793, N'550129', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C864 AS DateTime), CAST(0x0000A5FC0129C864 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (794, N'763659', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C867 AS DateTime), CAST(0x0000A5FC0129C867 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (795, N'828058', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C86A AS DateTime), CAST(0x0000A5FC0129C86A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (796, N'237983', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8C7 AS DateTime), CAST(0x0000A5FC0129C8C7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (797, N'529335', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8C9 AS DateTime), CAST(0x0000A5FC0129C8C9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (798, N'976456', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8CC AS DateTime), CAST(0x0000A5FC0129C8CC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (799, N'759577', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8D0 AS DateTime), CAST(0x0000A5FC0129C8D0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (800, N'494506', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8D3 AS DateTime), CAST(0x0000A5FC0129C8D3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (801, N'631940', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8D5 AS DateTime), CAST(0x0000A5FC0129C8D5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (802, N'120919', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8D8 AS DateTime), CAST(0x0000A5FC0129C8D8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (803, N'535160', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8DB AS DateTime), CAST(0x0000A5FC0129C8DB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (804, N'180097', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8DD AS DateTime), CAST(0x0000A5FC0129C8DD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (805, N'297901', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8E0 AS DateTime), CAST(0x0000A5FC0129C8E0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (806, N'127379', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8E2 AS DateTime), CAST(0x0000A5FC0129C8E2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (807, N'229143', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8E6 AS DateTime), CAST(0x0000A5FC0129C8E6 AS DateTime))
GO
print 'Processed 800 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (808, N'254189', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8E8 AS DateTime), CAST(0x0000A5FC0129C8E8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (809, N'906514', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8EB AS DateTime), CAST(0x0000A5FC0129C8EB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (810, N'133373', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8F0 AS DateTime), CAST(0x0000A5FC0129C8F0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (811, N'445082', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8F4 AS DateTime), CAST(0x0000A5FC0129C8F4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (812, N'405249', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8F8 AS DateTime), CAST(0x0000A5FC0129C8F8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (813, N'237217', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8FB AS DateTime), CAST(0x0000A5FC0129C8FB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (814, N'949992', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C8FE AS DateTime), CAST(0x0000A5FC0129C8FE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (815, N'942574', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C902 AS DateTime), CAST(0x0000A5FC0129C902 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (816, N'939229', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C907 AS DateTime), CAST(0x0000A5FC0129C907 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (817, N'678190', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C90A AS DateTime), CAST(0x0000A5FC0129C90A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (818, N'295532', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C90D AS DateTime), CAST(0x0000A5FC0129C90D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (819, N'718848', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C911 AS DateTime), CAST(0x0000A5FC0129C911 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (820, N'972268', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C914 AS DateTime), CAST(0x0000A5FC0129C914 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (821, N'538600', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C916 AS DateTime), CAST(0x0000A5FC0129C916 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (822, N'482010', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C919 AS DateTime), CAST(0x0000A5FC0129C919 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (823, N'147300', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C91C AS DateTime), CAST(0x0000A5FC0129C91C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (824, N'120325', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C91E AS DateTime), CAST(0x0000A5FC0129C91E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (825, N'615245', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C921 AS DateTime), CAST(0x0000A5FC0129C921 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (826, N'439737', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C924 AS DateTime), CAST(0x0000A5FC0129C924 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (827, N'316056', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C927 AS DateTime), CAST(0x0000A5FC0129C927 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (828, N'663417', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C92A AS DateTime), CAST(0x0000A5FC0129C92A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (829, N'311034', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C92D AS DateTime), CAST(0x0000A5FC0129C92D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (830, N'827500', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C931 AS DateTime), CAST(0x0000A5FC0129C931 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (831, N'707201', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C934 AS DateTime), CAST(0x0000A5FC0129C934 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (832, N'892557', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C937 AS DateTime), CAST(0x0000A5FC0129C937 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (833, N'325120', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C939 AS DateTime), CAST(0x0000A5FC0129C939 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (834, N'139732', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C93C AS DateTime), CAST(0x0000A5FC0129C93C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (835, N'638898', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C93F AS DateTime), CAST(0x0000A5FC0129C93F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (836, N'578915', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C942 AS DateTime), CAST(0x0000A5FC0129C942 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (837, N'493909', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C945 AS DateTime), CAST(0x0000A5FC0129C945 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (838, N'409422', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C947 AS DateTime), CAST(0x0000A5FC0129C947 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (839, N'913054', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C94B AS DateTime), CAST(0x0000A5FC0129C94B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (840, N'234780', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C94D AS DateTime), CAST(0x0000A5FC0129C94D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (841, N'481726', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C950 AS DateTime), CAST(0x0000A5FC0129C950 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (842, N'399717', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C953 AS DateTime), CAST(0x0000A5FC0129C953 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (843, N'803833', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C955 AS DateTime), CAST(0x0000A5FC0129C955 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (844, N'951768', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C958 AS DateTime), CAST(0x0000A5FC0129C958 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (845, N'116305', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C95B AS DateTime), CAST(0x0000A5FC0129C95B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (846, N'838509', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C95E AS DateTime), CAST(0x0000A5FC0129C95E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (847, N'219187', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C961 AS DateTime), CAST(0x0000A5FC0129C961 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (848, N'348412', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C964 AS DateTime), CAST(0x0000A5FC0129C964 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (849, N'955233', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C966 AS DateTime), CAST(0x0000A5FC0129C966 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (850, N'879698', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C969 AS DateTime), CAST(0x0000A5FC0129C969 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (851, N'924090', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C96C AS DateTime), CAST(0x0000A5FC0129C96C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (852, N'586304', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C96E AS DateTime), CAST(0x0000A5FC0129C96E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (853, N'800256', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C971 AS DateTime), CAST(0x0000A5FC0129C971 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (854, N'871654', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C974 AS DateTime), CAST(0x0000A5FC0129C974 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (855, N'435768', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C976 AS DateTime), CAST(0x0000A5FC0129C976 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (856, N'756869', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C979 AS DateTime), CAST(0x0000A5FC0129C979 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (857, N'791202', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C97C AS DateTime), CAST(0x0000A5FC0129C97C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (858, N'422599', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C97F AS DateTime), CAST(0x0000A5FC0129C97F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (859, N'320663', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C982 AS DateTime), CAST(0x0000A5FC0129C982 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (860, N'914407', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C985 AS DateTime), CAST(0x0000A5FC0129C985 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (861, N'639399', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129C988 AS DateTime), CAST(0x0000A5FC0129C988 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (862, N'306607', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CAFA AS DateTime), CAST(0x0000A5FC0129CAFA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (863, N'856770', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CAFD AS DateTime), CAST(0x0000A5FC0129CAFD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (864, N'578124', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CAFF AS DateTime), CAST(0x0000A5FC0129CAFF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (865, N'863329', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB02 AS DateTime), CAST(0x0000A5FC0129CB02 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (866, N'988572', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB05 AS DateTime), CAST(0x0000A5FC0129CB05 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (867, N'622146', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB08 AS DateTime), CAST(0x0000A5FC0129CB08 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (868, N'874176', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB0B AS DateTime), CAST(0x0000A5FC0129CB0B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (869, N'876797', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB0E AS DateTime), CAST(0x0000A5FC0129CB0E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (870, N'734475', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB12 AS DateTime), CAST(0x0000A5FC0129CB12 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (871, N'828279', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB14 AS DateTime), CAST(0x0000A5FC0129CB14 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (872, N'646665', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB17 AS DateTime), CAST(0x0000A5FC0129CB17 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (873, N'817386', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB1A AS DateTime), CAST(0x0000A5FC0129CB1A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (874, N'801978', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB1D AS DateTime), CAST(0x0000A5FC0129CB1D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (875, N'677937', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB20 AS DateTime), CAST(0x0000A5FC0129CB20 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (876, N'147114', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB27 AS DateTime), CAST(0x0000A5FC0129CB27 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (877, N'740366', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB2B AS DateTime), CAST(0x0000A5FC0129CB2B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (878, N'123475', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB2F AS DateTime), CAST(0x0000A5FC0129CB2F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (879, N'939216', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB35 AS DateTime), CAST(0x0000A5FC0129CB35 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (880, N'904984', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB38 AS DateTime), CAST(0x0000A5FC0129CB38 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (881, N'214435', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB3B AS DateTime), CAST(0x0000A5FC0129CB3B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (882, N'584768', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB3E AS DateTime), CAST(0x0000A5FC0129CB3E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (883, N'911998', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB9A AS DateTime), CAST(0x0000A5FC0129CB9A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (884, N'848688', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CB9D AS DateTime), CAST(0x0000A5FC0129CB9D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (885, N'197618', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBA0 AS DateTime), CAST(0x0000A5FC0129CBA0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (886, N'808767', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBA4 AS DateTime), CAST(0x0000A5FC0129CBA4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (887, N'159303', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBA7 AS DateTime), CAST(0x0000A5FC0129CBA7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (888, N'882035', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBAA AS DateTime), CAST(0x0000A5FC0129CBAA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (889, N'438165', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBB0 AS DateTime), CAST(0x0000A5FC0129CBB0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (890, N'267346', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBB4 AS DateTime), CAST(0x0000A5FC0129CBB4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (891, N'514427', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBB6 AS DateTime), CAST(0x0000A5FC0129CBB6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (892, N'124572', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBBA AS DateTime), CAST(0x0000A5FC0129CBBA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (893, N'728839', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBBD AS DateTime), CAST(0x0000A5FC0129CBBD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (894, N'884712', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBBF AS DateTime), CAST(0x0000A5FC0129CBBF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (895, N'733804', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBC2 AS DateTime), CAST(0x0000A5FC0129CBC2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (896, N'741078', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBC4 AS DateTime), CAST(0x0000A5FC0129CBC4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (897, N'733481', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBC7 AS DateTime), CAST(0x0000A5FC0129CBC7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (898, N'340859', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBCA AS DateTime), CAST(0x0000A5FC0129CBCA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (899, N'343323', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBCD AS DateTime), CAST(0x0000A5FC0129CBCD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (900, N'521591', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBD2 AS DateTime), CAST(0x0000A5FC0129CBD2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (901, N'575651', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBD5 AS DateTime), CAST(0x0000A5FC0129CBD5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (902, N'207581', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBD7 AS DateTime), CAST(0x0000A5FC0129CBD7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (903, N'407039', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBDA AS DateTime), CAST(0x0000A5FC0129CBDA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (904, N'583908', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBDC AS DateTime), CAST(0x0000A5FC0129CBDC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (905, N'446585', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBDF AS DateTime), CAST(0x0000A5FC0129CBDF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (906, N'652844', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBE2 AS DateTime), CAST(0x0000A5FC0129CBE2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (907, N'548651', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBE8 AS DateTime), CAST(0x0000A5FC0129CBE8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (908, N'924379', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBEB AS DateTime), CAST(0x0000A5FC0129CBEB AS DateTime))
GO
print 'Processed 900 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (909, N'344323', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBEE AS DateTime), CAST(0x0000A5FC0129CBEE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (910, N'175739', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBF1 AS DateTime), CAST(0x0000A5FC0129CBF1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (911, N'368076', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBF4 AS DateTime), CAST(0x0000A5FC0129CBF4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (912, N'179399', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBF6 AS DateTime), CAST(0x0000A5FC0129CBF6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (913, N'120914', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBF9 AS DateTime), CAST(0x0000A5FC0129CBF9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (914, N'996650', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBFC AS DateTime), CAST(0x0000A5FC0129CBFC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (915, N'315279', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CBFE AS DateTime), CAST(0x0000A5FC0129CBFE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (916, N'604809', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC01 AS DateTime), CAST(0x0000A5FC0129CC01 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (917, N'118825', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC03 AS DateTime), CAST(0x0000A5FC0129CC03 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (918, N'962685', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC07 AS DateTime), CAST(0x0000A5FC0129CC07 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (919, N'662977', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC09 AS DateTime), CAST(0x0000A5FC0129CC09 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (920, N'740786', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC0D AS DateTime), CAST(0x0000A5FC0129CC0D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (921, N'889194', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC0F AS DateTime), CAST(0x0000A5FC0129CC0F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (922, N'724740', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC12 AS DateTime), CAST(0x0000A5FC0129CC12 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (923, N'192447', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC15 AS DateTime), CAST(0x0000A5FC0129CC15 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (924, N'850554', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC18 AS DateTime), CAST(0x0000A5FC0129CC18 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (925, N'693789', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC1A AS DateTime), CAST(0x0000A5FC0129CC1A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (926, N'496224', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC1D AS DateTime), CAST(0x0000A5FC0129CC1D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (927, N'853300', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC20 AS DateTime), CAST(0x0000A5FC0129CC20 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (928, N'921438', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC23 AS DateTime), CAST(0x0000A5FC0129CC23 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (929, N'971706', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC7F AS DateTime), CAST(0x0000A5FC0129CC7F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (930, N'692760', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC82 AS DateTime), CAST(0x0000A5FC0129CC82 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (931, N'832832', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC85 AS DateTime), CAST(0x0000A5FC0129CC85 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (932, N'385798', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC88 AS DateTime), CAST(0x0000A5FC0129CC88 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (933, N'676509', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC8E AS DateTime), CAST(0x0000A5FC0129CC8E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (934, N'628231', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC91 AS DateTime), CAST(0x0000A5FC0129CC91 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (935, N'971315', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC94 AS DateTime), CAST(0x0000A5FC0129CC94 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (936, N'577046', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC97 AS DateTime), CAST(0x0000A5FC0129CC97 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (937, N'477197', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC99 AS DateTime), CAST(0x0000A5FC0129CC99 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (938, N'147750', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CC9C AS DateTime), CAST(0x0000A5FC0129CC9C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (939, N'491460', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCA0 AS DateTime), CAST(0x0000A5FC0129CCA0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (940, N'749265', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCA3 AS DateTime), CAST(0x0000A5FC0129CCA3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (941, N'708039', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCA6 AS DateTime), CAST(0x0000A5FC0129CCA6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (942, N'784454', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCA9 AS DateTime), CAST(0x0000A5FC0129CCA9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (943, N'456014', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCAC AS DateTime), CAST(0x0000A5FC0129CCAC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (944, N'565882', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCAF AS DateTime), CAST(0x0000A5FC0129CCAF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (945, N'536097', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCB2 AS DateTime), CAST(0x0000A5FC0129CCB2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (946, N'484645', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCB4 AS DateTime), CAST(0x0000A5FC0129CCB4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (947, N'695926', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCB7 AS DateTime), CAST(0x0000A5FC0129CCB7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (948, N'543823', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCBA AS DateTime), CAST(0x0000A5FC0129CCBA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (949, N'118957', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCBD AS DateTime), CAST(0x0000A5FC0129CCBD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (950, N'246730', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCBF AS DateTime), CAST(0x0000A5FC0129CCBF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (951, N'496644', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCC2 AS DateTime), CAST(0x0000A5FC0129CCC2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (952, N'919497', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCC5 AS DateTime), CAST(0x0000A5FC0129CCC5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (953, N'502867', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCC8 AS DateTime), CAST(0x0000A5FC0129CCC8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (954, N'337970', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCCB AS DateTime), CAST(0x0000A5FC0129CCCB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (955, N'146974', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCCE AS DateTime), CAST(0x0000A5FC0129CCCE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (956, N'694607', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCD1 AS DateTime), CAST(0x0000A5FC0129CCD1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (957, N'834825', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCD3 AS DateTime), CAST(0x0000A5FC0129CCD3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (958, N'812206', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCD6 AS DateTime), CAST(0x0000A5FC0129CCD6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (959, N'533522', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCD9 AS DateTime), CAST(0x0000A5FC0129CCD9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (960, N'987229', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCDB AS DateTime), CAST(0x0000A5FC0129CCDB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (961, N'935637', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCDE AS DateTime), CAST(0x0000A5FC0129CCDE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (962, N'408706', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCE1 AS DateTime), CAST(0x0000A5FC0129CCE1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (963, N'376314', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCE4 AS DateTime), CAST(0x0000A5FC0129CCE4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (964, N'996658', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCE7 AS DateTime), CAST(0x0000A5FC0129CCE7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (965, N'265433', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCE9 AS DateTime), CAST(0x0000A5FC0129CCE9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (966, N'398459', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCEC AS DateTime), CAST(0x0000A5FC0129CCEC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (967, N'652111', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCEF AS DateTime), CAST(0x0000A5FC0129CCEF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (968, N'294193', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCF2 AS DateTime), CAST(0x0000A5FC0129CCF2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (969, N'971512', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCF4 AS DateTime), CAST(0x0000A5FC0129CCF4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (970, N'467764', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCF7 AS DateTime), CAST(0x0000A5FC0129CCF7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (971, N'592702', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCFB AS DateTime), CAST(0x0000A5FC0129CCFB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (972, N'472697', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CCFF AS DateTime), CAST(0x0000A5FC0129CCFF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (973, N'441671', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD03 AS DateTime), CAST(0x0000A5FC0129CD03 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (974, N'612736', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD07 AS DateTime), CAST(0x0000A5FC0129CD07 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (975, N'870223', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD0B AS DateTime), CAST(0x0000A5FC0129CD0B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (976, N'196798', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD0F AS DateTime), CAST(0x0000A5FC0129CD0F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (977, N'968015', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD13 AS DateTime), CAST(0x0000A5FC0129CD13 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (978, N'745652', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD16 AS DateTime), CAST(0x0000A5FC0129CD16 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (979, N'390903', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD19 AS DateTime), CAST(0x0000A5FC0129CD19 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (980, N'201305', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD1C AS DateTime), CAST(0x0000A5FC0129CD1C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (981, N'101712', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD1F AS DateTime), CAST(0x0000A5FC0129CD1F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (982, N'939972', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD23 AS DateTime), CAST(0x0000A5FC0129CD23 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (983, N'450700', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD25 AS DateTime), CAST(0x0000A5FC0129CD25 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (984, N'194947', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD28 AS DateTime), CAST(0x0000A5FC0129CD28 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (985, N'439148', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD2B AS DateTime), CAST(0x0000A5FC0129CD2B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (986, N'549028', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD2E AS DateTime), CAST(0x0000A5FC0129CD2E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (987, N'309071', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD31 AS DateTime), CAST(0x0000A5FC0129CD31 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (988, N'373501', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD33 AS DateTime), CAST(0x0000A5FC0129CD33 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (989, N'865119', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD36 AS DateTime), CAST(0x0000A5FC0129CD36 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (990, N'912346', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD39 AS DateTime), CAST(0x0000A5FC0129CD39 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (991, N'596577', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD3D AS DateTime), CAST(0x0000A5FC0129CD3D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (992, N'479236', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD40 AS DateTime), CAST(0x0000A5FC0129CD40 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (993, N'921886', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD43 AS DateTime), CAST(0x0000A5FC0129CD43 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (994, N'203137', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD46 AS DateTime), CAST(0x0000A5FC0129CD46 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (995, N'759800', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD48 AS DateTime), CAST(0x0000A5FC0129CD48 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (996, N'172464', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD4B AS DateTime), CAST(0x0000A5FC0129CD4B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (997, N'791396', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD4E AS DateTime), CAST(0x0000A5FC0129CD4E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (998, N'543555', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD51 AS DateTime), CAST(0x0000A5FC0129CD51 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (999, N'938580', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD54 AS DateTime), CAST(0x0000A5FC0129CD54 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1000, N'939110', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD58 AS DateTime), CAST(0x0000A5FC0129CD58 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1001, N'537293', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD5B AS DateTime), CAST(0x0000A5FC0129CD5B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1002, N'707814', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD5E AS DateTime), CAST(0x0000A5FC0129CD5E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1003, N'334752', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD61 AS DateTime), CAST(0x0000A5FC0129CD61 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1004, N'508946', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD64 AS DateTime), CAST(0x0000A5FC0129CD64 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1005, N'191662', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD67 AS DateTime), CAST(0x0000A5FC0129CD67 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1006, N'301589', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD6A AS DateTime), CAST(0x0000A5FC0129CD6A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1007, N'388420', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD6D AS DateTime), CAST(0x0000A5FC0129CD6D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1008, N'465741', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD70 AS DateTime), CAST(0x0000A5FC0129CD70 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1009, N'954170', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD73 AS DateTime), CAST(0x0000A5FC0129CD73 AS DateTime))
GO
print 'Processed 1000 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1010, N'809509', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD75 AS DateTime), CAST(0x0000A5FC0129CD75 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1011, N'131714', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD78 AS DateTime), CAST(0x0000A5FC0129CD78 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1012, N'375375', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD7B AS DateTime), CAST(0x0000A5FC0129CD7B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1013, N'666344', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD7F AS DateTime), CAST(0x0000A5FC0129CD7F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1014, N'332216', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD82 AS DateTime), CAST(0x0000A5FC0129CD82 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1015, N'827016', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD85 AS DateTime), CAST(0x0000A5FC0129CD85 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1016, N'968601', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD87 AS DateTime), CAST(0x0000A5FC0129CD87 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1017, N'627069', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD8A AS DateTime), CAST(0x0000A5FC0129CD8A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1018, N'937344', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD8D AS DateTime), CAST(0x0000A5FC0129CD8D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1019, N'535761', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD90 AS DateTime), CAST(0x0000A5FC0129CD90 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1020, N'227080', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD92 AS DateTime), CAST(0x0000A5FC0129CD92 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1021, N'781534', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD95 AS DateTime), CAST(0x0000A5FC0129CD95 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1022, N'180989', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD99 AS DateTime), CAST(0x0000A5FC0129CD99 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1023, N'979104', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD9C AS DateTime), CAST(0x0000A5FC0129CD9C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1024, N'984122', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CD9E AS DateTime), CAST(0x0000A5FC0129CD9E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1025, N'454925', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDA1 AS DateTime), CAST(0x0000A5FC0129CDA1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1026, N'350281', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDA3 AS DateTime), CAST(0x0000A5FC0129CDA3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1027, N'938848', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDA6 AS DateTime), CAST(0x0000A5FC0129CDA6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1028, N'972209', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDA9 AS DateTime), CAST(0x0000A5FC0129CDA9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1029, N'976511', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDAC AS DateTime), CAST(0x0000A5FC0129CDAC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1030, N'287394', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDAF AS DateTime), CAST(0x0000A5FC0129CDAF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1031, N'102256', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDB2 AS DateTime), CAST(0x0000A5FC0129CDB2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1032, N'743514', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDB5 AS DateTime), CAST(0x0000A5FC0129CDB5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1033, N'447314', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDB8 AS DateTime), CAST(0x0000A5FC0129CDB8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1034, N'717181', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDBD AS DateTime), CAST(0x0000A5FC0129CDBD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1035, N'865424', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDC0 AS DateTime), CAST(0x0000A5FC0129CDC0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1036, N'959214', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDC2 AS DateTime), CAST(0x0000A5FC0129CDC2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1037, N'686424', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDC5 AS DateTime), CAST(0x0000A5FC0129CDC5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1038, N'547635', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDC8 AS DateTime), CAST(0x0000A5FC0129CDC8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1039, N'394532', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDCA AS DateTime), CAST(0x0000A5FC0129CDCA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1040, N'203538', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDCD AS DateTime), CAST(0x0000A5FC0129CDCD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1041, N'913228', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDD0 AS DateTime), CAST(0x0000A5FC0129CDD0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1042, N'655653', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDD3 AS DateTime), CAST(0x0000A5FC0129CDD3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1043, N'144320', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDD5 AS DateTime), CAST(0x0000A5FC0129CDD5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1044, N'927720', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDD9 AS DateTime), CAST(0x0000A5FC0129CDD9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1045, N'459665', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDDC AS DateTime), CAST(0x0000A5FC0129CDDC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1046, N'754925', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDDE AS DateTime), CAST(0x0000A5FC0129CDDE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1047, N'295800', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDE1 AS DateTime), CAST(0x0000A5FC0129CDE1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1048, N'250551', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDE4 AS DateTime), CAST(0x0000A5FC0129CDE4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1049, N'396894', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDE7 AS DateTime), CAST(0x0000A5FC0129CDE7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1050, N'292155', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDEA AS DateTime), CAST(0x0000A5FC0129CDEA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1051, N'533489', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDEC AS DateTime), CAST(0x0000A5FC0129CDEC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1052, N'432783', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDF0 AS DateTime), CAST(0x0000A5FC0129CDF0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1053, N'818366', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDF3 AS DateTime), CAST(0x0000A5FC0129CDF3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1054, N'103510', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDF5 AS DateTime), CAST(0x0000A5FC0129CDF5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1055, N'706493', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDF8 AS DateTime), CAST(0x0000A5FC0129CDF8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1056, N'100282', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDFB AS DateTime), CAST(0x0000A5FC0129CDFB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1057, N'986461', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CDFE AS DateTime), CAST(0x0000A5FC0129CDFE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1058, N'261054', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE00 AS DateTime), CAST(0x0000A5FC0129CE00 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1059, N'732446', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE03 AS DateTime), CAST(0x0000A5FC0129CE03 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1060, N'909118', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE05 AS DateTime), CAST(0x0000A5FC0129CE05 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1061, N'505463', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE09 AS DateTime), CAST(0x0000A5FC0129CE09 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1062, N'591721', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE0B AS DateTime), CAST(0x0000A5FC0129CE0B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1063, N'467068', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE0F AS DateTime), CAST(0x0000A5FC0129CE0F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1064, N'648494', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE12 AS DateTime), CAST(0x0000A5FC0129CE12 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1065, N'825144', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE15 AS DateTime), CAST(0x0000A5FC0129CE15 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1066, N'290153', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE18 AS DateTime), CAST(0x0000A5FC0129CE18 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1067, N'159361', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE23 AS DateTime), CAST(0x0000A5FC0129CE23 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1068, N'154368', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE26 AS DateTime), CAST(0x0000A5FC0129CE26 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1069, N'877863', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE29 AS DateTime), CAST(0x0000A5FC0129CE29 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1070, N'830036', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE2B AS DateTime), CAST(0x0000A5FC0129CE2B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1071, N'305116', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE2F AS DateTime), CAST(0x0000A5FC0129CE2F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1072, N'768107', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE32 AS DateTime), CAST(0x0000A5FC0129CE32 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1073, N'802300', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE35 AS DateTime), CAST(0x0000A5FC0129CE35 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1074, N'967043', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE37 AS DateTime), CAST(0x0000A5FC0129CE37 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1075, N'491685', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE3A AS DateTime), CAST(0x0000A5FC0129CE3A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1076, N'261445', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE3D AS DateTime), CAST(0x0000A5FC0129CE3D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1077, N'293623', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE3F AS DateTime), CAST(0x0000A5FC0129CE3F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1078, N'595017', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE42 AS DateTime), CAST(0x0000A5FC0129CE42 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1079, N'915937', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE45 AS DateTime), CAST(0x0000A5FC0129CE45 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1080, N'958181', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE48 AS DateTime), CAST(0x0000A5FC0129CE48 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1081, N'537678', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE4A AS DateTime), CAST(0x0000A5FC0129CE4A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1082, N'672481', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE4D AS DateTime), CAST(0x0000A5FC0129CE4D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1083, N'656260', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE50 AS DateTime), CAST(0x0000A5FC0129CE50 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1084, N'944569', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE54 AS DateTime), CAST(0x0000A5FC0129CE54 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1085, N'436578', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE57 AS DateTime), CAST(0x0000A5FC0129CE57 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1086, N'701511', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE5B AS DateTime), CAST(0x0000A5FC0129CE5B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1087, N'907819', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE5F AS DateTime), CAST(0x0000A5FC0129CE5F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1088, N'196552', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE63 AS DateTime), CAST(0x0000A5FC0129CE63 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1089, N'493236', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CE67 AS DateTime), CAST(0x0000A5FC0129CE67 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1090, N'228221', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CEC4 AS DateTime), CAST(0x0000A5FC0129CEC4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1091, N'682982', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CF22 AS DateTime), CAST(0x0000A5FC0129CF22 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1092, N'696594', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CF28 AS DateTime), CAST(0x0000A5FC0129CF28 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1093, N'575217', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CF31 AS DateTime), CAST(0x0000A5FC0129CF31 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1094, N'219416', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CF34 AS DateTime), CAST(0x0000A5FC0129CF34 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1095, N'208487', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CF91 AS DateTime), CAST(0x0000A5FC0129CF91 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1096, N'252888', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CF94 AS DateTime), CAST(0x0000A5FC0129CF94 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1097, N'963443', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CF96 AS DateTime), CAST(0x0000A5FC0129CF96 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1098, N'432054', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFA1 AS DateTime), CAST(0x0000A5FC0129CFA1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1099, N'634738', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFA4 AS DateTime), CAST(0x0000A5FC0129CFA4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1100, N'132999', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFA9 AS DateTime), CAST(0x0000A5FC0129CFA9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1101, N'113936', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFAD AS DateTime), CAST(0x0000A5FC0129CFAD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1102, N'480106', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFB0 AS DateTime), CAST(0x0000A5FC0129CFB0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1103, N'721142', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFB4 AS DateTime), CAST(0x0000A5FC0129CFB4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1104, N'972395', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFB8 AS DateTime), CAST(0x0000A5FC0129CFB8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1105, N'686769', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFBD AS DateTime), CAST(0x0000A5FC0129CFBD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1106, N'496950', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFC2 AS DateTime), CAST(0x0000A5FC0129CFC2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1107, N'723557', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFC6 AS DateTime), CAST(0x0000A5FC0129CFC6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1108, N'382540', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFC9 AS DateTime), CAST(0x0000A5FC0129CFC9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1109, N'809358', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFCC AS DateTime), CAST(0x0000A5FC0129CFCC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1110, N'566259', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFD0 AS DateTime), CAST(0x0000A5FC0129CFD0 AS DateTime))
GO
print 'Processed 1100 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1111, N'469669', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFD2 AS DateTime), CAST(0x0000A5FC0129CFD2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1112, N'585985', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFD5 AS DateTime), CAST(0x0000A5FC0129CFD5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1113, N'596239', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFD9 AS DateTime), CAST(0x0000A5FC0129CFD9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1114, N'598295', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFDC AS DateTime), CAST(0x0000A5FC0129CFDC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1115, N'575439', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFE4 AS DateTime), CAST(0x0000A5FC0129CFE4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1116, N'368826', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFE6 AS DateTime), CAST(0x0000A5FC0129CFE6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1117, N'857884', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFE9 AS DateTime), CAST(0x0000A5FC0129CFE9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1118, N'225942', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFEC AS DateTime), CAST(0x0000A5FC0129CFEC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1119, N'321631', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFEF AS DateTime), CAST(0x0000A5FC0129CFEF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1120, N'464710', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFF2 AS DateTime), CAST(0x0000A5FC0129CFF2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1121, N'134783', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFF5 AS DateTime), CAST(0x0000A5FC0129CFF5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1122, N'892458', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFF8 AS DateTime), CAST(0x0000A5FC0129CFF8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1123, N'489237', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFFA AS DateTime), CAST(0x0000A5FC0129CFFA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1124, N'246555', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129CFFE AS DateTime), CAST(0x0000A5FC0129CFFE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1125, N'278926', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D001 AS DateTime), CAST(0x0000A5FC0129D001 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1126, N'671260', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D004 AS DateTime), CAST(0x0000A5FC0129D004 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1127, N'746401', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D007 AS DateTime), CAST(0x0000A5FC0129D007 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1128, N'606221', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D00A AS DateTime), CAST(0x0000A5FC0129D00A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1129, N'158110', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D00D AS DateTime), CAST(0x0000A5FC0129D00D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1130, N'136685', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D010 AS DateTime), CAST(0x0000A5FC0129D010 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1131, N'810195', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D013 AS DateTime), CAST(0x0000A5FC0129D013 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1132, N'474540', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D015 AS DateTime), CAST(0x0000A5FC0129D015 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1133, N'328266', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D019 AS DateTime), CAST(0x0000A5FC0129D019 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1134, N'391393', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D01C AS DateTime), CAST(0x0000A5FC0129D01C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1135, N'389615', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D01F AS DateTime), CAST(0x0000A5FC0129D01F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1136, N'767938', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D023 AS DateTime), CAST(0x0000A5FC0129D023 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1137, N'668043', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D026 AS DateTime), CAST(0x0000A5FC0129D026 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1138, N'479182', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D029 AS DateTime), CAST(0x0000A5FC0129D029 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1139, N'770439', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D02B AS DateTime), CAST(0x0000A5FC0129D02B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1140, N'511485', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D02E AS DateTime), CAST(0x0000A5FC0129D02E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1141, N'123370', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D031 AS DateTime), CAST(0x0000A5FC0129D031 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1142, N'766454', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D034 AS DateTime), CAST(0x0000A5FC0129D034 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1143, N'251989', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D036 AS DateTime), CAST(0x0000A5FC0129D036 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1144, N'561688', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D03A AS DateTime), CAST(0x0000A5FC0129D03A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1145, N'850151', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D03D AS DateTime), CAST(0x0000A5FC0129D03D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1146, N'607773', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D040 AS DateTime), CAST(0x0000A5FC0129D040 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1147, N'952839', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D09E AS DateTime), CAST(0x0000A5FC0129D09E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1148, N'953141', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D0A1 AS DateTime), CAST(0x0000A5FC0129D0A1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1149, N'143080', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D0A4 AS DateTime), CAST(0x0000A5FC0129D0A4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1150, N'628238', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D0A8 AS DateTime), CAST(0x0000A5FC0129D0A8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1151, N'665191', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D108 AS DateTime), CAST(0x0000A5FC0129D108 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1152, N'968884', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D10D AS DateTime), CAST(0x0000A5FC0129D10D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1153, N'781452', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D110 AS DateTime), CAST(0x0000A5FC0129D110 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1154, N'939576', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D113 AS DateTime), CAST(0x0000A5FC0129D113 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1155, N'662470', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D118 AS DateTime), CAST(0x0000A5FC0129D118 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1156, N'919972', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D11C AS DateTime), CAST(0x0000A5FC0129D11C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1157, N'680123', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D120 AS DateTime), CAST(0x0000A5FC0129D120 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1158, N'523839', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D125 AS DateTime), CAST(0x0000A5FC0129D125 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1159, N'469149', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D128 AS DateTime), CAST(0x0000A5FC0129D128 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1160, N'188435', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D12A AS DateTime), CAST(0x0000A5FC0129D12A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1161, N'917319', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D12D AS DateTime), CAST(0x0000A5FC0129D12D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1162, N'194269', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D130 AS DateTime), CAST(0x0000A5FC0129D130 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1163, N'353935', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D133 AS DateTime), CAST(0x0000A5FC0129D133 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1164, N'386317', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D136 AS DateTime), CAST(0x0000A5FC0129D136 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1165, N'720961', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D139 AS DateTime), CAST(0x0000A5FC0129D139 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1166, N'377759', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D13B AS DateTime), CAST(0x0000A5FC0129D13B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1167, N'201570', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D13E AS DateTime), CAST(0x0000A5FC0129D13E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1168, N'309659', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D141 AS DateTime), CAST(0x0000A5FC0129D141 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1169, N'249662', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D144 AS DateTime), CAST(0x0000A5FC0129D144 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1170, N'619620', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D147 AS DateTime), CAST(0x0000A5FC0129D147 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1171, N'737347', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D14A AS DateTime), CAST(0x0000A5FC0129D14A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1172, N'635783', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D14E AS DateTime), CAST(0x0000A5FC0129D14E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1173, N'539456', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D151 AS DateTime), CAST(0x0000A5FC0129D151 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1174, N'687066', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D156 AS DateTime), CAST(0x0000A5FC0129D156 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1175, N'791244', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D15C AS DateTime), CAST(0x0000A5FC0129D15C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1176, N'248444', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D160 AS DateTime), CAST(0x0000A5FC0129D160 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1177, N'291470', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D164 AS DateTime), CAST(0x0000A5FC0129D164 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1178, N'258734', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D168 AS DateTime), CAST(0x0000A5FC0129D168 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1179, N'959635', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D16E AS DateTime), CAST(0x0000A5FC0129D16E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1180, N'734215', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D171 AS DateTime), CAST(0x0000A5FC0129D171 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1181, N'487661', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D174 AS DateTime), CAST(0x0000A5FC0129D174 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1182, N'477327', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D177 AS DateTime), CAST(0x0000A5FC0129D177 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1183, N'748116', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D17A AS DateTime), CAST(0x0000A5FC0129D17A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1184, N'363367', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D17C AS DateTime), CAST(0x0000A5FC0129D17C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1185, N'971400', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D17F AS DateTime), CAST(0x0000A5FC0129D17F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1186, N'743138', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D182 AS DateTime), CAST(0x0000A5FC0129D182 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1187, N'356003', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D186 AS DateTime), CAST(0x0000A5FC0129D186 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1188, N'473890', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D188 AS DateTime), CAST(0x0000A5FC0129D188 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1189, N'161834', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D18B AS DateTime), CAST(0x0000A5FC0129D18B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1190, N'969786', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D18F AS DateTime), CAST(0x0000A5FC0129D18F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1191, N'718255', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D192 AS DateTime), CAST(0x0000A5FC0129D192 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1192, N'217409', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D196 AS DateTime), CAST(0x0000A5FC0129D196 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1193, N'437143', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D198 AS DateTime), CAST(0x0000A5FC0129D198 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1194, N'181894', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D19B AS DateTime), CAST(0x0000A5FC0129D19B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1195, N'317165', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D19E AS DateTime), CAST(0x0000A5FC0129D19E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1196, N'282248', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D1A0 AS DateTime), CAST(0x0000A5FC0129D1A0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1197, N'206340', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D1A3 AS DateTime), CAST(0x0000A5FC0129D1A3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1198, N'495694', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D1A5 AS DateTime), CAST(0x0000A5FC0129D1A5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1199, N'784523', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D203 AS DateTime), CAST(0x0000A5FC0129D203 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1200, N'532734', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D207 AS DateTime), CAST(0x0000A5FC0129D207 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1201, N'738513', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D209 AS DateTime), CAST(0x0000A5FC0129D209 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1202, N'837576', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D20D AS DateTime), CAST(0x0000A5FC0129D20D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1203, N'173646', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D210 AS DateTime), CAST(0x0000A5FC0129D210 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1204, N'456116', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D213 AS DateTime), CAST(0x0000A5FC0129D213 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1205, N'874941', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D215 AS DateTime), CAST(0x0000A5FC0129D215 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1206, N'671995', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D218 AS DateTime), CAST(0x0000A5FC0129D218 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1207, N'957418', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D21B AS DateTime), CAST(0x0000A5FC0129D21B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1208, N'998865', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D21E AS DateTime), CAST(0x0000A5FC0129D21E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1209, N'536841', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D221 AS DateTime), CAST(0x0000A5FC0129D221 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1210, N'919570', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D223 AS DateTime), CAST(0x0000A5FC0129D223 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1211, N'834184', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D226 AS DateTime), CAST(0x0000A5FC0129D226 AS DateTime))
GO
print 'Processed 1200 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1212, N'445173', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D229 AS DateTime), CAST(0x0000A5FC0129D229 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1213, N'681277', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D22C AS DateTime), CAST(0x0000A5FC0129D22C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1214, N'751027', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D22F AS DateTime), CAST(0x0000A5FC0129D22F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1215, N'716006', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D232 AS DateTime), CAST(0x0000A5FC0129D232 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1216, N'738708', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D234 AS DateTime), CAST(0x0000A5FC0129D234 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1217, N'203162', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D237 AS DateTime), CAST(0x0000A5FC0129D237 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1218, N'700703', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D23A AS DateTime), CAST(0x0000A5FC0129D23A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1219, N'525990', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D23D AS DateTime), CAST(0x0000A5FC0129D23D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1220, N'265988', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D240 AS DateTime), CAST(0x0000A5FC0129D240 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1221, N'883622', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D243 AS DateTime), CAST(0x0000A5FC0129D243 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1222, N'969220', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D246 AS DateTime), CAST(0x0000A5FC0129D246 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1223, N'471802', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D248 AS DateTime), CAST(0x0000A5FC0129D248 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1224, N'550032', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D24B AS DateTime), CAST(0x0000A5FC0129D24B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1225, N'592556', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D24E AS DateTime), CAST(0x0000A5FC0129D24E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1226, N'378811', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D251 AS DateTime), CAST(0x0000A5FC0129D251 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1227, N'609544', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D254 AS DateTime), CAST(0x0000A5FC0129D254 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1228, N'987375', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D256 AS DateTime), CAST(0x0000A5FC0129D256 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1229, N'166745', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D258 AS DateTime), CAST(0x0000A5FC0129D258 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1230, N'411094', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D25B AS DateTime), CAST(0x0000A5FC0129D25B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1231, N'831472', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D25E AS DateTime), CAST(0x0000A5FC0129D25E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1232, N'198543', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D261 AS DateTime), CAST(0x0000A5FC0129D261 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1233, N'285244', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D264 AS DateTime), CAST(0x0000A5FC0129D264 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1234, N'129761', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D26A AS DateTime), CAST(0x0000A5FC0129D26A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1235, N'898770', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D26D AS DateTime), CAST(0x0000A5FC0129D26D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1236, N'801156', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D273 AS DateTime), CAST(0x0000A5FC0129D273 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1237, N'915142', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D276 AS DateTime), CAST(0x0000A5FC0129D276 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1238, N'779616', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D27A AS DateTime), CAST(0x0000A5FC0129D27A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1239, N'403068', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D280 AS DateTime), CAST(0x0000A5FC0129D280 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1240, N'878251', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D286 AS DateTime), CAST(0x0000A5FC0129D286 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1241, N'403379', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D289 AS DateTime), CAST(0x0000A5FC0129D289 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1242, N'744176', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D28C AS DateTime), CAST(0x0000A5FC0129D28C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1243, N'917055', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D28E AS DateTime), CAST(0x0000A5FC0129D28E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1244, N'366118', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D291 AS DateTime), CAST(0x0000A5FC0129D291 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1245, N'777466', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D293 AS DateTime), CAST(0x0000A5FC0129D293 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1246, N'775653', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D296 AS DateTime), CAST(0x0000A5FC0129D296 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1247, N'907392', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D298 AS DateTime), CAST(0x0000A5FC0129D298 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1248, N'800473', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D29B AS DateTime), CAST(0x0000A5FC0129D29B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1249, N'506201', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D29E AS DateTime), CAST(0x0000A5FC0129D29E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1250, N'141231', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2A1 AS DateTime), CAST(0x0000A5FC0129D2A1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1251, N'749721', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2A3 AS DateTime), CAST(0x0000A5FC0129D2A3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1252, N'333132', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2A7 AS DateTime), CAST(0x0000A5FC0129D2A7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1253, N'323685', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2A9 AS DateTime), CAST(0x0000A5FC0129D2A9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1254, N'818651', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2AC AS DateTime), CAST(0x0000A5FC0129D2AC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1255, N'511644', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2AE AS DateTime), CAST(0x0000A5FC0129D2AE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1256, N'250815', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2B1 AS DateTime), CAST(0x0000A5FC0129D2B1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1257, N'176249', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2B4 AS DateTime), CAST(0x0000A5FC0129D2B4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1258, N'595648', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2B7 AS DateTime), CAST(0x0000A5FC0129D2B7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1259, N'312059', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2B9 AS DateTime), CAST(0x0000A5FC0129D2B9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1260, N'787580', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2BF AS DateTime), CAST(0x0000A5FC0129D2BF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1261, N'897091', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2C2 AS DateTime), CAST(0x0000A5FC0129D2C2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1262, N'862111', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2C6 AS DateTime), CAST(0x0000A5FC0129D2C6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1263, N'603870', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2C9 AS DateTime), CAST(0x0000A5FC0129D2C9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1264, N'808514', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2CB AS DateTime), CAST(0x0000A5FC0129D2CB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1265, N'697396', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2CE AS DateTime), CAST(0x0000A5FC0129D2CE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1266, N'153637', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2D1 AS DateTime), CAST(0x0000A5FC0129D2D1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1267, N'112568', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2D4 AS DateTime), CAST(0x0000A5FC0129D2D4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1268, N'224408', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2D7 AS DateTime), CAST(0x0000A5FC0129D2D7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1269, N'473401', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2DA AS DateTime), CAST(0x0000A5FC0129D2DA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1270, N'867241', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2DD AS DateTime), CAST(0x0000A5FC0129D2DD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1271, N'938130', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2E0 AS DateTime), CAST(0x0000A5FC0129D2E0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1272, N'267151', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2E5 AS DateTime), CAST(0x0000A5FC0129D2E5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1273, N'585723', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2E7 AS DateTime), CAST(0x0000A5FC0129D2E7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1274, N'470136', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2EA AS DateTime), CAST(0x0000A5FC0129D2EA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1275, N'556586', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2ED AS DateTime), CAST(0x0000A5FC0129D2ED AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1276, N'424419', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2F0 AS DateTime), CAST(0x0000A5FC0129D2F0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1277, N'261019', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2F2 AS DateTime), CAST(0x0000A5FC0129D2F2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1278, N'300525', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2F5 AS DateTime), CAST(0x0000A5FC0129D2F5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1279, N'751350', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2F8 AS DateTime), CAST(0x0000A5FC0129D2F8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1280, N'505067', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2FB AS DateTime), CAST(0x0000A5FC0129D2FB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1281, N'954192', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D2FE AS DateTime), CAST(0x0000A5FC0129D2FE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1282, N'744405', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D301 AS DateTime), CAST(0x0000A5FC0129D301 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1283, N'364595', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D304 AS DateTime), CAST(0x0000A5FC0129D304 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1284, N'766128', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D307 AS DateTime), CAST(0x0000A5FC0129D307 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1285, N'160245', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D30A AS DateTime), CAST(0x0000A5FC0129D30A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1286, N'127305', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D30D AS DateTime), CAST(0x0000A5FC0129D30D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1287, N'344762', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D310 AS DateTime), CAST(0x0000A5FC0129D310 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1288, N'896242', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D313 AS DateTime), CAST(0x0000A5FC0129D313 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1289, N'528022', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D315 AS DateTime), CAST(0x0000A5FC0129D315 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1290, N'661721', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D31A AS DateTime), CAST(0x0000A5FC0129D31A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1291, N'475205', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D31D AS DateTime), CAST(0x0000A5FC0129D31D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1292, N'895503', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D320 AS DateTime), CAST(0x0000A5FC0129D320 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1293, N'559050', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D324 AS DateTime), CAST(0x0000A5FC0129D324 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1294, N'262494', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D326 AS DateTime), CAST(0x0000A5FC0129D326 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1295, N'388570', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D329 AS DateTime), CAST(0x0000A5FC0129D329 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1296, N'534955', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D32C AS DateTime), CAST(0x0000A5FC0129D32C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1297, N'704232', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D32F AS DateTime), CAST(0x0000A5FC0129D32F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1298, N'818890', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D332 AS DateTime), CAST(0x0000A5FC0129D332 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1299, N'251986', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D33A AS DateTime), CAST(0x0000A5FC0129D33A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1300, N'921077', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D33D AS DateTime), CAST(0x0000A5FC0129D33D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1301, N'365409', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D340 AS DateTime), CAST(0x0000A5FC0129D340 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1302, N'969936', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D343 AS DateTime), CAST(0x0000A5FC0129D343 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1303, N'250095', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D346 AS DateTime), CAST(0x0000A5FC0129D346 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1304, N'782985', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D349 AS DateTime), CAST(0x0000A5FC0129D349 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1305, N'150669', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D34B AS DateTime), CAST(0x0000A5FC0129D34B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1306, N'738677', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D34E AS DateTime), CAST(0x0000A5FC0129D34E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1307, N'492653', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D352 AS DateTime), CAST(0x0000A5FC0129D352 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1308, N'108181', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D355 AS DateTime), CAST(0x0000A5FC0129D355 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1309, N'902190', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D357 AS DateTime), CAST(0x0000A5FC0129D357 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1310, N'225735', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D35A AS DateTime), CAST(0x0000A5FC0129D35A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1311, N'440180', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D35E AS DateTime), CAST(0x0000A5FC0129D35E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1312, N'217446', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D360 AS DateTime), CAST(0x0000A5FC0129D360 AS DateTime))
GO
print 'Processed 1300 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1313, N'581747', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D363 AS DateTime), CAST(0x0000A5FC0129D363 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1314, N'461108', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D366 AS DateTime), CAST(0x0000A5FC0129D366 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1315, N'383593', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D369 AS DateTime), CAST(0x0000A5FC0129D369 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1316, N'194412', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D36C AS DateTime), CAST(0x0000A5FC0129D36C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1317, N'179385', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D36E AS DateTime), CAST(0x0000A5FC0129D36E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1318, N'721687', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D371 AS DateTime), CAST(0x0000A5FC0129D371 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1319, N'423923', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D374 AS DateTime), CAST(0x0000A5FC0129D374 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1320, N'873555', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D378 AS DateTime), CAST(0x0000A5FC0129D378 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1321, N'277747', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D37B AS DateTime), CAST(0x0000A5FC0129D37B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1322, N'902779', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D37E AS DateTime), CAST(0x0000A5FC0129D37E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1323, N'715107', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D380 AS DateTime), CAST(0x0000A5FC0129D380 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1324, N'950854', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D383 AS DateTime), CAST(0x0000A5FC0129D383 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1325, N'893719', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D385 AS DateTime), CAST(0x0000A5FC0129D385 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1326, N'816518', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D388 AS DateTime), CAST(0x0000A5FC0129D388 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1327, N'138606', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D38B AS DateTime), CAST(0x0000A5FC0129D38B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1328, N'149316', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D38E AS DateTime), CAST(0x0000A5FC0129D38E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1329, N'573599', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D391 AS DateTime), CAST(0x0000A5FC0129D391 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1330, N'175993', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D399 AS DateTime), CAST(0x0000A5FC0129D399 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1331, N'130261', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D39E AS DateTime), CAST(0x0000A5FC0129D39E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1332, N'815993', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3A1 AS DateTime), CAST(0x0000A5FC0129D3A1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1333, N'727275', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3A4 AS DateTime), CAST(0x0000A5FC0129D3A4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1334, N'232540', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3A7 AS DateTime), CAST(0x0000A5FC0129D3A7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1335, N'934393', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3AA AS DateTime), CAST(0x0000A5FC0129D3AA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1336, N'397461', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3AD AS DateTime), CAST(0x0000A5FC0129D3AD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1337, N'880398', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3AF AS DateTime), CAST(0x0000A5FC0129D3AF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1338, N'733834', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3B2 AS DateTime), CAST(0x0000A5FC0129D3B2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1339, N'111280', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3B5 AS DateTime), CAST(0x0000A5FC0129D3B5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1340, N'970828', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3B8 AS DateTime), CAST(0x0000A5FC0129D3B8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1341, N'718028', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3BC AS DateTime), CAST(0x0000A5FC0129D3BC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1342, N'321890', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3BF AS DateTime), CAST(0x0000A5FC0129D3BF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1343, N'688188', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3C3 AS DateTime), CAST(0x0000A5FC0129D3C3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1344, N'869173', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3C6 AS DateTime), CAST(0x0000A5FC0129D3C6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1345, N'154541', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3CA AS DateTime), CAST(0x0000A5FC0129D3CA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1346, N'955725', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3CD AS DateTime), CAST(0x0000A5FC0129D3CD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1347, N'937381', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3D4 AS DateTime), CAST(0x0000A5FC0129D3D4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1348, N'706925', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3DA AS DateTime), CAST(0x0000A5FC0129D3DA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1349, N'559574', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3DD AS DateTime), CAST(0x0000A5FC0129D3DD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1350, N'679174', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3E0 AS DateTime), CAST(0x0000A5FC0129D3E0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1351, N'855808', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3E3 AS DateTime), CAST(0x0000A5FC0129D3E3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1352, N'341421', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3E6 AS DateTime), CAST(0x0000A5FC0129D3E6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1353, N'133230', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3E9 AS DateTime), CAST(0x0000A5FC0129D3E9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1354, N'370661', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3EB AS DateTime), CAST(0x0000A5FC0129D3EB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1355, N'432137', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3EE AS DateTime), CAST(0x0000A5FC0129D3EE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1356, N'814171', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3F1 AS DateTime), CAST(0x0000A5FC0129D3F1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1357, N'600418', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3F4 AS DateTime), CAST(0x0000A5FC0129D3F4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1358, N'776552', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3F7 AS DateTime), CAST(0x0000A5FC0129D3F7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1359, N'748616', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3FA AS DateTime), CAST(0x0000A5FC0129D3FA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1360, N'244189', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D3FD AS DateTime), CAST(0x0000A5FC0129D3FD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1361, N'947140', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D400 AS DateTime), CAST(0x0000A5FC0129D400 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1362, N'462910', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D402 AS DateTime), CAST(0x0000A5FC0129D402 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1363, N'280383', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D405 AS DateTime), CAST(0x0000A5FC0129D405 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1364, N'563099', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D408 AS DateTime), CAST(0x0000A5FC0129D408 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1365, N'556288', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D40A AS DateTime), CAST(0x0000A5FC0129D40A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1366, N'469790', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D40D AS DateTime), CAST(0x0000A5FC0129D40D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1367, N'262681', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D410 AS DateTime), CAST(0x0000A5FC0129D410 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1368, N'143026', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D414 AS DateTime), CAST(0x0000A5FC0129D414 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1369, N'458594', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D416 AS DateTime), CAST(0x0000A5FC0129D416 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1370, N'409621', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D419 AS DateTime), CAST(0x0000A5FC0129D419 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1371, N'503863', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D41C AS DateTime), CAST(0x0000A5FC0129D41C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1372, N'529880', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D41F AS DateTime), CAST(0x0000A5FC0129D41F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1373, N'977892', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D422 AS DateTime), CAST(0x0000A5FC0129D422 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1374, N'873886', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D425 AS DateTime), CAST(0x0000A5FC0129D425 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1375, N'514111', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D42A AS DateTime), CAST(0x0000A5FC0129D42A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1376, N'663108', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D42E AS DateTime), CAST(0x0000A5FC0129D42E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1377, N'629993', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D432 AS DateTime), CAST(0x0000A5FC0129D432 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1378, N'986605', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D435 AS DateTime), CAST(0x0000A5FC0129D435 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1379, N'722704', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D438 AS DateTime), CAST(0x0000A5FC0129D438 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1380, N'324575', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D43A AS DateTime), CAST(0x0000A5FC0129D43A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1381, N'567709', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D43D AS DateTime), CAST(0x0000A5FC0129D43D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1382, N'380491', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D440 AS DateTime), CAST(0x0000A5FC0129D440 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1383, N'730511', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D443 AS DateTime), CAST(0x0000A5FC0129D443 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1384, N'182776', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D445 AS DateTime), CAST(0x0000A5FC0129D445 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1385, N'488343', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D448 AS DateTime), CAST(0x0000A5FC0129D448 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1386, N'135919', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D44C AS DateTime), CAST(0x0000A5FC0129D44C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1387, N'687418', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D44F AS DateTime), CAST(0x0000A5FC0129D44F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1388, N'865720', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D452 AS DateTime), CAST(0x0000A5FC0129D452 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1389, N'148707', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D455 AS DateTime), CAST(0x0000A5FC0129D455 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1390, N'335744', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D459 AS DateTime), CAST(0x0000A5FC0129D459 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1391, N'197917', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D45B AS DateTime), CAST(0x0000A5FC0129D45B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1392, N'416805', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D45F AS DateTime), CAST(0x0000A5FC0129D45F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1393, N'928471', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D462 AS DateTime), CAST(0x0000A5FC0129D462 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1394, N'810434', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D465 AS DateTime), CAST(0x0000A5FC0129D465 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1395, N'716271', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D467 AS DateTime), CAST(0x0000A5FC0129D467 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1396, N'546406', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D46B AS DateTime), CAST(0x0000A5FC0129D46B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1397, N'777906', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D46D AS DateTime), CAST(0x0000A5FC0129D46D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1398, N'419650', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D470 AS DateTime), CAST(0x0000A5FC0129D470 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1399, N'312136', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D474 AS DateTime), CAST(0x0000A5FC0129D474 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1400, N'929016', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D476 AS DateTime), CAST(0x0000A5FC0129D476 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1401, N'954984', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D479 AS DateTime), CAST(0x0000A5FC0129D479 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1402, N'524543', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D47C AS DateTime), CAST(0x0000A5FC0129D47C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1403, N'332361', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D47F AS DateTime), CAST(0x0000A5FC0129D47F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1404, N'799374', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D481 AS DateTime), CAST(0x0000A5FC0129D481 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1405, N'178645', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D484 AS DateTime), CAST(0x0000A5FC0129D484 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1406, N'296330', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D486 AS DateTime), CAST(0x0000A5FC0129D486 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1407, N'843001', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D48C AS DateTime), CAST(0x0000A5FC0129D48C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1408, N'574429', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D48E AS DateTime), CAST(0x0000A5FC0129D48E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1409, N'363039', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D491 AS DateTime), CAST(0x0000A5FC0129D491 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1410, N'883518', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D494 AS DateTime), CAST(0x0000A5FC0129D494 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1411, N'191797', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D496 AS DateTime), CAST(0x0000A5FC0129D496 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1412, N'863871', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D499 AS DateTime), CAST(0x0000A5FC0129D499 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1413, N'245975', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D49C AS DateTime), CAST(0x0000A5FC0129D49C AS DateTime))
GO
print 'Processed 1400 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1414, N'184335', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D49E AS DateTime), CAST(0x0000A5FC0129D49E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1415, N'269910', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4A1 AS DateTime), CAST(0x0000A5FC0129D4A1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1416, N'169817', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4A4 AS DateTime), CAST(0x0000A5FC0129D4A4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1417, N'578213', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4A7 AS DateTime), CAST(0x0000A5FC0129D4A7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1418, N'996701', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4AA AS DateTime), CAST(0x0000A5FC0129D4AA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1419, N'534318', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4AD AS DateTime), CAST(0x0000A5FC0129D4AD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1420, N'199830', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4B1 AS DateTime), CAST(0x0000A5FC0129D4B1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1421, N'171982', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4B4 AS DateTime), CAST(0x0000A5FC0129D4B4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1422, N'709659', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4B7 AS DateTime), CAST(0x0000A5FC0129D4B7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1423, N'540861', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4BA AS DateTime), CAST(0x0000A5FC0129D4BA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1424, N'278633', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4BC AS DateTime), CAST(0x0000A5FC0129D4BC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1425, N'816360', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4BF AS DateTime), CAST(0x0000A5FC0129D4BF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1426, N'580618', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4C2 AS DateTime), CAST(0x0000A5FC0129D4C2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1427, N'790549', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4C5 AS DateTime), CAST(0x0000A5FC0129D4C5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1428, N'266175', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4C9 AS DateTime), CAST(0x0000A5FC0129D4C9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1429, N'622577', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4CB AS DateTime), CAST(0x0000A5FC0129D4CB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1430, N'249510', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4CF AS DateTime), CAST(0x0000A5FC0129D4CF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1431, N'209969', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4D2 AS DateTime), CAST(0x0000A5FC0129D4D2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1432, N'962411', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4D5 AS DateTime), CAST(0x0000A5FC0129D4D5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1433, N'116145', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4D8 AS DateTime), CAST(0x0000A5FC0129D4D8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1434, N'207191', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4DA AS DateTime), CAST(0x0000A5FC0129D4DA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1435, N'542838', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4DD AS DateTime), CAST(0x0000A5FC0129D4DD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1436, N'118061', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4E0 AS DateTime), CAST(0x0000A5FC0129D4E0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1437, N'830508', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4E8 AS DateTime), CAST(0x0000A5FC0129D4E8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1438, N'764010', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4EB AS DateTime), CAST(0x0000A5FC0129D4EB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1439, N'375783', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4EF AS DateTime), CAST(0x0000A5FC0129D4EF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1440, N'807330', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4F1 AS DateTime), CAST(0x0000A5FC0129D4F1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1441, N'382000', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4F4 AS DateTime), CAST(0x0000A5FC0129D4F4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1442, N'993365', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4F7 AS DateTime), CAST(0x0000A5FC0129D4F7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1443, N'400949', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D4F9 AS DateTime), CAST(0x0000A5FC0129D4F9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1444, N'984646', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D501 AS DateTime), CAST(0x0000A5FC0129D501 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1445, N'498707', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D55E AS DateTime), CAST(0x0000A5FC0129D55E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1446, N'552620', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D562 AS DateTime), CAST(0x0000A5FC0129D562 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1447, N'679128', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D565 AS DateTime), CAST(0x0000A5FC0129D565 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1448, N'155733', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D568 AS DateTime), CAST(0x0000A5FC0129D568 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1449, N'324708', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D56B AS DateTime), CAST(0x0000A5FC0129D56B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1450, N'353713', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D56E AS DateTime), CAST(0x0000A5FC0129D56E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1451, N'429400', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D571 AS DateTime), CAST(0x0000A5FC0129D571 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1452, N'786317', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D573 AS DateTime), CAST(0x0000A5FC0129D573 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1453, N'704292', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D576 AS DateTime), CAST(0x0000A5FC0129D576 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1454, N'310927', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D57A AS DateTime), CAST(0x0000A5FC0129D57A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1455, N'919608', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D57C AS DateTime), CAST(0x0000A5FC0129D57C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1456, N'766585', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D589 AS DateTime), CAST(0x0000A5FC0129D589 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1457, N'940963', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D58C AS DateTime), CAST(0x0000A5FC0129D58C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1458, N'904184', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D58F AS DateTime), CAST(0x0000A5FC0129D58F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1459, N'160858', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D592 AS DateTime), CAST(0x0000A5FC0129D592 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1460, N'552381', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D595 AS DateTime), CAST(0x0000A5FC0129D595 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1461, N'967340', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D597 AS DateTime), CAST(0x0000A5FC0129D597 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1462, N'447804', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D59A AS DateTime), CAST(0x0000A5FC0129D59A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1463, N'383190', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D59E AS DateTime), CAST(0x0000A5FC0129D59E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1464, N'535820', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5A2 AS DateTime), CAST(0x0000A5FC0129D5A2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1465, N'688851', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5A5 AS DateTime), CAST(0x0000A5FC0129D5A5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1466, N'512703', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5A8 AS DateTime), CAST(0x0000A5FC0129D5A8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1467, N'725692', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5AB AS DateTime), CAST(0x0000A5FC0129D5AB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1468, N'473863', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5AE AS DateTime), CAST(0x0000A5FC0129D5AE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1469, N'124184', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5B1 AS DateTime), CAST(0x0000A5FC0129D5B1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1470, N'644863', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5B3 AS DateTime), CAST(0x0000A5FC0129D5B3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1471, N'190837', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5B6 AS DateTime), CAST(0x0000A5FC0129D5B6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1472, N'989858', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5B9 AS DateTime), CAST(0x0000A5FC0129D5B9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1473, N'215551', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5BB AS DateTime), CAST(0x0000A5FC0129D5BB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1474, N'881312', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5BE AS DateTime), CAST(0x0000A5FC0129D5BE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1475, N'358464', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5C1 AS DateTime), CAST(0x0000A5FC0129D5C1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1476, N'530270', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5C4 AS DateTime), CAST(0x0000A5FC0129D5C4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1477, N'495942', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5C8 AS DateTime), CAST(0x0000A5FC0129D5C8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1478, N'763669', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5CB AS DateTime), CAST(0x0000A5FC0129D5CB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1479, N'398020', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5CE AS DateTime), CAST(0x0000A5FC0129D5CE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1480, N'858859', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5D0 AS DateTime), CAST(0x0000A5FC0129D5D0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1481, N'205828', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5D4 AS DateTime), CAST(0x0000A5FC0129D5D4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1482, N'323792', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5D7 AS DateTime), CAST(0x0000A5FC0129D5D7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1483, N'725301', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5DA AS DateTime), CAST(0x0000A5FC0129D5DA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1484, N'338421', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5DD AS DateTime), CAST(0x0000A5FC0129D5DD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1485, N'774730', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5E0 AS DateTime), CAST(0x0000A5FC0129D5E0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1486, N'183449', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5E3 AS DateTime), CAST(0x0000A5FC0129D5E3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1487, N'261391', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5E6 AS DateTime), CAST(0x0000A5FC0129D5E6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1488, N'395002', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5E9 AS DateTime), CAST(0x0000A5FC0129D5E9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1489, N'618707', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5EC AS DateTime), CAST(0x0000A5FC0129D5EC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1490, N'415766', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5EF AS DateTime), CAST(0x0000A5FC0129D5EF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1491, N'559078', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5F1 AS DateTime), CAST(0x0000A5FC0129D5F1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1492, N'907245', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5F4 AS DateTime), CAST(0x0000A5FC0129D5F4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1493, N'643724', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5F7 AS DateTime), CAST(0x0000A5FC0129D5F7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1494, N'941769', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5FB AS DateTime), CAST(0x0000A5FC0129D5FB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1495, N'601125', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D5FE AS DateTime), CAST(0x0000A5FC0129D5FE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1496, N'179350', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D602 AS DateTime), CAST(0x0000A5FC0129D602 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1497, N'872552', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D607 AS DateTime), CAST(0x0000A5FC0129D607 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1498, N'382333', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D60A AS DateTime), CAST(0x0000A5FC0129D60A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1499, N'744845', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D60D AS DateTime), CAST(0x0000A5FC0129D60D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1500, N'954685', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D610 AS DateTime), CAST(0x0000A5FC0129D610 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1501, N'622876', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D612 AS DateTime), CAST(0x0000A5FC0129D612 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1502, N'981467', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D615 AS DateTime), CAST(0x0000A5FC0129D615 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1503, N'279419', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D618 AS DateTime), CAST(0x0000A5FC0129D618 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1504, N'503409', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D61C AS DateTime), CAST(0x0000A5FC0129D61C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1505, N'575708', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D61E AS DateTime), CAST(0x0000A5FC0129D61E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1506, N'382587', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D621 AS DateTime), CAST(0x0000A5FC0129D621 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1507, N'709648', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D624 AS DateTime), CAST(0x0000A5FC0129D624 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1508, N'814742', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D627 AS DateTime), CAST(0x0000A5FC0129D627 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1509, N'966320', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D62A AS DateTime), CAST(0x0000A5FC0129D62A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1510, N'884474', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D62D AS DateTime), CAST(0x0000A5FC0129D62D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1511, N'159770', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D630 AS DateTime), CAST(0x0000A5FC0129D630 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1512, N'211927', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D632 AS DateTime), CAST(0x0000A5FC0129D632 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1513, N'445294', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D635 AS DateTime), CAST(0x0000A5FC0129D635 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1514, N'736181', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D638 AS DateTime), CAST(0x0000A5FC0129D638 AS DateTime))
GO
print 'Processed 1500 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1515, N'503606', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D63B AS DateTime), CAST(0x0000A5FC0129D63B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1516, N'356255', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D63E AS DateTime), CAST(0x0000A5FC0129D63E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1517, N'665789', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D641 AS DateTime), CAST(0x0000A5FC0129D641 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1518, N'892330', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D644 AS DateTime), CAST(0x0000A5FC0129D644 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1519, N'885531', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D647 AS DateTime), CAST(0x0000A5FC0129D647 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1520, N'686185', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D64A AS DateTime), CAST(0x0000A5FC0129D64A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1521, N'589599', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D64D AS DateTime), CAST(0x0000A5FC0129D64D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1522, N'826640', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D650 AS DateTime), CAST(0x0000A5FC0129D650 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1523, N'636054', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D653 AS DateTime), CAST(0x0000A5FC0129D653 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1524, N'343001', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D656 AS DateTime), CAST(0x0000A5FC0129D656 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1525, N'389705', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D659 AS DateTime), CAST(0x0000A5FC0129D659 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1526, N'794519', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D65C AS DateTime), CAST(0x0000A5FC0129D65C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1527, N'945219', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D65F AS DateTime), CAST(0x0000A5FC0129D65F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1528, N'996934', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D662 AS DateTime), CAST(0x0000A5FC0129D662 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1529, N'681407', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D665 AS DateTime), CAST(0x0000A5FC0129D665 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1530, N'531393', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D667 AS DateTime), CAST(0x0000A5FC0129D667 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1531, N'911587', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D66A AS DateTime), CAST(0x0000A5FC0129D66A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1532, N'841084', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D66E AS DateTime), CAST(0x0000A5FC0129D66E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1533, N'675231', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D673 AS DateTime), CAST(0x0000A5FC0129D673 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1534, N'286998', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D677 AS DateTime), CAST(0x0000A5FC0129D677 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1535, N'356039', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D67B AS DateTime), CAST(0x0000A5FC0129D67B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1536, N'503254', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D67F AS DateTime), CAST(0x0000A5FC0129D67F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1537, N'765965', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D683 AS DateTime), CAST(0x0000A5FC0129D683 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1538, N'944693', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D686 AS DateTime), CAST(0x0000A5FC0129D686 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1539, N'434456', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D68A AS DateTime), CAST(0x0000A5FC0129D68A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1540, N'247823', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D68C AS DateTime), CAST(0x0000A5FC0129D68C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1541, N'567059', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D68F AS DateTime), CAST(0x0000A5FC0129D68F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1542, N'879391', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D692 AS DateTime), CAST(0x0000A5FC0129D692 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1543, N'788932', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D695 AS DateTime), CAST(0x0000A5FC0129D695 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1544, N'470438', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D698 AS DateTime), CAST(0x0000A5FC0129D698 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1545, N'802857', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D69B AS DateTime), CAST(0x0000A5FC0129D69B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1546, N'890253', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D69E AS DateTime), CAST(0x0000A5FC0129D69E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1547, N'117191', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D6A1 AS DateTime), CAST(0x0000A5FC0129D6A1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1548, N'507519', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D6A4 AS DateTime), CAST(0x0000A5FC0129D6A4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1549, N'585700', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D6A7 AS DateTime), CAST(0x0000A5FC0129D6A7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1550, N'609422', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D6A9 AS DateTime), CAST(0x0000A5FC0129D6A9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1551, N'182337', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D707 AS DateTime), CAST(0x0000A5FC0129D707 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1552, N'802561', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D70A AS DateTime), CAST(0x0000A5FC0129D70A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1553, N'274979', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D70C AS DateTime), CAST(0x0000A5FC0129D70C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1554, N'904507', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D710 AS DateTime), CAST(0x0000A5FC0129D710 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1555, N'184210', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D713 AS DateTime), CAST(0x0000A5FC0129D713 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1556, N'134260', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D718 AS DateTime), CAST(0x0000A5FC0129D718 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1557, N'931077', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D71B AS DateTime), CAST(0x0000A5FC0129D71B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1558, N'864219', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D71D AS DateTime), CAST(0x0000A5FC0129D71D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1559, N'926846', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D720 AS DateTime), CAST(0x0000A5FC0129D720 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1560, N'421805', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D723 AS DateTime), CAST(0x0000A5FC0129D723 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1561, N'124762', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D726 AS DateTime), CAST(0x0000A5FC0129D726 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1562, N'349338', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D728 AS DateTime), CAST(0x0000A5FC0129D728 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1563, N'942603', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D72B AS DateTime), CAST(0x0000A5FC0129D72B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1564, N'667704', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D72D AS DateTime), CAST(0x0000A5FC0129D72D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1565, N'934395', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D730 AS DateTime), CAST(0x0000A5FC0129D730 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1566, N'534258', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D733 AS DateTime), CAST(0x0000A5FC0129D733 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1567, N'109060', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D736 AS DateTime), CAST(0x0000A5FC0129D736 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1568, N'970866', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D738 AS DateTime), CAST(0x0000A5FC0129D738 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1569, N'611471', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D73B AS DateTime), CAST(0x0000A5FC0129D73B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1570, N'381070', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D73E AS DateTime), CAST(0x0000A5FC0129D73E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1571, N'160729', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D744 AS DateTime), CAST(0x0000A5FC0129D744 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1572, N'533355', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D747 AS DateTime), CAST(0x0000A5FC0129D747 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1573, N'124436', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D74A AS DateTime), CAST(0x0000A5FC0129D74A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1574, N'915594', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D74E AS DateTime), CAST(0x0000A5FC0129D74E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1575, N'334920', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D751 AS DateTime), CAST(0x0000A5FC0129D751 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1576, N'749958', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D754 AS DateTime), CAST(0x0000A5FC0129D754 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1577, N'579936', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D757 AS DateTime), CAST(0x0000A5FC0129D757 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1578, N'977561', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D763 AS DateTime), CAST(0x0000A5FC0129D763 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1579, N'478669', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D76C AS DateTime), CAST(0x0000A5FC0129D76C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1580, N'601275', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D76F AS DateTime), CAST(0x0000A5FC0129D76F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1581, N'341767', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D772 AS DateTime), CAST(0x0000A5FC0129D772 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1582, N'778624', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D775 AS DateTime), CAST(0x0000A5FC0129D775 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1583, N'357677', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D779 AS DateTime), CAST(0x0000A5FC0129D779 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1584, N'394158', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D77B AS DateTime), CAST(0x0000A5FC0129D77B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1585, N'853496', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D77E AS DateTime), CAST(0x0000A5FC0129D77E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1586, N'468419', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D781 AS DateTime), CAST(0x0000A5FC0129D781 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1587, N'120727', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D7DE AS DateTime), CAST(0x0000A5FC0129D7DE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1588, N'454958', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D7E2 AS DateTime), CAST(0x0000A5FC0129D7E2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1589, N'450545', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D7E5 AS DateTime), CAST(0x0000A5FC0129D7E5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1590, N'605875', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D7E9 AS DateTime), CAST(0x0000A5FC0129D7E9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1591, N'598363', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D7ED AS DateTime), CAST(0x0000A5FC0129D7ED AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1592, N'568775', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D7F0 AS DateTime), CAST(0x0000A5FC0129D7F0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1593, N'267623', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D7F3 AS DateTime), CAST(0x0000A5FC0129D7F3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1594, N'495110', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D7F6 AS DateTime), CAST(0x0000A5FC0129D7F6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1595, N'381151', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D7F8 AS DateTime), CAST(0x0000A5FC0129D7F8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1596, N'254792', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D7FB AS DateTime), CAST(0x0000A5FC0129D7FB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1597, N'955326', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D7FF AS DateTime), CAST(0x0000A5FC0129D7FF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1598, N'898970', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D802 AS DateTime), CAST(0x0000A5FC0129D802 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1599, N'630665', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D806 AS DateTime), CAST(0x0000A5FC0129D806 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1600, N'531029', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D80C AS DateTime), CAST(0x0000A5FC0129D80C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1601, N'318427', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D80F AS DateTime), CAST(0x0000A5FC0129D80F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1602, N'404347', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D812 AS DateTime), CAST(0x0000A5FC0129D812 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1603, N'706845', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D814 AS DateTime), CAST(0x0000A5FC0129D814 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1604, N'247116', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D818 AS DateTime), CAST(0x0000A5FC0129D818 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1605, N'144684', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D81B AS DateTime), CAST(0x0000A5FC0129D81B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1606, N'806831', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D81E AS DateTime), CAST(0x0000A5FC0129D81E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1607, N'132355', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D821 AS DateTime), CAST(0x0000A5FC0129D821 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1608, N'425585', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D824 AS DateTime), CAST(0x0000A5FC0129D824 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1609, N'563520', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D827 AS DateTime), CAST(0x0000A5FC0129D827 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1610, N'902429', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D82A AS DateTime), CAST(0x0000A5FC0129D82A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1611, N'416773', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D82D AS DateTime), CAST(0x0000A5FC0129D82D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1612, N'587366', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D830 AS DateTime), CAST(0x0000A5FC0129D830 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1613, N'427370', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D833 AS DateTime), CAST(0x0000A5FC0129D833 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1614, N'633898', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D836 AS DateTime), CAST(0x0000A5FC0129D836 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1615, N'551387', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D838 AS DateTime), CAST(0x0000A5FC0129D838 AS DateTime))
GO
print 'Processed 1600 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1616, N'680512', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D83C AS DateTime), CAST(0x0000A5FC0129D83C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1617, N'418780', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D83E AS DateTime), CAST(0x0000A5FC0129D83E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1618, N'660616', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D842 AS DateTime), CAST(0x0000A5FC0129D842 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1619, N'820727', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D845 AS DateTime), CAST(0x0000A5FC0129D845 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1620, N'232840', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D847 AS DateTime), CAST(0x0000A5FC0129D847 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1621, N'962175', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D84E AS DateTime), CAST(0x0000A5FC0129D84E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1622, N'911894', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D851 AS DateTime), CAST(0x0000A5FC0129D851 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1623, N'197885', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D854 AS DateTime), CAST(0x0000A5FC0129D854 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1624, N'225474', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D857 AS DateTime), CAST(0x0000A5FC0129D857 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1625, N'449023', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D85A AS DateTime), CAST(0x0000A5FC0129D85A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1626, N'556234', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D85D AS DateTime), CAST(0x0000A5FC0129D85D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1627, N'766617', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D861 AS DateTime), CAST(0x0000A5FC0129D861 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1628, N'230240', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D864 AS DateTime), CAST(0x0000A5FC0129D864 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1629, N'105261', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D867 AS DateTime), CAST(0x0000A5FC0129D867 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1630, N'310436', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D86B AS DateTime), CAST(0x0000A5FC0129D86B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1631, N'597390', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D86E AS DateTime), CAST(0x0000A5FC0129D86E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1632, N'535987', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D871 AS DateTime), CAST(0x0000A5FC0129D871 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1633, N'316210', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D874 AS DateTime), CAST(0x0000A5FC0129D874 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1634, N'772041', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D877 AS DateTime), CAST(0x0000A5FC0129D877 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1635, N'743004', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D87A AS DateTime), CAST(0x0000A5FC0129D87A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1636, N'194751', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D87D AS DateTime), CAST(0x0000A5FC0129D87D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1637, N'241488', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D884 AS DateTime), CAST(0x0000A5FC0129D884 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1638, N'801267', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D888 AS DateTime), CAST(0x0000A5FC0129D888 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1639, N'864271', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D88B AS DateTime), CAST(0x0000A5FC0129D88B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1640, N'617298', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D88E AS DateTime), CAST(0x0000A5FC0129D88E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1641, N'280710', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D892 AS DateTime), CAST(0x0000A5FC0129D892 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1642, N'196168', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D895 AS DateTime), CAST(0x0000A5FC0129D895 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1643, N'707225', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D898 AS DateTime), CAST(0x0000A5FC0129D898 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1644, N'846687', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D89A AS DateTime), CAST(0x0000A5FC0129D89A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1645, N'861960', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D89E AS DateTime), CAST(0x0000A5FC0129D89E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1646, N'502421', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D8A1 AS DateTime), CAST(0x0000A5FC0129D8A1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1647, N'203126', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D8A4 AS DateTime), CAST(0x0000A5FC0129D8A4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1648, N'326149', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D901 AS DateTime), CAST(0x0000A5FC0129D901 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1649, N'857482', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D904 AS DateTime), CAST(0x0000A5FC0129D904 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1650, N'373715', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D907 AS DateTime), CAST(0x0000A5FC0129D907 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1651, N'638383', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D90A AS DateTime), CAST(0x0000A5FC0129D90A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1652, N'462588', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D90C AS DateTime), CAST(0x0000A5FC0129D90C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1653, N'829484', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D910 AS DateTime), CAST(0x0000A5FC0129D910 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1654, N'828579', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D913 AS DateTime), CAST(0x0000A5FC0129D913 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1655, N'164311', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D916 AS DateTime), CAST(0x0000A5FC0129D916 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1656, N'808660', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D919 AS DateTime), CAST(0x0000A5FC0129D919 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1657, N'359705', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D91C AS DateTime), CAST(0x0000A5FC0129D91C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1658, N'630499', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D920 AS DateTime), CAST(0x0000A5FC0129D920 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1659, N'920734', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D928 AS DateTime), CAST(0x0000A5FC0129D928 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1660, N'617481', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D92B AS DateTime), CAST(0x0000A5FC0129D92B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1661, N'169582', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D92E AS DateTime), CAST(0x0000A5FC0129D92E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1662, N'205777', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D931 AS DateTime), CAST(0x0000A5FC0129D931 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1663, N'208009', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D934 AS DateTime), CAST(0x0000A5FC0129D934 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1664, N'916815', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D938 AS DateTime), CAST(0x0000A5FC0129D938 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1665, N'611092', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D93D AS DateTime), CAST(0x0000A5FC0129D93D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1666, N'411266', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D941 AS DateTime), CAST(0x0000A5FC0129D941 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1667, N'444313', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D943 AS DateTime), CAST(0x0000A5FC0129D943 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1668, N'129239', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D946 AS DateTime), CAST(0x0000A5FC0129D946 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1669, N'231062', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D949 AS DateTime), CAST(0x0000A5FC0129D949 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1670, N'429963', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D94C AS DateTime), CAST(0x0000A5FC0129D94C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1671, N'358930', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D94F AS DateTime), CAST(0x0000A5FC0129D94F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1672, N'282780', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D951 AS DateTime), CAST(0x0000A5FC0129D951 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1673, N'485189', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D953 AS DateTime), CAST(0x0000A5FC0129D953 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1674, N'576775', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D956 AS DateTime), CAST(0x0000A5FC0129D956 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1675, N'844275', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D959 AS DateTime), CAST(0x0000A5FC0129D959 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1676, N'139806', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D95C AS DateTime), CAST(0x0000A5FC0129D95C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1677, N'409248', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D95F AS DateTime), CAST(0x0000A5FC0129D95F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1678, N'720492', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D961 AS DateTime), CAST(0x0000A5FC0129D961 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1679, N'130722', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D964 AS DateTime), CAST(0x0000A5FC0129D964 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1680, N'558220', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D967 AS DateTime), CAST(0x0000A5FC0129D967 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1681, N'462684', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D96A AS DateTime), CAST(0x0000A5FC0129D96A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1682, N'617295', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D96C AS DateTime), CAST(0x0000A5FC0129D96C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1683, N'361965', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D96F AS DateTime), CAST(0x0000A5FC0129D96F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1684, N'595097', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D972 AS DateTime), CAST(0x0000A5FC0129D972 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1685, N'703399', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D975 AS DateTime), CAST(0x0000A5FC0129D975 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1686, N'117090', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D978 AS DateTime), CAST(0x0000A5FC0129D978 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1687, N'938814', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D97A AS DateTime), CAST(0x0000A5FC0129D97A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1688, N'402995', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D97F AS DateTime), CAST(0x0000A5FC0129D97F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1689, N'192216', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D982 AS DateTime), CAST(0x0000A5FC0129D982 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1690, N'209232', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D985 AS DateTime), CAST(0x0000A5FC0129D985 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1691, N'104043', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D987 AS DateTime), CAST(0x0000A5FC0129D987 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1692, N'465667', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D98A AS DateTime), CAST(0x0000A5FC0129D98A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1693, N'941288', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D98C AS DateTime), CAST(0x0000A5FC0129D98C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1694, N'777826', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D98F AS DateTime), CAST(0x0000A5FC0129D98F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1695, N'243575', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D991 AS DateTime), CAST(0x0000A5FC0129D991 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1696, N'323131', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D994 AS DateTime), CAST(0x0000A5FC0129D994 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1697, N'255555', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D997 AS DateTime), CAST(0x0000A5FC0129D997 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1698, N'584589', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D999 AS DateTime), CAST(0x0000A5FC0129D999 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1699, N'891515', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D99C AS DateTime), CAST(0x0000A5FC0129D99C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1700, N'878248', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D99F AS DateTime), CAST(0x0000A5FC0129D99F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1701, N'401306', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9A2 AS DateTime), CAST(0x0000A5FC0129D9A2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1702, N'911531', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9A5 AS DateTime), CAST(0x0000A5FC0129D9A5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1703, N'405844', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9A8 AS DateTime), CAST(0x0000A5FC0129D9A8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1704, N'169726', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9AA AS DateTime), CAST(0x0000A5FC0129D9AA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1705, N'361750', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9AD AS DateTime), CAST(0x0000A5FC0129D9AD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1706, N'296586', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9B0 AS DateTime), CAST(0x0000A5FC0129D9B0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1707, N'593886', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9B2 AS DateTime), CAST(0x0000A5FC0129D9B2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1708, N'656023', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9B5 AS DateTime), CAST(0x0000A5FC0129D9B5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1709, N'794656', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9B8 AS DateTime), CAST(0x0000A5FC0129D9B8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1710, N'737242', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9BB AS DateTime), CAST(0x0000A5FC0129D9BB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1711, N'571898', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9BF AS DateTime), CAST(0x0000A5FC0129D9BF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1712, N'373675', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9C2 AS DateTime), CAST(0x0000A5FC0129D9C2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1713, N'722577', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9C5 AS DateTime), CAST(0x0000A5FC0129D9C5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1714, N'969451', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9C9 AS DateTime), CAST(0x0000A5FC0129D9C9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1715, N'571179', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9CD AS DateTime), CAST(0x0000A5FC0129D9CD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1716, N'853841', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9D1 AS DateTime), CAST(0x0000A5FC0129D9D1 AS DateTime))
GO
print 'Processed 1700 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1717, N'916181', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9D6 AS DateTime), CAST(0x0000A5FC0129D9D6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1718, N'903374', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9DA AS DateTime), CAST(0x0000A5FC0129D9DA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1719, N'319120', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9DD AS DateTime), CAST(0x0000A5FC0129D9DD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1720, N'811164', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9E0 AS DateTime), CAST(0x0000A5FC0129D9E0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1721, N'614464', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9E3 AS DateTime), CAST(0x0000A5FC0129D9E3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1722, N'442692', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9E6 AS DateTime), CAST(0x0000A5FC0129D9E6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1723, N'373162', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129D9EC AS DateTime), CAST(0x0000A5FC0129D9EC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1724, N'758293', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DA49 AS DateTime), CAST(0x0000A5FC0129DA49 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1725, N'703736', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DA4C AS DateTime), CAST(0x0000A5FC0129DA4C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1726, N'326395', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DA4F AS DateTime), CAST(0x0000A5FC0129DA4F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1727, N'488067', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DA52 AS DateTime), CAST(0x0000A5FC0129DA52 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1728, N'296581', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DA55 AS DateTime), CAST(0x0000A5FC0129DA55 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1729, N'300856', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAB4 AS DateTime), CAST(0x0000A5FC0129DAB4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1730, N'621359', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAB7 AS DateTime), CAST(0x0000A5FC0129DAB7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1731, N'856407', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DABA AS DateTime), CAST(0x0000A5FC0129DABA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1732, N'698676', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DABD AS DateTime), CAST(0x0000A5FC0129DABD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1733, N'242228', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DABF AS DateTime), CAST(0x0000A5FC0129DABF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1734, N'357533', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAC2 AS DateTime), CAST(0x0000A5FC0129DAC2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1735, N'279439', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAC5 AS DateTime), CAST(0x0000A5FC0129DAC5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1736, N'838860', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAC9 AS DateTime), CAST(0x0000A5FC0129DAC9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1737, N'410846', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DACB AS DateTime), CAST(0x0000A5FC0129DACB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1738, N'744745', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DACE AS DateTime), CAST(0x0000A5FC0129DACE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1739, N'992557', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAD2 AS DateTime), CAST(0x0000A5FC0129DAD2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1740, N'138714', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAD4 AS DateTime), CAST(0x0000A5FC0129DAD4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1741, N'628595', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAD7 AS DateTime), CAST(0x0000A5FC0129DAD7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1742, N'858280', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DADA AS DateTime), CAST(0x0000A5FC0129DADA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1743, N'339410', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DADD AS DateTime), CAST(0x0000A5FC0129DADD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1744, N'197835', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DADF AS DateTime), CAST(0x0000A5FC0129DADF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1745, N'668462', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAE3 AS DateTime), CAST(0x0000A5FC0129DAE3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1746, N'934377', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAE7 AS DateTime), CAST(0x0000A5FC0129DAE7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1747, N'461861', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAEA AS DateTime), CAST(0x0000A5FC0129DAEA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1748, N'351459', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAED AS DateTime), CAST(0x0000A5FC0129DAED AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1749, N'911848', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAF0 AS DateTime), CAST(0x0000A5FC0129DAF0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1750, N'281040', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAF3 AS DateTime), CAST(0x0000A5FC0129DAF3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1751, N'305224', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAF5 AS DateTime), CAST(0x0000A5FC0129DAF5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1752, N'885547', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAF8 AS DateTime), CAST(0x0000A5FC0129DAF8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1753, N'594458', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAFB AS DateTime), CAST(0x0000A5FC0129DAFB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1754, N'551200', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DAFE AS DateTime), CAST(0x0000A5FC0129DAFE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1755, N'174242', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB01 AS DateTime), CAST(0x0000A5FC0129DB01 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1756, N'482637', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB04 AS DateTime), CAST(0x0000A5FC0129DB04 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1757, N'294883', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB07 AS DateTime), CAST(0x0000A5FC0129DB07 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1758, N'670283', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB0A AS DateTime), CAST(0x0000A5FC0129DB0A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1759, N'467704', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB0E AS DateTime), CAST(0x0000A5FC0129DB0E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1760, N'792189', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB10 AS DateTime), CAST(0x0000A5FC0129DB10 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1761, N'561746', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB13 AS DateTime), CAST(0x0000A5FC0129DB13 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1762, N'585282', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB16 AS DateTime), CAST(0x0000A5FC0129DB16 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1763, N'745920', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB19 AS DateTime), CAST(0x0000A5FC0129DB19 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1764, N'475047', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB1C AS DateTime), CAST(0x0000A5FC0129DB1C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1765, N'940336', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB1F AS DateTime), CAST(0x0000A5FC0129DB1F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1766, N'282283', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB22 AS DateTime), CAST(0x0000A5FC0129DB22 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1767, N'161514', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB25 AS DateTime), CAST(0x0000A5FC0129DB25 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1768, N'482409', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB28 AS DateTime), CAST(0x0000A5FC0129DB28 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1769, N'965461', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB2B AS DateTime), CAST(0x0000A5FC0129DB2B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1770, N'294159', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB2D AS DateTime), CAST(0x0000A5FC0129DB2D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1771, N'896056', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB30 AS DateTime), CAST(0x0000A5FC0129DB30 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1772, N'550144', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB33 AS DateTime), CAST(0x0000A5FC0129DB33 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1773, N'115017', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB35 AS DateTime), CAST(0x0000A5FC0129DB35 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1774, N'548077', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB38 AS DateTime), CAST(0x0000A5FC0129DB38 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1775, N'992757', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB3B AS DateTime), CAST(0x0000A5FC0129DB3B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1776, N'159605', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB3E AS DateTime), CAST(0x0000A5FC0129DB3E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1777, N'614427', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB40 AS DateTime), CAST(0x0000A5FC0129DB40 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1778, N'305665', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB44 AS DateTime), CAST(0x0000A5FC0129DB44 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1779, N'582823', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB46 AS DateTime), CAST(0x0000A5FC0129DB46 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1780, N'459632', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB49 AS DateTime), CAST(0x0000A5FC0129DB49 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1781, N'561395', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB4C AS DateTime), CAST(0x0000A5FC0129DB4C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1782, N'567917', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB4E AS DateTime), CAST(0x0000A5FC0129DB4E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1783, N'105074', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB51 AS DateTime), CAST(0x0000A5FC0129DB51 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1784, N'576298', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB54 AS DateTime), CAST(0x0000A5FC0129DB54 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1785, N'419752', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB57 AS DateTime), CAST(0x0000A5FC0129DB57 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1786, N'146813', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB5A AS DateTime), CAST(0x0000A5FC0129DB5A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1787, N'114552', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB5D AS DateTime), CAST(0x0000A5FC0129DB5D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1788, N'554459', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB5F AS DateTime), CAST(0x0000A5FC0129DB5F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1789, N'329130', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB64 AS DateTime), CAST(0x0000A5FC0129DB64 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1790, N'227047', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DB68 AS DateTime), CAST(0x0000A5FC0129DB68 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1791, N'778205', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DBC7 AS DateTime), CAST(0x0000A5FC0129DBC7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1792, N'764078', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DBCA AS DateTime), CAST(0x0000A5FC0129DBCA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1793, N'970053', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DBCC AS DateTime), CAST(0x0000A5FC0129DBCC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1794, N'661159', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DBCF AS DateTime), CAST(0x0000A5FC0129DBCF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1795, N'847551', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DBD2 AS DateTime), CAST(0x0000A5FC0129DBD2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1796, N'630153', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DBD6 AS DateTime), CAST(0x0000A5FC0129DBD6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1797, N'747063', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DBDA AS DateTime), CAST(0x0000A5FC0129DBDA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1798, N'767469', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DBE1 AS DateTime), CAST(0x0000A5FC0129DBE1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1799, N'250540', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DBE6 AS DateTime), CAST(0x0000A5FC0129DBE6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1800, N'374040', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DBEA AS DateTime), CAST(0x0000A5FC0129DBEA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1801, N'375572', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DBEE AS DateTime), CAST(0x0000A5FC0129DBEE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1802, N'295441', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DBF2 AS DateTime), CAST(0x0000A5FC0129DBF2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1803, N'421235', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DBF4 AS DateTime), CAST(0x0000A5FC0129DBF4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1804, N'766997', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DBF7 AS DateTime), CAST(0x0000A5FC0129DBF7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1805, N'430716', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DBFA AS DateTime), CAST(0x0000A5FC0129DBFA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1806, N'280653', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DBFD AS DateTime), CAST(0x0000A5FC0129DBFD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1807, N'842993', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC00 AS DateTime), CAST(0x0000A5FC0129DC00 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1808, N'491600', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC02 AS DateTime), CAST(0x0000A5FC0129DC02 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1809, N'302935', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC06 AS DateTime), CAST(0x0000A5FC0129DC06 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1810, N'258661', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC09 AS DateTime), CAST(0x0000A5FC0129DC09 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1811, N'404453', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC66 AS DateTime), CAST(0x0000A5FC0129DC66 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1812, N'188857', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC68 AS DateTime), CAST(0x0000A5FC0129DC68 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1813, N'432981', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC6B AS DateTime), CAST(0x0000A5FC0129DC6B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1814, N'201494', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC6E AS DateTime), CAST(0x0000A5FC0129DC6E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1815, N'245676', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC71 AS DateTime), CAST(0x0000A5FC0129DC71 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1816, N'757070', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC74 AS DateTime), CAST(0x0000A5FC0129DC74 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1817, N'639777', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC77 AS DateTime), CAST(0x0000A5FC0129DC77 AS DateTime))
GO
print 'Processed 1800 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1818, N'696910', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC7A AS DateTime), CAST(0x0000A5FC0129DC7A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1819, N'896297', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC7E AS DateTime), CAST(0x0000A5FC0129DC7E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1820, N'903120', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC80 AS DateTime), CAST(0x0000A5FC0129DC80 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1821, N'910303', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC83 AS DateTime), CAST(0x0000A5FC0129DC83 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1822, N'733545', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC86 AS DateTime), CAST(0x0000A5FC0129DC86 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1823, N'335047', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC89 AS DateTime), CAST(0x0000A5FC0129DC89 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1824, N'150895', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC8C AS DateTime), CAST(0x0000A5FC0129DC8C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1825, N'750816', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC8E AS DateTime), CAST(0x0000A5FC0129DC8E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1826, N'555601', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC91 AS DateTime), CAST(0x0000A5FC0129DC91 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1827, N'272614', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC95 AS DateTime), CAST(0x0000A5FC0129DC95 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1828, N'234760', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC98 AS DateTime), CAST(0x0000A5FC0129DC98 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1829, N'466805', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC9C AS DateTime), CAST(0x0000A5FC0129DC9C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1830, N'329497', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DC9F AS DateTime), CAST(0x0000A5FC0129DC9F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1831, N'270291', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCA2 AS DateTime), CAST(0x0000A5FC0129DCA2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1832, N'285547', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCA5 AS DateTime), CAST(0x0000A5FC0129DCA5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1833, N'517743', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCA8 AS DateTime), CAST(0x0000A5FC0129DCA8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1834, N'760538', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCAB AS DateTime), CAST(0x0000A5FC0129DCAB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1835, N'542050', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCAE AS DateTime), CAST(0x0000A5FC0129DCAE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1836, N'195218', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCB1 AS DateTime), CAST(0x0000A5FC0129DCB1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1837, N'664954', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCB4 AS DateTime), CAST(0x0000A5FC0129DCB4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1838, N'985375', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCB7 AS DateTime), CAST(0x0000A5FC0129DCB7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1839, N'991956', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCBA AS DateTime), CAST(0x0000A5FC0129DCBA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1840, N'329506', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCBD AS DateTime), CAST(0x0000A5FC0129DCBD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1841, N'868710', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCC0 AS DateTime), CAST(0x0000A5FC0129DCC0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1842, N'900839', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCC3 AS DateTime), CAST(0x0000A5FC0129DCC3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1843, N'660366', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCC6 AS DateTime), CAST(0x0000A5FC0129DCC6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1844, N'776132', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCC8 AS DateTime), CAST(0x0000A5FC0129DCC8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1845, N'709427', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCCB AS DateTime), CAST(0x0000A5FC0129DCCB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1846, N'762014', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCCD AS DateTime), CAST(0x0000A5FC0129DCCD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1847, N'805171', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCD0 AS DateTime), CAST(0x0000A5FC0129DCD0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1848, N'735563', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCD3 AS DateTime), CAST(0x0000A5FC0129DCD3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1849, N'294433', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCD6 AS DateTime), CAST(0x0000A5FC0129DCD6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1850, N'550310', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCDA AS DateTime), CAST(0x0000A5FC0129DCDA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1851, N'425724', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCDD AS DateTime), CAST(0x0000A5FC0129DCDD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1852, N'150913', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCDF AS DateTime), CAST(0x0000A5FC0129DCDF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1853, N'933057', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCE2 AS DateTime), CAST(0x0000A5FC0129DCE2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1854, N'850293', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCE5 AS DateTime), CAST(0x0000A5FC0129DCE5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1855, N'722247', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCE8 AS DateTime), CAST(0x0000A5FC0129DCE8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1856, N'856974', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCEB AS DateTime), CAST(0x0000A5FC0129DCEB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1857, N'121226', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DCED AS DateTime), CAST(0x0000A5FC0129DCED AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1858, N'344342', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD4B AS DateTime), CAST(0x0000A5FC0129DD4B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1859, N'257539', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD4E AS DateTime), CAST(0x0000A5FC0129DD4E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1860, N'940726', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD50 AS DateTime), CAST(0x0000A5FC0129DD50 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1861, N'343482', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD53 AS DateTime), CAST(0x0000A5FC0129DD53 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1862, N'924791', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD56 AS DateTime), CAST(0x0000A5FC0129DD56 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1863, N'846821', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD59 AS DateTime), CAST(0x0000A5FC0129DD59 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1864, N'295925', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD5C AS DateTime), CAST(0x0000A5FC0129DD5C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1865, N'290404', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD5F AS DateTime), CAST(0x0000A5FC0129DD5F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1866, N'360288', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD62 AS DateTime), CAST(0x0000A5FC0129DD62 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1867, N'750304', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD65 AS DateTime), CAST(0x0000A5FC0129DD65 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1868, N'648375', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD68 AS DateTime), CAST(0x0000A5FC0129DD68 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1869, N'784129', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD6B AS DateTime), CAST(0x0000A5FC0129DD6B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1870, N'866437', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD6E AS DateTime), CAST(0x0000A5FC0129DD6E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1871, N'954212', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD70 AS DateTime), CAST(0x0000A5FC0129DD70 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1872, N'693043', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD73 AS DateTime), CAST(0x0000A5FC0129DD73 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1873, N'153788', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD76 AS DateTime), CAST(0x0000A5FC0129DD76 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1874, N'283809', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD79 AS DateTime), CAST(0x0000A5FC0129DD79 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1875, N'435328', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD7C AS DateTime), CAST(0x0000A5FC0129DD7C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1876, N'649244', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD7E AS DateTime), CAST(0x0000A5FC0129DD7E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1877, N'987499', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD81 AS DateTime), CAST(0x0000A5FC0129DD81 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1878, N'831601', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD84 AS DateTime), CAST(0x0000A5FC0129DD84 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1879, N'999377', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD87 AS DateTime), CAST(0x0000A5FC0129DD87 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1880, N'391647', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD8A AS DateTime), CAST(0x0000A5FC0129DD8A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1881, N'726320', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD8D AS DateTime), CAST(0x0000A5FC0129DD8D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1882, N'746110', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD8F AS DateTime), CAST(0x0000A5FC0129DD8F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1883, N'566442', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD96 AS DateTime), CAST(0x0000A5FC0129DD96 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1884, N'810931', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD99 AS DateTime), CAST(0x0000A5FC0129DD99 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1885, N'869081', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DD9C AS DateTime), CAST(0x0000A5FC0129DD9C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1886, N'743952', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DDF9 AS DateTime), CAST(0x0000A5FC0129DDF9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1887, N'198136', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DDFC AS DateTime), CAST(0x0000A5FC0129DDFC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1888, N'273627', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DDFF AS DateTime), CAST(0x0000A5FC0129DDFF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1889, N'164943', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE02 AS DateTime), CAST(0x0000A5FC0129DE02 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1890, N'719110', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE04 AS DateTime), CAST(0x0000A5FC0129DE04 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1891, N'449180', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE07 AS DateTime), CAST(0x0000A5FC0129DE07 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1892, N'278936', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE0A AS DateTime), CAST(0x0000A5FC0129DE0A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1893, N'285370', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE0D AS DateTime), CAST(0x0000A5FC0129DE0D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1894, N'548762', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE10 AS DateTime), CAST(0x0000A5FC0129DE10 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1895, N'704001', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE13 AS DateTime), CAST(0x0000A5FC0129DE13 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1896, N'882385', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE16 AS DateTime), CAST(0x0000A5FC0129DE16 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1897, N'825931', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE19 AS DateTime), CAST(0x0000A5FC0129DE19 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1898, N'166991', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE1B AS DateTime), CAST(0x0000A5FC0129DE1B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1899, N'659794', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE1E AS DateTime), CAST(0x0000A5FC0129DE1E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1900, N'640847', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE21 AS DateTime), CAST(0x0000A5FC0129DE21 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1901, N'213737', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE25 AS DateTime), CAST(0x0000A5FC0129DE25 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1902, N'388029', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE28 AS DateTime), CAST(0x0000A5FC0129DE28 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1903, N'155146', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE2D AS DateTime), CAST(0x0000A5FC0129DE2D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1904, N'701651', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE30 AS DateTime), CAST(0x0000A5FC0129DE30 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1905, N'962694', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE33 AS DateTime), CAST(0x0000A5FC0129DE33 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1906, N'658807', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE35 AS DateTime), CAST(0x0000A5FC0129DE35 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1907, N'260189', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE38 AS DateTime), CAST(0x0000A5FC0129DE38 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1908, N'652608', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE3B AS DateTime), CAST(0x0000A5FC0129DE3B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1909, N'910728', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE3E AS DateTime), CAST(0x0000A5FC0129DE3E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1910, N'554227', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE41 AS DateTime), CAST(0x0000A5FC0129DE41 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1911, N'318876', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE45 AS DateTime), CAST(0x0000A5FC0129DE45 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1912, N'993709', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE48 AS DateTime), CAST(0x0000A5FC0129DE48 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1913, N'576622', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE4D AS DateTime), CAST(0x0000A5FC0129DE4D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1914, N'439083', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE50 AS DateTime), CAST(0x0000A5FC0129DE50 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1915, N'991468', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE53 AS DateTime), CAST(0x0000A5FC0129DE53 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1916, N'914641', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE56 AS DateTime), CAST(0x0000A5FC0129DE56 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1917, N'733837', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE59 AS DateTime), CAST(0x0000A5FC0129DE59 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1918, N'772397', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE5C AS DateTime), CAST(0x0000A5FC0129DE5C AS DateTime))
GO
print 'Processed 1900 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1919, N'347673', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE5F AS DateTime), CAST(0x0000A5FC0129DE5F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1920, N'698588', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE62 AS DateTime), CAST(0x0000A5FC0129DE62 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1921, N'322631', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE65 AS DateTime), CAST(0x0000A5FC0129DE65 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1922, N'289909', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE68 AS DateTime), CAST(0x0000A5FC0129DE68 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1923, N'238302', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE6B AS DateTime), CAST(0x0000A5FC0129DE6B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1924, N'271508', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE6E AS DateTime), CAST(0x0000A5FC0129DE6E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1925, N'266567', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE71 AS DateTime), CAST(0x0000A5FC0129DE71 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1926, N'341254', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE73 AS DateTime), CAST(0x0000A5FC0129DE73 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1927, N'484334', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE76 AS DateTime), CAST(0x0000A5FC0129DE76 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1928, N'296690', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE79 AS DateTime), CAST(0x0000A5FC0129DE79 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1929, N'467028', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE7C AS DateTime), CAST(0x0000A5FC0129DE7C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1930, N'962094', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE7E AS DateTime), CAST(0x0000A5FC0129DE7E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1931, N'969743', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE81 AS DateTime), CAST(0x0000A5FC0129DE81 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1932, N'930334', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE84 AS DateTime), CAST(0x0000A5FC0129DE84 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1933, N'259263', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE88 AS DateTime), CAST(0x0000A5FC0129DE88 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1934, N'797070', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE8D AS DateTime), CAST(0x0000A5FC0129DE8D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1935, N'569688', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE90 AS DateTime), CAST(0x0000A5FC0129DE90 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1936, N'366691', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE93 AS DateTime), CAST(0x0000A5FC0129DE93 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1937, N'672726', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE97 AS DateTime), CAST(0x0000A5FC0129DE97 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1938, N'963797', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE9B AS DateTime), CAST(0x0000A5FC0129DE9B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1939, N'314574', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DE9E AS DateTime), CAST(0x0000A5FC0129DE9E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1940, N'721735', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEA1 AS DateTime), CAST(0x0000A5FC0129DEA1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1941, N'789945', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEA4 AS DateTime), CAST(0x0000A5FC0129DEA4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1942, N'228890', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEA8 AS DateTime), CAST(0x0000A5FC0129DEA8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1943, N'337276', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEAB AS DateTime), CAST(0x0000A5FC0129DEAB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1944, N'795659', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEAE AS DateTime), CAST(0x0000A5FC0129DEAE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1945, N'811781', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEB1 AS DateTime), CAST(0x0000A5FC0129DEB1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1946, N'126358', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEB3 AS DateTime), CAST(0x0000A5FC0129DEB3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1947, N'497080', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEB6 AS DateTime), CAST(0x0000A5FC0129DEB6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1948, N'781012', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEB9 AS DateTime), CAST(0x0000A5FC0129DEB9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1949, N'579328', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEBD AS DateTime), CAST(0x0000A5FC0129DEBD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1950, N'497846', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEC0 AS DateTime), CAST(0x0000A5FC0129DEC0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1951, N'743870', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEC4 AS DateTime), CAST(0x0000A5FC0129DEC4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1952, N'897800', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEC7 AS DateTime), CAST(0x0000A5FC0129DEC7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1953, N'443702', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DECA AS DateTime), CAST(0x0000A5FC0129DECA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1954, N'631545', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DECD AS DateTime), CAST(0x0000A5FC0129DECD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1955, N'292871', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DED0 AS DateTime), CAST(0x0000A5FC0129DED0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1956, N'592704', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DED2 AS DateTime), CAST(0x0000A5FC0129DED2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1957, N'898346', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DED6 AS DateTime), CAST(0x0000A5FC0129DED6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1958, N'835392', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DED8 AS DateTime), CAST(0x0000A5FC0129DED8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1959, N'580740', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEDB AS DateTime), CAST(0x0000A5FC0129DEDB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1960, N'324860', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEDE AS DateTime), CAST(0x0000A5FC0129DEDE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1961, N'260404', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEE1 AS DateTime), CAST(0x0000A5FC0129DEE1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1962, N'159546', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEE4 AS DateTime), CAST(0x0000A5FC0129DEE4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1963, N'698045', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEE7 AS DateTime), CAST(0x0000A5FC0129DEE7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1964, N'976498', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEEA AS DateTime), CAST(0x0000A5FC0129DEEA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1965, N'384298', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEEE AS DateTime), CAST(0x0000A5FC0129DEEE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1966, N'549276', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEF1 AS DateTime), CAST(0x0000A5FC0129DEF1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1967, N'245190', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEF3 AS DateTime), CAST(0x0000A5FC0129DEF3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1968, N'308685', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEF6 AS DateTime), CAST(0x0000A5FC0129DEF6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1969, N'899693', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEF9 AS DateTime), CAST(0x0000A5FC0129DEF9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1970, N'405839', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DEFD AS DateTime), CAST(0x0000A5FC0129DEFD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1971, N'370037', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DF00 AS DateTime), CAST(0x0000A5FC0129DF00 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1972, N'126385', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DF03 AS DateTime), CAST(0x0000A5FC0129DF03 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1973, N'231421', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DF61 AS DateTime), CAST(0x0000A5FC0129DF61 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1974, N'953931', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DF64 AS DateTime), CAST(0x0000A5FC0129DF64 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1975, N'889621', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DF67 AS DateTime), CAST(0x0000A5FC0129DF67 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1976, N'157265', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DF6A AS DateTime), CAST(0x0000A5FC0129DF6A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1977, N'801993', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DF6D AS DateTime), CAST(0x0000A5FC0129DF6D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1978, N'594819', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DF6F AS DateTime), CAST(0x0000A5FC0129DF6F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1979, N'456851', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DF73 AS DateTime), CAST(0x0000A5FC0129DF73 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1980, N'433541', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DF77 AS DateTime), CAST(0x0000A5FC0129DF77 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1981, N'508435', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DF79 AS DateTime), CAST(0x0000A5FC0129DF79 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1982, N'311001', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DF7D AS DateTime), CAST(0x0000A5FC0129DF7D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1983, N'799082', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DF80 AS DateTime), CAST(0x0000A5FC0129DF80 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1984, N'829223', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DF82 AS DateTime), CAST(0x0000A5FC0129DF82 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1985, N'970703', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DF85 AS DateTime), CAST(0x0000A5FC0129DF85 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1986, N'352817', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DF88 AS DateTime), CAST(0x0000A5FC0129DF88 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1987, N'797777', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DF8B AS DateTime), CAST(0x0000A5FC0129DF8B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1988, N'241572', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DFE8 AS DateTime), CAST(0x0000A5FC0129DFE8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1989, N'826965', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DFEC AS DateTime), CAST(0x0000A5FC0129DFEC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1990, N'221170', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DFF0 AS DateTime), CAST(0x0000A5FC0129DFF0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1991, N'550312', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DFF5 AS DateTime), CAST(0x0000A5FC0129DFF5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1992, N'892971', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DFF9 AS DateTime), CAST(0x0000A5FC0129DFF9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1993, N'171185', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DFFC AS DateTime), CAST(0x0000A5FC0129DFFC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1994, N'265468', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129DFFF AS DateTime), CAST(0x0000A5FC0129DFFF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1995, N'489767', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E002 AS DateTime), CAST(0x0000A5FC0129E002 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1996, N'416547', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E005 AS DateTime), CAST(0x0000A5FC0129E005 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1997, N'187139', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E008 AS DateTime), CAST(0x0000A5FC0129E008 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1998, N'166104', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E00B AS DateTime), CAST(0x0000A5FC0129E00B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (1999, N'565283', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E00F AS DateTime), CAST(0x0000A5FC0129E00F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2000, N'807947', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E013 AS DateTime), CAST(0x0000A5FC0129E013 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2001, N'252956', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E016 AS DateTime), CAST(0x0000A5FC0129E016 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2002, N'861372', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E01A AS DateTime), CAST(0x0000A5FC0129E01A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2003, N'708296', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E01D AS DateTime), CAST(0x0000A5FC0129E01D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2004, N'840924', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E020 AS DateTime), CAST(0x0000A5FC0129E020 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2005, N'135156', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E023 AS DateTime), CAST(0x0000A5FC0129E023 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2006, N'590631', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E026 AS DateTime), CAST(0x0000A5FC0129E026 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2007, N'539443', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E02C AS DateTime), CAST(0x0000A5FC0129E02C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2008, N'356940', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E031 AS DateTime), CAST(0x0000A5FC0129E031 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2009, N'250012', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E035 AS DateTime), CAST(0x0000A5FC0129E035 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2010, N'122386', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E038 AS DateTime), CAST(0x0000A5FC0129E038 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2011, N'531571', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E03B AS DateTime), CAST(0x0000A5FC0129E03B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2012, N'373464', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E03E AS DateTime), CAST(0x0000A5FC0129E03E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2013, N'689215', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E09B AS DateTime), CAST(0x0000A5FC0129E09B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2014, N'703938', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E09E AS DateTime), CAST(0x0000A5FC0129E09E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2015, N'337691', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0A0 AS DateTime), CAST(0x0000A5FC0129E0A0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2016, N'700626', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0A3 AS DateTime), CAST(0x0000A5FC0129E0A3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2017, N'182606', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0A6 AS DateTime), CAST(0x0000A5FC0129E0A6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2018, N'850486', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0AA AS DateTime), CAST(0x0000A5FC0129E0AA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2019, N'678512', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0AD AS DateTime), CAST(0x0000A5FC0129E0AD AS DateTime))
GO
print 'Processed 2000 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2020, N'156397', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0B0 AS DateTime), CAST(0x0000A5FC0129E0B0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2021, N'913780', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0B2 AS DateTime), CAST(0x0000A5FC0129E0B2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2022, N'579733', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0B5 AS DateTime), CAST(0x0000A5FC0129E0B5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2023, N'764651', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0B8 AS DateTime), CAST(0x0000A5FC0129E0B8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2024, N'861092', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0BB AS DateTime), CAST(0x0000A5FC0129E0BB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2025, N'986502', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0BE AS DateTime), CAST(0x0000A5FC0129E0BE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2026, N'258836', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0C1 AS DateTime), CAST(0x0000A5FC0129E0C1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2027, N'811526', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0C4 AS DateTime), CAST(0x0000A5FC0129E0C4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2028, N'131098', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0CA AS DateTime), CAST(0x0000A5FC0129E0CA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2029, N'979998', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0CC AS DateTime), CAST(0x0000A5FC0129E0CC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2030, N'323582', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0D0 AS DateTime), CAST(0x0000A5FC0129E0D0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2031, N'611884', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0D3 AS DateTime), CAST(0x0000A5FC0129E0D3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2032, N'233928', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0D6 AS DateTime), CAST(0x0000A5FC0129E0D6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2033, N'983409', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0D9 AS DateTime), CAST(0x0000A5FC0129E0D9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2034, N'461940', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0DB AS DateTime), CAST(0x0000A5FC0129E0DB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2035, N'473446', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0DE AS DateTime), CAST(0x0000A5FC0129E0DE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2036, N'325362', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0E1 AS DateTime), CAST(0x0000A5FC0129E0E1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2037, N'521029', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0E4 AS DateTime), CAST(0x0000A5FC0129E0E4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2038, N'430099', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0E7 AS DateTime), CAST(0x0000A5FC0129E0E7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2039, N'247006', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0EA AS DateTime), CAST(0x0000A5FC0129E0EA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2040, N'389857', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0ED AS DateTime), CAST(0x0000A5FC0129E0ED AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2041, N'264407', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0F0 AS DateTime), CAST(0x0000A5FC0129E0F0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2042, N'572481', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0F3 AS DateTime), CAST(0x0000A5FC0129E0F3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2043, N'873663', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0F6 AS DateTime), CAST(0x0000A5FC0129E0F6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2044, N'860067', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0F9 AS DateTime), CAST(0x0000A5FC0129E0F9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2045, N'498161', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0FC AS DateTime), CAST(0x0000A5FC0129E0FC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2046, N'329207', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E0FF AS DateTime), CAST(0x0000A5FC0129E0FF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2047, N'209067', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E102 AS DateTime), CAST(0x0000A5FC0129E102 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2048, N'959658', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E106 AS DateTime), CAST(0x0000A5FC0129E106 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2049, N'966323', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E109 AS DateTime), CAST(0x0000A5FC0129E109 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2050, N'442482', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E10C AS DateTime), CAST(0x0000A5FC0129E10C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2051, N'410024', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E110 AS DateTime), CAST(0x0000A5FC0129E110 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2052, N'255602', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E112 AS DateTime), CAST(0x0000A5FC0129E112 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2053, N'139506', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E115 AS DateTime), CAST(0x0000A5FC0129E115 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2054, N'412270', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E119 AS DateTime), CAST(0x0000A5FC0129E119 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2055, N'953624', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E11C AS DateTime), CAST(0x0000A5FC0129E11C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2056, N'838674', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E11F AS DateTime), CAST(0x0000A5FC0129E11F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2057, N'709328', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E122 AS DateTime), CAST(0x0000A5FC0129E122 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2058, N'677150', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E127 AS DateTime), CAST(0x0000A5FC0129E127 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2059, N'906982', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E12A AS DateTime), CAST(0x0000A5FC0129E12A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2060, N'944499', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E12E AS DateTime), CAST(0x0000A5FC0129E12E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2061, N'593237', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E131 AS DateTime), CAST(0x0000A5FC0129E131 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2062, N'566107', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E134 AS DateTime), CAST(0x0000A5FC0129E134 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2063, N'107207', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E137 AS DateTime), CAST(0x0000A5FC0129E137 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2064, N'390259', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E13B AS DateTime), CAST(0x0000A5FC0129E13B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2065, N'228486', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E13E AS DateTime), CAST(0x0000A5FC0129E13E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2066, N'976773', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E142 AS DateTime), CAST(0x0000A5FC0129E142 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2067, N'335802', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E147 AS DateTime), CAST(0x0000A5FC0129E147 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2068, N'799929', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E14C AS DateTime), CAST(0x0000A5FC0129E14C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2069, N'646577', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E14F AS DateTime), CAST(0x0000A5FC0129E14F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2070, N'245299', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E153 AS DateTime), CAST(0x0000A5FC0129E153 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2071, N'701744', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E156 AS DateTime), CAST(0x0000A5FC0129E156 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2072, N'824186', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E15A AS DateTime), CAST(0x0000A5FC0129E15A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2073, N'699512', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E15C AS DateTime), CAST(0x0000A5FC0129E15C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2074, N'651735', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E160 AS DateTime), CAST(0x0000A5FC0129E160 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2075, N'306601', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E164 AS DateTime), CAST(0x0000A5FC0129E164 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2076, N'447007', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E167 AS DateTime), CAST(0x0000A5FC0129E167 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2077, N'979703', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E16B AS DateTime), CAST(0x0000A5FC0129E16B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2078, N'139097', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E16D AS DateTime), CAST(0x0000A5FC0129E16D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2079, N'845438', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E170 AS DateTime), CAST(0x0000A5FC0129E170 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2080, N'817691', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E172 AS DateTime), CAST(0x0000A5FC0129E172 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2081, N'864188', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E175 AS DateTime), CAST(0x0000A5FC0129E175 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2082, N'380407', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E178 AS DateTime), CAST(0x0000A5FC0129E178 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2083, N'536329', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E17B AS DateTime), CAST(0x0000A5FC0129E17B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2084, N'492303', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E17E AS DateTime), CAST(0x0000A5FC0129E17E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2085, N'278034', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E183 AS DateTime), CAST(0x0000A5FC0129E183 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2086, N'953169', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E1E0 AS DateTime), CAST(0x0000A5FC0129E1E0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2087, N'311184', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E1E3 AS DateTime), CAST(0x0000A5FC0129E1E3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2088, N'816119', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E1E5 AS DateTime), CAST(0x0000A5FC0129E1E5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2089, N'622775', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E1E8 AS DateTime), CAST(0x0000A5FC0129E1E8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2090, N'263533', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E1EB AS DateTime), CAST(0x0000A5FC0129E1EB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2091, N'749035', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E1EE AS DateTime), CAST(0x0000A5FC0129E1EE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2092, N'867410', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E1F1 AS DateTime), CAST(0x0000A5FC0129E1F1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2093, N'895972', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E1F4 AS DateTime), CAST(0x0000A5FC0129E1F4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2094, N'916411', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E1F7 AS DateTime), CAST(0x0000A5FC0129E1F7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2095, N'193443', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E1FA AS DateTime), CAST(0x0000A5FC0129E1FA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2096, N'651310', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E1FD AS DateTime), CAST(0x0000A5FC0129E1FD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2097, N'290261', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E200 AS DateTime), CAST(0x0000A5FC0129E200 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2098, N'363831', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E203 AS DateTime), CAST(0x0000A5FC0129E203 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2099, N'929086', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E206 AS DateTime), CAST(0x0000A5FC0129E206 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2100, N'575519', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E208 AS DateTime), CAST(0x0000A5FC0129E208 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2101, N'665362', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E20B AS DateTime), CAST(0x0000A5FC0129E20B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2102, N'989817', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E20E AS DateTime), CAST(0x0000A5FC0129E20E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2103, N'126837', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E211 AS DateTime), CAST(0x0000A5FC0129E211 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2104, N'118756', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E214 AS DateTime), CAST(0x0000A5FC0129E214 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2105, N'450049', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E217 AS DateTime), CAST(0x0000A5FC0129E217 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2106, N'365197', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E21A AS DateTime), CAST(0x0000A5FC0129E21A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2107, N'637106', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E21D AS DateTime), CAST(0x0000A5FC0129E21D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2108, N'429841', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E220 AS DateTime), CAST(0x0000A5FC0129E220 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2109, N'285794', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E223 AS DateTime), CAST(0x0000A5FC0129E223 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2110, N'296433', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E226 AS DateTime), CAST(0x0000A5FC0129E226 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2111, N'600409', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E229 AS DateTime), CAST(0x0000A5FC0129E229 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2112, N'369366', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E22C AS DateTime), CAST(0x0000A5FC0129E22C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2113, N'733326', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E22F AS DateTime), CAST(0x0000A5FC0129E22F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2114, N'399241', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E231 AS DateTime), CAST(0x0000A5FC0129E231 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2115, N'855221', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E234 AS DateTime), CAST(0x0000A5FC0129E234 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2116, N'805771', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E238 AS DateTime), CAST(0x0000A5FC0129E238 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2117, N'804599', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E23B AS DateTime), CAST(0x0000A5FC0129E23B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2118, N'218379', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E23E AS DateTime), CAST(0x0000A5FC0129E23E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2119, N'525271', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E240 AS DateTime), CAST(0x0000A5FC0129E240 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2120, N'778017', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E243 AS DateTime), CAST(0x0000A5FC0129E243 AS DateTime))
GO
print 'Processed 2100 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2121, N'924692', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E246 AS DateTime), CAST(0x0000A5FC0129E246 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2122, N'993067', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E249 AS DateTime), CAST(0x0000A5FC0129E249 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2123, N'665974', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E24C AS DateTime), CAST(0x0000A5FC0129E24C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2124, N'509201', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E24F AS DateTime), CAST(0x0000A5FC0129E24F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2125, N'711016', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E252 AS DateTime), CAST(0x0000A5FC0129E252 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2126, N'412231', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E254 AS DateTime), CAST(0x0000A5FC0129E254 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2127, N'798612', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E258 AS DateTime), CAST(0x0000A5FC0129E258 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2128, N'509856', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E25B AS DateTime), CAST(0x0000A5FC0129E25B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2129, N'285562', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E25E AS DateTime), CAST(0x0000A5FC0129E25E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2130, N'928527', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E261 AS DateTime), CAST(0x0000A5FC0129E261 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2131, N'626537', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E264 AS DateTime), CAST(0x0000A5FC0129E264 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2132, N'553005', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E267 AS DateTime), CAST(0x0000A5FC0129E267 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2133, N'136518', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E26A AS DateTime), CAST(0x0000A5FC0129E26A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2134, N'470212', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E26D AS DateTime), CAST(0x0000A5FC0129E26D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2135, N'672461', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E270 AS DateTime), CAST(0x0000A5FC0129E270 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2136, N'384667', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E274 AS DateTime), CAST(0x0000A5FC0129E274 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2137, N'299326', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E276 AS DateTime), CAST(0x0000A5FC0129E276 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2138, N'914100', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E27A AS DateTime), CAST(0x0000A5FC0129E27A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2139, N'236065', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E27D AS DateTime), CAST(0x0000A5FC0129E27D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2140, N'248975', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E280 AS DateTime), CAST(0x0000A5FC0129E280 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2141, N'985959', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E283 AS DateTime), CAST(0x0000A5FC0129E283 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2142, N'956536', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E286 AS DateTime), CAST(0x0000A5FC0129E286 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2143, N'947155', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E289 AS DateTime), CAST(0x0000A5FC0129E289 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2144, N'452567', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E28C AS DateTime), CAST(0x0000A5FC0129E28C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2145, N'821794', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E28F AS DateTime), CAST(0x0000A5FC0129E28F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2146, N'672757', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E292 AS DateTime), CAST(0x0000A5FC0129E292 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2147, N'170556', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E296 AS DateTime), CAST(0x0000A5FC0129E296 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2148, N'119659', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E29A AS DateTime), CAST(0x0000A5FC0129E29A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2149, N'882443', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E29F AS DateTime), CAST(0x0000A5FC0129E29F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2150, N'908771', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E2A3 AS DateTime), CAST(0x0000A5FC0129E2A3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2151, N'572645', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E2A7 AS DateTime), CAST(0x0000A5FC0129E2A7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2152, N'470654', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E2AA AS DateTime), CAST(0x0000A5FC0129E2AA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2153, N'436113', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E308 AS DateTime), CAST(0x0000A5FC0129E308 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2154, N'396162', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E365 AS DateTime), CAST(0x0000A5FC0129E365 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2155, N'431507', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E368 AS DateTime), CAST(0x0000A5FC0129E368 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2156, N'344839', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E374 AS DateTime), CAST(0x0000A5FC0129E374 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2157, N'828760', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E377 AS DateTime), CAST(0x0000A5FC0129E377 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2158, N'112124', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E379 AS DateTime), CAST(0x0000A5FC0129E379 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2159, N'922826', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E37C AS DateTime), CAST(0x0000A5FC0129E37C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2160, N'298640', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E37F AS DateTime), CAST(0x0000A5FC0129E37F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2161, N'714657', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E382 AS DateTime), CAST(0x0000A5FC0129E382 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2162, N'537243', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E493 AS DateTime), CAST(0x0000A5FC0129E493 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2163, N'853915', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E496 AS DateTime), CAST(0x0000A5FC0129E496 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2164, N'263251', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E498 AS DateTime), CAST(0x0000A5FC0129E498 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2165, N'440262', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E49B AS DateTime), CAST(0x0000A5FC0129E49B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2166, N'825675', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E49E AS DateTime), CAST(0x0000A5FC0129E49E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2167, N'825874', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4A1 AS DateTime), CAST(0x0000A5FC0129E4A1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2168, N'119367', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4A4 AS DateTime), CAST(0x0000A5FC0129E4A4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2169, N'883922', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4A9 AS DateTime), CAST(0x0000A5FC0129E4A9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2170, N'537745', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4AB AS DateTime), CAST(0x0000A5FC0129E4AB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2171, N'334210', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4AE AS DateTime), CAST(0x0000A5FC0129E4AE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2172, N'470846', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4B1 AS DateTime), CAST(0x0000A5FC0129E4B1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2173, N'228433', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4B4 AS DateTime), CAST(0x0000A5FC0129E4B4 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2174, N'587118', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4B7 AS DateTime), CAST(0x0000A5FC0129E4B7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2175, N'544954', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4B9 AS DateTime), CAST(0x0000A5FC0129E4B9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2176, N'674359', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4BD AS DateTime), CAST(0x0000A5FC0129E4BD AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2177, N'806580', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4BF AS DateTime), CAST(0x0000A5FC0129E4BF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2178, N'116687', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4C2 AS DateTime), CAST(0x0000A5FC0129E4C2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2179, N'710698', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4C7 AS DateTime), CAST(0x0000A5FC0129E4C7 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2180, N'421975', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4CA AS DateTime), CAST(0x0000A5FC0129E4CA AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2181, N'202319', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4CE AS DateTime), CAST(0x0000A5FC0129E4CE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2182, N'305182', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4D1 AS DateTime), CAST(0x0000A5FC0129E4D1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2183, N'399956', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4D3 AS DateTime), CAST(0x0000A5FC0129E4D3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2184, N'869222', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4D6 AS DateTime), CAST(0x0000A5FC0129E4D6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2185, N'552502', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4D9 AS DateTime), CAST(0x0000A5FC0129E4D9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2186, N'192314', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4DC AS DateTime), CAST(0x0000A5FC0129E4DC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2187, N'141016', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4DF AS DateTime), CAST(0x0000A5FC0129E4DF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2188, N'782553', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4E2 AS DateTime), CAST(0x0000A5FC0129E4E2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2189, N'818219', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4E5 AS DateTime), CAST(0x0000A5FC0129E4E5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2190, N'851914', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4E8 AS DateTime), CAST(0x0000A5FC0129E4E8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2191, N'736495', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4EB AS DateTime), CAST(0x0000A5FC0129E4EB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2192, N'256162', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4EE AS DateTime), CAST(0x0000A5FC0129E4EE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2193, N'367975', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4F0 AS DateTime), CAST(0x0000A5FC0129E4F0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2194, N'261883', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4F3 AS DateTime), CAST(0x0000A5FC0129E4F3 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2195, N'231349', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4F6 AS DateTime), CAST(0x0000A5FC0129E4F6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2196, N'669571', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4F9 AS DateTime), CAST(0x0000A5FC0129E4F9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2197, N'484549', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4FC AS DateTime), CAST(0x0000A5FC0129E4FC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2198, N'991080', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E4FF AS DateTime), CAST(0x0000A5FC0129E4FF AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2199, N'940577', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E503 AS DateTime), CAST(0x0000A5FC0129E503 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2200, N'859446', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E506 AS DateTime), CAST(0x0000A5FC0129E506 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2201, N'572131', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E509 AS DateTime), CAST(0x0000A5FC0129E509 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2202, N'928617', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E50C AS DateTime), CAST(0x0000A5FC0129E50C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2203, N'670253', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E50F AS DateTime), CAST(0x0000A5FC0129E50F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2204, N'526320', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E511 AS DateTime), CAST(0x0000A5FC0129E511 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2205, N'487955', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E514 AS DateTime), CAST(0x0000A5FC0129E514 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2206, N'632443', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E517 AS DateTime), CAST(0x0000A5FC0129E517 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2207, N'746031', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E51A AS DateTime), CAST(0x0000A5FC0129E51A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2208, N'524848', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E51D AS DateTime), CAST(0x0000A5FC0129E51D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2209, N'795149', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E521 AS DateTime), CAST(0x0000A5FC0129E521 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2210, N'587631', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E523 AS DateTime), CAST(0x0000A5FC0129E523 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2211, N'568222', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E526 AS DateTime), CAST(0x0000A5FC0129E526 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2212, N'109247', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E529 AS DateTime), CAST(0x0000A5FC0129E529 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2213, N'291238', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E52C AS DateTime), CAST(0x0000A5FC0129E52C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2214, N'863648', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E52F AS DateTime), CAST(0x0000A5FC0129E52F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2215, N'135712', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E531 AS DateTime), CAST(0x0000A5FC0129E531 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2216, N'899102', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E534 AS DateTime), CAST(0x0000A5FC0129E534 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2217, N'769477', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E538 AS DateTime), CAST(0x0000A5FC0129E538 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2218, N'760504', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E53A AS DateTime), CAST(0x0000A5FC0129E53A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2219, N'612126', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E53D AS DateTime), CAST(0x0000A5FC0129E53D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2220, N'497884', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E542 AS DateTime), CAST(0x0000A5FC0129E542 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2221, N'342898', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E546 AS DateTime), CAST(0x0000A5FC0129E546 AS DateTime))
GO
print 'Processed 2200 total records'
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2222, N'368327', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E54A AS DateTime), CAST(0x0000A5FC0129E54A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2223, N'255233', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E54D AS DateTime), CAST(0x0000A5FC0129E54D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2224, N'651643', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E550 AS DateTime), CAST(0x0000A5FC0129E550 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2225, N'531445', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E553 AS DateTime), CAST(0x0000A5FC0129E553 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2226, N'700117', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E557 AS DateTime), CAST(0x0000A5FC0129E557 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2227, N'457366', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E55B AS DateTime), CAST(0x0000A5FC0129E55B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2228, N'303213', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E55F AS DateTime), CAST(0x0000A5FC0129E55F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2229, N'991233', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E562 AS DateTime), CAST(0x0000A5FC0129E562 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2230, N'849355', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E564 AS DateTime), CAST(0x0000A5FC0129E564 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2231, N'904429', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E567 AS DateTime), CAST(0x0000A5FC0129E567 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2232, N'661032', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E56A AS DateTime), CAST(0x0000A5FC0129E56A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2233, N'265904', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E5C6 AS DateTime), CAST(0x0000A5FC0129E5C6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2234, N'431060', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E624 AS DateTime), CAST(0x0000A5FC0129E624 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2235, N'875947', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E627 AS DateTime), CAST(0x0000A5FC0129E627 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2236, N'981366', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E629 AS DateTime), CAST(0x0000A5FC0129E629 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2237, N'389162', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E62C AS DateTime), CAST(0x0000A5FC0129E62C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2238, N'204531', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E630 AS DateTime), CAST(0x0000A5FC0129E630 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2239, N'817974', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E633 AS DateTime), CAST(0x0000A5FC0129E633 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2240, N'213196', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E637 AS DateTime), CAST(0x0000A5FC0129E637 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2241, N'289298', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E695 AS DateTime), CAST(0x0000A5FC0129E695 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2242, N'543161', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E698 AS DateTime), CAST(0x0000A5FC0129E698 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2243, N'303515', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E69A AS DateTime), CAST(0x0000A5FC0129E69A AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2244, N'302265', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E69D AS DateTime), CAST(0x0000A5FC0129E69D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2245, N'440944', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E6A0 AS DateTime), CAST(0x0000A5FC0129E6A0 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2246, N'579622', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E6A5 AS DateTime), CAST(0x0000A5FC0129E6A5 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2247, N'336958', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E6A9 AS DateTime), CAST(0x0000A5FC0129E6A9 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2248, N'880358', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E6AC AS DateTime), CAST(0x0000A5FC0129E6AC AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2249, N'539527', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E6B1 AS DateTime), CAST(0x0000A5FC0129E6B1 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2250, N'412716', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E70E AS DateTime), CAST(0x0000A5FC0129E70E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2251, N'793299', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E711 AS DateTime), CAST(0x0000A5FC0129E711 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2252, N'370987', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E714 AS DateTime), CAST(0x0000A5FC0129E714 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2253, N'699938', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E71B AS DateTime), CAST(0x0000A5FC0129E71B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2254, N'668441', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E71F AS DateTime), CAST(0x0000A5FC0129E71F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2255, N'846855', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E722 AS DateTime), CAST(0x0000A5FC0129E722 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2256, N'451319', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E725 AS DateTime), CAST(0x0000A5FC0129E725 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2257, N'726151', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E728 AS DateTime), CAST(0x0000A5FC0129E728 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2258, N'785256', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E72C AS DateTime), CAST(0x0000A5FC0129E72C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2259, N'952177', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E72F AS DateTime), CAST(0x0000A5FC0129E72F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2260, N'611883', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E732 AS DateTime), CAST(0x0000A5FC0129E732 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2261, N'245163', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E735 AS DateTime), CAST(0x0000A5FC0129E735 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2262, N'115139', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E738 AS DateTime), CAST(0x0000A5FC0129E738 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2263, N'543086', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E73B AS DateTime), CAST(0x0000A5FC0129E73B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2264, N'308747', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E73D AS DateTime), CAST(0x0000A5FC0129E73D AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2265, N'243617', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E740 AS DateTime), CAST(0x0000A5FC0129E740 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2266, N'872909', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E745 AS DateTime), CAST(0x0000A5FC0129E745 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2267, N'778610', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E749 AS DateTime), CAST(0x0000A5FC0129E749 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2268, N'971738', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E74C AS DateTime), CAST(0x0000A5FC0129E74C AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2269, N'617299', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E74F AS DateTime), CAST(0x0000A5FC0129E74F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2270, N'806738', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E752 AS DateTime), CAST(0x0000A5FC0129E752 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2271, N'452083', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E755 AS DateTime), CAST(0x0000A5FC0129E755 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2272, N'990842', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E758 AS DateTime), CAST(0x0000A5FC0129E758 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2273, N'323378', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E75B AS DateTime), CAST(0x0000A5FC0129E75B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2274, N'899858', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E75E AS DateTime), CAST(0x0000A5FC0129E75E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2275, N'153145', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E762 AS DateTime), CAST(0x0000A5FC0129E762 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2276, N'235544', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E765 AS DateTime), CAST(0x0000A5FC0129E765 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2277, N'998237', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E768 AS DateTime), CAST(0x0000A5FC0129E768 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2278, N'755151', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E76B AS DateTime), CAST(0x0000A5FC0129E76B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2279, N'157940', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E76E AS DateTime), CAST(0x0000A5FC0129E76E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2280, N'859980', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E771 AS DateTime), CAST(0x0000A5FC0129E771 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2281, N'920251', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E775 AS DateTime), CAST(0x0000A5FC0129E775 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2282, N'855320', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E778 AS DateTime), CAST(0x0000A5FC0129E778 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2283, N'963883', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E77B AS DateTime), CAST(0x0000A5FC0129E77B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2284, N'763134', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E77E AS DateTime), CAST(0x0000A5FC0129E77E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2285, N'775485', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E781 AS DateTime), CAST(0x0000A5FC0129E781 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2286, N'961090', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E784 AS DateTime), CAST(0x0000A5FC0129E784 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2287, N'398801', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E788 AS DateTime), CAST(0x0000A5FC0129E788 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2288, N'641106', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E78B AS DateTime), CAST(0x0000A5FC0129E78B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2289, N'719499', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E78E AS DateTime), CAST(0x0000A5FC0129E78E AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2290, N'849609', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E791 AS DateTime), CAST(0x0000A5FC0129E791 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2291, N'149283', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E793 AS DateTime), CAST(0x0000A5FC0129E793 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2292, N'188054', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E796 AS DateTime), CAST(0x0000A5FC0129E796 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2293, N'460975', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E79B AS DateTime), CAST(0x0000A5FC0129E79B AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2294, N'423123', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E79F AS DateTime), CAST(0x0000A5FC0129E79F AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2295, N'122409', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E7A2 AS DateTime), CAST(0x0000A5FC0129E7A2 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2296, N'737334', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E7A6 AS DateTime), CAST(0x0000A5FC0129E7A6 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2297, N'326287', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E7A8 AS DateTime), CAST(0x0000A5FC0129E7A8 AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2298, N'248846', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E7AB AS DateTime), CAST(0x0000A5FC0129E7AB AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2299, N'821258', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E7AE AS DateTime), CAST(0x0000A5FC0129E7AE AS DateTime))
INSERT [dbo].[tblPinCode] ([PinCodeID], [PinCode], [Serial], [Serial2], [AccountManagerID], [ProductTypeID], [PinCodeStatus], [PinCodeBlockID], [CreatedDate], [UpdatedDate]) VALUES (2300, N'492707', N'', NULL, 2, 0, 1, 2, CAST(0x0000A5FC0129E7B1 AS DateTime), CAST(0x0000A5FC0129E7B1 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblPinCode] OFF
/****** Object:  Table [dbo].[tblMtConfig]    Script Date: 05/11/2016 10:56:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMtConfig](
	[MtConfigID] [bigint] IDENTITY(1,1) NOT NULL,
	[MtMessage] [nvarchar](1000) NOT NULL,
	[MtType] [tinyint] NOT NULL,
	[AccountManagerID] [bigint] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_tblMtConfig] PRIMARY KEY CLUSTERED 
(
	[MtConfigID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblMtConfig] ON
INSERT [dbo].[tblMtConfig] ([MtConfigID], [MtMessage], [MtType], [AccountManagerID], [CreatedDate], [UpdatedDate]) VALUES (1, N'Ban da tham gia thanh cong chuong trinh “Vua co suc khoe vua co tien” cua VIFASPORT voi ID: {0}. Hay gioi thieu khach mua san pham VIFASPORT de nhan duoc qua tang. Chi tiet xem tai www.vifasport.com', 1, 2, CAST(0x0000A5FE00B6FBAD AS DateTime), CAST(0x0000A5FE00B6FBAD AS DateTime))
INSERT [dbo].[tblMtConfig] ([MtConfigID], [MtMessage], [MtType], [AccountManagerID], [CreatedDate], [UpdatedDate]) VALUES (2, N'Sai cu phap.', 2, 2, CAST(0x0000A5FE00B6FBB4 AS DateTime), CAST(0x0000A5FE00B6FBB4 AS DateTime))
INSERT [dbo].[tblMtConfig] ([MtConfigID], [MtMessage], [MtType], [AccountManagerID], [CreatedDate], [UpdatedDate]) VALUES (3, N'Pincode khong dung.', 3, 2, CAST(0x0000A5FE00B6FBB6 AS DateTime), CAST(0x0000A5FE00B6FBB6 AS DateTime))
INSERT [dbo].[tblMtConfig] ([MtConfigID], [MtMessage], [MtType], [AccountManagerID], [CreatedDate], [UpdatedDate]) VALUES (4, N'Pincode da duoc su dung.', 4, 2, CAST(0x0000A5FE00B6FBB7 AS DateTime), CAST(0x0000A5FE00B6FBB7 AS DateTime))
INSERT [dbo].[tblMtConfig] ([MtConfigID], [MtMessage], [MtType], [AccountManagerID], [CreatedDate], [UpdatedDate]) VALUES (5, N'Chuong trinh da ket thuc.', 5, 2, CAST(0x0000A5FE00B6FBB9 AS DateTime), CAST(0x0000A5FE00B6FBB9 AS DateTime))
INSERT [dbo].[tblMtConfig] ([MtConfigID], [MtMessage], [MtType], [AccountManagerID], [CreatedDate], [UpdatedDate]) VALUES (6, N'Da co ma khach hang va phai nhan theo cau truc cua nguoi duoc gioi thieu.', 6, 2, CAST(0x0000A5FE00B6FBBB AS DateTime), CAST(0x0000A5FE00B6FBBB AS DateTime))
INSERT [dbo].[tblMtConfig] ([MtConfigID], [MtMessage], [MtType], [AccountManagerID], [CreatedDate], [UpdatedDate]) VALUES (7, N'Ban da gioi thieu thanh cong chuong trinh “Vua co suc khoe vua co tien” cua VIFASPORT. VIFASPORT (0967 99 00 99) se lien lac voi ban de trao qua.', 7, 2, CAST(0x0000A5FE00B6FBBD AS DateTime), CAST(0x0000A5FE00B6FBBD AS DateTime))
INSERT [dbo].[tblMtConfig] ([MtConfigID], [MtMessage], [MtType], [AccountManagerID], [CreatedDate], [UpdatedDate]) VALUES (8, N'Chuc mung ban da nhan duoc chiet khau 5% gia tri san pham ban vua 
mua. Vifasport (0967 99 00 99) se lien lac voi ban de trao qua.', 8, 2, CAST(0x0000A5FE00B6FBBF AS DateTime), CAST(0x0000A5FE00B6FBBF AS DateTime))
INSERT [dbo].[tblMtConfig] ([MtConfigID], [MtMessage], [MtType], [AccountManagerID], [CreatedDate], [UpdatedDate]) VALUES (9, N'Sai cu phap. Cu phap dung la: VIFA PINCODE hoac VIFA PINCODE CODE', 100, -1, CAST(0x0000A5FE00EC2675 AS DateTime), CAST(0x0000A5FE00EC2675 AS DateTime))
INSERT [dbo].[tblMtConfig] ([MtConfigID], [MtMessage], [MtType], [AccountManagerID], [CreatedDate], [UpdatedDate]) VALUES (10, N'Ma khach hang gioi thieu khong ton tai.', 9, 2, CAST(0x0000A5FE011227FE AS DateTime), CAST(0x0000A5FE011227FE AS DateTime))
SET IDENTITY_INSERT [dbo].[tblMtConfig] OFF
/****** Object:  Table [dbo].[tblMt]    Script Date: 05/11/2016 10:56:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMt](
	[MtID] [bigint] IDENTITY(1,1) NOT NULL,
	[MtMessage] [nvarchar](1000) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[ShortCreatedDate] [int] NULL,
	[MoID] [bigint] NULL,
	[MtStatus] [tinyint] NULL,
	[Remark] [nvarchar](1024) NULL,
	[GUID] [nvarchar](1024) NULL,
 CONSTRAINT [PK_tblMt] PRIMARY KEY CLUSTERED 
(
	[MtID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblMt] ON
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (1, N'Ma khach hang gioi thieu khong ton tai.', N'841656244808', CAST(0x0000A5FE012F6F75 AS DateTime), CAST(0x0000A5FE012F6F75 AS DateTime), 20160506, 13, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (2, N'Ma khach hang gioi thieu khong ton tai.', N'841656244808', CAST(0x0000A5FE012FE11D AS DateTime), CAST(0x0000A5FE012FE11D AS DateTime), 20160506, 14, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (3, N'Da co ma khach hang va phai nhan theo cau truc cua nguoi duoc gioi thieu.', N'841656244808', CAST(0x0000A5FE012FFA29 AS DateTime), CAST(0x0000A5FE012FFA29 AS DateTime), 20160506, 15, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (4, N'Pincode da duoc su dung.', N'841656244808', CAST(0x0000A5FE013057C8 AS DateTime), CAST(0x0000A5FE013057C8 AS DateTime), 20160506, 16, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (5, N'Pincode da duoc su dung.', N'841656244808', CAST(0x0000A5FE013082CC AS DateTime), CAST(0x0000A5FE013082CC AS DateTime), 20160506, 17, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (6, N'Ma khach hang gioi thieu khong ton tai.', N'841656244808', CAST(0x0000A5FE0130AC43 AS DateTime), CAST(0x0000A5FE0130AC43 AS DateTime), 20160506, 18, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (7, N'Ban da gioi thieu thanh cong chuong trinh “Vua co suc khoe vua co tien” cua VIFASPORT. VIFASPORT (0967 99 00 99) se lien lac voi ban de trao qua.', N'841656244909', CAST(0x0000A5FE0130C35C AS DateTime), CAST(0x0000A5FE0130C35C AS DateTime), 20160506, 19, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (8, N'Ban da tham gia thanh cong chuong trinh “Vua co suc khoe vua co tien” cua VIFASPORT voi ID: {0}. Hay gioi thieu khach mua san pham VIFASPORT de nhan duoc qua tang. Chi tiet xem tai www.vifasport.com', N'841656244808', CAST(0x0000A5FE0130C35E AS DateTime), CAST(0x0000A5FE0130C35E AS DateTime), 20160506, 19, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (9, N'Chuc mung ban da nhan duoc chiet khau 5% gia tri san pham ban vua 
mua. Vifasport (0967 99 00 99) se lien lac voi ban de trao qua.', N'841656244808', CAST(0x0000A5FE0130C35F AS DateTime), CAST(0x0000A5FE0130C35F AS DateTime), 20160506, 19, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (10, N'Ban da gioi thieu thanh cong chuong trinh “Vua co suc khoe vua co tien” cua VIFASPORT. VIFASPORT (0967 99 00 99) se lien lac voi ban de trao qua.', N'841656244909', CAST(0x0000A5FE013291D4 AS DateTime), CAST(0x0000A5FE013291D4 AS DateTime), 20160506, 20, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (11, N'Ban da tham gia thanh cong chuong trinh “Vua co suc khoe vua co tien” cua VIFASPORT voi ID: . Hay gioi thieu khach mua san pham VIFASPORT de nhan duoc qua tang. Chi tiet xem tai www.vifasport.com', N'841656244808', CAST(0x0000A5FE013291D6 AS DateTime), CAST(0x0000A5FE013291D6 AS DateTime), 20160506, 20, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (12, N'Chuc mung ban da nhan duoc chiet khau 5% gia tri san pham ban vua 
mua. Vifasport (0967 99 00 99) se lien lac voi ban de trao qua.', N'841656244808', CAST(0x0000A5FE013291D8 AS DateTime), CAST(0x0000A5FE013291D8 AS DateTime), 20160506, 20, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (13, N'Pincode da duoc su dung.', N'841656244808', CAST(0x0000A5FE01333459 AS DateTime), CAST(0x0000A5FE01333459 AS DateTime), 20160506, 21, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (14, N'Ban da gioi thieu thanh cong chuong trinh “Vua co suc khoe vua co tien” cua VIFASPORT. VIFASPORT (0967 99 00 99) se lien lac voi ban de trao qua.', N'841656244909', CAST(0x0000A5FE01335794 AS DateTime), CAST(0x0000A5FE01335794 AS DateTime), 20160506, 22, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (15, N'Ban da tham gia thanh cong chuong trinh “Vua co suc khoe vua co tien” cua VIFASPORT voi ID: AA111. Hay gioi thieu khach mua san pham VIFASPORT de nhan duoc qua tang. Chi tiet xem tai www.vifasport.com', N'841656244808', CAST(0x0000A5FE01335796 AS DateTime), CAST(0x0000A5FE01335796 AS DateTime), 20160506, 22, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (16, N'Chuc mung ban da nhan duoc chiet khau 5% gia tri san pham ban vua 
mua. Vifasport (0967 99 00 99) se lien lac voi ban de trao qua.', N'841656244808', CAST(0x0000A5FE01335799 AS DateTime), CAST(0x0000A5FE01335799 AS DateTime), 20160506, 22, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (17, N'Pincode da duoc su dung.', N'841656244808', CAST(0x0000A5FE013B1794 AS DateTime), CAST(0x0000A5FE013B1794 AS DateTime), 20160506, 23, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (18, N'Pincode da duoc su dung.', N'841656244707', CAST(0x0000A5FE013B7085 AS DateTime), CAST(0x0000A5FE013B7085 AS DateTime), 20160506, 24, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (19, N'Ma khach hang gioi thieu khong ton tai.', N'841656244707', CAST(0x0000A5FE013BBDF8 AS DateTime), CAST(0x0000A5FE013BBDF8 AS DateTime), 20160506, 25, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (20, N'Ban da gioi thieu thanh cong chuong trinh “Vua co suc khoe vua co tien” cua VIFASPORT. VIFASPORT (0967 99 00 99) se lien lac voi ban de trao qua.', N'841656244808', CAST(0x0000A5FE013BD17D AS DateTime), CAST(0x0000A5FE013BD17D AS DateTime), 20160506, 26, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (21, N'Chuc mung ban da nhan duoc chiet khau 5% gia tri san pham ban vua 
mua. Vifasport (0967 99 00 99) se lien lac voi ban de trao qua.', N'841656244707', CAST(0x0000A5FE013BD17F AS DateTime), CAST(0x0000A5FE013BD17F AS DateTime), 20160506, 26, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (22, N'Ban da gioi thieu thanh cong chuong trinh “Vua co suc khoe vua co tien” cua VIFASPORT. VIFASPORT (0967 99 00 99) se lien lac voi ban de trao qua.', N'841656244707', CAST(0x0000A5FE013CD845 AS DateTime), CAST(0x0000A5FE013CD845 AS DateTime), 20160506, 27, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (23, N'Chuc mung ban da nhan duoc chiet khau 5% gia tri san pham ban vua 
mua. Vifasport (0967 99 00 99) se lien lac voi ban de trao qua.', N'841656244707', CAST(0x0000A5FE013CD847 AS DateTime), CAST(0x0000A5FE013CD847 AS DateTime), 20160506, 27, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (24, N'Ban da gioi thieu thanh cong chuong trinh “Vua co suc khoe vua co tien” cua VIFASPORT. VIFASPORT (0967 99 00 99) se lien lac voi ban de trao qua.', N'841656244707', CAST(0x0000A5FE013DDC7E AS DateTime), CAST(0x0000A5FE013DDC7E AS DateTime), 20160506, 28, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (25, N'Ban da tham gia thanh cong chuong trinh “Vua co suc khoe vua co tien” cua VIFASPORT voi ID: XW591. Hay gioi thieu khach mua san pham VIFASPORT de nhan duoc qua tang. Chi tiet xem tai www.vifasport.com', N'841656244707', CAST(0x0000A5FE013DDC80 AS DateTime), CAST(0x0000A5FE013DDC80 AS DateTime), 20160506, 28, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (26, N'Chuc mung ban da nhan duoc chiet khau 5% gia tri san pham ban vua 
mua. Vifasport (0967 99 00 99) se lien lac voi ban de trao qua.', N'841656244707', CAST(0x0000A5FE013DDC82 AS DateTime), CAST(0x0000A5FE013DDC82 AS DateTime), 20160506, 28, 1, NULL, NULL)
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (27, N'Ban da gioi thieu thanh cong chuong trinh “Vua co suc khoe vua co tien” cua VIFASPORT. VIFASPORT (0967 99 00 99) se lien lac voi ban de trao qua.', N'841656244909', CAST(0x0000A60100BB9104 AS DateTime), CAST(0x0000A60100BB9104 AS DateTime), 20160509, 29, 3, N'<SMS>    <Code>200</Code>    <Message>Sending...</Message>    <Time></Time></SMS>', N'20160509112251')
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (28, N'Ban da tham gia thanh cong chuong trinh “Vua co suc khoe vua co tien” cua VIFASPORT voi ID: AA111. Hay gioi thieu khach mua san pham VIFASPORT de nhan duoc qua tang. Chi tiet xem tai www.vifasport.com', N'841656244909', CAST(0x0000A60100BB917C AS DateTime), CAST(0x0000A60100BB917C AS DateTime), 20160509, 29, 3, N'<SMS>    <Code>200</Code>    <Message>Sending...</Message>    <Time></Time></SMS>', N'20160509112251')
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (29, N'Chuc mung ban da nhan duoc chiet khau 5% gia tri san pham ban vua 
mua. Vifasport (0967 99 00 99) se lien lac voi ban de trao qua.', N'841656244909', CAST(0x0000A60100BB91F5 AS DateTime), CAST(0x0000A60100BB91F5 AS DateTime), 20160509, 29, 3, N'<SMS>    <Code>200</Code>    <Message>Sending...</Message>    <Time></Time></SMS>', N'20160509112251')
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (30, N'Ban da gioi thieu thanh cong chuong trinh “Vua co suc khoe vua co tien” cua VIFASPORT. VIFASPORT (0967 99 00 99) se lien lac voi ban de trao qua.', N'0904426381', CAST(0x0000A601010EB8A0 AS DateTime), CAST(0x0000A60101129C35 AS DateTime), 20160509, 30, 2, N'<SMS>    <Code>200</Code>    <Message>Sending...</Message>    <Time></Time></SMS>', N'20160509162537191')
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (31, N'Ban da tham gia thanh cong chuong trinh “Vua co suc khoe vua co tien” cua VIFASPORT voi ID: BB222. Hay gioi thieu khach mua san pham VIFASPORT de nhan duoc qua tang. Chi tiet xem tai www.vifasport.com', N'0904426381', CAST(0x0000A601010EB915 AS DateTime), CAST(0x0000A601010EB915 AS DateTime), 20160509, 30, 1, N'<SMS>    <Code>200</Code>    <Message>Sending...</Message>    <Time></Time></SMS>', N'20160509162537192')
INSERT [dbo].[tblMt] ([MtID], [MtMessage], [PhoneNumber], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MoID], [MtStatus], [Remark], [GUID]) VALUES (32, N'Chuc mung ban da nhan duoc chiet khau 5% gia tri san pham ban vua 
mua. Vifasport (0967 99 00 99) se lien lac voi ban de trao qua.', N'0904426381', CAST(0x0000A601010EB98E AS DateTime), CAST(0x0000A601010EB98E AS DateTime), 20160509, 30, 1, N'<SMS>    <Code>200</Code>    <Message>Sending...</Message>    <Time></Time></SMS>', N'20160509162537192')
SET IDENTITY_INSERT [dbo].[tblMt] OFF
/****** Object:  Table [dbo].[tblMo]    Script Date: 05/11/2016 10:56:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMo](
	[MoID] [bigint] IDENTITY(1,1) NOT NULL,
	[PinCodeID] [bigint] NOT NULL,
	[MoMessage] [nvarchar](1000) NOT NULL,
	[PhoneNumber] [nvarchar](50) NOT NULL,
	[ServiceType] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[ShortCreatedDate] [int] NULL,
	[MainKey] [nvarchar](50) NOT NULL,
	[SubKeyID] [bigint] NOT NULL,
	[MoStatus] [tinyint] NOT NULL,
	[Guid] [nvarchar](255) NULL,
	[ReferralCustomerId] [bigint] NULL,
	[CustomerId] [bigint] NULL,
 CONSTRAINT [PK_tblMo] PRIMARY KEY CLUSTERED 
(
	[MoID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblMo] ON
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (1, 0, N'vifa fa123 vi123', N'8416869', -1, CAST(0x0000A5FE011D8E79 AS DateTime), CAST(0x0000A5FE011D8E79 AS DateTime), 20160506, N'VIFA', 0, 7, N'766667', 0, 0)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (2, 0, N'chuc mung nam moi', N'01656244909', -1, CAST(0x0000A5FE011E2C7A AS DateTime), CAST(0x0000A5FE011E2C7A AS DateTime), 20160506, N'VIFA', 0, 9, N'766667', 0, 0)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (3, 0, N'vifa fa 123 vi123', N'01656244909', -1, CAST(0x0000A5FE011EA000 AS DateTime), CAST(0x0000A5FE011EA000 AS DateTime), 20160506, N'VIFA', 1, 4, N'766667', 0, 0)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (4, 0, N'vifa fa123', N'01656244909', -1, CAST(0x0000A5FE011F3286 AS DateTime), CAST(0x0000A5FE011F3286 AS DateTime), 20160506, N'VIFA', 1, 2, N'766667', 0, 0)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (5, 0, N'vifa fa123 AA222', N'01656244909', -1, CAST(0x0000A5FE011FDC3E AS DateTime), CAST(0x0000A5FE011FDC3E AS DateTime), 20160506, N'VIFA', 1, 2, N'766667', 0, 0)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (10, 0, N'vifa 104981 AA222', N'01656244909', -1, CAST(0x0000A5FE0122A9AE AS DateTime), CAST(0x0000A5FE0122A9AE AS DateTime), 20160506, N'VIFA', 1, 11, N'766667', 0, 0)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (11, 0, N'vifa 104981', N'01656244909', -1, CAST(0x0000A5FE0123313B AS DateTime), CAST(0x0000A5FE0123313B AS DateTime), 20160506, N'VIFA', 1, 10, N'766667', 0, 0)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (12, 1, N'vifa 104981', N'01656244808', -1, CAST(0x0000A5FE012D6857 AS DateTime), CAST(0x0000A5FE012D6857 AS DateTime), 20160506, N'VIFA', 1, 3, N'766667', 0, 7)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (13, 0, N'vifa 767870', N'01656244808', -1, CAST(0x0000A5FE012F6F73 AS DateTime), CAST(0x0000A5FE012F6F73 AS DateTime), 20160506, N'VIFA', 1, 10, N'766667', 0, 0)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (14, 0, N'vifa 767870', N'01656244808', -1, CAST(0x0000A5FE012FE11B AS DateTime), CAST(0x0000A5FE012FE11B AS DateTime), 20160506, N'VIFA', 1, 10, N'766667', 0, 0)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (15, 0, N'vifa 767870', N'01656244808', -1, CAST(0x0000A5FE012FFA27 AS DateTime), CAST(0x0000A5FE012FFA27 AS DateTime), 20160506, N'VIFA', 1, 10, N'766667', 0, 0)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (16, 0, N'vifa 104981 AA222', N'01656244808', -1, CAST(0x0000A5FE013057C6 AS DateTime), CAST(0x0000A5FE013057C6 AS DateTime), 20160506, N'VIFA', 1, 5, N'766667', 0, 0)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (17, 0, N'vifa 104981 AA111', N'01656244808', -1, CAST(0x0000A5FE013082CA AS DateTime), CAST(0x0000A5FE013082CA AS DateTime), 20160506, N'VIFA', 1, 5, N'766667', 0, 0)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (18, 0, N'vifa 767870 AA222', N'01656244808', -1, CAST(0x0000A5FE0130AC41 AS DateTime), CAST(0x0000A5FE0130AC41 AS DateTime), 20160506, N'VIFA', 1, 11, N'766667', 0, 0)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (19, 2, N'vifa 767870 AA111', N'01656244808', -1, CAST(0x0000A5FE0130C357 AS DateTime), CAST(0x0000A5FE0130C357 AS DateTime), 20160506, N'VIFA', 1, 12, N'766667', 1, 7)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (20, 3, N'vifa 576904 AA111', N'01656244808', -1, CAST(0x0000A5FE013291D0 AS DateTime), CAST(0x0000A5FE013291D0 AS DateTime), 20160506, N'VIFA', 1, 12, N'766667', 1, 7)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (21, 0, N'vifa 576904 AA111', N'01656244808', -1, CAST(0x0000A5FE01333456 AS DateTime), CAST(0x0000A5FE01333456 AS DateTime), 20160506, N'VIFA', 1, 5, N'766667', 0, 0)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (22, 4, N'vifa 562442 AA111', N'01656244808', -1, CAST(0x0000A5FE01335790 AS DateTime), CAST(0x0000A5FE01335790 AS DateTime), 20160506, N'VIFA', 1, 12, N'766667', 1, 7)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (23, 0, N'vifa 562442 AA111', N'01656244808', -1, CAST(0x0000A5FE013B1792 AS DateTime), CAST(0x0000A5FE013B1792 AS DateTime), 20160506, N'VIFA', 1, 5, N'766667', 0, 0)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (24, 0, N'vifa 562442 BK396', N'01656244707', -1, CAST(0x0000A5FE013B7083 AS DateTime), CAST(0x0000A5FE013B7083 AS DateTime), 20160506, N'VIFA', 1, 5, N'766667', 0, 0)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (25, 0, N'vifa 795730 BK397', N'01656244707', -1, CAST(0x0000A5FE013BBDF6 AS DateTime), CAST(0x0000A5FE013BBDF6 AS DateTime), 20160506, N'VIFA', 1, 11, N'766667', 0, 0)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (26, 5, N'vifa 795730 BK396', N'01656244707', -1, CAST(0x0000A5FE013BD17A AS DateTime), CAST(0x0000A5FE013BD17A AS DateTime), 20160506, N'VIFA', 1, 12, N'766667', 7, 8)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (27, 6, N'vifa 323486 XW591', N'01656244707', -1, CAST(0x0000A5FE013CD842 AS DateTime), CAST(0x0000A5FE013CD842 AS DateTime), 20160506, N'VIFA', 1, 12, N'766667', 8, 8)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (28, 7, N'vifa 970137 XW591', N'01656244707', -1, CAST(0x0000A5FE013DDC7A AS DateTime), CAST(0x0000A5FE013DDC7A AS DateTime), 20160506, N'VIFA', 1, 12, N'766667', 8, 8)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (29, 8, N'vifa 483637 AA111', N'01656244909', -1, CAST(0x0000A60100BB8FB5 AS DateTime), CAST(0x0000A60100BB8FB5 AS DateTime), 20160509, N'VIFA', 1, 12, N'766667', 1, 1)
INSERT [dbo].[tblMo] ([MoID], [PinCodeID], [MoMessage], [PhoneNumber], [ServiceType], [CreatedDate], [UpdatedDate], [ShortCreatedDate], [MainKey], [SubKeyID], [MoStatus], [Guid], [ReferralCustomerId], [CustomerId]) VALUES (30, 9, N'vifa 124972 BB222', N'84904426381', -1, CAST(0x0000A601010EB7C6 AS DateTime), CAST(0x0000A601010EB7C6 AS DateTime), 20160509, N'VIFA', 1, 12, N'766667', 2, 2)
SET IDENTITY_INSERT [dbo].[tblMo] OFF
/****** Object:  Table [dbo].[tblKeyword]    Script Date: 05/11/2016 10:56:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblKeyword](
	[KeywordID] [bigint] IDENTITY(1,1) NOT NULL,
	[KeywordValue] [nvarchar](50) NOT NULL,
	[AccountManagerID] [bigint] NOT NULL,
	[KeywordStatus] [tinyint] NOT NULL,
	[TotalProductType] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_tblKeyword] PRIMARY KEY CLUSTERED 
(
	[KeywordID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblKeyword] ON
INSERT [dbo].[tblKeyword] ([KeywordID], [KeywordValue], [AccountManagerID], [KeywordStatus], [TotalProductType], [CreatedDate], [UpdatedDate]) VALUES (1, N'VIFA', -1, 1, 0, CAST(0x0000A5FC010FA897 AS DateTime), CAST(0x0000A5FC010FA897 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblKeyword] OFF
/****** Object:  Table [dbo].[tblErrorMessage]    Script Date: 05/11/2016 10:56:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblErrorMessage](
	[ErrorMessageID] [bigint] IDENTITY(1,1) NOT NULL,
	[ErrorMessage] [nvarchar](max) NOT NULL,
	[SubKeyID] [bigint] NOT NULL,
	[AccountManagerID] [bigint] NOT NULL,
	[MaxCharacter] [int] NOT NULL,
	[ErrorType] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_tblErrorMessage] PRIMARY KEY CLUSTERED 
(
	[ErrorMessageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCustomer]    Script Date: 05/11/2016 10:56:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCustomer](
	[CustomerId] [bigint] IDENTITY(1,1) NOT NULL,
	[ReferralCustomerId] [bigint] NULL,
	[CustomerCode] [nvarchar](1024) NULL,
	[CustomerName] [nvarchar](1024) NULL,
	[CustomerPhone] [nvarchar](50) NULL,
	[CustomerAddress] [nvarchar](1024) NULL,
	[CustomerEmail] [nvarchar](1024) NULL,
	[CustomerNote] [nvarchar](max) NULL,
	[CustomerRemark] [nvarchar](1024) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblCustomer] ON
INSERT [dbo].[tblCustomer] ([CustomerId], [ReferralCustomerId], [CustomerCode], [CustomerName], [CustomerPhone], [CustomerAddress], [CustomerEmail], [CustomerNote], [CustomerRemark], [CreatedDate], [UpdatedDate]) VALUES (1, 0, N'AA111', N'Nguyễn Minh Hiếu', N'841656244909', N'Bạch Mã, quận 10', N'hieu.nm@fibo.vn', N'Ghi chú 1
Ghi chú 2
Ghi chú 3, 4 5', NULL, CAST(0x0000A5FD00E70FE0 AS DateTime), CAST(0x0000A5FD00E70FE0 AS DateTime))
INSERT [dbo].[tblCustomer] ([CustomerId], [ReferralCustomerId], [CustomerCode], [CustomerName], [CustomerPhone], [CustomerAddress], [CustomerEmail], [CustomerNote], [CustomerRemark], [CreatedDate], [UpdatedDate]) VALUES (2, 1, N'BB222', N'Sương Nguyện Ánh', N'84904426381', N'Bạch Mã, Hồ chí Minh', N'anh@fibo.vn', N'Ghi chú 2', NULL, CAST(0x0000A5FD00F046BC AS DateTime), CAST(0x0000A5FD00F046BC AS DateTime))
INSERT [dbo].[tblCustomer] ([CustomerId], [ReferralCustomerId], [CustomerCode], [CustomerName], [CustomerPhone], [CustomerAddress], [CustomerEmail], [CustomerNote], [CustomerRemark], [CreatedDate], [UpdatedDate]) VALUES (7, 0, N'BK396', NULL, N'841656244808', NULL, NULL, NULL, NULL, CAST(0x0000A5FE012D63D3 AS DateTime), CAST(0x0000A5FE012D63D3 AS DateTime))
INSERT [dbo].[tblCustomer] ([CustomerId], [ReferralCustomerId], [CustomerCode], [CustomerName], [CustomerPhone], [CustomerAddress], [CustomerEmail], [CustomerNote], [CustomerRemark], [CreatedDate], [UpdatedDate]) VALUES (8, 0, N'XW591', NULL, N'841656244707', NULL, NULL, NULL, NULL, CAST(0x0000A5FE013BB981 AS DateTime), CAST(0x0000A5FE013BB981 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblCustomer] OFF
/****** Object:  Table [dbo].[tblConfig]    Script Date: 05/11/2016 10:56:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblConfig](
	[ConfigID] [bigint] IDENTITY(1,1) NOT NULL,
	[AccountManagerID] [bigint] NULL,
	[SubKeyID] [bigint] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
 CONSTRAINT [PK_tblConfig] PRIMARY KEY CLUSTERED 
(
	[ConfigID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblAccount]    Script Date: 05/11/2016 10:56:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblAccount](
	[AccountID] [bigint] IDENTITY(1,1) NOT NULL,
	[LoginName] [nvarchar](255) NOT NULL,
	[PassWord] [nvarchar](1000) NOT NULL,
	[FullName] [nvarchar](1000) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[EmailAddress] [nvarchar](1000) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[ParentId] [bigint] NULL,
 CONSTRAINT [PK_tblAccount] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblAccount] ON
INSERT [dbo].[tblAccount] ([AccountID], [LoginName], [PassWord], [FullName], [PhoneNumber], [EmailAddress], [CreatedDate], [UpdatedDate], [ParentId]) VALUES (1, N'admin', N'e10adc3949ba59abbe56e057f20f883e', N'MT', N'0933774112', N'ntmtri23@gmail.com', CAST(0x0000A5FC00CA8461 AS DateTime), CAST(0x0000A5FC00CA8461 AS DateTime), 0)
INSERT [dbo].[tblAccount] ([AccountID], [LoginName], [PassWord], [FullName], [PhoneNumber], [EmailAddress], [CreatedDate], [UpdatedDate], [ParentId]) VALUES (2, N'vsuser', N'e10adc3949ba59abbe56e057f20f883e', N'vsuser', N'01656244909', N'hieu@fibo.vn', CAST(0x0000A5FC01108DE5 AS DateTime), CAST(0x0000A5FC01108DE5 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[tblAccount] OFF
/****** Object:  StoredProcedure [dbo].[spSubKey_GetFilter]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spSubKey_GetFilter 100,1,-1,9,'',0
CREATE PROCEDURE [dbo].[spSubKey_GetFilter]
	@pagesize int, 
	@pagenum int,
	@accountManagerId bigint,
	@keywordId bigint,
	@subKeyValue nvarchar(50),
	@subKeyStatus tinyint
AS 
BEGIN 
	declare @min as bigint
	declare @max as bigint
	declare @totalRec as bigint
	if(@pageSize=0)-- Get all list, now paging
	begin
		set @min=0
		set @max = 100000
	end
	else
	begin
		set @min=(@pagenum - 1) * @pageSize + 1 
		set @max =@pagenum * @pageSize
	end
	
	select @totalRec= COUNT(*)
	from tblSubKey 
	inner join tblKeyword on tblKeyword.KeywordID = tblSubKey.KeywordID
	inner join tblAccount on tblAccount.AccountID = tblSubKey.AccountManagerID
	where 	--( @accountManagerId = -1 or AccountManagerID= @accountManagerId )  and 
			( @subKeyValue = ''	or SubKeyValue= @subKeyValue ) and 
			tblSubKey.KeywordID = @keywordId and 
			SubKeyStatus <> 3 /* Deleted */
			--( @subKeyStatus = 0 or SubKeyStatus = @subKeyStatus)
		
	select @totalRec AS TotalRec,tblSubKeyTemp.*
	from ( 
	select ROW_NUMBER() OVER(ORDER BY tblSubKey.SubKeyID desc) AS RowNum, tblSubKey.*,tblKeyword.KeywordValue, tblAccount.LoginName
	from tblSubKey 
	inner join tblKeyword on tblKeyword.KeywordID = tblSubKey.KeywordID 
	inner join tblAccount on tblAccount.AccountID = tblSubKey.AccountManagerID
	where
		--( @accountManagerId = -1 or AccountManagerID= @accountManagerId )  and 
		( @subKeyValue = ''	or SubKeyValue= @subKeyValue ) and 
		tblSubKey.KeywordID = @keywordId and 
		SubKeyStatus <> 3 /* Deleted */
		--( @subKeyStatus = 0 or SubKeyStatus = @subKeyStatus)  
	) tblSubKeyTemp 
	where	 RowNum BETWEEN @min and @max
	ORDER BY SubKeyID DESC
 END
GO
/****** Object:  StoredProcedure [dbo].[spSubKey_GetAll]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCode_CountPinCodeIsUsedByAccountID 1
CREATE PROCEDURE [dbo].[spSubKey_GetAll]
AS 
BEGIN 
	select * from tblSubKey where SubKeyStatus <> 3 /* Active */
END

--select * from tblMo
GO
/****** Object:  StoredProcedure [dbo].[spSubKey_CheckExists]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spSubKey_CheckExists] 
@subKey varchar(255),
@loginName nvarchar(255)
AS 
BEGIN 
	Declare @result int = 0
    if((SELECT COUNT(*) FROM tblSubKey WHERE SubKeyValue=@subKey) > 0 or (select count(*) from tblAccount where LoginName = @loginName) > 0)
	begin
		set @result = 1 /* Subkey or loginname is exists */
	end
	select @result as Result
END
GO
/****** Object:  StoredProcedure [dbo].[spSubKey_ChangeStatus]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spSubKey_ChangeStatus] 
@subKeyId bigint,
@subKeyStatus tinyint
AS 
BEGIN 
    update tblSubKey set SubKeyStatus = @subKeyStatus where SubKeyID = @subKeyId
    select @@ROWCOUNT
END
GO
/****** Object:  StoredProcedure [dbo].[spSubKey_AddUpdate]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spConfig_AddUpdate -1,1,-1,'2015-03-22','2015-03-25',null,null
CREATE PROCEDURE [dbo].[spSubKey_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@accountManagerId bigint,
	@subkeyvalue nvarchar(1000),
	@keywordId bigint,
	@subkeystatus tinyint,
	@createdDate datetime,
	@updatedDate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id < 0) 
    begin 
    insert into tblSubKey( 
            AccountManagerID, 
			KeywordID,
            SubKeyValue, 
            SubKeyStatus,
			CreatedDate,
			UpdatedDate
		  ) 
    values(
            @accountManagerId, 
			@keywordId,
            @subkeyvalue, 
            @subkeystatus, 
            getdate(),
			getdate()
          ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblSubKey set 
        AccountManagerID = @accountManagerId,  
        SubKeyValue = @subkeyvalue,
		SubKeyStatus = @subkeystatus,
		UpdatedDate = @updatedDate
        where SubKeyID = @id
		set @id = @@ROWCOUNT 
    end 
    -- Insert statements for procedure here 
    SELECT @id as SubKeyID 
END
GO
/****** Object:  StoredProcedure [dbo].[spProductType_GetProductTypeByAccountID]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spProductType_GetProductTypeByAccountID] 
@managerId bigint
AS 
BEGIN 
    select * from tblProductType
    where ManagerID = @managerId
END
GO
/****** Object:  StoredProcedure [dbo].[spProductType_GetFilter]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spProductType_GetFilter]
	@pagesize int, 
	@pagenum int,
	@managerId bigint,
	@productTypeName nvarchar(1000),
	@productTypeKey nvarchar(1000),
	@productTypeStatus tinyint
AS 
BEGIN 
	declare @min as bigint
	declare @max as bigint
	declare @totalRec as bigint
	if(@pageSize=0)-- Get all list, now paging
	begin
		set @min=0
		set @max = 100000
	end
	else
	begin
		set @min=(@pagenum - 1) * @pageSize + 1 
		set @max =@pagenum * @pageSize
	end
	
	select @totalRec= COUNT(*)
	from tblProductType inner join tblAccount on tblAccount.AccountID = tblProductType.ManagerID
	where 	--( @accountManagerId = -1 or AccountManagerID= @accountManagerId )  and 
			( @productTypeName = ''	or ProductTypeName like N'%' + @productTypeName + '%' ) and 
			(@productTypeKey = '' or ProductTypeKey = @productTypeKey) and 
			(@productTypeStatus = 0 or ProductTypeStatus = @productTypeStatus) and 
			(tblAccount.AccountID = @managerId) and 
			ProductTypeStatus <> 100 /* Deleted */
			--( @subKeyStatus = 0 or SubKeyStatus = @subKeyStatus)
		
	select @totalRec AS TotalRec,tblProductTypeTemp.*
	from ( 
	select ROW_NUMBER() OVER(ORDER BY tblProductType.ProductTypeID desc) AS RowNum, tblProductType.*,tblAccount.LoginName
	from tblProductType inner join tblAccount on tblAccount.AccountID = tblProductType.ManagerID
	where
		--( @accountManagerId = -1 or AccountManagerID= @accountManagerId )  and 
		( @productTypeName = ''	or ProductTypeName like N'%' + @productTypeName + '%' ) and 
		(@productTypeKey = '' or ProductTypeKey = @productTypeKey) and 
		(@productTypeStatus = 0 or ProductTypeStatus = @productTypeStatus) and 
		(tblAccount.AccountID = @managerId) and 
		ProductTypeStatus <> 100 /* Deleted */
	) tblProductTypeTemp
	where	 RowNum BETWEEN @min and @max
	ORDER BY ProductTypeID DESC
 END
GO
/****** Object:  StoredProcedure [dbo].[spProductType_AddUpdate]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spProductType_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@productTypeName nvarchar(max),
	@productTypeKey nvarchar(100),
	@managerID bigint,
	@productTypeStatus tinyint,
	@createdDate datetime,
	@updatedDate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if ((select count(*) from tblProductType where ManagerID = @managerID and ProductTypeKey = @productTypeKey) <= 0) 
    begin 
    insert into tblProductType( 
            ProductTypeName, 
            ProductTypeKey, 
            ManagerID,
			ProductTypeStatus,
            CreatedDate, 
            UpdatedDate) 
    values(
            @productTypeName, 
            @productTypeKey, 
            @managerID,
			@productTypeStatus,
            getdate(), 
            getdate()
                ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblProductType set 
        ProductTypeName = @productTypeName,
		ProductTypeKey = @productTypeKey,
		ProductTypeStatus = @productTypeStatus,
        UpdatedDate = getdate()
        where ManagerID = @managerID and ProductTypeKey = @productTypeKey
    end 
    -- Insert statements for procedure here 
    SELECT @id as ProductTypeID 
END
GO
/****** Object:  StoredProcedure [dbo].[spPinCodeTemp_DeletePinCodeByAccountID]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCodeTemp_CountPinCodeByAccountID 14
CREATE PROCEDURE [dbo].[spPinCodeTemp_DeletePinCodeByAccountID]
	@accountId bigint
AS 
BEGIN 
	delete from tblPinCodeTemp where CreatedByID = @accountId
END

--select * from tblMo
GO
/****** Object:  StoredProcedure [dbo].[spPinCodeTemp_CountPinCodeByAccountID]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPinCodeTemp_CountPinCodeByAccountID]
	@accountId bigint
AS 
BEGIN 
	select count(pc.PinCodeTempID) from tblPinCodeTemp pc
	where pc.CreatedByID = @accountId
END

--select * from tblMo
GO
/****** Object:  StoredProcedure [dbo].[spPinCodeTemp_AddUpdate]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPinCodeTemp_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@pincode nvarchar(50),
	@serial nvarchar(50),
	@createdbyid bigint ,
	@blockid bigint,
	@createddate datetime,
	@updateddate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id <= 0) 
    begin 
    insert into tblPinCodeTemp( 
            PinCode, 
            Serial,
            CreatedByID,
            BlockID,
            CreatedDate
           ) 
    values(
            @pincode, 
            @serial,
            @createdbyid,
            @blockid,
            @createddate
           ) 
        set @id = @@identity 
		--update fortune.tblPinCodeBlock set TotalPinCode = TotalPinCode + 1 where PinCodeBlockID = @pincodeblockid
    end 
    else 
    begin 
        update tblPinCodeTemp set 
        PinCode = @pincode, 
        Serial = @serial
        where PinCodeTempID = @id 
    end 
    -- Insert statements for procedure here 
    SELECT @id as PinCodeTempId 
END
GO
/****** Object:  StoredProcedure [dbo].[spPinCodeBlock_GetList]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCodeBlock_GetList
CREATE PROCEDURE [dbo].[spPinCodeBlock_GetList]
@accountManagerId bigint
AS 
BEGIN 
	select  * from tblPinCodeBlock where PinCodeBlockStatus = 1  /* Active */ and AccountManagerID = @accountManagerId
 END
GO
/****** Object:  StoredProcedure [dbo].[spPincodeBlock_Delete]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCodeBlock_Delete 45
CREATE PROC [dbo].[spPincodeBlock_Delete] 
@pincodeBlockId bigint
AS 
BEGIN 
	Declare @rowEff int = 0
    update tblPinCodeBlock set PinCodeBlockStatus = 100 where PinCodeBlockID = @pincodeBlockId
	set @rowEff = @rowEff + @@ROWCOUNT
	update tblPinCode set PinCodeStatus = 100 where PinCodeBlockID = @pincodeBlockId
	set @rowEff = @rowEff + @@ROWCOUNT
	select @rowEff
END

--select * from tblPinCode where PinCodeBlockID = 45

--update tblPinCode set PinCodeStatus = 1 where PinCodeBlockID = 45
GO
/****** Object:  StoredProcedure [dbo].[spPinCodeBlock_AddUpdate]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCodeBlock_AddUpdate -1,'cc',1,null,null
CREATE PROCEDURE [dbo].[spPinCodeBlock_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@pinCodeBlockName nvarchar(1000),
	@pinCodeBlockStatus tinyint,
	@accountManagerId bigint,
	@createdDate datetime,
	@updatedDate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id < 0) 
    begin 
    insert into tblPinCodeBlock( 
            PinCodeBlockName,
			PinCodeBlockStatus,
			AccountManagerID,
			CreatedDate,
			UpdatedDate
			) 
    values(
            @pinCodeBlockName, 
            @pinCodeBlockStatus, 
			@accountManagerId,
			getdate(),
			getdate()
            ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblPinCodeBlock set 
        PinCodeBlockName = @pinCodeBlockName,  
        PinCodeBlockStatus = @pinCodeBlockStatus
        where PinCodeBlockID = @id
		set @id = @@ROWCOUNT 
    end 
    -- Insert statements for procedure here 
    SELECT @id as PinCodeBlockId 
END
GO
/****** Object:  StoredProcedure [dbo].[spPinCode_UpdatePinCodeStatus]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPinCode_UpdatePinCodeStatus]
	@pinCodeStatus tinyint,
	@pinCodeId bigint
AS 
BEGIN 
	update tblPinCode set PinCodeStatus = @pinCodeStatus,UpdatedDate = getdate() where PinCodeID = @pinCodeId
	select @@ROWCOUNT
END

--select * from tblMt
GO
/****** Object:  StoredProcedure [dbo].[spPinCode_GetPinCode]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCode_CountPinCodeIsUsedByAccountID 1
CREATE PROCEDURE [dbo].[spPinCode_GetPinCode]
@pinCode nvarchar(50),
@accountManagerId bigint
AS 
BEGIN 
	select top 1 * from tblPinCode where PinCode = @pinCode and AccountManagerID = @accountManagerId and PinCodeStatus <> 3 /* Is Deleted */
END

--select * from tblMo
GO
/****** Object:  StoredProcedure [dbo].[spPinCode_GetFilter]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCode_GetFilter 1000,1,'','',-1,-1,1,'2015-01-01','2015-08-01'
CREATE PROCEDURE [dbo].[spPinCode_GetFilter]
	@pagesize int, 
	@currentPage int,
	@pinCode nvarchar(255),
	@serial nvarchar(255),
	@accountManagerID bigint,
	@pincodeblockid bigint,
	@pinCodeStatus tinyint,
	@fromDate datetime,
	@toDate datetime
AS 
BEGIN 
	declare @min as bigint
	declare @max as bigint
	declare @totalRec as bigint
	if(@pageSize=0)-- Get all list, now paging
	begin
		set @min=0
		set @max = 100000
	end
	else
	begin
		set @min=(@currentPage - 1) * @pageSize + 1 
		set @max = @currentPage * @pageSize
	end
	select @totalRec= COUNT(*)
	from tblPinCode pc 
	left join tblPinCodeBlock pcb on pc.PinCodeBlockID = pcb.PinCodeBlockID
	where 	( @accountManagerId = -1 or pc.AccountManagerID= @accountManagerId )  and 
			( @pinCode = ''	or PinCode= @pinCode ) and 
			( @serial = '' or Serial = @serial) and 
			( @pinCodeStatus = 0 or PinCodeStatus = @pinCodeStatus) and 
			( @pincodeblockid = 0 or pc.PinCodeBlockID = @pincodeblockid) --and 
			--( pc.CreatedDate > @fromDate and pc.CreatedDate < @toDate)
		
	select @totalRec AS TotalRec,tblPinCodeTemp.*
	from ( 
	select ROW_NUMBER() OVER(ORDER BY pc.PinCodeID desc) AS RowNum, pc.*,pcb.PinCodeBlockName
	from tblPinCode pc 
	left join tblPinCodeBlock pcb on pc.PinCodeBlockID = pcb.PinCodeBlockID
	where
		( @accountManagerId = -1 or pc.AccountManagerID= @accountManagerId )  and 
		( @pinCode = ''	or PinCode= @pinCode ) and 
		( @serial = '' or Serial = @serial) and 
		( @pinCodeStatus = 0 or PinCodeStatus = @pinCodeStatus) and 
		( @pincodeblockid = 0 or pc.PinCodeBlockID = @pincodeblockid) --and 
		--( pc.CreatedDate > @fromDate and pc.CreatedDate < @toDate)
	) tblPinCodeTemp 
	where	RowNum BETWEEN @min and @max
	ORDER BY PinCodeID DESC
 END
GO
/****** Object:  StoredProcedure [dbo].[spPinCode_CountPinCodeIsUsedByAccountID]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCode_CountPinCodeIsUsedByAccountID 1
CREATE PROCEDURE [dbo].[spPinCode_CountPinCodeIsUsedByAccountID]
	@accountId bigint
AS 
BEGIN 
	select count(pc.PinCodeID) from tblPinCode pc
	where pc.AccountManagerID = @accountId and pc.PinCodeStatus = 2 /* IsUsed */
END

--select * from tblMo
GO
/****** Object:  StoredProcedure [dbo].[spPinCode_CountPinCodeByAccountID]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPinCode_CountPinCodeByAccountID]
	@accountId bigint
AS 
BEGIN 
	select count(pc.PinCodeID) from tblPinCode pc
	where pc.AccountManagerID = @accountId
END

--select * from tblMo
GO
/****** Object:  StoredProcedure [dbo].[spPinCode_CheckPinCodeExists]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spPinCode_CheckPinCodeExists] 
@pinCode varchar(255),
@accountManagerId bigint
AS 
BEGIN 
    SELECT COUNT(*) FROM tblPinCode WHERE PinCode=@pinCode and AccountManagerID = @accountManagerId
END
GO
/****** Object:  StoredProcedure [dbo].[spPinCode_Checking]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spPinCode_Checking] 
@pinCode varchar(255),
@serial varchar(255)
AS 
BEGIN 
    SELECT * from tblPinCode pc inner join tblAccount acc on acc.AccountID = pc.AccountManagerID 
	where pc.PinCode = @pinCode and 
	pc.PinCodeStatus <> 3 /* Deleted */
END
GO
/****** Object:  StoredProcedure [dbo].[spPinCode_AddUpdate]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPinCode_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@pincode nvarchar(50),
	@serial nvarchar(50),
	@serial2 nvarchar(50),
	@accountmanagerid bigint ,
	@productTypeId bigint,
	@pincodeblockid bigint,
	@pincodestatus tinyint,
	@createddate datetime,
	@updateddate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id <= 0) 
    begin 
    insert into tblPinCode( 
            PinCode, 
            Serial,
			Serial2, 
            AccountManagerID,
			ProductTypeID,
            PinCodeStatus,
			PinCodeBlockID,
            CreatedDate, 
            UpdatedDate) 
    values(
            @pincode, 
            @serial,
			@serial2, 
            @accountmanagerid,
			@productTypeId,
            @pincodestatus,
			@pincodeblockid,
            @createddate, 
            @updateddate
                ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblPinCode set 
        PinCode = @pincode, 
        Serial = @serial,
		Serial2 = @serial2, 
        AccountManagerID = @accountmanagerid,
        PinCodeStatus=@pincodestatus,
        UpdatedDate = @updateddate
        where PinCodeID = @id 
    end 
    -- Insert statements for procedure here 
    SELECT @id as PinCodeId 
END
GO
/****** Object:  StoredProcedure [dbo].[spMtConfig_UpdateDefaultWrongSyntaxMessage]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spMtConfig_UpdateDefaultWrongSyntaxMessage] 
@message nvarchar(1000)
AS 
BEGIN 
    update tblMtConfig set MtMessage = @message 
    where AccountManagerID = -1 and MtType = 100
	select @@ROWCOUNT
END
GO
/****** Object:  StoredProcedure [dbo].[spMtConfig_GetDefaultWrongSyntaxMessage]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spMtConfig_GetDefaultWrongSyntaxMessage] 
AS 
BEGIN 
    select * from tblMtConfig
    where AccountManagerID = -1 and MtType = 100
END
GO
/****** Object:  StoredProcedure [dbo].[spMtConfig_GetConfigByAccountID]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spMtConfig_GetConfigByAccountID] 
@accountId bigint
AS 
BEGIN 
    select * from tblMtConfig
    where AccountManagerID = @accountId
END
GO
/****** Object:  StoredProcedure [dbo].[spMtConfig_AddUpdate]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spMtConfig_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@mtMessage nvarchar(max),
	@mtType tinyint,
	@accountmanagerid bigint,
	@createdDate datetime,
	@updatedDate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if ((select count(*) from tblMtConfig where AccountManagerID = @accountmanagerid and MtType = @mtType) <= 0) 
    begin 
    insert into tblMtConfig( 
            MtMessage, 
            MtType, 
            AccountManagerID,
            CreatedDate, 
            UpdatedDate) 
    values(
            @mtMessage, 
            @mtType, 
            @accountmanagerid,
            getdate(), 
            getdate()
                ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblMtConfig set 
        MtMessage = @mtMessage,  
        UpdatedDate = getdate()
        where AccountManagerID = @accountmanagerid and MtType = @mtType
    end 
    -- Insert statements for procedure here 
    SELECT @id as MtConfigId 
END
GO
/****** Object:  StoredProcedure [dbo].[spMt_UpdateStatusMt]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spMt_UpdateStatusMt]
	@guid nvarchar(1024),
	@status tinyint
as
begin
	update tblMt set MtStatus=@status,UpdatedDate=GETDATE()
	where guid=@guid
	
	select @@ROWCOUNT
end
GO
/****** Object:  StoredProcedure [dbo].[spMt_GetTotalPage]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spMt_GetTotalPage]
	@pagesize int,
	@moid bigint,
	@phonenumber nvarchar(50),
	@message nvarchar(500)
AS 
BEGIN 
	declare    @pageTemporary int
	set        @pageTemporary = 0 
	select     @pageTemporary =	count( *) 
	from	tblMt
	where 
		( @moid = 0	or MoId = @moid )  and 
		( @phonenumber = ''	or PhoneNumber = @phonenumber )  and 
		( @message = ''	or MtMessage = @message )   
	if(@pageTemporary%@pagesize=0) 
		begin
			set @pageTemporary = @pageTemporary/@pagesize 
		end
	else
		begin
			set @pageTemporary = @pageTemporary/@pagesize +1 
		end
	select @pageTemporary as TotalPage 
END

--select * from tblMt
GO
/****** Object:  StoredProcedure [dbo].[spMt_GetList]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spMt_GetList]
	@moid bigint,
	@phonenumber nvarchar(50),
	@message nvarchar(500)
AS 
BEGIN 
	select  *
	from tblMt
	where		( @moid = 0	or MoId = @moid )  and 
		( @phonenumber = ''	or PhoneNumber = @phonenumber )  and 
		( @message = ''	or MtMessage = @message )   
	order by MtId desc
 END
GO
/****** Object:  StoredProcedure [dbo].[spMt_GetFilter]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spMt_GetFilter] --1000,1,0,'','',0,getdate(),getdate(),0
	@pagesize int, 
	@pagenum int,
	@moid bigint,
	@phonenumber nvarchar(50),
	@message nvarchar(500),
	@accountManagerId bigint,
	@fromDate datetime,
	@toDate datetime,
	@status tinyint
AS 
BEGIN 
	declare @min as bigint
	declare @max as bigint
	declare @totalRec as bigint
	if(@pageSize=0)-- Get all list, now paging
	begin
		set @min=0
		set @max = 100000
	end
	else
	begin
		set @min=(@pagenum - 1) * @pageSize + 1 
		set @max =@pagenum * @pageSize
	end
	
	select @totalRec= COUNT(*)
	from tblMt mt 
	inner join tblMo mo on mt.MoID = mo.MoID 
	inner join tblSubKey s on s.SubKeyID = mo.SubKeyID
	where 	( @moid = 0	or mt.MoId= @moid )  and 
			( @phonenumber = ''	or mt.PhoneNumber= @phonenumber )  and 
			( @message = ''	or MtMessage like '%'+@message+'%' ) and 
			( mt.CreatedDate BETWEEN  @fromdate and @todate) and
			s.AccountManagerID = @accountManagerId and
			(@status=0 or MtStatus=@status)
		
	select @totalRec AS TotalRec,tblMtTemp.*
	from ( 
	select ROW_NUMBER() OVER(ORDER BY mt.MtId desc) AS RowNum, mt.*
	from tblMt mt 
	inner join tblMo mo on mt.MoID = mo.MoID 
	inner join tblSubKey s on s.SubKeyID = mo.SubKeyID
	where
		( @moid = 0	or mt.MoId= @moid )  and 
		( @phonenumber = ''	or mt.PhoneNumber= @phonenumber )  and 
		( @message = ''	or MtMessage like '%'+@message+'%' ) and 
		( mt.CreatedDate BETWEEN  @fromdate and @todate) and
		s.AccountManagerID = @accountManagerId and
		(@status=0 or MtStatus=@status)
	) tblMtTemp 
	where	 RowNum BETWEEN @min and @max
	ORDER BY MtId DESC
 END
GO
/****** Object:  StoredProcedure [dbo].[spMt_AddUpdate]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spConfig_AddUpdate -1,1,-1,'2015-03-22','2015-03-25',null,null
--select * from tblMt
CREATE PROCEDURE [dbo].[spMt_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@mtMessage nvarchar(1000),
	@phoneNumber nvarchar(1000),
	@shortCreatedDate int,
	@moId bigint,
	@createdDate datetime,
	@updatedDate datetime,
	@MTStatus tinyint,
	@Remark nvarchar(1024),
	@guid nvarchar(1024)
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id < 0) 
    begin 
    insert into tblMt( 
           MtMessage,
		   PhoneNumber,
		   ShortCreatedDate,
		   MoID,
		   MtStatus,
		   CreatedDate,
		   UpdatedDate,
		   remark,
		   guid
			) 
    values(
            @mtMessage, 
            @phoneNumber, 
			@shortCreatedDate,
			@moId,
			@MTStatus,
			getdate(),
			getdate(),
			@Remark,
			@guid
           ) 
        set @id = @@identity 
    end 
    -- Insert statements for procedure here 
    SELECT @id as MtId 
END
GO
/****** Object:  StoredProcedure [dbo].[spMo_ViewAllMoErrorSyntax]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spMo_ViewAllMoErrorSyntax]
	@pagesize int, 
	@pagenum int,
	@pincode nvarchar(250),
	@phonenumber nvarchar(50),
	@message nvarchar(500),
	@mostatus tinyint,
	@fromdate datetime,
	@todate datetime,
	@moId bigint,
	@accountManagerId bigint
AS 
BEGIN 
	declare @min as bigint
	declare @max as bigint
	declare @totalRec as bigint
	if(@pageSize=0)-- Get all list, now paging
	begin
		set @min=0
		set @max = 100000
	end
	else
	begin
		set @min=(@pagenum - 1) * @pageSize + 1 
		set @max =@pagenum * @pageSize
	end
	
	select @totalRec= COUNT(*)
	from tblMo mo 
	left join tblPinCode on mo.PinCodeID = tblPinCode.PinCodeID 
	--inner join tblSubKey sub on sub.SubKeyID = mo.SubKeyID
	where 	( @pincode = ''	or tblPinCode.PinCode = @pincode )  and 
			( @phonenumber = ''	or mo.PhoneNumber = @phonenumber )  and 
			( @message = ''		or mo.MoMessage like + '%' + @message + '%' )  and 
			(( @fromdate = 0 and @todate = 0)or(mo.CreatedDate BETWEEN  @fromdate and @todate )) and
			(@moId = -1 or mo.MoID = @moId) and mo.SubKeyID = 0
			--sub.AccountManagerID = @accountManagerId

	select @totalRec AS TotalRec,tblMoTemp.*
	from ( 
	select ROW_NUMBER() OVER(ORDER BY mo.MoId desc) AS RowNum, mo.*, isnull(PinCode, '')	as PinCode
	from tblMo mo
	left join tblPinCode on mo.PinCodeID = tblPinCode.PinCodeID 
	--inner join tblSubKey sub on sub.SubKeyID = mo.SubKeyID
	where
		( @pincode = ''	or tblPinCode.PinCode = @pincode )  and 
		( @phonenumber = ''	or mo.PhoneNumber = @phonenumber )  and 
		( @message = ''		or mo.MoMessage like + '%' + @message + '%' )  and 
		(( @fromdate = 0 and @todate = 0)or(mo.CreatedDate BETWEEN  @fromdate and @todate ) ) and 
		(@moId = -1 or mo.MoID = @moId) and mo.SubKeyID = 0
		--sub.AccountManagerID = @accountManagerId
	) tblMoTemp 
	where	 RowNum BETWEEN @min and @max
	ORDER BY MoId DESC
 END
GO
/****** Object:  StoredProcedure [dbo].[spMo_GetFilter]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spMo_GetFilter 100,1,'','','',0,0,0,-1,-1
CREATE PROCEDURE [dbo].[spMo_GetFilter]
	@pagesize int, 
	@pagenum int,
	@pincode nvarchar(250),
	@phonenumber nvarchar(50),
	@message nvarchar(500),
	@mostatus tinyint,
	@fromdate datetime,
	@todate datetime,
	@moId bigint,
	@accountManagerId bigint
AS 
BEGIN 
	declare @min as bigint
	declare @max as bigint
	declare @totalRec as bigint
	if(@pageSize=0)-- Get all list, now paging
	begin
		set @min=0
		set @max = 100000
	end
	else
	begin
		set @min=(@pagenum - 1) * @pageSize + 1 
		set @max =@pagenum * @pageSize
	end
	
	select @totalRec= COUNT(*)
	from tblMo mo 
	left join tblPinCode on mo.PinCodeID = tblPinCode.PinCodeID 
	inner join tblSubKey s on mo.SubKeyID = s.SubKeyID
	--inner join tblProductType pt on pt.ProductTypeID = mo.SubKeyID
	where 	( @pincode = ''	or tblPinCode.PinCode = @pincode )  and 
			( @phonenumber = ''	or mo.PhoneNumber = @phonenumber )  and 
			(@mostatus = 0 or mo.MoStatus = @mostatus) and 
			( @message = ''		or mo.MoMessage like + '%' + @message + '%' )  and 
			(( @fromdate = 0 and @todate = 0)or(mo.CreatedDate BETWEEN  @fromdate and @todate )) and
			(@moId = -1 or mo.MoID = @moId) and 
			s.AccountManagerID = @accountManagerId

	select @totalRec AS TotalRec,tblMoTemp.*
	from ( 
	select ROW_NUMBER() OVER(ORDER BY mo.MoId desc) AS RowNum, mo.*,s.SubKeyValue as SubKeyValue, isnull(PinCode, '')	as PinCode,
	referral.CustomerCode as ReferralCustomerCode,customer.CustomerCode as CustomerCode
	from tblMo mo
	left join tblPinCode on mo.PinCodeID = tblPinCode.PinCodeID 
	inner join tblSubKey s on mo.SubKeyID = s.SubKeyID
	left join tblCustomer as referral on mo.ReferralCustomerId=referral.CustomerId
	left join tblCustomer as customer on mo.CustomerId=customer.CustomerId
	where
		( @pincode = ''	or tblPinCode.PinCode = @pincode )  and 
		( @phonenumber = ''	or mo.PhoneNumber = @phonenumber )  and 
		( @message = ''		or mo.MoMessage like + '%' + @message + '%' )  and 
		(@mostatus = 0 or mo.MoStatus = @mostatus) and 
		(( @fromdate = 0 and @todate = 0)or(mo.CreatedDate BETWEEN  @fromdate and @todate ) ) and 
		(@moId = -1 or mo.MoID = @moId) and 
		s.AccountManagerID = @accountManagerId
	) tblMoTemp 
	where	 RowNum BETWEEN @min and @max
	ORDER BY MoId DESC
 END
GO
/****** Object:  StoredProcedure [dbo].[spMo_CountMoByAccountID]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spMo_CountMoByAccountID]
	@accountId bigint
AS 
BEGIN 
	select count(mo.MoID) from tblMo mo 
	inner join tblSubKey sub on sub.SubKeyID = mo.SubKeyID
	where sub.AccountManagerID = @accountId
END

--select * from tblMo
GO
/****** Object:  StoredProcedure [dbo].[spMo_AddUpdate]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spConfig_AddUpdate -1,1,-1,'2015-03-22','2015-03-25',null,null
--select * from tblMo
CREATE PROCEDURE [dbo].[spMo_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@pincodeid bigint,
	@moMessage nvarchar(1000),
	@phoneNumber nvarchar(1000),
	@shortCreatedDate int,
	@mainKey nvarchar(1000),
	@subKeyID bigint,
	@moStatus tinyint,
	@guid nvarchar(1000),
	@createdDate datetime,
	@updatedDate datetime,
	@ReferralCustomerId bigint,
	@CustomerId bigint
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id < 0) 
    begin 
    insert into tblMo( 
            PinCodeID, 
            MoMessage, 
            PhoneNumber,
            ServiceType,
			ShortCreatedDate,
			CreatedDate,
			UpdatedDate,
			MainKey,
			SubKeyID,
			MoStatus,
			[Guid],
			ReferralCustomerId,
			CustomerId
			) 
    values(
            @pincodeid, 
            @moMessage, 
            @phoneNumber, 
            -1,
			@shortCreatedDate,
			getdate(),
			getdate(),
			@mainKey,
			@subKeyID,
			@moStatus,
			@guid,
			@ReferralCustomerId,
			@CustomerId
           ) 
        set @id = @@identity 
    end 
    -- Insert statements for procedure here 
    SELECT @id as MoId 
END
GO
/****** Object:  StoredProcedure [dbo].[spKeyword_GetFilter]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spKeyword_GetFilter]
	@pagesize int, 
	@pagenum int,
	@accountManagerId bigint,
	@keyword nvarchar(50),
	@keywordStatus tinyint
AS 
BEGIN 
	declare @min as bigint
	declare @max as bigint
	declare @totalRec as bigint
	if(@pageSize=0)-- Get all list, now paging
	begin
		set @min=0
		set @max = 100000
	end
	else
	begin
		set @min=(@pagenum - 1) * @pageSize + 1 
		set @max =@pagenum * @pageSize
	end
	
	select @totalRec= COUNT(*)
	from tblKeyword --inner join tblAccount on tblAccount.AccountID = tblKeyword.AccountManagerID
	where 	--( @accountManagerId = -1 or AccountManagerID= @accountManagerId )  and 
			( @keyword = ''	or KeywordValue= @keyword ) and 
			KeywordStatus <> 3 /* Deleted */
			--( @subKeyStatus = 0 or SubKeyStatus = @subKeyStatus)
		
	select @totalRec AS TotalRec,tblKeywordTemp.*
	from ( 
	select ROW_NUMBER() OVER(ORDER BY tblKeyword.KeywordID desc) AS RowNum, tblKeyword.*--,tblAccount.LoginName
	from tblKeyword --inner join tblAccount on tblAccount.AccountID = tblKeyword.AccountManagerID
	where
		--( @accountManagerId = -1 or AccountManagerID= @accountManagerId )  and 
		( @keyword = ''	or KeywordValue= @keyword ) and 
		KeywordStatus <> 3 /* Deleted */
		--( @subKeyStatus = 0 or SubKeyStatus = @subKeyStatus)  
	) tblKeywordTemp 
	where	 RowNum BETWEEN @min and @max
	ORDER BY KeywordID DESC
 END
GO
/****** Object:  StoredProcedure [dbo].[spKeyword_GetAll]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spPinCode_CountPinCodeIsUsedByAccountID 1
CREATE PROCEDURE [dbo].[spKeyword_GetAll]
AS 
BEGIN 
	select * from tblKeyword where KeywordStatus <> 3 /* Active */
END

--select * from tblMo
GO
/****** Object:  StoredProcedure [dbo].[spKeyword_CheckExists]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spKeyword_CheckExists] 
@keyword varchar(255),
@loginName nvarchar(255)
AS 
BEGIN 
	Declare @result int = 0
    if((SELECT COUNT(*) FROM tblKeyword WHERE KeywordValue=@keyword) > 0 or (select count(*) from tblAccount where LoginName = @loginName) > 0)
	begin
		set @result = 1 /* Subkey or loginname is exists */
	end
	select @result as Result
END
GO
/****** Object:  StoredProcedure [dbo].[spKeyword_ChangeStatus]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spKeyword_ChangeStatus] 
@keywordId bigint,
@keywordStatus tinyint
AS 
BEGIN 
    update tblKeyword set KeywordStatus = @keywordStatus where KeywordID = @keywordId
    select @@ROWCOUNT
END
GO
/****** Object:  StoredProcedure [dbo].[spKeyword_AddUpdate]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from tblKeyword
--spConfig_AddUpdate -1,1,-1,'2015-03-22','2015-03-25',null,null
CREATE PROCEDURE [dbo].[spKeyword_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@accountManagerId bigint,
	@keywordValue nvarchar(1000),
	@keywordstatus tinyint,
	@totalProductType int,
	@createdDate datetime,
	@updatedDate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id < 0) 
    begin 
    insert into tblKeyword( 
            AccountManagerID, 
            KeywordValue, 
            KeywordStatus,
			TotalProductType,
			CreatedDate,
			UpdatedDate
		  ) 
    values(
            @accountManagerId, 
            @keywordValue, 
            @keywordstatus,
			@totalProductType, 
            getdate(),
			getdate()
          ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblKeyword set 
        AccountManagerID = @accountManagerId,  
        KeywordValue = @keywordValue,
		KeywordStatus = @keywordstatus,
		UpdatedDate = @updatedDate
        where KeywordID = @id
		set @id = @@ROWCOUNT 
    end 
    -- Insert statements for procedure here 
    SELECT @id as KeywordID 
END
GO
/****** Object:  StoredProcedure [dbo].[spCustomer_Update]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCustomer_Update]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
    @customername nvarchar(1024), 
    @customeraddress nvarchar(1024), 
    @customeremail nvarchar(1024), 
    @customernote nvarchar(MAX)
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
	SET NOCOUNT ON; 
    if (@id <= 0) 
    begin 
        set @id = 0
    end 
    else 
    begin 
        update tblCustomer set 
        CustomerName = @customername, 
        CustomerAddress = @customeraddress, 
        CustomerEmail = @customeremail, 
        CustomerNote = @customernote
        where CustomerId = @id 
        
        set @id=@@ROWCOUNT
    end 
    -- Insert statements for procedure here 
    SELECT @id as CustomerId 
END
GO
/****** Object:  StoredProcedure [dbo].[spCustomer_GetTotalPage]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCustomer_GetTotalPage]
	@pagesize int,
	@referralcustomerid bigint,
	@customercode nvarchar(1024),
	@customername nvarchar(1024),
	@customerphone nvarchar(50),
	@customeraddress nvarchar(1024),
	@customeremail nvarchar(1024),
	@customernote nvarchar(MAX),
	@customerremark nvarchar(1024)
AS 
BEGIN 
	declare    @pageTemporary int
	set        @pageTemporary = 0 
	select     @pageTemporary =	count( *) 
	from	tblCustomer
	where 
		( @referralcustomerid = 0	or @referralcustomerid=ReferralCustomerId )  and 
		( @customercode = ''	or @customercode=CustomerCode )  and 
		( @customername = ''	or @customername=CustomerName )  and 
		( @customerphone = ''	or @customerphone=CustomerPhone )  and 
		( @customeraddress = ''	or @customeraddress=CustomerAddress )  and 
		( @customeremail = ''	or @customeremail=CustomerEmail )  and 
		( @customernote = ''	or @customernote=CustomerNote )  and 
		( @customerremark = ''	or @customerremark=CustomerRemark )   
	if(@pageTemporary%@pagesize=0) 
		begin
			set @pageTemporary = @pageTemporary/@pagesize 
		end
	else
		begin
			set @pageTemporary = @pageTemporary/@pagesize +1 
		end
	select @pageTemporary as TotalPage 
END
GO
/****** Object:  StoredProcedure [dbo].[spCustomer_GetList]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCustomer_GetList]
	@pagesize int, 
	@pagenum int,
	@referralcustomerid bigint,
	@customercode nvarchar(1024),
	@customername nvarchar(1024),
	@customerphone nvarchar(50),
	@customeraddress nvarchar(1024),
	@customeremail nvarchar(1024),
	@customernote nvarchar(MAX),
	@customerremark nvarchar(1024)
AS 
BEGIN 
	select * 
	from ( 
	select ROW_NUMBER() OVER(ORDER BY tblCustomer.CustomerId desc) AS RowNum, *
	from tblCustomer
	where
		( @referralcustomerid = 0	or @referralcustomerid=ReferralCustomerId )  and 
		( @customercode = ''	or @customercode=CustomerCode )  and 
		( @customername = ''	or @customername=CustomerName )  and 
		( @customerphone = ''	or @customerphone=CustomerPhone )  and 
		( @customeraddress = ''	or @customeraddress=CustomerAddress )  and 
		( @customeremail = ''	or @customeremail=CustomerEmail )  and 
		( @customernote = ''	or @customernote=CustomerNote )  and 
		( @customerremark = ''	or @customerremark=CustomerRemark )   
	) tblCustomerTemp 
	where	RowNum BETWEEN (@pagenum - 1) * @pagesize + 1 AND @pagenum * @pagesize 
	order by CustomerId desc 
 END
GO
/****** Object:  StoredProcedure [dbo].[spCustomer_GetFilter]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCustomer_GetFilter]
	@pagesize int, 
	@pagenum int,
	@FromDate datetime,
	@ToDate datetime,
	@CustomerName nvarchar(1024),
	@CustomerPhone nvarchar(1024),
	@CustomerAddress nvarchar(1024),
	@CustomerEmail nvarchar(1024),
	@CustomerCode nvarchar(1024),
	@ReferralCustomerCode nvarchar(1024)
AS 
BEGIN 
	declare @min as bigint
	declare @max as bigint
	declare @totalRec as bigint
	if(@pageSize=0)-- Get all list, now paging
	begin
		set @min=0
		set @max = 100000
	end
	else
	begin
		set @min=(@pagenum - 1) * @pageSize + 1 
		set @max =@pagenum * @pageSize
	end
	
	------------------------
	
	select @totalRec= COUNT(*)
	from tblcustomer as customer 
	left join tblcustomer as referral on customer.ReferralCustomerId = referral.customerid 
	where 
	(customer.customername like '%'+@CustomerName+'%' or @CustomerName = '') and
	(customer.customerphone like '%'+@CustomerPhone+'%' or @CustomerPhone = '') and
	(customer.customeraddress like '%'+@CustomerAddress+'%' or @CustomerAddress = '') and	
	(customer.customeremail like '%'+@CustomerEmail+'%' or @CustomerEmail = '') and	
	(customer.customercode like '%'+@CustomerCode+'%' or @CustomerCode = '') and	
	(referral.customercode like '%'+@ReferralCustomerCode+'%' or @ReferralCustomerCode = '') and	
	customer.createddate between @FromDate and @ToDate
	--------------------------------
	
	select @totalRec AS TotalRec,tblMtTemp.*
	from 
	( 
		select ROW_NUMBER() OVER(ORDER BY customer.customerid desc) AS RowNum, 
		customer.CustomerId,customer.CustomerCode,customer.CustomerName,customer.CustomerPhone,customer.CustomerEmail,customer.CustomerAddress,customer.CustomerNote,customer.CreatedDate,
		referral.customercode as ReferralCustomerCode
		from tblcustomer customer 
		left join tblcustomer as referral on customer.ReferralCustomerId = referral.customerid 
		where
			(customer.customername like '%'+@CustomerName+'%' or @CustomerName = '') and
			(customer.customerphone like '%'+@CustomerPhone+'%' or @CustomerPhone = '') and
			(customer.customeraddress like '%'+@CustomerAddress+'%' or @CustomerAddress = '') and	
			(customer.customeremail like '%'+@CustomerEmail+'%' or @CustomerEmail = '') and	
			(customer.customercode like '%'+@CustomerCode+'%' or @CustomerCode = '') and	
			(referral.customercode like '%'+@ReferralCustomerCode+'%' or @ReferralCustomerCode = '') and	
			customer.createddate between @FromDate and @ToDate
	) tblMtTemp 
	where	 RowNum BETWEEN @min and @max
	ORDER BY customerid DESC
 END
GO
/****** Object:  StoredProcedure [dbo].[spCustomer_GetCustomerBy]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spCustomer_GetCustomerBy] --'841656244909',''
@phone varchar(50),
@code nvarchar(100)
AS 
BEGIN 
    select top 1 * from tblCustomer
    where (CustomerPhone=@phone or @phone='')
    and (CustomerCode=@code or @code='')
END
GO
/****** Object:  StoredProcedure [dbo].[spCustomer_GetCustomer]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spCustomer_GetCustomer] 
@customerid bigint 
AS 
BEGIN 
    SELECT * FROM tblCustomer WHERE CustomerId=@customerid 
END
GO
/****** Object:  StoredProcedure [dbo].[spCustomer_ExistsCustomerCode]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCustomer_ExistsCustomerCode]  
    -- Add the parameters for the stored procedure here 
    @code nvarchar(50)
AS 
BEGIN 
    if exists (select customerid from tblcustomer where customercode=@code)
    begin
		select 1
    end
    else
    begin
		select 0
    end
END
GO
/****** Object:  StoredProcedure [dbo].[spCustomer_AddUpdate]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCustomer_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
    @referralcustomerid bigint, 
    @customercode nvarchar(1024), 
    @customername nvarchar(1024), 
    @customerphone nvarchar(50), 
    @customeraddress nvarchar(1024), 
    @customeremail nvarchar(1024), 
    @customernote nvarchar(MAX), 
    @customerremark nvarchar(1024), 
    @createddate datetime, 
    @updateddate datetime 
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id <= 0) 
    begin 
    insert into tblCustomer( 
            ReferralCustomerId, 
            CustomerCode, 
            CustomerName, 
            CustomerPhone, 
            CustomerAddress, 
            CustomerEmail, 
            CustomerNote, 
            CustomerRemark, 
            CreatedDate, 
            UpdatedDate) 
    values(
            @referralcustomerid, 
            @customercode, 
            @customername, 
            @customerphone, 
            @customeraddress, 
            @customeremail, 
            @customernote, 
            @customerremark, 
            @createddate, 
            @updateddate
                ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblCustomer set 
        ReferralCustomerId = @referralcustomerid, 
        CustomerCode = @customercode, 
        CustomerName = @customername, 
        CustomerPhone = @customerphone, 
        CustomerAddress = @customeraddress, 
        CustomerEmail = @customeremail, 
        CustomerNote = @customernote, 
        CustomerRemark = @customerremark, 
        CreatedDate = @createddate, 
        UpdatedDate = @updateddate
        where CustomerId = @id 
    end 
    -- Insert statements for procedure here 
    SELECT @id as CustomerId 
END
GO
/****** Object:  StoredProcedure [dbo].[spConfig_GetConfigByAccountID]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spConfig_GetConfigByAccountID] 
@accountId bigint
AS 
BEGIN 
    select top 1 * from tblConfig
    where AccountManagerID = @accountId
END
GO
/****** Object:  StoredProcedure [dbo].[spConfig_AddUpdate]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spConfig_AddUpdate -1,1,-1,'2015-03-22','2015-03-25',null,null
CREATE PROCEDURE [dbo].[spConfig_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@accountManagerId bigint,
	@subkeyId bigint,
	@startDate datetime,
	@enddate datetime,
	@createdDate datetime,
	@updatedDate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if ((select count(*) from tblConfig where AccountManagerID = @accountmanagerid) <= 0) 
    begin 
    insert into tblConfig( 
            AccountManagerID, 
            SubKeyID, 
            StartDate,
            EndDate
			) 
    values(
            @accountManagerId, 
            @subkeyId, 
            @startDate, 
            @enddate
                ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblConfig set 
        StartDate = @startDate,  
        EndDate = @enddate
        where AccountManagerID = @accountmanagerid
		set @id = @@ROWCOUNT 
    end 
    -- Insert statements for procedure here 
    SELECT @id as ConfigId 
END
GO
/****** Object:  StoredProcedure [dbo].[spAccount_Login]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spAccount_Login] 
@userName nvarchar(255),
@passWord nvarchar(255) 
AS 
BEGIN 
    select * from tblAccount
    where LoginName = @userName and [PassWord] = @passWord
END
GO
/****** Object:  StoredProcedure [dbo].[spAccount_CheckAutoLogin]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spAccount_CheckAutoLogin] 
@accountId bigint,
@parentId bigint 
AS 
BEGIN 
    select * from tblAccount
    where AccountID = @accountId and ParentId = @parentId
END
GO
/****** Object:  StoredProcedure [dbo].[spAccount_ChangePassword]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spAccount_ChangePassword] 
@loginName nvarchar(255),
@newPassword nvarchar(255)
AS 
BEGIN 
    update tblAccount set [Password] = @newPassword where LoginName = @loginName
    select @@ROWCOUNT
END
GO
/****** Object:  StoredProcedure [dbo].[spAccount_AddUpdate]    Script Date: 05/11/2016 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--spConfig_AddUpdate -1,1,-1,'2015-03-22','2015-03-25',null,null
CREATE PROCEDURE [dbo].[spAccount_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
	@loginName nvarchar(1000),
	@password nvarchar(1000),
	@fullname nvarchar(1000),
	@phoneNumber nvarchar(1000),
	@emailAddress nvarchar(1000),
	@parentId bigint,
	@createdDate datetime,
	@updatedDate datetime
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id < 0) 
    begin 
    insert into tblAccount( 
            LoginName, 
            [PassWord], 
            FullName,
            PhoneNumber,
			EmailAddress,
			ParentId,
			CreatedDate,
			UpdatedDate
			) 
    values(
            @loginName, 
            --'e10adc3949ba59abbe56e057f20f883e',
			@password, 
            @fullname, 
            @phoneNumber,
			@emailAddress,
			@parentId,
			getdate(),
			getdate()
            ) 
        set @id = @@identity 
    end 
    else 
    begin 
        update tblAccount set 
        PhoneNumber = @phoneNumber,  
        EmailAddress = @emailAddress,
		FullName = @fullname,
		ParentId = @parentId
        where AccountID = @id
		set @id = @@ROWCOUNT 
    end 
    -- Insert statements for procedure here 
    SELECT @id as AccountId 
END
GO
/****** Object:  Default [DF_tblAccount_CreatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblAccount] ADD  CONSTRAINT [DF_tblAccount_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_tblAccount_UpdatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblAccount] ADD  CONSTRAINT [DF_tblAccount_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
/****** Object:  Default [DF__tblCustom__Refer__6383C8BA]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblCustomer] ADD  DEFAULT ((0)) FOR [ReferralCustomerId]
GO
/****** Object:  Default [DF__tblCustom__Creat__6477ECF3]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblCustomer] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF__tblCustom__Updat__656C112C]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblCustomer] ADD  DEFAULT (getdate()) FOR [UpdatedDate]
GO
/****** Object:  Default [DF_tblErrorMessage_CreatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblErrorMessage] ADD  CONSTRAINT [DF_tblErrorMessage_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_tblErrorMessage_UpdatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblErrorMessage] ADD  CONSTRAINT [DF_tblErrorMessage_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
/****** Object:  Default [DF_tblKeyword_CreatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblKeyword] ADD  CONSTRAINT [DF_tblKeyword_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_tblKeyword_UpdatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblKeyword] ADD  CONSTRAINT [DF_tblKeyword_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
/****** Object:  Default [DF_tblMo_CreatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblMo] ADD  CONSTRAINT [DF_tblMo_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_tblMo_UpdatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblMo] ADD  CONSTRAINT [DF_tblMo_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
/****** Object:  Default [DF_tblMo_ShortCreatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblMo] ADD  CONSTRAINT [DF_tblMo_ShortCreatedDate]  DEFAULT (CONVERT([varchar](8),getdate(),(112))) FOR [ShortCreatedDate]
GO
/****** Object:  Default [DF_tblMt_CreatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblMt] ADD  CONSTRAINT [DF_tblMt_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_tblMt_UpdatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblMt] ADD  CONSTRAINT [DF_tblMt_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
/****** Object:  Default [DF_tblMt_ShortCreatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblMt] ADD  CONSTRAINT [DF_tblMt_ShortCreatedDate]  DEFAULT (CONVERT([varchar](8),getdate(),(112))) FOR [ShortCreatedDate]
GO
/****** Object:  Default [DF_tblMtConfig_CreatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblMtConfig] ADD  CONSTRAINT [DF_tblMtConfig_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_tblMtConfig_UpdatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblMtConfig] ADD  CONSTRAINT [DF_tblMtConfig_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
/****** Object:  Default [DF_tblPinCode_CreatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblPinCode] ADD  CONSTRAINT [DF_tblPinCode_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_tblPinCode_UpdatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblPinCode] ADD  CONSTRAINT [DF_tblPinCode_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
/****** Object:  Default [DF_tblPinCodeBlock_CreatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblPinCodeBlock] ADD  CONSTRAINT [DF_tblPinCodeBlock_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_tblPinCodeBlock_UpdatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblPinCodeBlock] ADD  CONSTRAINT [DF_tblPinCodeBlock_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
/****** Object:  Default [DF_tblPinCodeTemp_CreatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblPinCodeTemp] ADD  CONSTRAINT [DF_tblPinCodeTemp_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_tblProductType_CreatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblProductType] ADD  CONSTRAINT [DF_tblProductType_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_tblProductType_UpdatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblProductType] ADD  CONSTRAINT [DF_tblProductType_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
/****** Object:  Default [DF_tblSubKey_CreatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblSubKey] ADD  CONSTRAINT [DF_tblSubKey_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_tblSubKey_UpdatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblSubKey] ADD  CONSTRAINT [DF_tblSubKey_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
/****** Object:  Default [DF_tblSuccessMessage_CreatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblSuccessMessage] ADD  CONSTRAINT [DF_tblSuccessMessage_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_tblSuccessMessage_UpdatedDate]    Script Date: 05/11/2016 10:56:29 ******/
ALTER TABLE [dbo].[tblSuccessMessage] ADD  CONSTRAINT [DF_tblSuccessMessage_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
