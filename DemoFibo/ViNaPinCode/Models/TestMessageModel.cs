﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VinaVN.Models
{
    public class TestMessageModel
    {
        public TestMessageModel()
        {
            message = "";
            phone = "";
            service = "8077";
            port = "8077";
            sub = main = "CD";
        }

        public string message { get; set; }
        public string phone { get; set; }
        public string service { get; set; }
        public string port { get; set; }
        public string guids { get; set; }
        public string main { get; set; }
        public string sub { get; set; }
    }
}