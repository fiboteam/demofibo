﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VinaVN.Models
{
    public class MtConfigViewModel
    {
        public string MtForSuccess { get; set; }
        public string MtForWrongSystax { get; set; }
        public string MtForWrongPinCode { get; set; }
        public string MtForPinCodeIsUsed { get; set; }
        public string MtForSubKeyIsDeactive { get; set; }
		public string MtForWrongSyntax2 { get; set; }
		public string MtForSuccess2 { get; set; }
		public string MtForSuccess3 { get; set; }
		public string MtForWrongCustomer { get; set; }
	}
}