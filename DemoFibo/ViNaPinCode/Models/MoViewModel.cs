﻿using System;
using System.Collections.Generic;
using VinaVN.Code;
using Vina.DTO;
using Utility;

namespace VinaVN.Models
{
    public class MoViewModel : Model
    {
        public MoViewModel()
        {
            MoId = -1;
            PhoneNumber = "";
            PinCode = "";
            DataList = new List<Mo>();
            Page = new Paging() {PageSize = 100};
            FromDate = DateTime.Now.AddDays(-365);
            ToDate = DateTime.Now.AddDays(365);
            //FromDateTime = DateTime.Now.AddDays(-365).ToString();
            //ToDateTime = DateTime.Now.AddDays(365).ToString();
        }
        /// <summary>
        /// Vị trí trang hiện tại
        /// </summary>
        public int CurrentPage { get; set; }
        /// <summary>
        /// Moid tin nhắn
        /// </summary>
        public long MoId { get; set; }
        /// <summary>
        /// Phân trang
        /// </summary>
        public Paging Page { get; set; }
        /// <summary>
        /// Search số phone
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// Search mã card
        /// </summary>
        public string PinCode { get; set; }
        /// <summary>
        /// Loại MO
        /// </summary>
        public MoStatus MoStatus { get; set; }
        /// <summary>
        /// Tìm từ ngày
        /// </summary>
        //public int FromDate { get; set; }
        /// <summary>
        /// Tìm đến ngày
        /// </summary>
        //public int ToDate { get; set; }

        public List<Mo> DataList { get; set; }
        /// <summary>
        /// Nội dung cần tìm kiếm
        /// </summary>
        public string MoMessage { get; set; }

        /// <summary>
        /// Truyền thông tin qua URL IsPopup= true để show thêm khung search hoặc chỉ show Grid dữ liệu
        /// </summary>
        public bool IsPopup { get; set; }

        public string FromDateTime { get; set; }

        public string ToDateTime { get; set; }
    }
}