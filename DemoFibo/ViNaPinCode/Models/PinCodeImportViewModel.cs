﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Utility;

namespace VinaVN.Models
{
    public class PinCodeImportViewModel
    {
        public PinCodeImportViewModel()
        {
            PinCodeBlockID = 0;
        }
        public ExcelSheetAt ExcelSheetAt { get; set; }
        public ExcelColunmAt ExcelColunmPinCode { get; set; }
        public ExcelColunmAt ExcelColunmSerialCode { get; set; }
        public ExcelColunmAt ExcelColunmSerialCode2 { get; set; }
        public long ProductTypeID { get; set; }
        public long PinCodeBlockID { get; set; }
    }
}