﻿using System;
using System.Collections.Generic;
using VinaVN.Code;
using Vina.DTO;
using Utility;
using System.ComponentModel.DataAnnotations;

namespace VinaVN.Models
{
    public class CustomerViewModel : Model
    {
		public CustomerViewModel()
        {
            DataList = new List<Customer>();
            Page = new Paging() {PageSize = 100};
            FromDate = DateTime.Now.AddDays(-365);
            ToDate = DateTime.Now.AddDays(365);
            //FromDateTime = DateTime.Now.AddDays(-365).ToString();
            //ToDateTime = DateTime.Now.AddDays(365).ToString();
        }
		/// <summary>
		/// Vị trí trang hiện tại
		/// </summary>
		public int CurrentPage { get; set; }
		/// <summary>
		/// Phân trang
		/// </summary>
		public Paging Page { get; set; }
		public long ReferralCustomerId { get; set; }


		public string CustomerCode { get; set; }
		public string ReferralCustomerCode { get; set; }


		public string CustomerName { get; set; }


		public string CustomerPhone { get; set; }


		public string CustomerAddress { get; set; }


		public string CustomerEmail { get; set; }


		public string CustomerNote { get; set; }


		public string CustomerRemark { get; set; } 

        public List<Customer> DataList { get; set; }

        public string FromDateTime { get; set; }

        public string ToDateTime { get; set; }
    }

	public class CustomerEditModel : Model
	{
		public CustomerEditModel()
		{
	
		}
		public long ReferralCustomerId { get; set; }

		public string CustomerCode { get; set; }
		public string ReferralCustomerCode { get; set; }
		[Required(ErrorMessage = "Thông tin không được để trống")]
		public string CustomerName { get; set; }


		public string CustomerPhone { get; set; }

		[Required(ErrorMessage="Thông tin không được để trống")]
		public string CustomerAddress { get; set; }

		[Required(ErrorMessage = "Thông tin không được để trống")]
		public string CustomerEmail { get; set; }


		public string CustomerNote { get; set; }


		public string CustomerRemark { get; set; }
	}
}