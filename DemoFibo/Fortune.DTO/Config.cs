﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vina.DTO
{
    public class Config : BusinessObject
    {
        public Config()
            : base()
        {
            AccountManagerID = -1;
            SubKeyID = -1;
            StartDate = DateTime.Now;
            EndDate = DateTime.Now.AddDays(30);
        }
        
        public long AccountManagerID { get; set; }
        public long SubKeyID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
