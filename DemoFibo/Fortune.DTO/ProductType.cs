﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace Vina.DTO
{
    public class ProductType : BusinessObject
    {
        public string ProductTypeName { get; set; }
        public string ProductTypeKey { get; set; }
        public ProductTypeStatus ProductTypeStatus { get; set; }
        public long ManagerID { get; set; }
       
    }
}
