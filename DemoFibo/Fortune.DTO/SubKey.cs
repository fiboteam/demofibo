using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Utility;

namespace Vina.DTO
{
    public class SubKey : BusinessObject
    {
        public SubKey()
            : base()
        {
            AccountManagerID = -1;
            SubKeyValue = "";
            SubKeyStatus = SubKeyStatus.Active;
        }

        public long AccountManagerID { get; set; }
        public long KeywordID { get; set; }

        public string SubKeyValue { get; set; }

        public SubKeyStatus SubKeyStatus { get; set; }
    }
}
