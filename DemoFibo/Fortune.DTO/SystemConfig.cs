using System.ComponentModel.DataAnnotations;

namespace Vina.DTO
{
    public class SystemConfig : BusinessObject
    {

		public SystemConfig():base()
		{
		    MtSuccessContent = "";
		    MtWrongSyntax = "";
		    MtCardNotExits = "";
		    MtCardUsed = "";
		    MtExpired = "";
		    MtViettelCard = "";
		}

        /// <summary>
        /// Cấu hình nội dung đúng
        /// </summary>
        [Required(ErrorMessage = "Bạn cần nhập nội dung này")]
		public string MtSuccessContent { get; set; } 
        /// <summary>
        /// Cấu hình nội dung sai cú pháp
        /// </summary>
        [Required(ErrorMessage = "Bạn cần nhập nội dung này")]
		public string MtWrongSyntax { get; set; } 
        /// <summary>
        /// Cấu hình card ko tồn tại
        /// </summary>
        [Required(ErrorMessage = "Bạn cần nhập nội dung này")]
		public string MtCardNotExits { get; set; } 

        /// <summary>
        /// Cấu hình card đã sử dụng
        /// </summary>
        [Required(ErrorMessage = "Bạn cần nhập nội dung này")]
		public string MtCardUsed { get; set; } 
        /// <summary>
        /// Cấu hình đã hết hạn chương trình
        /// </summary>
        [Required(ErrorMessage = "Bạn cần nhập nội dung này")]
		public string MtExpired { get; set; } 
        /// <summary>
        /// Cấu hình nội dung trả về card viettel
        /// </summary>
        [Required(ErrorMessage = "Bạn cần nhập nội dung này")]
		public string MtViettelCard { get; set; } 


    }
}
