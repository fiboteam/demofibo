using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vina.DTO;
using Utility;

namespace Vina.DAO
{
    public class SqlAccountDao : SqlDaoBase<Account>
    {
        public SqlAccountDao()
        {
            TableName = "tblAccount";
            EntityIDName = "AccountID";
            StoreProcedurePrefix = "spAccount_";
        }

        public Account CheckLogin(string userName, string passWord)
        {
            string encryPass = GFunction.GetMD5(passWord);
            object[] parms = { "@userName", userName, "@passWord", encryPass };
            Account account = DbAdapter1.Read("spAccount_Login", Make, true, parms);
            return account;
        }

        public Account CheckAutoLogin(long accountId, long parentId)
        {
            object[] parms = { "@accountId", accountId, "@parentId", parentId };
            Account account = DbAdapter1.Read("spAccount_CheckAutoLogin", Make, true, parms);
            return account;
        }


        public int ChangePassword(string loginName, string newPassword)
        {
            object[] parms = { "@loginName", loginName, "@newPassword", newPassword };
            return Convert.ToInt32(DbAdapter1.ExcecuteScalar("spAccount_ChangePassword", true, parms));
        }

        public List<Account> GetAll()
        {
            string sql = String.Format("select * from tblAccount where AccountStatus = 1 ");
            object[] parms = { };
            return DbAdapter1.ReadList(sql,Make,false,parms);
        }

        public SqlAccountDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }
    }
}
