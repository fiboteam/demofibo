﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vina.DTO;

namespace Vina.DAO
{
    public class SqlProductTypeDao : SqlDaoBase<ProductType>
    {
        public SqlProductTypeDao()
        {
            TableName = "tblProductType";
            EntityIDName = "ProductTypeID";
            StoreProcedurePrefix = "spProductType_";
        }
        public List<ProductType> GetWithFilter(long pagesize,
            long pagenum,
            long managerId,
            string productTypeName,
            string productTypeKey,
            int productTypeStatus = 0
            )
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms =
                    {
                        "@pagesize", pagesize, 
                        "@pagenum", pagenum, 
                        "@managerId" ,managerId,
                        "@productTypeName", productTypeName, 
                        "@productTypeKey", productTypeKey,
                        "@productTypeStatus", productTypeStatus
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<ProductType> GetProductTypeByAccountID(long managerId)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetProductTypeByAccountID";
                object[] parms =
                    {
                        "@managerId", managerId
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
