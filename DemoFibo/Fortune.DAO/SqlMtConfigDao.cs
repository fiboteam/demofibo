﻿using Vina.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vina.DAO
{
    public class SqlMtConfigDao : SqlDaoBase<MtConfig>
    {
        public SqlMtConfigDao()
        {
            TableName = "tblMtConfig";
            EntityIDName = "MtConfigID";
            StoreProcedurePrefix = "spMtConfig_";
        }
        public SqlMtConfigDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }
        
        public List<MtConfig> GetConfigByAccountID(long accountId)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetConfigByAccountID";
                object[] parms = { "@accountId", accountId };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public MtConfig GetDefaultWrongSyntaxMessage()
        {
            try
            {
                string sql = "spMtConfig_GetDefaultWrongSyntaxMessage";
                object[] parms = { };
                return DbAdapter1.Read(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int UpdateDefaultWrongSyntaxMessage(string message)
        {
            try
            {
                string sql = "spMtConfig_UpdateDefaultWrongSyntaxMessage";
                object[] parms = { "@message", message };
                return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
            }
            catch (Exception)
            {
                return -1;
            }
        }
        

        
    }
}
