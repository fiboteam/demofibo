using Vina.DTO;
using System;
using System.Collections.Generic;

namespace Vina.DAO
{
    public class SqlSubKeyDao : SqlDaoBase<SubKey>
    {
        public SqlSubKeyDao()
        {
            TableName = "tblSubKey";
            EntityIDName = "SubKeyID";
            StoreProcedurePrefix = "spSubKey_";
        }
        
        public SqlSubKeyDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

        public List<SubKey> GetWithFilter(long pagesize, 
            long pagenum, 
            long accountManagerId,
            long keywordId,
            string subKeyValue, 
            int subKeyStatus = 0
            )
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms =
                    {
                        "@pagesize", pagesize, 
                        "@pagenum", pagenum, 
                        "@accountManagerId" ,accountManagerId,
                        "@keywordId", keywordId,
                        "@subKeyValue", subKeyValue, 
                        "@subKeyStatus", subKeyStatus
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<SubKey> GetAll()
        {
            try
            {
                string sql = "spSubKey_GetAll";
                object[] parms = { };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        

        public int GetTotalPage(long pagesize, 
            long topUpId, 
            long moId, 
            string phoneNumber = "", 
            string message = "", 
            int topUpStatus = 0, 
            int isSentSms = 0, 
            string cardCode = "",
            int fromdate = 0,
            int todate = 0)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetTotalPage";
                object[] parms =
                    {
                        "@pagesize", pagesize, 
                        "@topupid", topUpId, 
                        "@moid", moId, 
                        "@phonenumber", phoneNumber,
                        "@message", message, 
                        "@topupstatus", topUpStatus, 
                        "@issentsms", isSentSms,
                        "@fromdate", fromdate,
                        "@todate", todate
                    };
                return DbAdapter1.GetCount(sql, true, parms);
            }
            catch (Exception)
            {
                return -1;
            }

        }

        public List<SubKey> SearchTopUp(long topUpId,
            long moId, 
            string phoneNumber = "", 
            string message = "", 
            int topUpStatus = 0, 
            int isSentSms = 0, 
            string cardCode = "", 
            DateTime? fromdate = null,
            DateTime? todate = null)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetList";
                object[] parms =
                    {
                        "@topupid", topUpId, 
                        "@moid", moId, 
                        "@phonenumber", phoneNumber, 
                        "@message", message,
                        "@topupstatus", topUpStatus, 
                        "@issentsms", isSentSms,
                        "@fromdate", fromdate?? DateTime.MinValue.AddYears(1800),
                        "@todate", (todate!=null && todate!= DateTime.MaxValue)? todate.Value.AddSeconds(86399):DateTime.MaxValue
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Lấy thông tin topup thông qua guid topup
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public SubKey GetTopUpWithGuid(string guid)
        {
            try
            {
                string sql = String.Format("{0}GetTopUpWithGuid", StoreProcedurePrefix);
                object[] parms = { "@guid", guid };

                return DbAdapter1.Read(sql, Make, true, parms);
            }
            catch
            {
                return null;
            }
        }

        public List<SubKey> GetDistinctPhone(long pagesize,
            long pagenum,
            long topUpId,
            long moId,
            string phoneNumber = "",
            string message = "",
            int topUpStatus = 0,
            int isSentSms = 0,
            string cardCode = "",
            int fromdate = 0,
            int todate = 0)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetDistinctPhone";
                object[] parms =
                    {
                        "@pagesize", pagesize, 
                        "@pagenum", pagenum, 
                        "@topupid", topUpId, 
                        "@moid", moId,
                        "@phonenumber", phoneNumber, 
                        "@message", message, 
                        "@topupstatus", topUpStatus,
                        "@issentsms", isSentSms,
                        "@fromdate", fromdate,
                        "@todate", todate
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<SubKey> Report(int fromDate, int toDate)
        {
            try
            {
                string sql = "spTopup_Report";
                object[] parms = { "@fromDate", fromDate, "@toDate", toDate };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int CheckExists(string subkey,string loginName)
        {
            try
            {
                string sql = "spSubKey_CheckExists";
                object[] parms = { "@subkey", subkey, "@loginName", loginName };
                return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int ChangeStatus(long subKeyId,int subKeyStatus)
        {
            try
            {
                string sql = "spSubKey_ChangeStatus";
                object[] parms = { "@subKeyId", subKeyId, "@subKeyStatus", subKeyStatus };
                return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
            }
            catch (Exception)
            {
                return 0;
            }
        }

        
    }
}
