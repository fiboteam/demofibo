﻿using Vina.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vina.DAO
{
    public class SqlConfigDao  : SqlDaoBase<Config>
    {
        public SqlConfigDao()
        {
            TableName = "tblConfig";
            EntityIDName = "ConfigID";
            StoreProcedurePrefix = "spConfig_";
        }

        public Config GetConfigByAccountID(long accountId)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetConfigByAccountID";
                object[] parms = { "@accountId", accountId };
                return DbAdapter1.Read(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
