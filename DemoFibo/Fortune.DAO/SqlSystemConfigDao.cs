using System;
using System.Collections.Generic;
using Vina.DTO;

namespace Vina.DAO
{
    public class SqlSystemConfigDao : SqlDaoBase<SystemConfig>
    {
        public SqlSystemConfigDao()
        {
            TableName = "tblSystemConfig";
            EntityIDName = "SystemConfigId";
            StoreProcedurePrefix = "spSystemConfig_";
        }
        public SqlSystemConfigDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

    }
}
