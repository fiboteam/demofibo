using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vina.DTO;

namespace Vina.DAO
{
    public class SqlMtDao : SqlDaoBase<Mt>
    {
        public SqlMtDao()
        {
            TableName = "tblMt";
            EntityIDName = "MtID";
            StoreProcedurePrefix = "spMt_";
        }
        public SqlMtDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

        public List<Mt> GetWithFilter(long pagesize, 
            long pagenum, 
            long moId, 
            DateTime fromDate,
            DateTime toDate,
            string phoneNumber = "", 
            string message = "",
            long accountManagerId = -1,
			short status=0
            )
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms = { "@pagesize", pagesize, "@pagenum", pagenum, "@moid", moId, "@phonenumber", phoneNumber, "@message", message, "@accountManagerId", accountManagerId, "@fromDate", fromDate,"@toDate", toDate,"@status",status };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int GetTotalPage(long pagesize, 
            long moId, 
            string phoneNumber = "", 
            string message = "")
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetTotalPage";
                object[] parms = { "@pagesize", pagesize, "@moid", moId, "@phonenumber", phoneNumber, "@message", message };
                return DbAdapter1.GetCount(sql, true, parms);
            }
            catch (Exception)
            {
                return -1;
            }

        }

        public List<Mt> SearchMt(long moId, 
            string phoneNumber = "", 
            string message = "")
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetList";
                object[] parms = { "@moid", moId, "@phonenumber", phoneNumber, "@message", message };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

		public bool UpdateStatus(string SMSGUID, short status)
		{
			string sql = StoreProcedurePrefix + "UpdateStatusMt";
			object[] parms = { "@guid", SMSGUID, "@status", status };
			var result = Extensions.AsLong(DbAdapter1.ExcecuteScalar(sql, true, parms));

			if (result > 0)
				return true;
			return false;
		}
	}
}
