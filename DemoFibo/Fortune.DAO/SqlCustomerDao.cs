using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vina.DTO;

namespace Vina.DAO
{
    public class SqlCustomerDao : SqlDaoBase<Customer>
    {
        public SqlCustomerDao()
        {
            TableName = "tblCustomer";
			EntityIDName = "CustomerId";
            StoreProcedurePrefix = "spCustomer_";
        }

		public List<Customer> GetWithFilter(long PageSize, long CurrentPage, DateTime FromDate, DateTime ToDate, string CustomerName, string CustomerPhone, string CustomerAddress, string CustomerEmail, string CustomerCode, string ReferralCustomerCode)
		{
			try
			{
				string sql = StoreProcedurePrefix + "GetFilter";
				object[] parms =
                    {
                        "@pagesize", PageSize, 
                        "@pagenum", CurrentPage, 
                        "@FromDate", FromDate, 
                        "@ToDate", ToDate,
                        "@CustomerName", CustomerName, 
                        "@CustomerPhone", CustomerPhone, 
                        "@CustomerAddress", CustomerAddress,
                        "@CustomerEmail", CustomerEmail,
                        "@CustomerCode", CustomerCode,
                        "@ReferralCustomerCode",ReferralCustomerCode
                    };
				return DbAdapter1.ReadList(sql, Make, true, parms);
			}
			catch (Exception)
			{
				return null;
			}
		}

		public override bool Update(Customer businessObject)
		{
			string sql = StoreProcedurePrefix + "Update";
			object[] parms =
                    {
                        "@id", businessObject.ID, 
                        "@customername", businessObject.CustomerName, 
                        "@customeraddress", businessObject.CustomerAddress,
                        "@customeremail",businessObject.CustomerEmail,
                        "@customernote", businessObject.CustomerNote
                    };
			businessObject.ID = Extensions.AsLong(DbAdapter1.ExcecuteScalar(sql, true, parms));

			if (businessObject.ID > 0)
				return true;
			return false;
		}

		public bool ExistsCustomerCode(string code)
		{
			string sql = StoreProcedurePrefix + "ExistsCustomerCode";
			object[] parms =
                    {
                        "@code", code
                    };
			var id = Extensions.AsLong(DbAdapter1.ExcecuteScalar(sql, true, parms));

			if (id > 0)
				return true;
			return false;
		}

		public Customer GetCustomerBy(string phone, string code)
		{
			try
			{
				string sql = StoreProcedurePrefix + "GetCustomerBy";
				object[] parms =
                    {
                        "@phone", phone, 
                        "@code", code
                    };
				return DbAdapter1.Read(sql, Make, true, parms);
			}
			catch (Exception)
			{
				return null;
			}
		}

		public long InsertAndGetId(Customer cus)
		{
			string sql = StoreProcedurePrefix + "AddUpdate";
			cus.ID = Extensions.AsLong(DbAdapter1.ExcecuteScalar(sql, true, Take(cus)));

			return cus.ID;
		}
	}
}
